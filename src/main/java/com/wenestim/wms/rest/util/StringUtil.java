/**
 *
 */
package com.wenestim.wms.rest.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;


/**
 * @author Andrew
 *
 */
public class StringUtil {

	private static Logger LOGGER = Logger.getLogger(StringUtil.class);

	/**
	 *
	 */
	public StringUtil() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param CHK_STRING
	 * @return
	 */
	public static String getEmptyNullPrevString(String CHK_STRING) {
		try {
			CHK_STRING = CHK_STRING == null || CHK_STRING.equals("") || CHK_STRING.equals("null") ? null : CHK_STRING;
		} catch (Exception e) {
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return CHK_STRING;
	}

	/**
	 * @param I_DATE
	 * @return
	 */
	public static String getConversionDateFormat(String I_DATE) {
		String rtnDate = "";
		try {
			if (I_DATE != null) {
				rtnDate = I_DATE.substring(0, 10);
			}
		} catch (Exception e) {
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return rtnDate;
	}

	/**
	 * @param inputStream
	 * @return
	 */
	public static String getInputStreamToString(InputStream inputStream) {
		String rsltString = "";
		try {
			StringBuilder stringBuilder = new StringBuilder();
			BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
			String line;

			while((line = in.readLine()) != null) {
				stringBuilder.append(line);
			}
			rsltString = stringBuilder.toString();

		} catch (Exception e) {
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return rsltString;
	}

	public static HashMap getParamMap(String inputString)
	{
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		try
		{
			int    splitPos   = inputString.indexOf("^");
			if( splitPos == -1)
			{
				return paramMap;
			}
			String paramString = inputString.substring(0,splitPos);
			paramString = "[" + paramString.substring(1, paramString.length()) + "]";
			JSONArray paramList = new JSONArray(paramString);

			for(int i=0; i<paramList.length(); i++)
			{
				JSONObject jsonObject = paramList.getJSONObject(i);
				Iterator iterator = jsonObject.keys();
				while (iterator.hasNext()) {
				    String key   = iterator.next().toString();
					String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				    paramMap.put(key, value);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return paramMap;
	}

	public static HashMap<String,Object> getParamMap(InputStream inputStream)
	{
		HashMap<String,Object> paramMap = new HashMap<String,Object>();
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
	        JSONObject jsonObject = (new JSONArray(getString)).getJSONObject(0);
//	        System.out.println("jSONObject:"+jsonObject);
			Iterator iterator = jsonObject.keys();
			while (iterator.hasNext())
			{
			    String key = iterator.next().toString();
				String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				if( value.equals("") )
				{
					value = null;
				}
			    paramMap.put(key, value);
//				LOGGER.info(key + ":"+value);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
			paramMap.put("errorMsg", e.getMessage());
		}
		return paramMap;
	}

	public static ArrayList<HashMap<String,Object>> getJsonToHashMapList(String paramString)
	{
		ArrayList<HashMap<String,Object>> paramList = new ArrayList<HashMap<String,Object>>();

		try
		{
			JSONArray jsArray = new JSONArray(paramString);
			for (int i=0; i<jsArray.length(); i++)
			{

				HashMap<String,Object> paramMap = new HashMap<String,Object>();
				JSONObject jsonObject = jsArray.getJSONObject(i);
				Iterator iterator = jsonObject.keys();
				while (iterator.hasNext())
				{
				    String key = iterator.next().toString();
				    String value = jsonObject.has(key) ? jsonObject.getString(key) : "";
				    if( value != null && value.equals("") )
					{
						value = null;
					}
				    paramMap.put(key, value);
//					LOGGER.info(key + ":"+value);

				}
				paramList.add(paramMap);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
			HashMap<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("errorMsg", e.getMessage());
			paramList.add(paramMap);
		}
		return paramList;
	}

	public static HashMap<String,Object> copyHashMap(HashMap<String,Object> srcMap, HashMap<String,Object> tarMap)
	{
		try
		{
			for ( String key : srcMap.keySet() )
			{
				String value = (String)srcMap.get(key);
				tarMap.put(key, value);
//				LOGGER.info(key + ":"+value);
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return tarMap;
	}

	public static ArrayList<Object> getJsonToObjectList(String paramString)
	{
		ArrayList<Object> paramList = new ArrayList<Object>();

		try
		{
			JSONArray jsArray = new JSONArray(paramString);
			for (int i=0; i<jsArray.length(); i++)
			{
				if( !jsArray.get(i).equals(""))
				{
					paramList.add(jsArray.get(i));
				}
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		return paramList;
	}

	public static String getRegExpIsPlusNumber()
	{
		return "^[0-9]*[.]?[0-9|.]+$";
	}

	public static String getRegExpIsAllNumber()
	{
		return "^-?[0-9]*[.]?[0-9|.]+$";
	}
}
