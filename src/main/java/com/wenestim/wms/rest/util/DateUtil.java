/**
 *
 */
package com.wenestim.wms.rest.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.apache.log4j.Logger;

/**
 * @author Andrew
 *
 */
public class DateUtil {

	final static Logger LOGGER = Logger.getLogger(DateUtil.class);

	public DateUtil() {

	}

	public static String getBeforeDateByString(String stringDate, float days)
	{
		stringDate    = stringDate.replaceAll("-","");
		int dateYear  = new Integer(stringDate.substring(0, 4));
		int dateMonth = new Integer(stringDate.substring(4 ,6));
		int dateDay   = new Integer(stringDate.substring(6, 8));

		return LocalDate.of(dateYear, dateMonth, dateDay).minusDays((int)days).format(DateTimeFormatter.BASIC_ISO_DATE);
	}

	public static LocalDate getBeforeDateByLocal(LocalDate localDate, float days)
	{
		return LocalDate.of(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()).minusDays((int)days);
	}

	public static String getAfterDateByString(String stringDate, float days)
	{
		stringDate    = stringDate.replaceAll("-","");
		int dateYear  = new Integer(stringDate.substring(0, 4));
		int dateMonth = new Integer(stringDate.substring(4 ,6));
		int dateDay   = new Integer(stringDate.substring(6, 8));

		return LocalDate.of(dateYear, dateMonth, dateDay).plusDays((int)days).format(DateTimeFormatter.BASIC_ISO_DATE);
	}

	public static LocalDate getAfterDateByLocal(LocalDate localDate, float days)
	{
		return LocalDate.of(localDate.getYear(), localDate.getMonthValue(), localDate.getDayOfMonth()).plusDays((int)days);
	}
}
