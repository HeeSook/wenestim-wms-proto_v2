/**
 *
 */
package com.wenestim.wms.rest.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.entity.production.WMS_EXC_DEMAND;
import com.wenestim.wms.rest.entity.production.WMS_EXC_INVENTORY;
import com.wenestim.wms.rest.entity.production.WMS_EXC_MRP;
import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;
import com.wenestim.wms.rest.service.production.WMS_EXC_MRP_Service;

/**
 * @author Andrew
 *
 */
public class MrpUtil
{
	final static Logger  LOGGER             = Logger.getLogger(MrpUtil.class);
	final static WMS_EXC_MRP_Service excMrp = new WMS_EXC_MRP_Service();
//	final static MrpTest excMrp = new MrpTest();

	public MrpUtil()
	{
		// TODO Auto-generated constructor stub
	}

	public static void createDemandBySafetyStock()
	{
		List<WMS_MST_MRP>       masterList = excMrp.masterList  ;
		List<WMS_EXC_INVENTORY> totInvList = excMrp.invTotalList;
		List<WMS_EXC_DEMAND>    demandList = excMrp.demandList  ;
		try
		{
			masterList = masterList.stream()
				.filter(item -> item.getSAFETY_STOCK() != null && item.getSAFETY_STOCK() > 0)
		        .collect(Collectors.toList())
		        ;

			int demandSeq            = 1000000;
			WMS_EXC_DEMAND safDemand = null;
			WMS_EXC_INVENTORY inv    = null;

			for(WMS_MST_MRP master : masterList)
			{
				String demandType   = "SAFETY_STOCK";
				String plantCode    = master.getPLANT_CODE()   ;
				String materialCode = master.getMATERIAL_CODE();
				String mergeCode    = getMergeCode(plantCode, materialCode)      ;
				String orderNo      = getMergeCode(mergeCode, excMrp.planCurrent);
				String sortKey      = excMrp.planCurrent+"-4-SAFETY_STOCK"       ;
				float  safetyStock  = master.getSAFETY_STOCK();
				float  lotSize      = master.getLOT_SIZE()    ;
				safDemand           = excMrp.createReqDemand(demandSeq,"INIT_SAFETY_STOCK",orderNo,"-",plantCode, materialCode,excMrp.planCurrent,0,lotSize,sortKey);

				inv                 = getBacklog(safDemand, plantCode, materialCode, safetyStock, false);

				float availQty   = inv.getAVAIL_QTY()  ;
				float remainQty  = inv.getREMAIN_QTY() ;
				float backlogQty = inv.getBACKLOG_QTY();

				if( backlogQty > 0)
				{
//					System.out.println("safDemand plantCode:"+plantCode+",materialCode:"+materialCode+",backlogQty:"+backlogQty+",orderNo:"+orderNo);
					safDemand.setDEMAND_TYPE(demandType);
					safDemand.setREQ_QTY(backlogQty)    ;
					demandList.add(safDemand);
					demandSeq++;
				}
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("createDemandBySafetyStock:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static WMS_EXC_INVENTORY getBacklog(WMS_EXC_DEMAND demand, String plantCode, String materialCode, float reqQty, boolean isGreaterZero)
	{
		WMS_EXC_INVENTORY dynInv = new WMS_EXC_INVENTORY();
		try
		{
			String demandType  = demand.getDEMAND_TYPE();
			float  lotSize     = demand.getLOT_SIZE()   ;

			float backlogQty = reqQty;
			float availQty   = 0;
			float assignQty  = reqQty;
			float remainQty  = 0;

			float curAvailQty = getInvInfo(plantCode,materialCode).getREMAIN_QTY();

			List<WMS_EXC_INVENTORY> totInvList = excMrp.invTotalList ;
			List<WMS_EXC_INVENTORY> invList    = null;

			if( isGreaterZero )
			{
				invList = totInvList.stream()
						.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
										item.getMATERIAL_CODE().equals(materialCode) &&
										item.getINV_QTY() > 0
								)
						.collect(Collectors.toList())
						;
			}
			else
			{
				invList = totInvList.stream()
						.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
								        item.getMATERIAL_CODE().equals(materialCode)
								)
						.collect(Collectors.toList())
						;
			}

			if( invList.size() > 0)
			{
//				if( demandType.equals("LOT_SIZE") && lotSize >  0 && lotSize > curAvailQty)
//				{
//					float quotient  = (float)Math.floor(reqQty/lotSize);
//					float lotReqQty = quotient*lotSize;
//
//					for(WMS_EXC_INVENTORY inv : invList)
//					{
//						availQty  = inv.getREMAIN_QTY();
//						assignQty = 0;
//						remainQty = availQty;
//
//						for(int i=1; i<=quotient; i++)
//						{
//							remainQty = availQty - lotSize*i;
//							assignQty = lotSize*i;
//
//							if( remainQty < 0 || assignQty > lotReqQty )
//							{
//								assignQty = lotSize*(i-1);
//								break;
//							}
//						}
//						remainQty  = availQty   - assignQty;
//						backlogQty = backlogQty - assignQty;
//
//						inv.setASSIGN_QTY(assignQty)  ;
//						inv.setAVAIL_QTY(availQty)    ;
//						inv.setREMAIN_QTY(remainQty)  ;
//						inv.setBACKLOG_QTY(backlogQty);
//						dynInv = inv;
//					}
//				}
//				else
//				{
					for(WMS_EXC_INVENTORY inv : invList)
					{
						availQty  = inv.getREMAIN_QTY();
						remainQty = availQty - backlogQty;

						if( remainQty < 0 )
						{
							availQty   = Math.max(availQty, 0);
							assignQty  = availQty;
							remainQty  = 0;
							backlogQty = backlogQty - availQty;
						}
						else
						{
							backlogQty = 0;
						}

						inv.setASSIGN_QTY(assignQty)  ;
						inv.setAVAIL_QTY(availQty)    ;
						inv.setREMAIN_QTY(remainQty)  ;
						inv.setBACKLOG_QTY(backlogQty);
						dynInv = inv;
					}
//				}
			}
			else
			{
				dynInv.setASSIGN_QTY(0);
				dynInv.setINV_QTY(0)   ;
				dynInv.setAVAIL_QTY(0) ;
				dynInv.setREMAIN_QTY(0);
				dynInv.setBACKLOG_QTY(backlogQty);
			}
//			System.out.println("getBacklog demandType:"+demandType+",plantCode:"+plantCode+",materialCode:"+materialCode+",reqQty:"+reqQty+",availQty:"+availQty+",assignQty:"+assignQty+",remainQty:"+remainQty+",backlogQty:"+backlogQty + ",lotSize:"+lotSize);
			createConsumeInvList(dynInv, demand, reqQty);
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("getBacklog:" + ex.getMessage());
			ex.printStackTrace();
		}
		return dynInv;
	}

	public static HashMap<String,List<WMS_EXC_MRP>> createItemBomMap()
	{
		HashMap<String,List<WMS_EXC_MRP>> itemBomMap = new HashMap<String,List<WMS_EXC_MRP>>();
		try
		{
			List<WMS_MST_MRP> masterList = excMrp.masterList;
			List<WMS_MST_BOM> bomList    = excMrp.bomList   ;

			for(WMS_MST_BOM bom : bomList)
			{
				String plantCode    = bom.getPROD_PLANT_CODE()   ;
				String materialCode = bom.getPROD_MATERIAL_CODE();
				String mergeCode    = getMergeCode(plantCode, materialCode);

				if( itemBomMap.get(mergeCode) == null )
				{
					List<WMS_EXC_MRP> itemBomList = new ArrayList<WMS_EXC_MRP>();
					createItemBomList( 1, plantCode, materialCode, itemBomList);

					itemBomMap.put(mergeCode, itemBomList);
				}
			}

		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("createItemBomMap:" + ex.getMessage());
			ex.printStackTrace();
		}
		return itemBomMap;
	}

	public static void createItemBomList(int bomLevel, String plantCode, String materialCode, List<WMS_EXC_MRP> itemBomList)
	{
		List<WMS_MST_BOM> dataList = null;
		List<WMS_MST_BOM> bomList  = excMrp.bomList;
		try
		{
			String mergeCode = getMergeCode(plantCode,materialCode);
			createItemBomLevel(bomLevel,plantCode,materialCode);

			WMS_MST_MRP mstMrp       = getMasterInfo(plantCode, materialCode);
			WMS_EXC_INVENTORY excInv = getInvInfo(plantCode,materialCode);

			dataList = bomList.stream()
		                .filter(item ->	item.getPROD_PLANT_CODE().equals(plantCode)       &&
		                				item.getPROD_MATERIAL_CODE().equals(materialCode)
		                		)
		                .collect(Collectors.toList())
		                ;

			for(WMS_MST_BOM bom : dataList)
			{
				int    compBomLevel     = bomLevel + 1;
				String compPlantCode    = bom.getCOMP_PLANT_CODE()   ;
				String compMaterialCode = bom.getCOMP_MATERIAL_CODE();
				float  compQty          = bom.getCOMP_QTY()          ;
				float  prodQty          = bom.getPROD_QTY()          ;

				if( dataList.size() > 1 && compMaterialCode.equals("-") )
				{
					continue;
				}

				WMS_EXC_MRP excMrp  = new WMS_EXC_MRP();
				excMrp.setBOM_LEVEL(bomLevel)                   ;
				excMrp.setPLANT_CODE(plantCode)                 ;
				excMrp.setMATERIAL_CODE(materialCode)           ;
				excMrp.setMERGE_CODE(mergeCode)                 ;
				excMrp.setPROD_PLANT_CODE(plantCode)            ;
				excMrp.setPROD_MATERIAL_CODE(materialCode)      ;
				excMrp.setPROD_QTY(prodQty)                     ;
				excMrp.setCOMP_PLANT_CODE(compPlantCode)        ;
				excMrp.setCOMP_MATERIAL_CODE(compMaterialCode)  ;
				excMrp.setCOMP_QTY(compQty)                     ;
				excMrp.setLEAD_TIME(mstMrp.getLEAD_TIME())      ;
				excMrp.setSAFETY_STOCK(mstMrp.getSAFETY_STOCK());
				excMrp.setLOT_SIZE(mstMrp.getLOT_SIZE())        ;
				excMrp.setINV_QTY(excInv.getINV_QTY())          ;
				excMrp.setAVAIL_QTY(excInv.getAVAIL_QTY())      ;
				excMrp.setREMAIN_QTY(excInv.getREMAIN_QTY())    ;

				itemBomList.add(excMrp);
				createItemBomList( compBomLevel, compPlantCode, compMaterialCode, itemBomList);
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("createItemBomList:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static void createItemBomLevel(int bomLevel, String plantCode, String materialCode)
	{
		List<WMS_EXC_MRP> itemBomLevelList = excMrp.itemBomLevelList;
		try
		{
			List<WMS_EXC_MRP> dataList = itemBomLevelList.stream()
					.filter(item ->	item.getPLANT_CODE().equals(plantCode)       &&
									item.getMATERIAL_CODE().equals(materialCode)
							)
					.collect(Collectors.toList());

			if(dataList.size() == 0)
			{
				WMS_EXC_MRP bomLevelItem = new WMS_EXC_MRP();
				bomLevelItem.setBOM_LEVEL(bomLevel)        ;
				bomLevelItem.setPLANT_CODE(plantCode)      ;
				bomLevelItem.setMATERIAL_CODE(materialCode);


				itemBomLevelList.add(bomLevelItem);
			}
			else
			{
				WMS_EXC_MRP bomLevelItem = dataList.get(0);
				int curBomLevel = bomLevelItem.getBOM_LEVEL();
				if( bomLevel > curBomLevel)
				{
					bomLevelItem.setBOM_LEVEL(bomLevel);
				}
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("createItemBomLevel:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static WMS_MST_MRP getMasterInfo(String plantCode, String materialCode)
	{
		float leadTime     = 0;
		float lotSize      =-1;
		float safetyStock  = 0;

		WMS_MST_MRP rtnMrp           = new WMS_MST_MRP();
		List<WMS_MST_MRP> masterList = excMrp.masterList;
		try
		{
			List<WMS_MST_MRP> dataList = masterList.stream()
			.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
	        				item.getMATERIAL_CODE().equals(materialCode)
	        				)
	        .collect(Collectors.toList())
	        ;

			for(WMS_MST_MRP mstMrp : dataList)
			{
				leadTime    = mstMrp.getLEAD_TIME()    == null ?  0 : mstMrp.getLEAD_TIME()   ;
				lotSize     = mstMrp.getLOT_SIZE()     == null ? -1 : mstMrp.getLOT_SIZE()    ;
				safetyStock = mstMrp.getSAFETY_STOCK() == null ?  0 : mstMrp.getSAFETY_STOCK();
			}

			rtnMrp.setLEAD_TIME(leadTime);
			rtnMrp.setLOT_SIZE(lotSize)  ;
			rtnMrp.setSAFETY_STOCK(safetyStock);
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("getMasterInfo:" + ex.getMessage());
			ex.printStackTrace();
		}

		return rtnMrp;
	}

	public static WMS_EXC_INVENTORY getInvInfo(String plantCode, String materialCode)
	{
		float availQty   = 0;
		float remainQty  = 0;

		WMS_EXC_INVENTORY excInv = new WMS_EXC_INVENTORY();
		try
		{
			List<WMS_EXC_INVENTORY> totInvList = excMrp.invTotalList ;

			List<WMS_EXC_INVENTORY> dataList = totInvList.stream()
						.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
										item.getMATERIAL_CODE().equals(materialCode)
								)
						.collect(Collectors.toList())
						;

			for(WMS_EXC_INVENTORY inv : dataList)
			{
				excInv = inv;
			}

			if( dataList.size() == 0)
			{
				excInv.setINV_QTY(0)   ;
				excInv.setAVAIL_QTY(0) ;
				excInv.setREMAIN_QTY(0);
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("getInvInfo:" + ex.getMessage());
			ex.printStackTrace();
		}

//		System.out.println("getBacklogWithInv plantCode:"+plantCode+",materialCode:"+materialCode+",reqQty:"+reqQty+",availQty:"+availQty+",assignQty:"+assignQty+",remainQty:"+remainQty+",backlogQty:"+backlogQty);
		return excInv;
	}

	public static WMS_EXC_MRP getStartBomLevel(List<WMS_EXC_MRP> itemBomList)
	{
		return itemBomList.stream()
		.filter(item -> item.getBOM_LEVEL() == 1)
        .collect(Collectors.toList())
        .get(0);
	}

	public static String getMergeCode(String concat1, String concat2)
	{
		return concat1 + "::" + concat2;
	}

	public static int getMaxBomLevel(List<WMS_EXC_MRP> itemBomList)
	{
		int maxBomLevel = itemBomList.stream().max(Comparator.comparingInt(WMS_EXC_MRP::getBOM_LEVEL)).get().getBOM_LEVEL();
		return maxBomLevel;
	}

	public static String getSortKey(String reqDate, String demandType)
	{
		String sortKey = "";
		try
		{
			if( demandType.equals("ORDER"))
			{
				sortKey = reqDate+"::1";
			}
			else if(demandType.equals("SALES_FCST"))
			{
				sortKey = reqDate+"::2";
			}
			else if(demandType.equals("PROD_DEMAND"))
			{
				sortKey = reqDate+"::3";
			}
			else if(demandType.equals("SAFETY_STOCK"))
			{
				sortKey = reqDate+"::4";
			}
			else
			{
				sortKey = reqDate+"::5";
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("getSortKey:" + ex.getMessage());
			ex.printStackTrace();
		}
		return sortKey;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor)
    {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

	public static String getMaxPlanOutDate(List<WMS_EXC_MRP> planList, String plantCode, String materialCode)
	{
		String maxOutDate = "";
		try
		{
			List<WMS_EXC_MRP> dataList = planList.stream()
					.filter(item -> item.getPROD_PLANT_CODE().equals(plantCode) &&
	        				item.getPROD_MATERIAL_CODE().equals(materialCode)
	        				)
			.collect(Collectors.toList())
			;

			WMS_EXC_MRP item  = dataList.stream().max((WMS_EXC_MRP item1, WMS_EXC_MRP item2) -> item1.getPLAN_OUT_DATE().compareTo(item2.getPLAN_OUT_DATE())).get();
			maxOutDate = item.getPLAN_OUT_DATE();

			for(WMS_EXC_MRP plan : dataList)
			{
				plan.setPLAN_OUT_DATE(maxOutDate);
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("getMaxPlanOutDate:" + ex.getMessage());
			ex.printStackTrace();
		}
		return maxOutDate;
	}


	public static void createConsumeInvList(WMS_EXC_INVENTORY inv, WMS_EXC_DEMAND demand, float reqQty)
	{
		if( inv.getASSIGN_QTY() == 0 )
		{
			return;
		}

		try
		{
			List<WMS_EXC_INVENTORY> invPeggingList = excMrp.invPeggingList;

			String plantCode    = inv.getPLANT_CODE()   ;
			String materialCode = inv.getMATERIAL_CODE();
			float  assignReq    = inv.getASSIGN_QTY()   ;
			float  remainReq    = assignReq;
			float  totAssignQty = 0;

			List<WMS_EXC_INVENTORY> invDetailList = excMrp.invDetailList;
			List<WMS_EXC_INVENTORY> dataList = 	invDetailList.stream()
					.filter(item ->
							item.getPLANT_CODE().equals(plantCode)       &&
	        				item.getMATERIAL_CODE().equals(materialCode) &&
	        				item.getAVAIL_QTY() >=0
	        				)
			.collect(Collectors.toList())
			;

			for(WMS_EXC_INVENTORY invDetail : dataList)
			{
				boolean isBreak = false;

				WMS_EXC_INVENTORY newInv = new WMS_EXC_INVENTORY();

				float invQty     = invDetail.getINV_QTY()   ;
				float availQty   = invDetail.getREMAIN_QTY();
				float assignQty  = 0;
				float remainQty  = 0;
				float backlogQty = 0;

				remainReq -= availQty;

				if( remainReq >  0 )
				{
					assignQty = availQty;
					remainQty = 0;
				}
				else
				{
					isBreak   = true;
					assignQty = availQty-Math.abs(remainReq);
					remainQty = Math.abs(remainReq);
				}

				totAssignQty += assignQty;
				backlogQty    = reqQty - totAssignQty;

				invDetail.setASSIGN_QTY(assignQty);
				invDetail.setREMAIN_QTY(remainQty);

				newInv.setORDER_NO(demand.getORDER_NO())   ;
				newInv.setPLAN_SEQ(demand.getPLAN_SEQ())   ;
				newInv.setDEMAND_TYPE(demand.getDEMAND_TYPE()) ;
				newInv.setPLANT_CODE(inv.getPLANT_CODE())  ;
				newInv.setMATERIAL_CODE(inv.getMATERIAL_CODE());
				newInv.setSTORAGE_LOC(inv.getSTORAGE_LOC());
				newInv.setINV_QTY(invQty)                  ;
				newInv.setAVAIL_QTY(availQty)              ;
				newInv.setREMAIN_QTY(remainQty)            ;
				newInv.setASSIGN_QTY(assignQty)            ;
				newInv.setBACKLOG_QTY(backlogQty)          ;
				newInv.setREQ_QTY(reqQty)                  ;
				newInv.setTOT_ASSIGN_QTY(totAssignQty)     ;

				invPeggingList.add(newInv);

//				System.out.println("plantCode:"+plantCode+",materialCode:"+materialCode+",reqQty:"+assignReq+",availQty:"+availQty+",assignQty:"+assignQty+",remainQty:"+remainQty+",totAssignQty:"+totAssignQty);

				if( isBreak )
				{
					break;
				}
			}
		}
		catch(Exception ex)
		{
			excMrp.result = "E";
			LOGGER.error("createConsumeInvList:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public static void printPlanData(List<WMS_EXC_MRP> dataList, String printType)
	{
		int i=0;
		for(WMS_EXC_MRP data : dataList)
		{
			i++;
			String orderNo     = data.getORDER_NO()          ;
			String demandType  = data.getDEMAND_TYPE()       ;
			String mergeCode   = data.getMERGE_CODE()        ;
			String compCode    = data.getCOMP_MATERIAL_CODE();
			String beforeCode  = data.getBEFORE_COMP_CODE()  ;
			String reqDate     = data.getREQ_DATE()          ;
			String planInDate  = data.getPLAN_IN_DATE()      ;
			String planOutDate = data.getPLAN_OUT_DATE()     ;
			int    planSeq     = data.getPLAN_SEQ()          ;
			int    bomLevel    = data.getBOM_LEVEL()         ;
			float  reqQty      = data.getREQ_QTY()           ;
			float  invQty      = data.getINV_QTY()           ;
			float  availQty    = data.getAVAIL_QTY()         ;
			float  remainQty   = data.getREMAIN_QTY()        ;
			float  assignQty   = data.getASSIGN_QTY()        ;
			float  backlogQty  = data.getBACKLOG_QTY()       ;
			float  compQty     = data.getCOMP_QTY()          ;
			float  leadTime    = data.getLEAD_TIME()         ;
//			System.out.println("Pegging ["+printType+"] seq:"+i+",orderNo:"+orderNo+",planSeq:"+planSeq+",demandType:"+demandType+",bomLevel:"+bomLevel+",prodCode:"+mergeCode+",compCode:"+compCode+",reqDate:"+reqDate+",inDate:"+planInDate+",outDate:"+planOutDate+",reqQty:"+reqQty+",invQty:"+invQty+",availQty:"+availQty+",assignQty:"+assignQty+",backlogQty:"+backlogQty+",compQty:"+compQty+",leadTime:"+leadTime);
		}
	}

	public static void printDemandData(List<WMS_EXC_DEMAND> dataList, String printType)
	{
		dataList.sort((WMS_EXC_DEMAND item1, WMS_EXC_DEMAND item2) -> item1.getSORT_KEY().compareTo(item2.getSORT_KEY()));
		int i=0;
		for(WMS_EXC_DEMAND data : dataList)
		{
			i++;
			String orderNo     = data.getORDER_NO()   ;
			String demandType  = data.getDEMAND_TYPE();
			String mergeCode   = data.getMERGE_CODE() ;
			String reqDate     = data.getREQ_DATE()   ;
			String prodCode    = data.getPROD_CODE()  ;
			int    planSeq     = data.getPLAN_SEQ()   ;
			float  reqQty      = data.getREQ_QTY()    ;
			float  invQty      = data.getINV_QTY()    ;
			float  availQty    = data.getAVAIL_QTY()  ;
			float  remainQty   = data.getREMAIN_QTY() ;
			float  assignQty   = data.getASSIGN_QTY() ;
			float  backlogQty  = data.getBACKLOG_QTY();
			float  lotSize     = data.getLOT_SIZE()   ;
//			System.out.println("Demand ["+printType+"] seq:"+i+",prodCode:"+prodCode+",materialCode:"+mergeCode+",reqDate:"+reqDate+",reqQty:"+reqQty + ",demandType:"+demandType+",orderNo:"+orderNo+",invQty:"+invQty+",availQty:"+availQty+",remainQty:"+remainQty+",assignQty:"+assignQty+",backlogQty:"+backlogQty+",lotSize:"+lotSize);
		}
	}
}
