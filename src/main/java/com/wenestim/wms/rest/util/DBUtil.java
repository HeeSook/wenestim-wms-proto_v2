/**
 * 
 */
package com.wenestim.wms.rest.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * @author Andrew
 *
 */
public class DBUtil {
	
	final static Logger LOGGER = Logger.getLogger(DBUtil.class);
	
	private final static String PROPERTY = "wenestim.wms.properties";

	/**
	 * 
	 */
	public DBUtil() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * 
	 * @return dbConnection
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static Connection getOraDBConnection() throws IOException, ClassNotFoundException, SQLException {
		Properties properties = new Properties();
		Connection dbConnection = null;
		
		try {
			InputStream input = 
					DBUtil.class.getClassLoader().getResourceAsStream(PROPERTY);
			if(input==null){
				throw new Exception("Unable to find ["+PROPERTY+"]");
			}
			
			//load a properties file from class path, inside static method
			properties.load(input);

            //get the property value 
    		String ORA_DB_DRIVER = properties.getProperty("ORA_DB_DRIVER");
    		
    		String ORA_DB_CONNECTION = properties.getProperty("ORA_DB_CONNECTION");
    		String ORA_DB_USER = properties.getProperty("ORA_DB_USER");
    		String ORA_DB_PASS = properties.getProperty("ORA_DB_PASS");
    		
			Class.forName(ORA_DB_DRIVER);
			dbConnection = DriverManager.getConnection(ORA_DB_CONNECTION, ORA_DB_USER, ORA_DB_PASS);
			
			return dbConnection;
			
		} catch (IOException e) {
			LOGGER.error("IOException: " + e.getMessage());
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			LOGGER.error("ClassNotFoundException: " + e.getMessage());
			e.printStackTrace();
		} catch (SQLException e) {
			LOGGER.error("SQLException: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error("Exception: " + e.getMessage());
			e.printStackTrace();
		} 
//		finally {
//			if(input!=null){
//        		try {
//        			input.close();
//        		} catch (IOException e) {
//        			LOGGER.error("IOException Error:" + e.getMessage() + "\n");
//        		}
//        	}
//		}

		return dbConnection;
		
	}

}
