/**
 * 
 */
package com.wenestim.wms.rest.util;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

/**
 * @author Andrew
 *
 */
public class MyBatisUtil {
	
	private static Logger LOGGER = Logger.getLogger(MyBatisUtil.class);
	private static SqlSessionFactory factory;

	/**
	 * 
	 */
	public MyBatisUtil() {
		// TODO Auto-generated constructor stub
	}
	
	static {
		Reader reader = null;
		
		try {
			reader = Resources.getResourceAsReader("wenestim-wms-config.xml");
			factory = new SqlSessionFactoryBuilder().build(reader);
		} catch (IOException e) {
			LOGGER.error("MyBatisUtil() IOException:"+e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
	}
	
	/**
	 * @return
	 */
	public static SqlSessionFactory getSqlSessionFactory()
	{
		return factory;
	}

}
