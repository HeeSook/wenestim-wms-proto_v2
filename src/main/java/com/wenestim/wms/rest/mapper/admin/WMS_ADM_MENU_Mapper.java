package com.wenestim.wms.rest.mapper.admin;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface WMS_ADM_MENU_Mapper
{
	public List<HashMap<String,Object>> getMenuList(HashMap<String,Object> paramMap);

	@Select("SELECT LPAD(IFNULL(MAX(A.MENU_ID),0)+1,4,'0') FROM WMS_ADM_MENU A WHERE A.SYSTEM_ID = #{SYSTEM_ID} ")
	public String getMenuId(@Param("SYSTEM_ID") String SYSTEM_ID);

	public Boolean updateMenu(HashMap<String, Object> dataMap);
}
