package com.wenestim.wms.rest.mapper.inventory;

import java.util.HashMap;
import java.util.List;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_LEDGER;

public interface WMS_DYN_LEDGER_Mapper
{
	public List<WMS_DYN_LEDGER> getMaterialLedgerList(HashMap<String,Object> paramMap);
}
