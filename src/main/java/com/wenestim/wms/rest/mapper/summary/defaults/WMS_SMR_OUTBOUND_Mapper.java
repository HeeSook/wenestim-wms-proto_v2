package com.wenestim.wms.rest.mapper.summary.defaults;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author Andrew
 *
 */
public interface WMS_SMR_OUTBOUND_Mapper {

	/**
	 * GET OUTBOUND ORDER DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return List<HashMap<?, ?>>
	 */
    public List<HashMap<?, ?>> getOutboundOrderDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE); 
	
	public List<HashMap<?, ?>> getOutboundOrderTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE);
	
	public List<HashMap<?, ?>> getOutboundOrderDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE);

	/**
	 * GET OUTBOUND PICKING DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return List<HashMap<?, ?>>
	 */
    public List<HashMap<?, ?>> getOutboundPickingDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE); 
	
	public List<HashMap<?, ?>> getOutboundPickingTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE);
	
	public List<HashMap<?, ?>> getOutboundPickingDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE);

	/**
	 * GET OUTBOUND DELIVERY DAILY DETAIL
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return List<HashMap<?, ?>>
	 */
    public List<HashMap<?, ?>> getOutboundDeliveryDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE); 
	
	public List<HashMap<?, ?>> getOutboundDeliveryTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE);
	
	public List<HashMap<?, ?>> getOutboundDeliveryDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE);
	
}
