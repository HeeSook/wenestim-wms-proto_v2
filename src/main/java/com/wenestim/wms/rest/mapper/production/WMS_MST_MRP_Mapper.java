package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;

public interface WMS_MST_MRP_Mapper
{
	public List<WMS_MST_MRP> getMrpMasterList(HashMap<String,Object> paramMap);

	public Boolean updateMrpMasterList(HashMap<String,Object> dataMap);
}
