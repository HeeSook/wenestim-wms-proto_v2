/**
 *
 */
package com.wenestim.wms.rest.mapper.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_DELIVERY;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;


/**
 * @author Andrew
 *
 */
public interface WMS_DYN_DELIVERY_Mapper {

    public List<WMS_DYN_DELIVERY> getAllDeliveryHeader(
			@Param("CLIENT_ID"         ) String CLIENT_ID         ,
			@Param("COMPANY_CODE"      ) String COMPANY_CODE      ,
			@Param("DO_NO"             ) String DO_NO             ,
			@Param("CUST_CODE"         ) String CUST_CODE         ,
			@Param("STATUS"            ) String STATUS            ,
			@Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
			@Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
    		);

    
    
    
    public WMS_DYN_PICKING getDcrInfo(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
    		);
    
    public List<WMS_DYN_PICKING> getAllDeliveryDetail(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
    		);
    
    public List<WMS_DYN_PICKING> getAllDeliveryDetailPdf(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
    		);
    
    public List<WMS_DYN_PICKING> getAllDeliveryDetailPdf_1000(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
    		);
    
	public Boolean insertDelivery(WMS_DYN_DELIVERY oWMS_DYN_DELIVERY);

	public Boolean updateDelivery(HashMap dataMap);

	public Boolean deleteDelivery(WMS_DYN_DELIVERY oWMS_DYN_DELIVERYD);

	public Boolean updateDeliveryInfo(HashMap<String, Object> dataMap);

	/* Noah Kim 4/16/2018
	 * */
	@Select("SELECT DISTINCT CUST_CODE FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND DO_NO = #{DO_NO} AND DEL_FLAG = 'N'")
	public String getCUSTOM_NUM(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			);

	/* Noah Kim 4/16/2018
	 * */
	@Select("SELECT DELIVERY_TIME FROM WMS_MST_CUSTOMER WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND CUST_CODE = #{CUST_CODE} AND DEL_FLAG = 'N'")
	public String getDeliveryTime(
			@Param("CUST_CODE"   ) String CUST_CODE   ,
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE
			);

	/* Noah Kim 4/16/2018
	 * */
	@Select("SELECT EXISTS (SELECT 1 FROM WMS_DYN_DELIVERY WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND DO_NO = #{DO_NO})")
	public Boolean chkDeliveryNum(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			);


}
