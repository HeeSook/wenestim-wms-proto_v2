package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

import com.wenestim.wms.rest.entity.production.WMS_MST_ROUTE;

public interface WMS_MST_ROUTE_Mapper
{
	public List<WMS_MST_ROUTE> getRouteList(HashMap<String,Object> paramMap);

	public Boolean updateRouteList(HashMap<String,Object> dataMap);

	public Boolean deleteRouteList(HashMap<String,Object> dataMap);
}
