package com.wenestim.wms.rest.mapper.summary.defaults;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author Andrew
 *
 */
public interface WMS_SMR_INBOUND_Mapper {
	
	/**
	 * GET INBOUND ORDER DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundOrderDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE); 
	
	public List<HashMap<?, ?>> getInboundOrderTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE);
	
	public List<HashMap<?, ?>> getInboundOrderDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE);

	/**
	 * GET INBOUND RECEIVE DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundReceiveDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE); 
	
	public List<HashMap<?, ?>> getInboundReceiveTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE);
	
	public List<HashMap<?, ?>> getInboundReceiveDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE);

	/**
	 * GET INBOUND REMAIN DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundRemainDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE); 
	
	public List<HashMap<?, ?>> getInboundRemainTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE);
	
}
