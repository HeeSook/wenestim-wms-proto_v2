/**
 *
 */
package com.wenestim.wms.rest.mapper.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;


/**
 * @author Andrew
 *
 */
public interface WMS_DYN_PICKING_Mapper {

    public List<WMS_DYN_PICKING> getAllPicking();

	public List<WMS_DYN_PICKING> getCurrentTodayPickingNo(@Param("CREATE_DATE") String CREATE_DATE);

	public Boolean insertPicking(HashMap<String, Object> dataMap);

	public Boolean insertPickingPDA(WMS_DYN_PICKING oWMS_DYN_PICKING);

	public Boolean updatePicking(WMS_DYN_PICKING oWMS_DYN_PICKING);

	public Boolean confirmPackingStatus(HashMap<String, Object> dataMap);

	// GWANGHYUN KIM 4/8/2018
	public Boolean scanUpdatePicking(WMS_DYN_PICKING oWMS_DYN_PICKING);

	public Boolean deletePicking(WMS_DYN_PICKING oWMS_DYN_PICKINGD);


	public List<WMS_DYN_PICKING> getAllPickingByCompany(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PL_NO") String PL_NO,
			@Param("CUST_CODE") String CUST_CODE, @Param("HEADER_FLAG") String HEADER_FLAG,
			@Param("CREATE_DATE1") String CREATE_DATE1, @Param("CREATE_DATE2") String CREATE_DATE2);

	public List<WMS_DYN_PICKING> getAllPickingByCompanyGI(
			@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PL_NO") String PL_NO,
			@Param("CUST_CODE") String CUST_CODE, @Param("HEADER_FLAG") String HEADER_FLAG,
			@Param("CREATE_DATE1") String CREATE_DATE1, @Param("CREATE_DATE2") String CREATE_DATE2
			);

    public List<WMS_DYN_PICKING> getPickingStatusList(
			@Param("CLIENT_ID"        ) String CLIENT_ID	    ,
			@Param("COMPANY_CODE"     ) String COMPANY_CODE	    ,
			@Param("PL_NO"            ) String PL_NO			,
			@Param("CUST_CODE"        ) String CUST_CODE		,
			@Param("STATUS"           ) String STATUS           ,
			@Param("FROM_PICKING_DATE") String FROM_PICKING_DATE,
			@Param("TO_PICKING_DATE"  ) String TO_PICKING_DATE
    		);

 // GWANGHYUN KIM 8/20/2018
 	public List<WMS_DYN_PICKING> getSOItem(
			@Param("CLIENT_ID"    ) String CLIENT_ID	,
			@Param("COMPANY_CODE" ) String COMPANY_CODE	,
 			@Param("SO_NO"        ) String SO_NO        ,
 			@Param("MATERIAL_CODE") String MATERIAL_CODE,
 			@Param("SO_ITEM_NO"   ) String SO_ITEM_NO   ,
 			@Param("ISSUE_DATE"   ) String ISSUE_DATE   ,
 			@Param("GET_BARCODE"  ) String GET_BARCODE
 			);

 	 // CHAN PARK 01/12/2019
 	public List<HashMap<String,Object>> getSoItemNo(
			@Param("CLIENT_ID"    ) String CLIENT_ID	,
			@Param("COMPANY_CODE" ) String COMPANY_CODE	,
 			@Param("SO_NO"        ) String SO_NO
 			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT ORDER_QTY FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND SO_NO = #{SO_NO} AND SO_ITEM_NO = #{SO_ITEM_NO} AND DEL_FLAG = 'N' limit 1")
	public float getOrderQty(
			@Param("CLIENT_ID"    ) String CLIENT_ID	,
			@Param("COMPANY_CODE" ) String COMPANY_CODE	,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT SCAN_QTY FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND SO_NO = #{SO_NO} AND SO_ITEM_NO = #{SO_ITEM_NO} AND DEL_FLAG = 'N' limit 1")
	public float getScanQty(
			@Param("CLIENT_ID"    ) String CLIENT_ID	,
			@Param("COMPANY_CODE" ) String COMPANY_CODE	,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPicking(
			@Param("CLIENT_ID"   ) String CLIENT_ID	  ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PL_NO"       ) String PL_NO
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND BARCODE_NO = #{BARCODE_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPickingByBarcode_NO(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPickingMaterial(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PL_NO"        ) String PL_NO       ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND SO_NO = #{SO_NO} AND SO_ITEM_NO = #{SO_ITEM_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsItemNo(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO,
			@Param("SO_NO"        ) String SO_NO
			);

	// GWANGHYUN KIM 4/8/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND SO_NO = #{SO_NO} AND SO_ITEM_NO = #{SO_ITEM_NO} AND STATUS = 'CS' AND DEL_FLAG = 'N')")
	public Boolean chkItemScanStatus(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			);

	// GWANGHYUN KIM 5/30/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PICKING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PL_NO = #{PL_NO} AND (STATUS IS NULL OR STATUS = 'PS') AND DEL_FLAG = 'N')")
	public Boolean chkPickingScanStatus(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PL_NO"       ) String PL_NO
			);

	// GWANGHYUN KIM 5/30/2018
    public Boolean updateScanQty(WMS_DYN_PICKING oWMS_DYN_PICKING);

	// GWANGHYUN KIM 5/29/2018
	public WMS_DYN_PICKING getCurrentPickingData(WMS_DYN_PICKING oWMS_DYN_PICKING);

	public Boolean updateSgiEaccountNo(HashMap<String, Object> dataMap);

	public Boolean updateSOBarCode(HashMap<String,Object> dataMap);



}
