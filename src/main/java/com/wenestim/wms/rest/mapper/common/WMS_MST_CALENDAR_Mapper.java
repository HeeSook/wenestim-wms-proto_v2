/**
 *
 */
package com.wenestim.wms.rest.mapper.common;

import java.util.HashMap;
import java.util.List;


/**
 * @author Andrew
 *
 */
public interface WMS_MST_CALENDAR_Mapper
{
	public List<HashMap<String, Object>> getDateList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getWeekList(HashMap<String, Object> paramMap);

}
