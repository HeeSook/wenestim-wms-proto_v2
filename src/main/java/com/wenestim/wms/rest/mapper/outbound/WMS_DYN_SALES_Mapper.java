/**
 *
 */
package com.wenestim.wms.rest.mapper.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_SALES;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_SALES_Mapper {

	public List<WMS_DYN_SALES> getAllSales();

	public List<WMS_DYN_SALES> getAllSalesByCompany(
			@Param("CLIENT_ID") String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PL_NO") String PL_NO,
			@Param("MAIN_FLAG") String MAIN_FLAG,
			@Param("SALES_ORG_CODE") String SALES_ORG_CODE,
			@Param("SO_NO") String SO_NO,
			@Param("SO_TYPE") String SO_TYPE,
			@Param("SO_ITEM_NO") String SO_ITEM_NO,
			@Param("CUST_CODE") String CUST_CODE,
			@Param("DELIVERY_DATE1") String DELIVERY_DATE1,
			@Param("DELIVERY_DATE2") String DELIVERY_DATE2
			);

	public Boolean insertSales(HashMap<String, Object> dataMap);

	public Boolean updateSales(HashMap<String, Object> dataMap);

	public Boolean deleteSales(HashMap<String, Object> dataMap);

	public Boolean deleteAllSales(
			@Param("CLIENT_ID") String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("SO_NO") String SO_NO,
			@Param("userId") String userId
			);

	public Boolean updateHeader(HashMap<String, Object> dataMap);

}
