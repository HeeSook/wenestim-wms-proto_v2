/**
 *
 */
package com.wenestim.wms.rest.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.common.WMS_MST_USER;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_USER_Mapper {

	public List<WMS_MST_USER> getAllUser(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE
			);

	public WMS_MST_USER getUser(@Param("COMPANY_CODE") String COMPANY_CODE, @Param("WSID") int WSID);

	public Boolean insertUser(WMS_MST_USER oWMS_MST_USER);

	public Boolean updateUser(WMS_MST_USER oWMS_MST_USER);

	public Boolean deleteUser(WMS_MST_USER oWMS_MST_USER);

	@Select("SELECT EXISTS(SELECT 1 FROM WMS_MST_USER WHERE USER_ID = #{USER_ID} AND DEL_FLAG = 'N')")
	public Boolean checkExitUserID(@Param("USER_ID") String USER_ID);

	/**
	 * User Login Mapper
	 * @param USER_ID
	 * @param USER_PW
	 * @return
	 */
	public WMS_MST_USER getUser(@Param("USER_ID") String USER_ID,@Param("USER_PW") String USER_PW);

	public WMS_MST_USER getLogin(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("USER_ID"     ) String USER_ID,
			@Param("USER_PW"     ) String USER_PW);

	public WMS_MST_USER getCompanyCode(String USER_ID);

}
