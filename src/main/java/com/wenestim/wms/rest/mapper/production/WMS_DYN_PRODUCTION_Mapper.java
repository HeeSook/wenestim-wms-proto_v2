/**
 *
 */
package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_PRODUCTION_Mapper
{
	public List<HashMap<String,Object>> getPlanList(HashMap<String, Object> paramMap);

	public Boolean updatePlanList(HashMap<String, Object> dataMap);


}
