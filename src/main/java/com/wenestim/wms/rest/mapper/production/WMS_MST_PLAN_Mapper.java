package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.production.WMS_MST_PLAN;

public interface WMS_MST_PLAN_Mapper
{
	public List<WMS_MST_PLAN> getPlanVerList(HashMap<String, Object> paramMap);

	@Select("SELECT FN_CREATE_PLAN_VER(#{CLIENT_ID},#{COMPANY_CODE},#{PLAN_CURRENT},#{USER_ID})")
	public String  createPlan(HashMap<String, Object> dataMap);

	public Boolean updatePlan(HashMap<String,Object> dataMap);

	public Boolean confirmPlan(HashMap<String,Object> dataMap);
}
