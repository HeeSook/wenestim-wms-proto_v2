package com.wenestim.wms.rest.mapper.summary.cartech;

import java.util.HashMap;
import java.util.List;

public interface WMS_SMR_PRODUCTION_Mapper
{
	public HashMap<String,Object>       getTotalProductionQty(HashMap<String, Object> paramMap);

	public List<HashMap<String,Object>> getTotalProductionList(HashMap<String, Object> paramMap);

	public List<HashMap<String,Object>> getDetailProductionList(HashMap<String, Object> paramMap);

	public List<HashMap<String,Object>> getProductionResultList(HashMap<String, Object> paramMap);

	public List<HashMap<String,Object>> getProductionRunTimeList(HashMap<String, Object> paramMap);
}
