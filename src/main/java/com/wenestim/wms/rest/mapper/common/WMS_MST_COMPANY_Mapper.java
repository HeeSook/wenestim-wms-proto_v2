package com.wenestim.wms.rest.mapper.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.common.WMS_MST_COMPANY;

public interface WMS_MST_COMPANY_Mapper
{
    public List<WMS_MST_COMPANY> getCompanyList(
    		@Param("CLIENT_ID")    String CLIENT_ID   ,
    		@Param("COMPANY_CODE") String COMPANY_CODE
    		);

    public Boolean updateCompany(HashMap<String, Object> dataMap);
}
