/**
 *
 */
package com.wenestim.wms.rest.mapper.inventory;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_STOCK_MOVEMENT;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_STOCK_MOVEMENT_Mapper {

    public List<WMS_DYN_STOCK_MOVEMENT> getInventoryList(
            @Param("CLIENT_ID"       ) String CLIENT_ID       ,
            @Param("COMPANY_CODE"    ) String COMPANY_CODE    ,
            @Param("PLANT_CODE"      ) String PLANT_CODE      ,
            @Param("MOVEMENT_TYPE"   ) String MOVEMENT_TYPE   ,
            @Param("CURR_STORAGE_LOC") String CURR_STORAGE_LOC,
            @Param("MOVE_STORAGE_LOC") String MOVE_STORAGE_LOC,
            @Param("MATERIAL_CODE"   ) String MATERIAL_CODE   ,
            @Param("POST_DATE"       ) String POST_DATE
            );


	public List<WMS_DYN_STOCK_MOVEMENT> getMovementList(
            @Param("CLIENT_ID"     ) String CLIENT_ID     ,
            @Param("COMPANY_CODE"  ) String COMPANY_CODE  ,
            @Param("PLANT_CODE"    ) String PLANT_CODE    ,
            @Param("MOVEMENT_TYPE" ) String MOVEMENT_TYPE ,
            @Param("STORAGE_LOC"   ) String STORAGE_LOC   ,
            @Param("MATERIAL_TYPE" ) String MATERIAL_TYPE ,
            @Param("MATERIAL_CODE" ) String MATERIAL_CODE ,
            @Param("FROM_POST_DATE") String FROM_POST_DATE,
            @Param("TO_POST_DATE"  ) String TO_POST_DATE
            );

//	public List<WMS_DYN_STOCK_MOVEMENT> getLatestMovementList(HashMap<String, Object> paramMap);

    public Boolean insertStockMovement(HashMap<String, Object> dataMap);

    public Boolean updateStockMovement(WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT);

    public Boolean deleteStockMovement(WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);
}
