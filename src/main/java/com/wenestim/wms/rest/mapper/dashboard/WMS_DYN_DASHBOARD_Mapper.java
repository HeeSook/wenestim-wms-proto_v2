/**
 *
 */
package com.wenestim.wms.rest.mapper.dashboard;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.common.WMS_MST_COMCODE;
import com.wenestim.wms.rest.entity.dashboard.WMS_DYN_DASHBOARD;

public interface WMS_DYN_DASHBOARD_Mapper {

	List<WMS_DYN_DASHBOARD> getSupplierState1(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE);
	List<WMS_DYN_DASHBOARD> getSupplierState2(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE);
	List<WMS_DYN_DASHBOARD> getCustomerState1(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE);
	List<WMS_DYN_DASHBOARD> getCustomerState2(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE);


}
