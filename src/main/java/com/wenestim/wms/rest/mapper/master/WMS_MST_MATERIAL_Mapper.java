/**
 *
 */
package com.wenestim.wms.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.master.WMS_MST_MATERIAL;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_MATERIAL_Mapper {

	public List<WMS_MST_MATERIAL> getAllMaterial();

	public Boolean insertMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL);

	public Boolean updateMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL);

	public Boolean deleteMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL);

	public List<WMS_MST_MATERIAL> getAllMaterialByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE);

	public List<WMS_MST_MATERIAL> getMaterial(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("MATERIAL_CODE") String MATERIAL_CODE);

	public List<WMS_MST_MATERIAL> getMaterialByMaterialType(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("TYPE") String TYPE);

	public List<WMS_MST_MATERIAL> getMaterialListByPlanMaterialCodeType(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("MATERIAL_CODE") String MATERIAL_CODE, @Param("PLANT_CODE") String PLANT_CODE, @Param("MATERIAL_TYPE") String MATERIAL_TYPE, @Param("DEL_FLAG") String DEL_FLAG);

	public Boolean insertMaterialList(HashMap<String, Object> materialList);

	public Boolean updateMaterialList(HashMap<String, Object> materialList);

	public Boolean deleteMaterialList(HashMap<String, Object> materialList);

	@Select("SELECT MATERIAL_CODE, MATERIAL_NAME, CONCAT('[',MATERIAL_CODE,'] ',MATERIAL_NAME) AS MATERIAL_DESC FROM WMS_MST_MATERIAL WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND DEL_FLAG = 'N' GROUP BY MATERIAL_CODE")
	public List<WMS_MST_MATERIAL> getMaterialCodeByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

	public Boolean excelUpload(HashMap<String, Object> dataMap);

}