/**
 *
 */
package com.wenestim.wms.rest.mapper.inventory;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_INVENTORY;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_INVENTORY_Mapper {

    public List<WMS_DYN_INVENTORY> getAllInventory(
            @Param("CLIENT_ID"    ) String CLIENT_ID    ,
            @Param("COMPANY_CODE" ) String COMPANY_CODE ,
            @Param("PLANT_CODE"   ) String PLANT_CODE   ,
            @Param("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @Param("MATERIAL_TYPE") String MATERIAL_TYPE,
            @Param("MATERIAL_CODE") String MATERIAL_CODE,
            @Param("INCLUDE_ZERO" ) String INCLUDE_ZERO
    		);

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

    public Boolean insertInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY);

	public Boolean updateInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY);

	public Boolean deleteInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY);

	public Boolean excelUpload(HashMap<String, Object> dataMap);

}
