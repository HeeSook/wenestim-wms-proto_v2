package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

public interface WMS_DYN_MRP_Mapper
{
	public List<HashMap<String,Object>> getMrpList(HashMap<String,Object> paramMap);
}
