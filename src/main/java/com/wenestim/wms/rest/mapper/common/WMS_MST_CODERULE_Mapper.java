/**
 *
 */
package com.wenestim.wms.rest.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.common.WMS_MST_CODERULE;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_CODERULE_Mapper {

    public List<WMS_MST_CODERULE> getAllCodeRule();

	public Boolean insertCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE);

	public Boolean updateCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE);

	public Boolean deleteCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE);

	@Select("SELECT FN_GET_DYN_DOC_NO(#{CLIENT_ID}, #{COMPANY_CODE},#{CATEGORY2}, #{CODE}, ${NEED_CNT})")
	public String getDocNo(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("CATEGORY2") String CATEGORY2, @Param("CODE") String CODE, @Param("NEED_CNT") Integer NEED_CNT);

	@Select("SELECT FN_MSTCODE_CHECK(#{CLIENT_ID}, #{COMPANY_CODE}, #{CATEGORY2}, #{CODE}, ${NEED_CNT})")
	public String getAvailableMstCode(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("CATEGORY2") String CATEGORY2, @Param("CODE") String CODE, @Param("NEED_CNT") Integer NEED_CNT);

}
