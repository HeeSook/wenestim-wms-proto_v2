/**
 *
 */
package com.wenestim.wms.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.master.WMS_MST_INFOCODE;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_INFOCODE_Mapper {

    public List<WMS_MST_INFOCODE> getAllInfoCode();

	public Boolean insertInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODE);

	public Boolean updateInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODE);

	public Boolean deleteInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODED);

	public List<WMS_MST_INFOCODE> getInfoCodeList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@Param("INFO_NO") String INFO_NO, @Param("CATEGORY") String CATEGORY, @Param("FROM_DATE") String FROM_DATE);

	public List<WMS_MST_INFOCODE> getInfoCodeByValidDate(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@Param("VALID_DATE") String VALID_DATE);

	public Boolean insertInfoCodeList(HashMap<String, Object> infoRecordList);

	public Boolean updateInfoCodeList(HashMap<String, Object> infoRecordList);

	public Boolean deleteInfoCodeList(HashMap<String, Object> infoRecordList);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

	public Boolean excelUpload(HashMap<String, Object> dataMap);

}
