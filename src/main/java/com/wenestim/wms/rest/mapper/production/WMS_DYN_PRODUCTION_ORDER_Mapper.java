package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

public interface WMS_DYN_PRODUCTION_ORDER_Mapper
{
	public List<HashMap<String,Object>> getProductionOrderList(HashMap<String, Object> paramMap);

	public Boolean updateProductionOrder(HashMap<String,Object> dataMap);
}
