/**
 *
 */
package com.wenestim.wms.rest.mapper.inbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING_GR;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_PURCHASING_GR_Mapper {

    public List<WMS_DYN_PURCHASING_GR> getAllPurchasingGr(
            @Param("CLIENT_ID"         ) String CLIENT_ID         ,
            @Param("COMPANY_CODE"      ) String COMPANY_CODE      ,
            @Param("PLANT_CODE"        ) String PLANT_CODE        ,
            @Param("PO_NO"             ) String PO_NO             ,
            @Param("VENDOR_CODE"       ) String VENDOR_CODE       ,
            @Param("FROM_PO_DATE"      ) String FROM_PO_DATE      ,
            @Param("TO_PO_DATE"        ) String TO_PO_DATE        ,
            @Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
            @Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE  ,
            @Param("POST_DATE"         ) String POST_DATE
    		);

	public Boolean insertPurchasingGr(WMS_DYN_PURCHASING_GR oWMS_DYN_PURCHASING_GR);

	public Boolean updatePurchasingGr(HashMap dataMap);

	public Boolean deletePurchasingGr(HashMap dataMap);

	// GWANGHYUN KIM 4/7/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PURCHASING_GR WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND BARCODE_NO = #{BARCODE_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPurchasingByBarcode_NO(
            @Param("CLIENT_ID"   ) String CLIENT_ID   ,
            @Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			);

	// GWANGHYUN KIM 8/14/2018
	@Select("SELECT SUM(GR_QTY) FROM WMS_DYN_PURCHASING_GR WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND PO_ITEM_NO = #{PO_ITEM_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND DEL_FLAG = 'N'")
	public float getGR_Qty(
            @Param("CLIENT_ID"    ) String CLIENT_ID   ,
            @Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);

	// GWANGHYUN KIM 8/14/2018
	@Update("UPDATE WMS_DYN_PURCHASING_GR SET STATUS = #{STATUS} WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND PO_ITEM_NO = #{PO_ITEM_NO} AND MATERIAL_CODE = #{MATERIAL_CODE}")
	public Boolean updateGRstatus(
			@Param("STATUS"       ) String STATUS      ,
            @Param("CLIENT_ID"    ) String CLIENT_ID   ,
            @Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);
}
