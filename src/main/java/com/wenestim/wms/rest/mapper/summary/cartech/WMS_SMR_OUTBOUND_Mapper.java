package com.wenestim.wms.rest.mapper.summary.cartech;

import java.util.HashMap;
import java.util.List;

/**
 * @author Andrew
 *
 */
public interface WMS_SMR_OUTBOUND_Mapper
{

	public HashMap<String, Object> getTotalSalesCount(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getTotalSalesList( HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDetailSalesList(HashMap<String, Object> paramMap);
}
