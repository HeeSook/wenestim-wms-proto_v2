/**
 *
 */
package com.wenestim.wms.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.master.WMS_MST_VENDOR;


/**
 * @author Andrew
 *
 */
public interface WMS_MST_VENDOR_Mapper {

    public List<WMS_MST_VENDOR> getAllVendor();

	public Boolean insertVendor(WMS_MST_VENDOR oWMS_MST_VENDOR);

	public Boolean updateVendor(WMS_MST_VENDOR oWMS_MST_VENDOR);

	public Boolean deleteVendor(WMS_MST_VENDOR oWMS_MST_VENDORD);

	public List<WMS_MST_VENDOR> getVendorList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE, @Param("VENDOR_NAME") String VENDOR_NAME, @Param("DEL_FLAG") String DEL_FLAG);

	public Boolean insertVendorList(HashMap<String, Object> vendorList);

	public Boolean updateVendorList(HashMap<String, Object> vendorList);

	public Boolean deleteVendorList(HashMap<String, Object> vendorList);

	//@Select("SELECT VENDOR_CODE, VENDOR_NAME, CONCAT('[',VENDOR_CODE,'] ',VENDOR_NAME) AS VENDOR_DESC FROM WMS_MST_VENDOR WHERE COMPANY_CODE = #{COMPANY_CODE} AND PURCHASE_ORG_CODE = #{PURCHASE_ORG_CODE} AND PURCHASE_GRP_CODE = #{PURCHASE_GRP_CODE} AND DEL_FLAG = 'N' GROUP BY VENDOR_CODE")
	public List<WMS_MST_VENDOR> getVendorCodeByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE, @Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

	public Boolean excelUpload(HashMap<String, Object> dataMap);
}
