/**
 *
 */
package com.wenestim.wms.rest.mapper.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.common.WMS_MST_COMCODE;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_COMCODE_Mapper {

    public List<WMS_MST_COMCODE> getAllComCode();
  
    // GWANGHYUN KIM 5/17/2018
    public List<WMS_MST_COMCODE> getAllComCodeByCategory(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CATEGORY1") String CATEGORY1,@Param("CATEGORY2") String CATEGORY2);

	public List<WMS_MST_COMCODE> getComCodeByCategory(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CATEGORY1") String CATEGORY1,@Param("CATEGORY2") String CATEGORY2);

    public Boolean insertComCode(WMS_MST_COMCODE oWMS_MST_COMCODE);

	public Boolean updateComCode(WMS_MST_COMCODE oWMS_MST_COMCODE);

	public Boolean deleteComCode(WMS_MST_COMCODE oWMS_MST_COMCODE);
	
	// GWANGHYUN KIM 5/17/2018
	@Select("SELECT COUNT(CLIENT_ID) FROM WMS_MST_COMCODE WHERE CATEGORY1 = #{CATEGORY1} AND CATEGORY2 = #{CATEGORY2} AND DEL_FLAG = 'N'")
	public int countSORT1(@Param("CATEGORY1") String CATEGORY1, @Param("CATEGORY2") String CATEGORY2);

}
