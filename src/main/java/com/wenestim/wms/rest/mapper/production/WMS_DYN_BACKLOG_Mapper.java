package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

import com.wenestim.wms.rest.entity.production.WMS_DYN_BACKLOG;

public interface WMS_DYN_BACKLOG_Mapper
{
	public List<WMS_DYN_BACKLOG> getBacklogList(HashMap<String,Object> paramMap);

	public Boolean updateBacklogList(HashMap<String,Object> dataMap);
}
