package com.wenestim.wms.rest.mapper.master;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.master.WMS_MST_PROMOTION;


public interface WMS_MST_PROMOTION_Mapper {

	public List<WMS_MST_PROMOTION> getAllPromotionByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DEL_FLAG") String DEL_FLAG);

	public Boolean insertPromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION);

	public Boolean updatePromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION);

	public Boolean deletePromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION);

	@Select("SELECT MAX(PROMO_CODE) FROM WMS_MST_PROMOTION")
	public int getMaxPromoCode();
}
