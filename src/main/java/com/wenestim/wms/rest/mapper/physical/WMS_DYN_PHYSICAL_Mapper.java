/**
 *
 */
package com.wenestim.wms.rest.mapper.physical;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.physical.WMS_DYN_PHYSICAL;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_PHYSICAL_Mapper {

    public List<WMS_DYN_PHYSICAL> getAllPhysical(
            @Param("CLIENT_ID"    ) String CLIENT_ID    ,
    		@Param("COMPANY_CODE" ) String COMPANY_CODE ,
            @Param("PLANT_CODE"   ) String PLANT_CODE   ,
            @Param("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @Param("MATERIAL_TYPE") String MATERIAL_TYPE,
            @Param("MATERIAL_CODE") String MATERIAL_CODE
    		);

	public Boolean insertPhysical(WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL);

	public Boolean updatePhysical(HashMap<String,Object> dataMap);

	public Boolean deletePhysical(WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL);

	// GWANGHYUN KIM 4/7/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PHYSICAL WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND BARCODE_NO = #{BARCODE_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPhysicalByBarcode_NO(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			);

}
