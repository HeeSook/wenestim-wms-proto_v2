package com.wenestim.wms.rest.mapper.interfaces;

import java.util.HashMap;

public interface WMS_IFS_RECEIVE_MES_Mapper
{
	public boolean insertMesActualData(HashMap<String, Object> dataMap);

	public boolean insertMesSpmData(HashMap<String, Object> dataMap);
}
