package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

public interface WMS_DYN_PLANNED_ORDER_Mapper
{
	public List<HashMap<String,Object>> getPlannedOrderList(HashMap<String, Object> paramMap);

	public Boolean updatePlannedOrder(HashMap<String,Object> dataMap);
}
