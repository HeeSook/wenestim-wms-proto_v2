package com.wenestim.wms.rest.mapper.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.entity.production.WMS_EXC_DEMAND;
import com.wenestim.wms.rest.entity.production.WMS_EXC_INVENTORY;
import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;

public interface WMS_EXC_MRP_Mapper
{
	public List<WMS_EXC_DEMAND>getDemandList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PLAN_VER"    ) String PLAN_VER,
			@Param("PLAN_TYPE"   ) String PLAN_TYPE
			 );

	public List<WMS_MST_BOM>getBomList(
			 @Param("CLIENT_ID"   ) String CLIENT_ID,
			 @Param("COMPANY_CODE") String COMPANY_CODE
			 );

	 public List<WMS_EXC_INVENTORY> getInvList(
			 @Param("CLIENT_ID"   ) String CLIENT_ID,
			 @Param("COMPANY_CODE") String COMPANY_CODE
			 );

	 public List<WMS_EXC_INVENTORY> getInvDetailList(
			 @Param("CLIENT_ID"   ) String CLIENT_ID,
			 @Param("COMPANY_CODE") String COMPANY_CODE
			 );

	 public List<WMS_MST_MRP> getMrpMasterList(
			 @Param("CLIENT_ID"   ) String CLIENT_ID,
			 @Param("COMPANY_CODE") String COMPANY_CODE
			 );

	 public Boolean updateStartMrp(HashMap<String, Object> dataMap);

	 public Boolean updateEndMrp(HashMap<String, Object> dataMap);

	 public Boolean insertMasterList(HashMap<String, Object> dataMap);

	 public Boolean insertDemandList(HashMap<String, Object> dataMap);

	 public Boolean insertInvList(HashMap<String, Object> dataMap);

	 public Boolean insertPeggingList(HashMap<String, Object> dataMap);

	 public Boolean insertReqItemList(HashMap<String, Object> dataMap);

	 public Boolean insertDynMrp(HashMap<String, Object> dataMap);

	 public Boolean deleteMrpData(HashMap<String, Object> dataMap);
}
