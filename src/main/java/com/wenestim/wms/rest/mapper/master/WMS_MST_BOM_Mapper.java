/**
 *
 */
package com.wenestim.wms.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;


/**
 * @author Andrew
 *
 */
public interface WMS_MST_BOM_Mapper {

    public List<WMS_MST_BOM> getAllBom(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE);

	public Boolean insertBom(WMS_MST_BOM oWMS_MST_BOM);

	public Boolean updateBom(WMS_MST_BOM oWMS_MST_BOM);

	public Boolean deleteBom(WMS_MST_BOM oWMS_MST_BOM);

	public List<WMS_MST_BOM> getBomProdList(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE);

	public Boolean insertBomProdList(HashMap<String, Object> bomProdList);

	public Boolean updateBomProdList(HashMap<String, Object> bomProdList);

	public Boolean deleteBomProdList(HashMap<String, Object> bomProdList);

	public List<WMS_MST_BOM> getBomCompList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE);

	public Boolean insertBomCompList(HashMap<String, Object> bomCompList);

	public Boolean updateBomCompList(HashMap<String, Object> bomCompList);

	public Boolean deleteBomCompList(HashMap<String, Object> bomCompList);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

	public Boolean excelUpload(HashMap<String, Object> dataMap);

}
