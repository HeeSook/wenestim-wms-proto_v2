/**
 *
 */
package com.wenestim.wms.rest.mapper.inbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING;

/**
 * @author Andrew
 *
 */
public interface WMS_DYN_PURCHASING_Mapper {

    public List<WMS_DYN_PURCHASING> getAllPurchasing(
            @Param("CLIENT_ID"         ) String CLIENT_ID          ,
            @Param("COMPANY_CODE"      ) String COMPANY_CODE       ,
            @Param("PLANT_CODE"        ) String PLANT_CODE         ,
            @Param("PURCHASE_ORG"      ) String PURCHASE_ORG       ,
            @Param("PURCHASE_GRP"      ) String PURCHASE_GRP       ,
            @Param("PO_TYPE"           ) String PO_TYPE            ,
            @Param("MATERIAL_CODE"     ) String MATERIAL_CODE      ,
            @Param("VENDOR_CODE"       ) String VENDOR_CODE        ,
            @Param("PO_NO"             ) String PO_NO              ,
            @Param("FROM_PO_DATE"      ) String FROM_PO_DATE       ,
            @Param("TO_PO_DATE"        ) String TO_PO_DATE         ,
            @Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE ,
            @Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
            );

    public List<WMS_DYN_PURCHASING> getPurchasingByCompany(
    		@Param("CLIENT_ID"         ) String CLIENT_ID	  	   ,
			@Param("COMPANY_CODE"      ) String COMPANY_CODE	   ,
			@Param("MAIN_FLAG"         ) String MAIN_FLAG          ,
			@Param("PURCHASE_ORG"      ) String PURCHASE_ORG	   ,
			@Param("PURCHASE_GRP"      ) String PURCHASE_GRP	   ,
			@Param("PLANT"             ) String PLANT		       ,
			@Param("PO_TYPE"           ) String PO_TYPE		       ,
			@Param("FROM_PO_NO"        ) String FROM_PO_NO	       ,
			@Param("TO_PO_NO"          ) String TO_PO_NO		   ,
			@Param("FROM_PO_DATE"      ) String FROM_PO_DATE	   ,
			@Param("TO_PO_DATE"        ) String TO_PO_DATE	       ,
			@Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE ,
			@Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE   ,
			@Param("FROM_VENDOR_CODE"  ) String FROM_VENDOR_CODE   ,
			@Param("TO_VENDOR_CODE"    ) String TO_VENDOR_CODE	   ,
			@Param("MATERIAL_CODE"     ) String MATERIAL_CODE
    		);

	@Select("SELECT * FROM WMS_DYN_PURCHASING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND PO_ITEM_NO = #{PO_ITEM_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND DEL_FLAG = 'N'")
    public WMS_DYN_PURCHASING getPurchasing(
    		@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
    		@Param("PO_NO"        ) String PO_NO       ,
    		@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
    		@Param("MATERIAL_CODE") String MATERIAL_CODE
    		);

	public Boolean insertPurchasing(HashMap<String, Object> dataMap);

	public Boolean updatePurchasing(HashMap<String, Object> dataMap);

	// GWANGHYUN KIM 4/15/2018
	public Boolean updatePurchasingFromPDA(WMS_DYN_PURCHASING oWMS_DYN_PURCHASING);

	public Boolean deletePurchasing(HashMap<String, Object> dataMap);

	//public Boolean deletePurchasing(WMS_DYN_PURCHASING oWMS_DYN_PURCHASING);

	// GWANGHYUN KIM 4/7/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PURCHASING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND DEL_FLAG = 'N')")
	public Boolean chkExistsPoNumber(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PO_NO"       ) String PO_NO
			);

	// GWANGHYUN KIM 4/15/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PURCHASING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND PO_ITEM_NO = #{PO_ITEM_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND GR_FLAG = 'Y' AND DEL_FLAG = 'N')")
	public Boolean chkGRStauts(
    		@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);

	// GWANGHYUN KIM 4/7/2018
	@Select("SELECT EXISTS(SELECT 1 FROM WMS_DYN_PURCHASING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND DEL_FLAG = 'N')")
	public Boolean chkExistsMaterial(
    		@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);

	// GWANGHYUN KIM 8/14/2018
	@Select("SELECT PO_QTY FROM WMS_DYN_PURCHASING WHERE CLIENT_ID = #{CLIENT_ID} AND COMPANY_CODE = #{COMPANY_CODE} AND PO_NO = #{PO_NO} AND PO_ITEM_NO = #{PO_ITEM_NO} AND MATERIAL_CODE = #{MATERIAL_CODE} AND DEL_FLAG = 'N'")
	public float getPO_Qty(
    		@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			);

	public Boolean deleteAllPO(
			@Param("CLIENT_ID") String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PO_NO") String PO_NO,
			@Param("INPUT_TYPE") String INPUT_TYPE,
			@Param("userId") String userId
			);

}
