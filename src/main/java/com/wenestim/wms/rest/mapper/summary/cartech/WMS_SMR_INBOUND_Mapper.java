package com.wenestim.wms.rest.mapper.summary.cartech;

import java.util.HashMap;
import java.util.List;

/**
 * @author Andrew
 *
 */
public interface WMS_SMR_INBOUND_Mapper
{

	public HashMap<String, Object> getTotalOrderCount(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getTotalOrderList( HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDetailOrderList(HashMap<String, Object> paramMap);
}
