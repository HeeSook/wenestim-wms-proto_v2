/**
 *
 */
package com.wenestim.wms.rest.mapper.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.wenestim.wms.rest.entity.master.WMS_MST_CUSTOMER;

/**
 * @author Andrew
 *
 */
public interface WMS_MST_CUSTOMER_Mapper {

    public List<WMS_MST_CUSTOMER> getAllCustomer();

	public Boolean insertCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER);

	public Boolean updateCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER);

	public Boolean deleteCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER);

	public List<WMS_MST_CUSTOMER> getAllCustomerByCompany(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE);

	public List<WMS_MST_CUSTOMER> getCustomerList(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CUST_CODE") String CUST_CODE,
			@Param("SALES_ORG_CODE") String SALES_ORG_CODE,@Param("SALES_DIV_CODE") String SALES_DIV_CODE,@Param("CUST_NAME") String CUST_NAME, @Param("DEL_FLAG") String DEL_FLAG);

	public Boolean insertCustomerList(HashMap<String, Object> paramHashMap);

	public Boolean updateCustomerList(HashMap<String, Object> paramHashMap);

	public Boolean deleteCustomerList(HashMap<String, Object> paramHashMap);

	public List<HashMap<String, Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap);

	public Boolean excelUpload(HashMap<String, Object> dataMap);

}
