package com.wenestim.wms.rest.mapper.summary.cartech;

import java.util.HashMap;
import java.util.List;

public interface WMS_SMR_INVENTORY_Mapper
{
	public HashMap<String, Object> getTotalInventoryQty(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getTotalInventoryList(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDetailInventoryQty(HashMap<String, Object> paramMap);

	public List<HashMap<String, Object>> getDetailInventoryList(HashMap<String, Object> paramMap);

}
