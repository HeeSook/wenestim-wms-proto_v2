/**
 *
 */
package com.wenestim.wms.rest.entity.outbound;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_SALES
{
	public WMS_DYN_SALES()
	{
	}

	private Integer WSID           ;
	private String  CLIENT_ID	   ;
	private String  COMPANY_CODE   ;
    private String  SO_NO          ;
    private String  SO_NO_H   	   ;
    private Integer SO_ITEM_NO     ;
    private String  SO_TYPE        ;
    private String  SO_TYPE_H	   ;
    private String  CUST_CODE      ;
    private String  CUST_NAME	   ;
    private String  MATERIAL_CODE  ;
    private String  MATERIAL_NAME  ;
    private String  DELIVERY_DATE  ;
    private Float   ORDER_QTY      ;
    private String  UNIT           ;
    private Float   CONV_QTY       ;
    private String  CONV_UNIT      ;
    private String  SALES_ORG_CODE ;
    private String  PL_NO          ;
    private String  HEADER_FLAG    ;
    private String  MAIN_FLAG	   ;
    private String  DEL_FLAG       ;
    private String  CREATE_USER    ;
    private String  CREATE_DATE    ;
    private String  UPDATE_USER    ;
    private String  UPDATE_DATE    ;
    private String  DELETE_USER    ;
    private String  DELETE_DATE    ;

    private String  PROMO_NAME		;
    private String  PROMO_DESC	  	;
    private Float   PROMO_DISC	  	;
    private Float   SALES_PRICE     ;
    private Float   ITEM_AMOUNT     ;
    private String  CURRENCY	  	;

    private String  PLANT_CODE  	;

    private String  CUST_PO_NO  	;
    private String  CUST_PO_ITEM_NO	;
    private Float   CUST_PO_QTY  	;
    private Integer ITEM_RANK   	;
    private Integer ITEM_INDEX   	;
    private Float   CONV_COUNT   	;
    private String  GROUP_ID        ;
    private String  PL_FLAG         ;
    private String  PART_NO         ;

    private String  EO_NO           ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getSO_NO() {
		return SO_NO;
	}

	public void setSO_NO(String sO_NO) {
		SO_NO = sO_NO;
	}

	public Integer getSO_ITEM_NO() {
		return SO_ITEM_NO;
	}

	public void setSO_ITEM_NO(Integer sO_ITEM_NO) {
		SO_ITEM_NO = sO_ITEM_NO;
	}

	public String getSO_TYPE() {
		return SO_TYPE;
	}

	public void setSO_TYPE(String sO_TYPE) {
		SO_TYPE = sO_TYPE;
	}

	public String getCUST_CODE() {
		return CUST_CODE;
	}

	public void setCUST_CODE(String cUST_CODE) {
		CUST_CODE = cUST_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getDELIVERY_DATE() {
		return DELIVERY_DATE;
	}

	public void setDELIVERY_DATE(String dELIVERY_DATE) {
		DELIVERY_DATE = dELIVERY_DATE;
	}

	public Float getORDER_QTY() {
		return ORDER_QTY;
	}

	public void setORDER_QTY(Float oRDER_QTY) {
		ORDER_QTY = oRDER_QTY;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public Float getCONV_QTY() {
		return CONV_QTY;
	}

	public void setCONV_QTY(Float cONV_QTY) {
		CONV_QTY = cONV_QTY;
	}

	public String getCONV_UNIT() {
		return CONV_UNIT;
	}

	public void setCONV_UNIT(String cONV_UNIT) {
		CONV_UNIT = cONV_UNIT;
	}

	public String getSALES_ORG_CODE() {
		return SALES_ORG_CODE;
	}

	public void setSALES_ORG_CODE(String sALES_ORG_CODE) {
		SALES_ORG_CODE = sALES_ORG_CODE;
	}

	public String getPL_NO() {
		return PL_NO;
	}

	public void setPL_NO(String pL_NO) {
		PL_NO = pL_NO;
	}

	public String getHEADER_FLAG() {
		return HEADER_FLAG;
	}

	public void setHEADER_FLAG(String hEADER_FLAG) {
		HEADER_FLAG = hEADER_FLAG;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getCUST_NAME() {
		return CUST_NAME;
	}

	public void setCUST_NAME(String cUST_NAME) {
		CUST_NAME = cUST_NAME;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getMAIN_FLAG() {
		return MAIN_FLAG;
	}

	public void setMAIN_FLAG(String mAIN_FLAG) {
		MAIN_FLAG = mAIN_FLAG;
	}

	public String getSO_NO_H() {
		return SO_NO_H;
	}

	public void setSO_NO_H(String sO_NO_H) {
		SO_NO_H = sO_NO_H;
	}

	public String getSO_TYPE_H() {
		return SO_TYPE_H;
	}

	public void setSO_TYPE_H(String sO_TYPE_H) {
		SO_TYPE_H = sO_TYPE_H;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getPROMO_NAME() {
		return PROMO_NAME;
	}

	public void setPROMO_NAME(String pROMO_NAME) {
		PROMO_NAME = pROMO_NAME;
	}

	public String getPROMO_DESC() {
		return PROMO_DESC;
	}

	public void setPROMO_DESC(String pROMO_DESC) {
		PROMO_DESC = pROMO_DESC;
	}

	public Float getPROMO_DISC() {
		return PROMO_DISC;
	}

	public void setPROMO_DISC(Float pROMO_DISC) {
		PROMO_DISC = pROMO_DISC;
	}

	public Float getSALES_PRICE() {
		return SALES_PRICE;
	}

	public void setSALES_PRICE(Float sALES_PRICE) {
		SALES_PRICE = sALES_PRICE;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}


	public Float getITEM_AMOUNT() {
		return ITEM_AMOUNT;
	}

	public void setITEM_AMOUNT(Float iTEM_AMOUNT) {
		ITEM_AMOUNT = iTEM_AMOUNT;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getCUST_PO_NO() {
		return CUST_PO_NO;
	}

	public void setCUST_PO_NO(String cUST_PO_NO) {
		CUST_PO_NO = cUST_PO_NO;
	}

	public String getCUST_PO_ITEM_NO() {
		return CUST_PO_ITEM_NO;
	}

	public void setCUST_PO_ITEM_NO(String cUST_PO_ITEM_NO) {
		CUST_PO_ITEM_NO = cUST_PO_ITEM_NO;
	}

	public Float getCUST_PO_QTY() {
		return CUST_PO_QTY;
	}

	public void setCUST_PO_QTY(Float cUST_PO_QTY) {
		CUST_PO_QTY = cUST_PO_QTY;
	}

	public Integer getITEM_RANK() {
		return ITEM_RANK;
	}

	public void setITEM_RANK(Integer iTEM_RANK) {
		ITEM_RANK = iTEM_RANK;
	}


	public Integer getITEM_INDEX() {
		return ITEM_INDEX;
	}

	public void setITEM_INDEX(Integer iTEM_INDEX) {
		ITEM_INDEX = iTEM_INDEX;
	}

	public Float getCONV_COUNT() {
		return CONV_COUNT;
	}

	public void setCONV_COUNT(Float cONV_COUNT) {
		CONV_COUNT = cONV_COUNT;
	}

	public String getGROUP_ID() {
		return GROUP_ID;
	}

	public void setGROUP_ID(String gROUP_ID) {
		GROUP_ID = gROUP_ID;
	}

	public String getPL_FLAG() {
		return PL_FLAG;
	}

	public void setPL_FLAG(String pL_FLAG) {
		PL_FLAG = pL_FLAG;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getEO_NO() {
		return EO_NO;
	}

	public void setEO_NO(String eO_NO) {
		EO_NO = eO_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_SALES [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", SO_NO=" + SO_NO + ", SO_NO_H=" + SO_NO_H + ", SO_ITEM_NO=" + SO_ITEM_NO + ", SO_TYPE=" + SO_TYPE
				+ ", SO_TYPE_H=" + SO_TYPE_H + ", CUST_CODE=" + CUST_CODE + ", CUST_NAME=" + CUST_NAME
				+ ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_NAME=" + MATERIAL_NAME + ", DELIVERY_DATE="
				+ DELIVERY_DATE + ", ORDER_QTY=" + ORDER_QTY + ", UNIT=" + UNIT + ", CONV_QTY=" + CONV_QTY
				+ ", CONV_UNIT=" + CONV_UNIT + ", SALES_ORG_CODE=" + SALES_ORG_CODE + ", PL_NO=" + PL_NO
				+ ", HEADER_FLAG=" + HEADER_FLAG + ", MAIN_FLAG=" + MAIN_FLAG + ", DEL_FLAG=" + DEL_FLAG
				+ ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER
				+ ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE
				+ ", PROMO_NAME=" + PROMO_NAME + ", PROMO_DESC=" + PROMO_DESC + ", PROMO_DISC=" + PROMO_DISC
				+ ", SALES_PRICE=" + SALES_PRICE + ", ITEM_AMOUNT=" + ITEM_AMOUNT + ", CURRENCY=" + CURRENCY
				+ ", PLANT_CODE=" + PLANT_CODE + ", CUST_PO_NO=" + CUST_PO_NO + ", CUST_PO_ITEM_NO=" + CUST_PO_ITEM_NO
				+ ", CUST_PO_QTY=" + CUST_PO_QTY + ", ITEM_RANK=" + ITEM_RANK + ", ITEM_INDEX=" + ITEM_INDEX
				+ ", CONV_COUNT=" + CONV_COUNT + ", GROUP_ID=" + GROUP_ID + ", PL_FLAG=" + PL_FLAG + ", PART_NO="
				+ PART_NO + ", EO_NO=" + EO_NO + "]";
	}
}
