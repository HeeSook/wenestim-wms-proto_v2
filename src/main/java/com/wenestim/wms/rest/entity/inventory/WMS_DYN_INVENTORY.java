/**
 *
 */
package com.wenestim.wms.rest.entity.inventory;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_INVENTORY
{
	public WMS_DYN_INVENTORY()
	{
	}

	private Integer WSID         ;
	private String COMPANY_CODE  ;
    private String STORAGE_LOC   ;
    private String PLANT_CODE    ;
    private String MATERIAL_CODE ;
    private float  INV_QTY       ;
    private String UNIT          ;
    private float  PER_QTY       ;
    private String BIN_LOC       ;
    private String BIN_LEVEL     ;
    private String BIN_POSITION  ;
    private String CREATE_USER   ;
    private String CREATE_DATE   ;
    private String UPDATE_USER   ;
    private String UPDATE_DATE   ;

    private String MATERIAL_NAME ;
    private String PLANT_NAME    ;
    private String MATERIAL_TYPE ;
    private float  REMAIN_QTY    ;

    private String  PART_NO      ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public float getINV_QTY() {
		return INV_QTY;
	}

	public void setINV_QTY(float iNV_QTY) {
		INV_QTY = iNV_QTY;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public float getPER_QTY() {
		return PER_QTY;
	}

	public void setPER_QTY(float pER_QTY) {
		PER_QTY = pER_QTY;
	}

	public String getBIN_LOC() {
		return BIN_LOC;
	}

	public void setBIN_LOC(String bIN_LOC) {
		BIN_LOC = bIN_LOC;
	}

	public String getBIN_LEVEL() {
		return BIN_LEVEL;
	}

	public void setBIN_LEVEL(String bIN_LEVEL) {
		BIN_LEVEL = bIN_LEVEL;
	}

	public String getBIN_POSITION() {
		return BIN_POSITION;
	}

	public void setBIN_POSITION(String bIN_POSITION) {
		BIN_POSITION = bIN_POSITION;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getPLANT_NAME() {
		return PLANT_NAME;
	}

	public void setPLANT_NAME(String pLANT_NAME) {
		PLANT_NAME = pLANT_NAME;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public float getREMAIN_QTY() {
		return REMAIN_QTY;
	}

	public void setREMAIN_QTY(float rEMAIN_QTY) {
		REMAIN_QTY = rEMAIN_QTY;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_INVENTORY [WSID=" + WSID + ", COMPANY_CODE=" + COMPANY_CODE + ", STORAGE_LOC=" + STORAGE_LOC
				+ ", PLANT_CODE=" + PLANT_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", INV_QTY=" + INV_QTY + ", UNIT="
				+ UNIT + ", PER_QTY=" + PER_QTY + ", BIN_LOC=" + BIN_LOC + ", BIN_LEVEL=" + BIN_LEVEL
				+ ", BIN_POSITION=" + BIN_POSITION + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE
				+ ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", MATERIAL_NAME=" + MATERIAL_NAME
				+ ", PLANT_NAME=" + PLANT_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", REMAIN_QTY=" + REMAIN_QTY
				+ ", PART_NO=" + PART_NO + "]";
	}
}

