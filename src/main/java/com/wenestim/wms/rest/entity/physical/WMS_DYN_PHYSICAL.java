/**
 *
 */
package com.wenestim.wms.rest.entity.physical;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PHYSICAL
{
	public WMS_DYN_PHYSICAL()
	{
	}

	private Integer WSID          ;
	private String  CLIENT_ID     ;
	private String  COMPANY_CODE  ;
    private Integer DOC_NO        ;
    private String  STORAGE_LOC   ;
    private String  MATERIAL_CODE ;
    private String  POST_DATE     ;
    private String  BARCODE_NO    ;
    private Double  INV_QTY       ;
    private Double  COUNT_QTY     ;
    private Double  DIFF_QTY      ;
    private String  UNIT          ;
    private String  DEL_FLAG      ;
    private String  CREATE_USER   ;
    private String  CREATE_DATE   ;
    private String  UPDATE_USER   ;
    private String  UPDATE_DATE   ;
    private String  DELETE_USER   ;
    private String  DELETE_DATE   ;

	private Integer INV_WSID      ;
	private Integer PHY_WSID      ;
    private String  MATERIAL_NAME ;
    private String  PLANT_NAME    ;
    private String  MATERIAL_TYPE ;
    private String  MOVEMENT_TYPE ;
    private Double  MOVE_QTY      ;
    private String  CURR_STORAGE_LOC ;
    private String  MOVE_STORAGE_LOC ;
    private String  EDIT_FLAG      ;
    private String  DUMMY_FLAG     ;
	private Integer ROWID          ;

    private String  PART_NO        ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCLIENT_ID() {
        return CLIENT_ID;
    }

    public void setCLIENT_ID(String cLIENT_ID) {
        CLIENT_ID = cLIENT_ID;
    }

    public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public Integer getDOC_NO() {
		return DOC_NO;
	}

	public void setDOC_NO(Integer dOC_NO) {
		DOC_NO = dOC_NO;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getPOST_DATE() {
		return POST_DATE;
	}

	public void setPOST_DATE(String pOST_DATE) {
		POST_DATE = pOST_DATE;
	}

	public String getBARCODE_NO() {
		return BARCODE_NO;
	}

	public void setBARCODE_NO(String bARCODE_NO) {
		BARCODE_NO = bARCODE_NO;
	}

	public Double getINV_QTY() {
		return INV_QTY;
	}

	public void setINV_QTY(Double iNV_QTY) {
		INV_QTY = iNV_QTY;
	}

	public Double getCOUNT_QTY() {
		return COUNT_QTY;
	}

	public void setCOUNT_QTY(Double cOUNT_QTY) {
		COUNT_QTY = cOUNT_QTY;
	}

	public Double getDIFF_QTY() {
		return DIFF_QTY;
	}

	public void setDIFF_QTY(Double dIFF_QTY) {
		DIFF_QTY = dIFF_QTY;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public Integer getINV_WSID() {
		return INV_WSID;
	}

	public void setINV_WSID(Integer iNV_WSID) {
		INV_WSID = iNV_WSID;
	}

	public Integer getPHY_WSID() {
		return PHY_WSID;
	}

	public void setPHY_WSID(Integer pHY_WSID) {
		PHY_WSID = pHY_WSID;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getPLANT_NAME() {
		return PLANT_NAME;
	}

	public void setPLANT_NAME(String pLANT_NAME) {
		PLANT_NAME = pLANT_NAME;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public String getMOVEMENT_TYPE() {
		return MOVEMENT_TYPE;
	}

	public void setMOVEMENT_TYPE(String mOVEMENT_TYPE) {
		MOVEMENT_TYPE = mOVEMENT_TYPE;
	}

	public Double getMOVE_QTY() {
		return MOVE_QTY;
	}

	public void setMOVE_QTY(Double mOVE_QTY) {
		MOVE_QTY = mOVE_QTY;
	}

	public String getCURR_STORAGE_LOC() {
		return CURR_STORAGE_LOC;
	}

	public void setCURR_STORAGE_LOC(String cURR_STORAGE_LOC) {
		CURR_STORAGE_LOC = cURR_STORAGE_LOC;
	}

	public String getEDIT_FLAG() {
		return EDIT_FLAG;
	}

	public void setEDIT_FLAG(String eDIT_FLAG) {
		EDIT_FLAG = eDIT_FLAG;
	}

	public String getMOVE_STORAGE_LOC() {
		return MOVE_STORAGE_LOC;
	}

	public void setMOVE_STORAGE_LOC(String mOVE_STORAGE_LOC) {
		MOVE_STORAGE_LOC = mOVE_STORAGE_LOC;
	}

	public String getDUMMY_FLAG() {
		return DUMMY_FLAG;
	}

	public void setDUMMY_FLAG(String dUMMY_FLAG) {
		DUMMY_FLAG = dUMMY_FLAG;
	}

	public Integer getROWID() {
		return ROWID;
	}

	public void setROWID(Integer rOWID) {
		ROWID = rOWID;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_PHYSICAL [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", DOC_NO=" + DOC_NO + ", STORAGE_LOC=" + STORAGE_LOC + ", MATERIAL_CODE=" + MATERIAL_CODE
				+ ", POST_DATE=" + POST_DATE + ", BARCODE_NO=" + BARCODE_NO + ", INV_QTY=" + INV_QTY + ", COUNT_QTY="
				+ COUNT_QTY + ", DIFF_QTY=" + DIFF_QTY + ", UNIT=" + UNIT + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER="
				+ CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE="
				+ UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", INV_WSID="
				+ INV_WSID + ", PHY_WSID=" + PHY_WSID + ", MATERIAL_NAME=" + MATERIAL_NAME + ", PLANT_NAME="
				+ PLANT_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", MOVEMENT_TYPE=" + MOVEMENT_TYPE + ", MOVE_QTY="
				+ MOVE_QTY + ", CURR_STORAGE_LOC=" + CURR_STORAGE_LOC + ", MOVE_STORAGE_LOC=" + MOVE_STORAGE_LOC
				+ ", EDIT_FLAG=" + EDIT_FLAG + ", DUMMY_FLAG=" + DUMMY_FLAG + ", ROWID=" + ROWID + ", PART_NO="
				+ PART_NO + "]";
	}
}
