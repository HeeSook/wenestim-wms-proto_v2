/**
 *
 */
package com.wenestim.wms.rest.entity.inventory;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_STOCK_MOVEMENT
{
    public WMS_DYN_STOCK_MOVEMENT()
    {
    }

    private Integer WSID              ;
    private String  CLIENT_ID         ;
    private String  COMPANY_CODE      ;
    private Integer DOC_NO            ;
    private String  MATERIAL_CODE     ;
    private String  POST_DATE         ;
    private String  MOVEMENT_TYPE     ;
    private Double  INV_QTY           ;
    private String  CURR_STORAGE_LOC  ;
    private Double  CURR_QTY          ;
    private String  MOVE_STORAGE_LOC  ;
    private Double  MOVE_QTY          ;
    private String  ORG_LOC           ;
    private String  CHG_LOC           ;
    private String  TRANSFER_LOC      ;
    private Double  TRANSFER_QTY      ;
    private String  TRANSFER_DATE     ;
    private String  UNIT              ;
    private String  CREATE_USER       ;
    private String  CREATE_DATE       ;
    private String  UPDATE_USER       ;
    private String  UPDATE_DATE       ;
    private String  EDIT_FLAG         ;

    private String  MATERIAL_NAME     ;

    private String  PLANT_CODE        ;
    private String  PLANT_NAME        ;
    private String  MATERIAL_TYPE     ;
    private String  STORAGE_LOC       ;
    private String  INPUT_TYPE        ;

    private Integer REF_DOC_NO        ;

    private String  PART_NO           ;
    private String  SHIFT             ;

    private String  MOVE_MATERIAL_CODE;

    public Integer getWSID() {
        return WSID;
    }

    public void setWSID(Integer wSID) {
        WSID = wSID;
    }

    public String getCLIENT_ID() {
        return CLIENT_ID;
    }

    public void setCLIENT_ID(String cLIENT_ID) {
        CLIENT_ID = cLIENT_ID;
    }

    public String getCOMPANY_CODE() {
        return COMPANY_CODE;
    }

    public void setCOMPANY_CODE(String cOMPANY_CODE) {
        COMPANY_CODE = cOMPANY_CODE;
    }

    public Integer getDOC_NO() {
        return DOC_NO;
    }

    public void setDOC_NO(Integer dOC_NO) {
        DOC_NO = dOC_NO;
    }

    public String getMATERIAL_CODE() {
        return MATERIAL_CODE;
    }

    public void setMATERIAL_CODE(String mATERIAL_CODE) {
        MATERIAL_CODE = mATERIAL_CODE;
    }

    public String getPOST_DATE() {
        return POST_DATE;
    }

    public void setPOST_DATE(String pOST_DATE) {
        POST_DATE = pOST_DATE;
    }

    public String getMOVEMENT_TYPE() {
        return MOVEMENT_TYPE;
    }

    public void setMOVEMENT_TYPE(String mOVEMENT_TYPE) {
        MOVEMENT_TYPE = mOVEMENT_TYPE;
    }

    public String getCURR_STORAGE_LOC() {
        return CURR_STORAGE_LOC;
    }

    public void setCURR_STORAGE_LOC(String cURR_STORAGE_LOC) {
        CURR_STORAGE_LOC = cURR_STORAGE_LOC;
    }

    public Double getCURR_QTY() {
        return CURR_QTY;
    }

    public void setCURR_QTY(Double cURR_QTY) {
        CURR_QTY = cURR_QTY;
    }

    public String getMOVE_STORAGE_LOC() {
        return MOVE_STORAGE_LOC;
    }

    public void setMOVE_STORAGE_LOC(String mOVE_STORAGE_LOC) {
        MOVE_STORAGE_LOC = mOVE_STORAGE_LOC;
    }

    public Double getMOVE_QTY() {
        return MOVE_QTY;
    }

    public void setMOVE_QTY(Double mOVE_QTY) {
        MOVE_QTY = mOVE_QTY;
    }

    public String getUNIT() {
        return UNIT;
    }

    public void setUNIT(String uNIT) {
        UNIT = uNIT;
    }

    public String getCREATE_USER() {
        return CREATE_USER;
    }

    public void setCREATE_USER(String cREATE_USER) {
        CREATE_USER = cREATE_USER;
    }

    public String getCREATE_DATE() {
        return CREATE_DATE;
    }

    public void setCREATE_DATE(String cREATE_DATE) {
        CREATE_DATE = cREATE_DATE;
    }

    public String getUPDATE_USER() {
        return UPDATE_USER;
    }

    public void setUPDATE_USER(String uPDATE_USER) {
        UPDATE_USER = uPDATE_USER;
    }

    public String getUPDATE_DATE() {
        return UPDATE_DATE;
    }

    public void setUPDATE_DATE(String uPDATE_DATE) {
        UPDATE_DATE = uPDATE_DATE;
    }

    public String getEDIT_FLAG() {
        return EDIT_FLAG;
    }

    public void setEDIT_FLAG(String eDIT_FLAG) {
        EDIT_FLAG = eDIT_FLAG;
    }

    public String getMATERIAL_NAME() {
        return MATERIAL_NAME;
    }

    public void setMATERIAL_NAME(String mATERIAL_NAME) {
        MATERIAL_NAME = mATERIAL_NAME;
    }

    public Double getINV_QTY() {
        return INV_QTY;
    }

    public void setINV_QTY(Double iNV_QTY) {
        INV_QTY = iNV_QTY;
    }

    public String getPLANT_NAME() {
        return PLANT_NAME;
    }

    public void setPLANT_NAME(String pLANT_NAME) {
        PLANT_NAME = pLANT_NAME;
    }

    public String getMATERIAL_TYPE() {
        return MATERIAL_TYPE;
    }

    public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
        MATERIAL_TYPE = mATERIAL_TYPE;
    }

    public String getSTORAGE_LOC() {
        return STORAGE_LOC;
    }

    public void setSTORAGE_LOC(String sTORAGE_LOC) {
        STORAGE_LOC = sTORAGE_LOC;
    }

    public String getINPUT_TYPE() {
        return INPUT_TYPE;
    }

    public void setINPUT_TYPE(String iNPUT_TYPE) {
        INPUT_TYPE = iNPUT_TYPE;
    }

	public String getORG_LOC() {
		return ORG_LOC;
	}

	public void setORG_LOC(String oRG_LOC) {
		ORG_LOC = oRG_LOC;
	}

	public String getCHG_LOC() {
		return CHG_LOC;
	}

	public void setCHG_LOC(String cHG_LOC) {
		CHG_LOC = cHG_LOC;
	}

    public Double getTRANSFER_QTY() {
		return TRANSFER_QTY;
	}

	public void setTRANSFER_QTY(Double tRANSFER_QTY) {
		TRANSFER_QTY = tRANSFER_QTY;
	}

	public String getTRANSFER_DATE() {
		return TRANSFER_DATE;
	}

	public void setTRANSFER_DATE(String tRANSFER_DATE) {
		TRANSFER_DATE = tRANSFER_DATE;
	}

	public String getTRANSFER_LOC() {
		return TRANSFER_LOC;
	}

	public void setTRANSFER_LOC(String tRANSFER_LOC) {
		TRANSFER_LOC = tRANSFER_LOC;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public Integer getREF_DOC_NO() {
		return REF_DOC_NO;
	}

	public void setREF_DOC_NO(Integer rEF_DOC_NO) {
		REF_DOC_NO = rEF_DOC_NO;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getSHIFT() {
		return SHIFT;
	}

	public void setSHIFT(String sHIFT) {
		SHIFT = sHIFT;
	}

	public String getMOVE_MATERIAL_CODE() {
		return MOVE_MATERIAL_CODE;
	}

	public void setMOVE_MATERIAL_CODE(String mOVE_MATERIAL_CODE) {
		MOVE_MATERIAL_CODE = mOVE_MATERIAL_CODE;
	}

	@Override
	public String toString() {
		return "WMS_DYN_STOCK_MOVEMENT [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", DOC_NO=" + DOC_NO + ", MATERIAL_CODE=" + MATERIAL_CODE + ", POST_DATE=" + POST_DATE
				+ ", MOVEMENT_TYPE=" + MOVEMENT_TYPE + ", INV_QTY=" + INV_QTY + ", CURR_STORAGE_LOC=" + CURR_STORAGE_LOC
				+ ", CURR_QTY=" + CURR_QTY + ", MOVE_STORAGE_LOC=" + MOVE_STORAGE_LOC + ", MOVE_QTY=" + MOVE_QTY
				+ ", ORG_LOC=" + ORG_LOC + ", CHG_LOC=" + CHG_LOC + ", TRANSFER_LOC=" + TRANSFER_LOC + ", TRANSFER_QTY="
				+ TRANSFER_QTY + ", TRANSFER_DATE=" + TRANSFER_DATE + ", UNIT=" + UNIT + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", EDIT_FLAG=" + EDIT_FLAG + ", MATERIAL_NAME=" + MATERIAL_NAME + ", PLANT_CODE=" + PLANT_CODE
				+ ", PLANT_NAME=" + PLANT_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", STORAGE_LOC=" + STORAGE_LOC
				+ ", INPUT_TYPE=" + INPUT_TYPE + ", REF_DOC_NO=" + REF_DOC_NO + ", PART_NO=" + PART_NO + ", SHIFT="
				+ SHIFT + ", MOVE_MATERIAL_CODE=" + MOVE_MATERIAL_CODE + "]";
	}
}

