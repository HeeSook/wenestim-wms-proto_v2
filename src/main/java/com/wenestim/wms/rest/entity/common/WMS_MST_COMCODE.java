/**
 *
 */
package com.wenestim.wms.rest.entity.common;

/**
 * @author Andrew
 *
 */
public class WMS_MST_COMCODE
{
	public WMS_MST_COMCODE()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID	 ;
	private String  COMPANY_CODE ;
	private String  CATEGORY1    ;
	private String  CATEGORY2    ;
	private String  CODE         ;
	private String  CODE_DESC    ;
	private Integer SORT1        ;
	private Integer SORT2        ;
	private String  COMMENT      ;
	private String  DEL_FLAG     ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
    private String  DELETE_USER  ;
	private String  DELETE_DATE  ;

	/*20180618 Add By HHS*/
	private String  ATTR1         ;
	private String  ATTR2         ;
	private String  ATTR3         ;
	private String  CATEGORY2_CODE;
	private String  COMPANY_NAME  ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE()
	{
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getCATEGORY1() {
		return CATEGORY1;
	}

	public void setCATEGORY1(String cATEGORY1) {
		CATEGORY1 = cATEGORY1;
	}

	public String getCATEGORY2() {
		return CATEGORY2;
	}

	public void setCATEGORY2(String cATEGORY2) {
		CATEGORY2 = cATEGORY2;
	}

	public String getCODE() {
		return CODE;
	}

	public void setCODE(String cODE) {
		CODE = cODE;
	}

	public String getCODE_DESC() {
		return CODE_DESC;
	}

	public void setCODE_DESC(String cODE_DESC) {
		CODE_DESC = cODE_DESC;
	}

	public Integer getSORT1() {
		return SORT1;
	}

	public void setSORT1(Integer sORT1) {
		SORT1 = sORT1;
	}

	public Integer getSORT2() {
		return SORT2;
	}
	public void setSORT2(Integer sORT2) {
		SORT2 = sORT2;
	}

	public String getCOMMENT() {
		return COMMENT;
	}

	public void setCOMMENT(String cOMMENT) {
		COMMENT = cOMMENT;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getATTR1() {
		return ATTR1;
	}

	public void setATTR1(String aTTR1) {
		ATTR1 = aTTR1;
	}

	public String getATTR2() {
		return ATTR2;
	}

	public void setATTR2(String aTTR2) {
		ATTR2 = aTTR2;
	}

	public String getATTR3() {
		return ATTR3;
	}

	public void setATTR3(String aTTR3) {
		ATTR3 = aTTR3;
	}

	public String getCATEGORY2_CODE() {
		return CATEGORY2_CODE;
	}

	public void setCATEGORY2_CODE(String cATEGORY2_CODE) {
		CATEGORY2_CODE = cATEGORY2_CODE;
	}

	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}

	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}

	@Override
	public String toString() {
		return "WMS_MST_COMCODE [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", CATEGOTY1=" + CATEGORY1 + ", CATEGOTY2=" + CATEGORY2 + ", CODE=" + CODE + ", CODE_DESC="
				+ CODE_DESC + ", SORT1=" + SORT1 + ", SORT2=" + SORT2 + ", COMMENT=" + COMMENT + ", DEL_FLAG="
				+ DEL_FLAG + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER="
				+ UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE="
				+ DELETE_DATE + ", ATTR1=" + ATTR1 + ", CATEGORY2_CODE=" + CATEGORY2_CODE + "]";
	}


}