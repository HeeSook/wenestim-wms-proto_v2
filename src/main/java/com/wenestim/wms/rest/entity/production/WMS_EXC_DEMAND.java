/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_EXC_DEMAND extends WMS_EXC_INVENTORY
{
	private String ORDER_NO      ;
	private String DEMAND_TYPE   ;
    private String MERGE_CODE    ;
    private String PLANT_CODE    ;
    private String PROD_CODE     ;
    private String MATERIAL_CODE ;
    private String REQ_DATE      ;
    private float  REQ_QTY       ;
    private float  LOT_SIZE      ;
    private String SORT_KEY      ;

	public WMS_EXC_DEMAND()
    {
    }

	@Override
	public String getORDER_NO() {
		return ORDER_NO;
	}

	@Override
	public void setORDER_NO(String oRDER_NO) {
		ORDER_NO = oRDER_NO;
	}

	@Override
	public String getDEMAND_TYPE() {
		return DEMAND_TYPE;
	}

	@Override
	public void setDEMAND_TYPE(String dEMAND_TYPE) {
		DEMAND_TYPE = dEMAND_TYPE;
	}

	public String getMERGE_CODE() {
		return MERGE_CODE;
	}

	public void setMERGE_CODE(String mERGE_CODE) {
		MERGE_CODE = mERGE_CODE;
	}

	@Override
	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	@Override
	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getPROD_CODE() {
		return PROD_CODE;
	}

	public void setPROD_CODE(String pROD_CODE) {
		PROD_CODE = pROD_CODE;
	}

	@Override
	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	@Override
	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getREQ_DATE() {
		return REQ_DATE;
	}

	public void setREQ_DATE(String rEQ_DATE) {
		REQ_DATE = rEQ_DATE;
	}

	@Override
	public float getREQ_QTY() {
		return REQ_QTY;
	}

	@Override
	public void setREQ_QTY(float rEQ_QTY) {
		REQ_QTY = rEQ_QTY;
	}

	public float getLOT_SIZE() {
		return LOT_SIZE;
	}

	public void setLOT_SIZE(float lOT_SIZE) {
		LOT_SIZE = lOT_SIZE;
	}

	public String getSORT_KEY() {
		return SORT_KEY;
	}

	public void setSORT_KEY(String sORT_KEY) {
		SORT_KEY = sORT_KEY;
	}

	@Override
	public String toString() {
		return "WMS_EXC_DEMAND [ORDER_NO=" + ORDER_NO + ", DEMAND_TYPE=" + DEMAND_TYPE + ", MERGE_CODE=" + MERGE_CODE
				+ ", PLANT_CODE=" + PLANT_CODE + ", PROD_CODE=" + PROD_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE
				+ ", REQ_DATE=" + REQ_DATE + ", REQ_QTY=" + REQ_QTY + ", LOT_SIZE=" + LOT_SIZE + ", SORT_KEY="
				+ SORT_KEY + "]";
	}
}

