/**
 *
 */
package com.wenestim.wms.rest.entity.common;

/**
 * @author Andrew
 *
 */
public class WMS_MST_COMPANY
{
	public WMS_MST_COMPANY()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID	 ;
	private String  COMPANY_CODE ;
	private String  COMPANY_NAME ;
	private String  ADDRESS      ;
	private String  ADDRESS1     ;
	private String  ADDRESS2     ;
	private String  COUNTRY_CODE ;
	private String  PHONE        ;
	private String  CONTACT_NAME ;
	private String  CONTACT_EMAIL;
	private String  CONTACT_PHONE;
	private String  LOGO_NAME    ;
	private String  PRICE_FORMAT ;
	private String  DEL_FLAG     ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
    private String  DELETE_USER  ;
	private String  DELETE_DATE  ;

	private String  CODE_DESC    ;

	public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}

	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getADDRESS1() {
		return ADDRESS1;
	}

	public void setADDRESS1(String aDDRESS1) {
		ADDRESS1 = aDDRESS1;
	}

	public String getADDRESS2() {
		return ADDRESS2;
	}

	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}

	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}

	public void setCOUNTRY_CODE(String cOUNTRY_CODE) {
		COUNTRY_CODE = cOUNTRY_CODE;
	}

	public String getPHONE() {
		return PHONE;
	}

	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}

	public String getCONTACT_NAME() {
		return CONTACT_NAME;
	}

	public void setCONTACT_NAME(String cONTACT_NAME) {
		CONTACT_NAME = cONTACT_NAME;
	}

	public String getCONTACT_EMAIL() {
		return CONTACT_EMAIL;
	}

	public void setCONTACT_EMAIL(String cONTACT_EMAIL) {
		CONTACT_EMAIL = cONTACT_EMAIL;
	}

	public String getCONTACT_PHONE() {
		return CONTACT_PHONE;
	}

	public void setCONTACT_PHONE(String cONTACT_PHONE) {
		CONTACT_PHONE = cONTACT_PHONE;
	}

	public String getLOGO_NAME() {
		return LOGO_NAME;
	}

	public void setLOGO_NAME(String lOGO_NAME) {
		LOGO_NAME = lOGO_NAME;
	}

	public String getPRICE_FORMAT() {
		return PRICE_FORMAT;
	}

	public void setPRICE_FORMAT(String pRICE_FORMAT) {
		PRICE_FORMAT = pRICE_FORMAT;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getCODE_DESC() {
		return CODE_DESC;
	}

	public void setCODE_DESC(String cODE_DESC) {
		CODE_DESC = cODE_DESC;
	}

	@Override
	public String toString() {
		return "WMS_MST_COMPANY [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", COMPANY_NAME=" + COMPANY_NAME + ", ADDRESS=" + ADDRESS + ", ADDRESS1=" + ADDRESS1 + ", ADDRESS2="
				+ ADDRESS2 + ", COUNTRY_CODE=" + COUNTRY_CODE + ", PHONE=" + PHONE + ", CONTACT_NAME=" + CONTACT_NAME
				+ ", CONTACT_EMAIL=" + CONTACT_EMAIL + ", CONTACT_PHONE=" + CONTACT_PHONE + ", LOGO_NAME=" + LOGO_NAME
				+ ", PRICE_FORMAT=" + PRICE_FORMAT + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", CODE_DESC=" + CODE_DESC + "]";
	}
}