/**
 *
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_INFOCODE
{
	public WMS_MST_INFOCODE()
	{
	}

	private Integer WSID         ;
	private String CLIENT_ID     ;
	private String COMPANY_CODE  ;
    private String VENDOR_CODE   ;
    private String MATERIAL_CODE ;
    private String FROM_DATE     ;
    private String CATEGORY      ;
    private Float  UNIT_PRICE    ;
    private String CURRENCY      ;
    private Float  INFO_QTY      ;
    private String INFO_UNIT     ;
    private Integer INFO_NO      ;
    private String ACT_FLAG      ;
    private String DEL_FLAG      ;
    private String CREATE_USER   ;
    private String CREATE_DATE   ;
    private String UPDATE_USER   ;
    private String UPDATE_DATE   ;
    private String DELETE_USER   ;
    private String DELETE_DATE   ;

    // For Search
    private String VENDOR_NAME   ;
    private String MATERIAL_NAME ;

    // For List
    private String PURCHASE_ORG_CODE;
    private String PURCHASE_GRP_CODE;

    private String SUB_CONTRACT;
    private String PART_NO     ;
    private String PART_NAME   ;

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	/**
	 * @return the vENDOR_CODE
	 */
	public String getVENDOR_CODE() {
		return VENDOR_CODE;
	}

	/**
	 * @param vENDOR_CODE the vENDOR_CODE to set
	 */
	public void setVENDOR_CODE(String vENDOR_CODE) {
		VENDOR_CODE = vENDOR_CODE;
	}

	/**
	 * @return the mATERIAL_CODE
	 */
	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	/**
	 * @param mATERIAL_CODE the mATERIAL_CODE to set
	 */
	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	/**
	 * @return the fROM_DATE
	 */
	public String getFROM_DATE() {
		return FROM_DATE;
	}

	/**
	 * @param fROM_DATE the fROM_DATE to set
	 */
	public void setFROM_DATE(String fROM_DATE) {
		FROM_DATE = fROM_DATE;
	}

	/**
	 * @return the cATEGORY
	 */
	public String getCATEGORY() {
		return CATEGORY;
	}

	/**
	 * @param cATEGORY the cATEGORY to set
	 */
	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}

	/**
	 * @return the uNIT_PRICE
	 */
	public Float getUNIT_PRICE() {
		return UNIT_PRICE;
	}

	/**
	 * @param uNIT_PRICE the uNIT_PRICE to set
	 */
	public void setUNIT_PRICE(Float uNIT_PRICE) {
		UNIT_PRICE = uNIT_PRICE;
	}

	/**
	 * @return the cURRENCY
	 */
	public String getCURRENCY() {
		return CURRENCY;
	}

	/**
	 * @param cURRENCY the cURRENCY to set
	 */
	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	/**
	 * @return the iNFO_QTY
	 */
	public Float getINFO_QTY() {
		return INFO_QTY;
	}

	/**
	 * @param iNFO_QTY the iNFO_QTY to set
	 */
	public void setINFO_QTY(Float iNFO_QTY) {
		INFO_QTY = iNFO_QTY;
	}

	/**
	 * @return the iNFO_UNIT
	 */
	public String getINFO_UNIT() {
		return INFO_UNIT;
	}

	/**
	 * @param iNFO_UNIT the iNFO_UNIT to set
	 */
	public void setINFO_UNIT(String iNFO_UNIT) {
		INFO_UNIT = iNFO_UNIT;
	}

	/**
	 * @return the iNFO_NO
	 */
	public Integer getINFO_NO() {
		return INFO_NO;
	}

	/**
	 * @param iNFO_NO the iNFO_NO to set
	 */
	public void setINFO_NO(Integer iNFO_NO) {
		INFO_NO = iNFO_NO;
	}

	/**
	 * @return the aCT_FLAG
	 */
	public String getACT_FLAG() {
		return ACT_FLAG;
	}

	/**
	 * @param aCT_FLAG the aCT_FLAG to set
	 */
	public void setACT_FLAG(String aCT_FLAG) {
		ACT_FLAG = aCT_FLAG;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	/**
	 * @return the vENDOR_NAME
	 */
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}

	/**
	 * @param vENDOR_NAME the vENDOR_NAME to set
	 */
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}

	/**
	 * @return the mATERIAL_NAME
	 */
	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	/**
	 * @param mATERIAL_NAME the mATERIAL_NAME to set
	 */
	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	/**
	 * @return the pURCHASE_ORG_CODE
	 */
	public String getPURCHASE_ORG_CODE() {
		return PURCHASE_ORG_CODE;
	}

	/**
	 * @param pURCHASE_ORG_CODE the pURCHASE_ORG_CODE to set
	 */
	public void setPURCHASE_ORG_CODE(String pURCHASE_ORG_CODE) {
		PURCHASE_ORG_CODE = pURCHASE_ORG_CODE;
	}

	/**
	 * @return the pURCHASE_GRP_CODE
	 */
	public String getPURCHASE_GRP_CODE() {
		return PURCHASE_GRP_CODE;
	}

	/**
	 * @param pURCHASE_GRP_CODE the pURCHASE_GRP_CODE to set
	 */
	public void setPURCHASE_GRP_CODE(String pURCHASE_GRP_CODE) {
		PURCHASE_GRP_CODE = pURCHASE_GRP_CODE;
	}

	public String getSUB_CONTRACT() {
		return SUB_CONTRACT;
	}

	public void setSUB_CONTRACT(String sUB_CONTRACT) {
		SUB_CONTRACT = sUB_CONTRACT;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getPART_NAME() {
		return PART_NAME;
	}

	public void setPART_NAME(String pART_NAME) {
		PART_NAME = pART_NAME;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WMS_MST_INFOCODE [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", VENDOR_CODE=" + VENDOR_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", FROM_DATE=" + FROM_DATE
				+ ", CATEGORY=" + CATEGORY + ", UNIT_PRICE=" + UNIT_PRICE + ", CURRENCY=" + CURRENCY + ", INFO_QTY="
				+ INFO_QTY + ", INFO_UNIT=" + INFO_UNIT + ", INFO_NO=" + INFO_NO + ", ACT_FLAG=" + ACT_FLAG
				+ ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE
				+ ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER
				+ ", DELETE_DATE=" + DELETE_DATE + ", VENDOR_NAME=" + VENDOR_NAME + ", MATERIAL_NAME=" + MATERIAL_NAME
				+ ", PURCHASE_ORG_CODE=" + PURCHASE_ORG_CODE + ", PURCHASE_GRP_CODE=" + PURCHASE_GRP_CODE + "]";
	}
}

