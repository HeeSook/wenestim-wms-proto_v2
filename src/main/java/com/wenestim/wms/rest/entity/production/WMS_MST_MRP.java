/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_MST_MRP
{
	public WMS_MST_MRP()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID    ;
	private String  COMPANY_CODE ;
	private String  PLANT_CODE   ;
	private String  MATERIAL_CODE;
	private String  MATERIAL_TYPE;
	private String  MATERIAL_NAME;
	private Float   LEAD_TIME    ;
	private Float   LOT_SIZE     ;
	private Float   SAFETY_STOCK ;
	private Float   PROD_QTY     ;
	private String  PROD_UNIT    ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
	private String  DELETE_USER  ;
	private String  DELETE_DATE  ;

    private String  PART_NO      ;


	public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public Float getLEAD_TIME() {
		return LEAD_TIME;
	}

	public void setLEAD_TIME(Float lEAD_TIME) {
		LEAD_TIME = lEAD_TIME;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public Float getLOT_SIZE() {
		return LOT_SIZE;
	}

	public void setLOT_SIZE(Float lOT_SIZE) {
		LOT_SIZE = lOT_SIZE;
	}

	public Float getSAFETY_STOCK() {
		return SAFETY_STOCK;
	}

	public void setSAFETY_STOCK(Float sAFETY_STOCK) {
		SAFETY_STOCK = sAFETY_STOCK;
	}

	public Float getPROD_QTY() {
		return PROD_QTY;
	}

	public void setPROD_QTY(Float pROD_QTY) {
		PROD_QTY = pROD_QTY;
	}

	public String getPROD_UNIT() {
		return PROD_UNIT;
	}

	public void setPROD_UNIT(String pROD_UNIT) {
		PROD_UNIT = pROD_UNIT;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_MST_MRP [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", PLANT_CODE=" + PLANT_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_TYPE=" + MATERIAL_TYPE
				+ ", MATERIAL_NAME=" + MATERIAL_NAME + ", LEAD_TIME=" + LEAD_TIME + ", LOT_SIZE=" + LOT_SIZE
				+ ", SAFETY_STOCK=" + SAFETY_STOCK + ", PROD_QTY=" + PROD_QTY + ", PROD_UNIT=" + PROD_UNIT
				+ ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER
				+ ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE
				+ ", PART_NO=" + PART_NO + "]";
	}
}
