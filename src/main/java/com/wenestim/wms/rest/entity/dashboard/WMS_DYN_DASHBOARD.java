/**
 *
 */
package com.wenestim.wms.rest.entity.dashboard;

public class WMS_DYN_DASHBOARD
{
	public WMS_DYN_DASHBOARD()
	{
	}

	private Integer TOTAL_PO			;
	private Integer TOTAL_GR			;
	private Integer	TODAY_PO			;
	private Integer TODAY_GR			;
	private Integer REMAIN_GR			;
	private Integer TOMORROW_PO			;
	private Integer RECEIVING_RATIO		;
	private Integer GR_DAYS				;
	
	private Integer G_COUNT				;
	private Integer B_COUNT				;
	private Integer O_COUNT				;
	private Integer G_RATIO				;
	private Integer B_RATIO				;
	private Integer O_RATIO				;
	
	private Integer TOTAL_SO			;
	private Integer TOTAL_PICK			;
	private Integer TOTAL_DELIVERED		;
	private Integer REMAIN_DELIVERED	;
	private Integer DELIVERED_RATIO		;
	private Integer TOMORROW_SO			;
	
	private Integer LOAD_TIME			;
	private Integer PICK_DAYS			;
	private Integer DELIVER_DAYS		;
	public Integer getTOTAL_PO() {
		return TOTAL_PO;
	}
	public void setTOTAL_PO(Integer tOTAL_PO) {
		TOTAL_PO = tOTAL_PO;
	}
	public Integer getTOTAL_GR() {
		return TOTAL_GR;
	}
	public void setTOTAL_GR(Integer tOTAL_GR) {
		TOTAL_GR = tOTAL_GR;
	}
	public Integer getTODAY_PO() {
		return TODAY_PO;
	}
	public void setTODAY_PO(Integer tODAY_PO) {
		TODAY_PO = tODAY_PO;
	}
	public Integer getTODAY_GR() {
		return TODAY_GR;
	}
	public void setTODAY_GR(Integer tODAY_GR) {
		TODAY_GR = tODAY_GR;
	}
	public Integer getREMAIN_GR() {
		return REMAIN_GR;
	}
	public void setREMAIN_GR(Integer rEMAIN_GR) {
		REMAIN_GR = rEMAIN_GR;
	}
	public Integer getTOMORROW_PO() {
		return TOMORROW_PO;
	}
	public void setTOMORROW_PO(Integer tOMORROW_PO) {
		TOMORROW_PO = tOMORROW_PO;
	}
	public Integer getRECEIVING_RATIO() {
		return RECEIVING_RATIO;
	}
	public void setRECEIVING_RATIO(Integer rECEIVING_RATIO) {
		RECEIVING_RATIO = rECEIVING_RATIO;
	}
	public Integer getGR_DAYS() {
		return GR_DAYS;
	}
	public void setGR_DAYS(Integer gR_DAYS) {
		GR_DAYS = gR_DAYS;
	}
	public Integer getG_COUNT() {
		return G_COUNT;
	}
	public void setG_COUNT(Integer g_COUNT) {
		G_COUNT = g_COUNT;
	}
	public Integer getB_COUNT() {
		return B_COUNT;
	}
	public void setB_COUNT(Integer b_COUNT) {
		B_COUNT = b_COUNT;
	}
	public Integer getO_COUNT() {
		return O_COUNT;
	}
	public void setO_COUNT(Integer o_COUNT) {
		O_COUNT = o_COUNT;
	}
	public Integer getG_RATIO() {
		return G_RATIO;
	}
	public void setG_RATIO(Integer g_RATIO) {
		G_RATIO = g_RATIO;
	}
	public Integer getB_RATIO() {
		return B_RATIO;
	}
	public void setB_RATIO(Integer b_RATIO) {
		B_RATIO = b_RATIO;
	}
	public Integer getO_RATIO() {
		return O_RATIO;
	}
	public void setO_RATIO(Integer o_RATIO) {
		O_RATIO = o_RATIO;
	}
	public Integer getTOTAL_SO() {
		return TOTAL_SO;
	}
	public void setTOTAL_SO(Integer tOTAL_SO) {
		TOTAL_SO = tOTAL_SO;
	}
	public Integer getTOTAL_PICK() {
		return TOTAL_PICK;
	}
	public void setTOTAL_PICK(Integer tOTAL_PICK) {
		TOTAL_PICK = tOTAL_PICK;
	}
	public Integer getTOTAL_DELIVERED() {
		return TOTAL_DELIVERED;
	}
	public void setTOTAL_DELIVERED(Integer tOTAL_DELIVERED) {
		TOTAL_DELIVERED = tOTAL_DELIVERED;
	}
	public Integer getREMAIN_DELIVERED() {
		return REMAIN_DELIVERED;
	}
	public void setREMAIN_DELIVERED(Integer rEMAIN_DELIVERED) {
		REMAIN_DELIVERED = rEMAIN_DELIVERED;
	}
	public Integer getDELIVERED_RATIO() {
		return DELIVERED_RATIO;
	}
	public void setDELIVERED_RATIO(Integer dELIVERED_RATIO) {
		DELIVERED_RATIO = dELIVERED_RATIO;
	}
	public Integer getTOMORROW_SO() {
		return TOMORROW_SO;
	}
	public void setTOMORROW_SO(Integer tOMORROW_SO) {
		TOMORROW_SO = tOMORROW_SO;
	}
	public Integer getLOAD_TIME() {
		return LOAD_TIME;
	}
	public void setLOAD_TIME(Integer lOAD_TIME) {
		LOAD_TIME = lOAD_TIME;
	}
	public Integer getPICK_DAYS() {
		return PICK_DAYS;
	}
	public void setPICK_DAYS(Integer pICK_DAYS) {
		PICK_DAYS = pICK_DAYS;
	}
	public Integer getDELIVER_DAYS() {
		return DELIVER_DAYS;
	}
	public void setDELIVER_DAYS(Integer dELIVER_DAYS) {
		DELIVER_DAYS = dELIVER_DAYS;
	}
	@Override
	public String toString() {
		return "WMS_DYN_DASHBOARD [TOTAL_PO=" + TOTAL_PO + ", TOTAL_GR=" + TOTAL_GR + ", TODAY_PO=" + TODAY_PO
				+ ", TODAY_GR=" + TODAY_GR + ", REMAIN_GR=" + REMAIN_GR + ", TOMORROW_PO=" + TOMORROW_PO
				+ ", RECEIVING_RATIO=" + RECEIVING_RATIO + ", GR_DAYS=" + GR_DAYS + ", G_COUNT=" + G_COUNT
				+ ", B_COUNT=" + B_COUNT + ", O_COUNT=" + O_COUNT + ", G_RATIO=" + G_RATIO + ", B_RATIO=" + B_RATIO
				+ ", O_RATIO=" + O_RATIO + ", TOTAL_SO=" + TOTAL_SO + ", TOTAL_PICK=" + TOTAL_PICK
				+ ", TOTAL_DELIVERED=" + TOTAL_DELIVERED + ", REMAIN_DELIVERED=" + REMAIN_DELIVERED
				+ ", DELIVERED_RATIO=" + DELIVERED_RATIO + ", TOMORROW_SO=" + TOMORROW_SO + ", LOAD_TIME=" + LOAD_TIME
				+ ", PICK_DAYS=" + PICK_DAYS + ", DELIVER_DAYS=" + DELIVER_DAYS + "]";
	}
	
	
    
}

