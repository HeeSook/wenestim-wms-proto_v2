/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_MST_ROUTE
{
	public WMS_MST_ROUTE()
	{
	}


	private Integer WSID         ;
	private String  ROWID        ;
	private String  CLIENT_ID    ;
	private String  COMPANY_CODE ;
	private String  PLANT_CODE   ;
	private String  MATERIAL_CODE;
	private String  MATERIAL_NAME;
	private String  MATERIAL_TYPE;
	private Float   PROD_QTY     ;
	private String  PROD_UNIT    ;
	private Integer OP_NO        ;
	private String  OP_NAME      ;
	private String  ACTIVITY1    ;
	private Float   TIME1        ;
	private String  ACTIVITY2    ;
	private Float   TIME2        ;
	private String  ACTIVITY3    ;
	private Float   TIME3        ;
	private String  ACTIVITY4    ;
	private Float   TIME4        ;
	private String  ACTIVITY5    ;
	private Float   TIME5        ;
	private String  COST_CENTER  ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
	private String  DELETE_USER  ;
	private String  DELETE_DATE  ;

    private String  PART_NO      ;

	public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public Integer getOP_NO() {
		return OP_NO;
	}

	public void setOP_NO(Integer oP_NO) {
		OP_NO = oP_NO;
	}

	public String getOP_NAME() {
		return OP_NAME;
	}

	public void setOP_NAME(String oP_NAME) {
		OP_NAME = oP_NAME;
	}

	public String getACTIVITY1() {
		return ACTIVITY1;
	}

	public void setACTIVITY1(String aCTIVITY1) {
		ACTIVITY1 = aCTIVITY1;
	}

	public Float getTIME1() {
		return TIME1;
	}

	public void setTIME1(Float tIME1) {
		TIME1 = tIME1;
	}

	public String getACTIVITY2() {
		return ACTIVITY2;
	}

	public void setACTIVITY2(String aCTIVITY2) {
		ACTIVITY2 = aCTIVITY2;
	}

	public Float getTIME2() {
		return TIME2;
	}

	public void setTIME2(Float tIME2) {
		TIME2 = tIME2;
	}

	public String getACTIVITY3() {
		return ACTIVITY3;
	}

	public void setACTIVITY3(String aCTIVITY3) {
		ACTIVITY3 = aCTIVITY3;
	}

	public Float getTIME3() {
		return TIME3;
	}

	public void setTIME3(Float tIME3) {
		TIME3 = tIME3;
	}

	public String getACTIVITY4() {
		return ACTIVITY4;
	}

	public void setACTIVITY4(String aCTIVITY4) {
		ACTIVITY4 = aCTIVITY4;
	}

	public Float getTIME4() {
		return TIME4;
	}

	public void setTIME4(Float tIME4) {
		TIME4 = tIME4;
	}

	public String getACTIVITY5() {
		return ACTIVITY5;
	}

	public void setACTIVITY5(String aCTIVITY5) {
		ACTIVITY5 = aCTIVITY5;
	}

	public Float getTIME5() {
		return TIME5;
	}

	public void setTIME5(Float tIME5) {
		TIME5 = tIME5;
	}

	public String getCOST_CENTER() {
		return COST_CENTER;
	}

	public void setCOST_CENTER(String cOST_CENTER) {
		COST_CENTER = cOST_CENTER;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getROWID() {
		return ROWID;
	}

	public void setROWID(String rOWID) {
		ROWID = rOWID;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public Float getPROD_QTY() {
		return PROD_QTY;
	}

	public void setPROD_QTY(Float pROD_QTY) {
		PROD_QTY = pROD_QTY;
	}

	public String getPROD_UNIT() {
		return PROD_UNIT;
	}

	public void setPROD_UNIT(String pROD_UNIT) {
		PROD_UNIT = pROD_UNIT;
	}

	@Override
	public String toString() {
		return "WMS_MST_ROUTE [WSID=" + WSID + ", ROWID=" + ROWID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE="
				+ COMPANY_CODE + ", PLANT_CODE=" + PLANT_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_NAME="
				+ MATERIAL_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", PROD_QTY=" + PROD_QTY + ", PROD_UNIT="
				+ PROD_UNIT + ", OP_NO=" + OP_NO + ", OP_NAME=" + OP_NAME + ", ACTIVITY1=" + ACTIVITY1 + ", TIME1="
				+ TIME1 + ", ACTIVITY2=" + ACTIVITY2 + ", TIME2=" + TIME2 + ", ACTIVITY3=" + ACTIVITY3 + ", TIME3="
				+ TIME3 + ", ACTIVITY4=" + ACTIVITY4 + ", TIME4=" + TIME4 + ", ACTIVITY5=" + ACTIVITY5 + ", TIME5="
				+ TIME5 + ", COST_CENTER=" + COST_CENTER + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE="
				+ CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER="
				+ DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", PART_NO=" + PART_NO + "]";
	}
}
