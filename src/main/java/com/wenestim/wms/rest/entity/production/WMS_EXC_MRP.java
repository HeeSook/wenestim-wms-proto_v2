/**
 *
 */
package com.wenestim.wms.rest.entity.production;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;

/**
 * @author Andrew
 *
 */
public class WMS_EXC_MRP extends WMS_MST_BOM
{

	public WMS_EXC_MRP() {
		// TODO Auto-generated constructor stub
	}

	private String  ORDER_NO      ;
	private String  BEFORE_COMP_CODE;
	private String  DEMAND_TYPE   ;
	private int     BOM_LEVEL     ;
	private int     PLAN_SEQ      ;
    private String  PLANT_CODE    ;
    private String  MATERIAL_CODE ;
    private String  MERGE_CODE    ;
	private String  DEMAND_DATE   ;
	private String  DEMAND_ITEM   ;
	private float   DEMAND_QTY    ;
    private String  REQ_DATE      ;
	private String  PLAN_IN_DATE  ;
    private String  PLAN_OUT_DATE ;
	private float   REQ_QTY       ;
	private float   INV_QTY       ;
    private float   AVAIL_QTY     ;
    private float   ASSIGN_QTY    ;
    private float   REMAIN_QTY    ;
    private float   BACKLOG_QTY   ;
    private float   LEAD_TIME     ;
    private float   SAFETY_STOCK  ;
    private float   LOT_SIZE      ;
    private String  SORT_KEY      ;


	public int getBOM_LEVEL() {
		return BOM_LEVEL;
	}

	public void setBOM_LEVEL(int bOM_LEVEL) {
		BOM_LEVEL = bOM_LEVEL;
	}

	public int getPLAN_SEQ() {
		return PLAN_SEQ;
	}

	public void setPLAN_SEQ(int pLAN_SEQ) {
		PLAN_SEQ = pLAN_SEQ;
	}

	public float getINV_QTY() {
		return INV_QTY;
	}

	public void setINV_QTY(float iNV_QTY) {
		INV_QTY = iNV_QTY;
	}

	public float getAVAIL_QTY() {
		return AVAIL_QTY;
	}

	public void setAVAIL_QTY(float aVAIL_QTY) {
		AVAIL_QTY = aVAIL_QTY;
	}

	public float getASSIGN_QTY() {
		return ASSIGN_QTY;
	}

	public void setASSIGN_QTY(float aSSIGN_QTY) {
		ASSIGN_QTY = aSSIGN_QTY;
	}
	public String getREQ_DATE() {
		return REQ_DATE;
	}

	public void setREQ_DATE(String rEQ_DATE) {
		REQ_DATE = rEQ_DATE;
	}

	public float getREQ_QTY() {
		return REQ_QTY;
	}

	public void setREQ_QTY(float rEQ_QTY) {
		REQ_QTY = rEQ_QTY;
	}

	public float getBACKLOG_QTY() {
		return BACKLOG_QTY;
	}

	public void setBACKLOG_QTY(float bACKLOG_QTY) {
		BACKLOG_QTY = bACKLOG_QTY;
	}

	public float getLEAD_TIME() {
		return LEAD_TIME;
	}

	public void setLEAD_TIME(float lEAD_TIME) {
		LEAD_TIME = lEAD_TIME;
	}

	public float getSAFETY_STOCK() {
		return SAFETY_STOCK;
	}

	public void setSAFETY_STOCK(float sAFETY_STOCK) {
		SAFETY_STOCK = sAFETY_STOCK;
	}

	public float getLOT_SIZE() {
		return LOT_SIZE;
	}

	public void setLOT_SIZE(float lOT_SIZE) {
		LOT_SIZE = lOT_SIZE;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getMERGE_CODE() {
		return MERGE_CODE;
	}

	public void setMERGE_CODE(String mERGE_CODE) {
		MERGE_CODE = mERGE_CODE;
	}

	public float getREMAIN_QTY() {
		return REMAIN_QTY;
	}

	public void setREMAIN_QTY(float rEMAIN_QTY) {
		REMAIN_QTY = rEMAIN_QTY;
	}

	public String getDEMAND_TYPE() {
		return DEMAND_TYPE;
	}

	public void setDEMAND_TYPE(String dEMAND_TYPE) {
		DEMAND_TYPE = dEMAND_TYPE;
	}

	public String getBEFORE_COMP_CODE() {
		return BEFORE_COMP_CODE;
	}

	public void setBEFORE_COMP_CODE(String bEFORE_COMP_CODE) {
		BEFORE_COMP_CODE = bEFORE_COMP_CODE;
	}

	public String getPLAN_IN_DATE() {
		return PLAN_IN_DATE;
	}

	public void setPLAN_IN_DATE(String pLAN_IN_DATE) {
		PLAN_IN_DATE = pLAN_IN_DATE;
	}

	public String getPLAN_OUT_DATE() {
		return PLAN_OUT_DATE;
	}

	public void setPLAN_OUT_DATE(String pLAN_OUT_DATE) {
		PLAN_OUT_DATE = pLAN_OUT_DATE;
	}

	public String getDEMAND_DATE() {
		return DEMAND_DATE;
	}

	public void setDEMAND_DATE(String dEMAND_DATE) {
		DEMAND_DATE = dEMAND_DATE;
	}

	public String getDEMAND_ITEM() {
		return DEMAND_ITEM;
	}

	public void setDEMAND_ITEM(String dEMAND_ITEM) {
		DEMAND_ITEM = dEMAND_ITEM;
	}

	public float getDEMAND_QTY() {
		return DEMAND_QTY;
	}

	public void setDEMAND_QTY(float dEMAND_QTY) {
		DEMAND_QTY = dEMAND_QTY;
	}

	public String getORDER_NO() {
		return ORDER_NO;
	}

	public void setORDER_NO(String oRDER_NO) {
		ORDER_NO = oRDER_NO;
	}

	public String getSORT_KEY() {
		return SORT_KEY;
	}

	public void setSORT_KEY(String sORT_KEY) {
		SORT_KEY = sORT_KEY;
	}

	@Override
	public String toString()
	{
		return "WMS_EXC_MRP [ "
				+ " DEMAND_TYPE   = " + DEMAND_TYPE  + ","
				+ " BOM_LEVEL     = " + BOM_LEVEL    + ","
				+ " PLANT_CODE    = " + PLANT_CODE   + ","
				+ " MATERIAL_CODE = " + MATERIAL_CODE+ ","
				+ " MERGE_CODE    = " + MERGE_CODE   + ","
                + " PROD_PLANT_CODE    = " + super.getPROD_PLANT_CODE()     + ","
                + " PROD_MATERIAL_CODE = " + super.getPROD_MATERIAL_CODE()  + ","
                + " PROD_QTY           = " + super.getPROD_QTY()            + ","
                + " COMP_PLANT_CODE    = " + super.getCOMP_PLANT_CODE()     + ","
                + " COMP_MATERIAL_CODE = " + super.getCOMP_MATERIAL_CODE()  + ","
                + " COMP_QTY           = " + super.getCOMP_QTY()            + ","
//                + " BEFORE_DATE   = " + BEFORE_DATE  + ","
                + " REQ_DATE      = " + REQ_DATE     + ","
                + " REQ_QTY       = " + REQ_QTY      + ","
                + " INV_QTY       = " + INV_QTY      + ","
                + " AVAIL_QTY     = " + AVAIL_QTY    + ","
            	+ " ASSIGN_QTY    = " + ASSIGN_QTY   + ","
        		+ " REMAIN_QTY    = " + REMAIN_QTY   + ","
            	+ " BACKLOG_QTY   = " + BACKLOG_QTY  + ","
        		+ " LEAD_TIME     = " + LEAD_TIME    + ","
        		+ " SAFETY_STOCK  = " + SAFETY_STOCK + ","
        		+ " LOT_SIZE      = " + LOT_SIZE     +
            "]";
    }
}
