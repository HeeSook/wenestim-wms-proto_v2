/**
 * 
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_PROMOTION {

	public WMS_MST_PROMOTION() {
		// TODO Auto-generated constructor stub
	}
	
	private Integer WSID;
	private String  CLIENT_ID;
	private String  COMPANY_CODE;
	private Integer PROMO_CODE;
	private String  PROMO_DESC;
	private Float   PROMO_DISC;
	private String  PROMO_NAME;
	private String  DEL_FLAG;
	private String  CREATE_USER;
	private String  CREATE_DATE;
	private String  UPDATE_USER;
	private String  UPDATE_DATE;
	private String  DELETE_USER;
	private String  DELETE_DATE;
	
	/**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}
	
	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public Integer getPROMO_CODE() {
		return PROMO_CODE;
	}

	public void setPROMO_CODE(Integer pROMO_CODE) {
		PROMO_CODE = pROMO_CODE;
	}

	/**
	 * @return the pROMO_DESC
	 */
	public String getPROMO_DESC() {
		return PROMO_DESC;
	}

	/**
	 * @param pROMO_DESC the pROMO_DESC to set
	 */
	public void setPROMO_DESC(String pROMO_DESC) {
		PROMO_DESC = pROMO_DESC;
	}

	/**
	 * @return the pROMO_DISC
	 */
	public Float getPROMO_DISC() {
		return PROMO_DISC;
	}

	/**
	 * @param pROMO_DISC the pROMO_DISC to set
	 */
	public void setPROMO_DISC(Float pROMO_DISC) {
		PROMO_DISC = pROMO_DISC;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getPROMO_NAME() {
		return PROMO_NAME;
	}

	public void setPROMO_NAME(String pROMO_NAME) {
		PROMO_NAME = pROMO_NAME;
	}

	@Override
	public String toString() {
		return "WMS_MST_PROMOTION [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", PROMO_CODE=" + PROMO_CODE + ", PROMO_DESC=" + PROMO_DESC + ", PROMO_DISC=" + PROMO_DISC
				+ ", PROMO_NAME=" + PROMO_NAME + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + "]";
	}

	
	
}
