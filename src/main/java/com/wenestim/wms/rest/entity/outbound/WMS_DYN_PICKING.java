/**
 *
 */
package com.wenestim.wms.rest.entity.outbound;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PICKING
{
	public WMS_DYN_PICKING()
	{
	}

	private Integer WSID           ;
	private String  CLIENT_ID      ;
	private String  COMPANY_CODE   ;
    private String  SO_NO          ;
    private Integer SO_ITEM_NO     ;
    private String  PL_NO          ;
    private String  POST_DATE      ;
    private String  CUST_CODE      ;
    private String  CUST_NAME      ;
    private String  PLANT_CODE     ;
    private String  MATERIAL_CODE  ;
    private String  MATERIAL_NAME  ;
    private String  DELIVERY_DATE  ;
    private Float   ORDER_QTY      ;
    private String  UNIT           ;
    private Float   CONV_QTY       ;
    private String  CONV_UNIT      ;
    private String  BARCODE_NO     ;
    private Float   SCAN_QTY       ;
    private Float   TOTAL_SCAN     ;
	private String  SALES_ORG_CODE ;
    private String  CONFIRM_USER   ;
    private String  CONFIRM_DATE   ;
    private String  STORAGE_LOC    ;
    private String  BIN_LOC        ;
    private String  BIN_LEVEL      ;
    private String  BIN_POSITION   ;
    private String  MOVEMENT_TYPE  ;
    private String  STATUS         ;
    private String  STATUS_DESC    ;
    private String  DO_NO          ;
    private String  HEADER_FLAG    ;
    private String  DEL_FLAG       ;
    private String  CREATE_USER    ;
    private String  CREATE_DATE    ;
    private String  UPDATE_USER    ;
    private String  UPDATE_DATE    ;
    private String  DELETE_USER    ;
    private String  DELETE_DATE    ;
    private Integer DOC_NO		   ;

    private String  SCAN_UNIT      ;
    private Integer ROWID          ;

    private Integer SALES_PRICE    ;
    private Integer ITEM_AMOUNT    ;
    private String  MATERIAL_TYPE  ;
    private String  CURRENCY	   ;
    private String  SGI_EACCOUNT_NO;

    private String  GI_TEMPLATE_CODE;
    private String  GI_TEMPLATE_NAME;
    private String  GI_TEMPLATE_DESC;

    private String  CUST_PO_NO        ;
    private Float   CUST_PO_QTY       ;
    private String  CUST_MATERIAL_CODE;
    private String  CUST_MATERIAL_NAME;

    private String  COMPANY_NAME ;
    private String  ADDRESS1     ;
    private String  ADDRESS2     ;
    private String  EO_NO        ;
    private String  CUST_ADDRESS ;

    private String  LINE_ITEM    ;
    private String  DELIVERY_TIME;

    private String  CUST_PO_ITEM_NO;
    private String  CONTAINER_TYPE ;
    private String  CONTAINER_NO   ;

    private String  CARRIER_NAME   ;
    private String  DCR_NO         ;
    private String  EDI_CONFIRM_NO ;
    private String  CARRIER_CODE   ;
    private String  TRAILER_NO     ;
    private String  TRAILER_COUNTRY_CODE;
    private String  TRAILER_SHIP_STATE  ;
    private String  TRAILER_SHIP_CITY   ;

    private String  PART_NO         ;

    private String  CUST_ADDR1      ;
    private String  CUST_ADDR2      ;

    private String  SUPPLIER_CODE     ;
    private String  SUPPLIER_NAME     ;
    private String  SUPPLIER_COUNTRY  ;
    private String  SUPPLIER_STATE    ;
    private String  SUPPLIER_CITY     ;
    private String  SUPPLIER_STREET1  ;
    private String  SUPPLIER_STREET2  ;
    private String  SUPPLIER_ZIP_CODE ;

    private String  SHIP_TO_CODE      ;
    private String  SHIP_TO_NAME      ;
    private String  SHIP_TO_COUNTRY   ;
    private String  SHIP_TO_STATE     ;
    private String  SHIP_TO_CITY      ;
    private String  SHIP_TO_STREET1   ;
    private String  SHIP_TO_STREET2   ;
    private String  SHIP_TO_ZIP_CODE  ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCLIENT_ID() {
        return CLIENT_ID;
    }

    public void setCLIENT_ID(String cLIENT_ID) {
        CLIENT_ID = cLIENT_ID;
    }

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getSO_NO() {
		return SO_NO;
	}

	public void setSO_NO(String sO_NO) {
		SO_NO = sO_NO;
	}

	public Integer getSO_ITEM_NO() {
		return SO_ITEM_NO;
	}

	public void setSO_ITEM_NO(Integer sO_ITEM_NO) {
		SO_ITEM_NO = sO_ITEM_NO;
	}

	public String getPL_NO() {
		return PL_NO;
	}

	public void setPL_NO(String pL_NO) {
		PL_NO = pL_NO;
	}

	public String getPOST_DATE() {
		return POST_DATE;
	}

	public void setPOST_DATE(String pOST_DATE) {
		POST_DATE = pOST_DATE;
	}

	public String getCUST_CODE() {
		return CUST_CODE;
	}

	public void setCUST_CODE(String cUST_CODE) {
		CUST_CODE = cUST_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getDELIVERY_DATE() {
		return DELIVERY_DATE;
	}

	public void setDELIVERY_DATE(String dELIVERY_DATE) {
		DELIVERY_DATE = dELIVERY_DATE;
	}

	public Float getORDER_QTY() {
		return ORDER_QTY;
	}

	public void setORDER_QTY(Float oRDER_QTY) {
		ORDER_QTY = oRDER_QTY;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public Float getCONV_QTY() {
		return CONV_QTY;
	}

	public void setCONV_QTY(Float cONV_QTY) {
		CONV_QTY = cONV_QTY;
	}

	public String getCONV_UNIT() {
		return CONV_UNIT;
	}

	public void setCONV_UNIT(String cONV_UNIT) {
		CONV_UNIT = cONV_UNIT;
	}

	public String getBARCODE_NO() {
		return BARCODE_NO;
	}

	public void setBARCODE_NO(String bARCODE_NO) {
		BARCODE_NO = bARCODE_NO;
	}

	public Float getSCAN_QTY() {
		return SCAN_QTY;
	}

	public void setSCAN_QTY(Float sCAN_QTY) {
		SCAN_QTY = sCAN_QTY;
	}

	public Float getTOTAL_SCAN() {
		return TOTAL_SCAN;
	}

	public void setTOTAL_SCAN(Float tOTAL_SCAN) {
		TOTAL_SCAN = tOTAL_SCAN;
	}

	public String getSALES_ORG_CODE() {
		return SALES_ORG_CODE;
	}

	public void setSALES_ORG_CODE(String sALES_ORG_CODE) {
		SALES_ORG_CODE = sALES_ORG_CODE;
	}

	public String getCONFIRM_USER() {
		return CONFIRM_USER;
	}

	public void setCONFIRM_USER(String cONFIRM_USER) {
		CONFIRM_USER = cONFIRM_USER;
	}

	public String getCONFIRM_DATE() {
		return CONFIRM_DATE;
	}

	public void setCONFIRM_DATE(String cONFIRM_DATE) {
		CONFIRM_DATE = cONFIRM_DATE;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public String getBIN_LOC() {
		return BIN_LOC;
	}

	public void setBIN_LOC(String bIN_LOC) {
		BIN_LOC = bIN_LOC;
	}

	public String getBIN_LEVEL() {
		return BIN_LEVEL;
	}

	public void setBIN_LEVEL(String bIN_LEVEL) {
		BIN_LEVEL = bIN_LEVEL;
	}

	public String getBIN_POSITION() {
		return BIN_POSITION;
	}

	public void setBIN_POSITION(String bIN_POSITION) {
		BIN_POSITION = bIN_POSITION;
	}

	public String getMOVEMENT_TYPE() {
		return MOVEMENT_TYPE;
	}

	public void setMOVEMENT_TYPE(String mOVEMENT_TYPE) {
		MOVEMENT_TYPE = mOVEMENT_TYPE;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getDO_NO() {
		return DO_NO;
	}

	public void setDO_NO(String dO_NO) {
		DO_NO = dO_NO;
	}

	public String getHEADER_FLAG() {
		return HEADER_FLAG;
	}

	public void setHEADER_FLAG(String hEADER_FLAG) {
		HEADER_FLAG = hEADER_FLAG;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getCUST_NAME() {
		return CUST_NAME;
	}

	public void setCUST_NAME(String cUST_NAME) {
		CUST_NAME = cUST_NAME;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getSCAN_UNIT() {
		return SCAN_UNIT;
	}

	public void setSCAN_UNIT(String sCAN_UNIT) {
		SCAN_UNIT = sCAN_UNIT;
	}

	public String getSTATUS_DESC() {
		return STATUS_DESC;
	}

	public void setSTATUS_DESC(String sTATUS_DESC) {
		STATUS_DESC = sTATUS_DESC;
	}

	public Integer getROWID() {
		return ROWID;
	}

	public void setROWID(Integer rOWID) {
		ROWID = rOWID;
	}

	public Integer getDOC_NO() {
		return DOC_NO;
	}

	public void setDOC_NO(Integer dOC_NO) {
		DOC_NO = dOC_NO;
	}


	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public Integer getSALES_PRICE() {
		return SALES_PRICE;
	}

	public void setSALES_PRICE(Integer sALES_PRICE) {
		SALES_PRICE = sALES_PRICE;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public Integer getITEM_AMOUNT() {
		return ITEM_AMOUNT;
	}

	public void setITEM_AMOUNT(Integer iTEM_AMOUNT) {
		ITEM_AMOUNT = iTEM_AMOUNT;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getSGI_EACCOUNT_NO() {
		return SGI_EACCOUNT_NO;
	}

	public void setSGI_EACCOUNT_NO(String sGI_EACCOUNT_NO) {
		SGI_EACCOUNT_NO = sGI_EACCOUNT_NO;
	}

	public String getGI_TEMPLATE_CODE() {
		return GI_TEMPLATE_CODE;
	}

	public void setGI_TEMPLATE_CODE(String gI_TEMPLATE_CODE) {
		GI_TEMPLATE_CODE = gI_TEMPLATE_CODE;
	}

	public String getGI_TEMPLATE_NAME() {
		return GI_TEMPLATE_NAME;
	}

	public void setGI_TEMPLATE_NAME(String gI_TEMPLATE_NAME) {
		GI_TEMPLATE_NAME = gI_TEMPLATE_NAME;
	}


	/**
	 * @return the cUST_PO_NO
	 */
	public String getCUST_PO_NO() {
		return CUST_PO_NO;
	}

	/**
	 * @param cUST_PO_NO the cUST_PO_NO to set
	 */
	public void setCUST_PO_NO(String cUST_PO_NO) {
		CUST_PO_NO = cUST_PO_NO;
	}

	/**
	 * @return the cUST_MATERIAL_CODE
	 */
	public String getCUST_MATERIAL_CODE() {
		return CUST_MATERIAL_CODE;
	}

	/**
	 * @param cUST_MATERIAL_CODE the cUST_MATERIAL_CODE to set
	 */
	public void setCUST_MATERIAL_CODE(String cUST_MATERIAL_CODE) {
		CUST_MATERIAL_CODE = cUST_MATERIAL_CODE;
	}

	/**
	 * @return the cUST_MATERIAL_NAME
	 */
	public String getCUST_MATERIAL_NAME() {
		return CUST_MATERIAL_NAME;
	}

	/**
	 * @param cUST_MATERIAL_NAME the cUST_MATERIAL_NAME to set
	 */
	public void setCUST_MATERIAL_NAME(String cUST_MATERIAL_NAME) {
		CUST_MATERIAL_NAME = cUST_MATERIAL_NAME;
	}

//	/**
//	 * @return the bARCODE
//	 */
//	public String getBARCODE() {
//		return BARCODE;
//	}
//
//	/**
//	 * @param bARCODE the bARCODE to set
//	 */
//	public void setBARCODE(String bARCODE) {
//		BARCODE = bARCODE;
//	}

	/**
	 * @return the sUPPLIER_CODE
	 */
	public String getSUPPLIER_CODE() {
		return SUPPLIER_CODE;
	}

	/**
	 * @param sUPPLIER_CODE the sUPPLIER_CODE to set
	 */
	public void setSUPPLIER_CODE(String sUPPLIER_CODE) {
		SUPPLIER_CODE = sUPPLIER_CODE;
	}

	/**
	 * @return the cOMPANY_NAME
	 */
	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}

	/**
	 * @param cOMPANY_NAME the cOMPANY_NAME to set
	 */
	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}

	/**
	 * @return the aDDRESS1
	 */
	public String getADDRESS1() {
		return ADDRESS1;
	}

	/**
	 * @param aDDRESS1 the aDDRESS1 to set
	 */
	public void setADDRESS1(String aDDRESS1) {
		ADDRESS1 = aDDRESS1;
	}

	/**
	 * @return the aDDRESS2
	 */
	public String getADDRESS2() {
		return ADDRESS2;
	}

	/**
	 * @param aDDRESS2 the aDDRESS2 to set
	 */
	public void setADDRESS2(String aDDRESS2) {
		ADDRESS2 = aDDRESS2;
	}

	/**
	 * @return the eO_NO
	 */
	public String getEO_NO() {
		return EO_NO;
	}

	/**
	 * @param eO_NO the eO_NO to set
	 */
	public void setEO_NO(String eO_NO) {
		EO_NO = eO_NO;
	}

	/**
	 * @return the cUST_ADDRESS
	 */
	public String getCUST_ADDRESS() {
		return CUST_ADDRESS;
	}

	/**
	 * @param cUST_ADDRESS the cUST_ADDRESS to set
	 */
	public void setCUST_ADDRESS(String cUST_ADDRESS) {
		CUST_ADDRESS = cUST_ADDRESS;
	}

	/**
	 * @return the lINE_ITEM
	 */
	public String getLINE_ITEM() {
		return LINE_ITEM;
	}

	/**
	 * @param lINE_ITEM the lINE_ITEM to set
	 */
	public void setLINE_ITEM(String lINE_ITEM) {
		LINE_ITEM = lINE_ITEM;
	}

	/**
	 * @return the dELIVERY_TIME
	 */
	public String getDELIVERY_TIME() {
		return DELIVERY_TIME;
	}

	/**
	 * @param dELIVERY_TIME the dELIVERY_TIME to set
	 */
	public void setDELIVERY_TIME(String dELIVERY_TIME) {
		DELIVERY_TIME = dELIVERY_TIME;
	}

	/**
	 * @return the cUST_PO_ITEM_NO
	 */
	public String getCUST_PO_ITEM_NO() {
		return CUST_PO_ITEM_NO;
	}

	/**
	 * @param cUST_PO_ITEM_NO the cUST_PO_ITEM_NO to set
	 */
	public void setCUST_PO_ITEM_NO(String cUST_PO_ITEM_NO) {
		CUST_PO_ITEM_NO = cUST_PO_ITEM_NO;
	}

	/**
	 * @return the cONTAINER_TYPE
	 */
	public String getCONTAINER_TYPE() {
		return CONTAINER_TYPE;
	}

	/**
	 * @param cONTAINER_TYPE the cONTAINER_TYPE to set
	 */
	public void setCONTAINER_TYPE(String cONTAINER_TYPE) {
		CONTAINER_TYPE = cONTAINER_TYPE;
	}

	/**
	 * @return the cONTAINER_NO
	 */
	public String getCONTAINER_NO() {
		return CONTAINER_NO;
	}

	/**
	 * @param cONTAINER_NO the cONTAINER_NO to set
	 */
	public void setCONTAINER_NO(String cONTAINER_NO) {
		CONTAINER_NO = cONTAINER_NO;
	}

	/**
	 * @return the sHIP_TO_NAME
	 */
	public String getSHIP_TO_NAME() {
		return SHIP_TO_NAME;
	}

	/**
	 * @param sHIP_TO_NAME the sHIP_TO_NAME to set
	 */
	public void setSHIP_TO_NAME(String sHIP_TO_NAME) {
		SHIP_TO_NAME = sHIP_TO_NAME;
	}

	/**
	 * @return the cARRIER_NAME
	 */
	public String getCARRIER_NAME() {
		return CARRIER_NAME;
	}

	/**
	 * @param cARRIER_NAME the cARRIER_NAME to set
	 */
	public void setCARRIER_NAME(String cARRIER_NAME) {
		CARRIER_NAME = cARRIER_NAME;
	}

	/**
	 * @return the dCR_NO
	 */
	public String getDCR_NO() {
		return DCR_NO;
	}

	/**
	 * @param dCR_NO the dCR_NO to set
	 */
	public void setDCR_NO(String dCR_NO) {
		DCR_NO = dCR_NO;
	}

	/**
	 * @return the eDI_CONFIRM_NO
	 */
	public String getEDI_CONFIRM_NO() {
		return EDI_CONFIRM_NO;
	}

	/**
	 * @param eDI_CONFIRM_NO the eDI_CONFIRM_NO to set
	 */
	public void setEDI_CONFIRM_NO(String eDI_CONFIRM_NO) {
		EDI_CONFIRM_NO = eDI_CONFIRM_NO;
	}

	/**
	 * @return the sHIP_TO_CODE
	 */
	public String getSHIP_TO_CODE() {
		return SHIP_TO_CODE;
	}

	/**
	 * @param sHIP_TO_CODE the sHIP_TO_CODE to set
	 */
	public void setSHIP_TO_CODE(String sHIP_TO_CODE) {
		SHIP_TO_CODE = sHIP_TO_CODE;
	}

	/**
	 * @return the cARRIER_CODE
	 */
	public String getCARRIER_CODE() {
		return CARRIER_CODE;
	}

	/**
	 * @param cARRIER_CODE the cARRIER_CODE to set
	 */
	public void setCARRIER_CODE(String cARRIER_CODE) {
		CARRIER_CODE = cARRIER_CODE;
	}

	/**
	 * @return the tRAILER_NO
	 */
	public String getTRAILER_NO() {
		return TRAILER_NO;
	}

	/**
	 * @param tRAILER_NO the tRAILER_NO to set
	 */
	public void setTRAILER_NO(String tRAILER_NO) {
		TRAILER_NO = tRAILER_NO;
	}

	/**
	 * @return the tRAILER_COUNTRY_CODE
	 */
	public String getTRAILER_COUNTRY_CODE() {
		return TRAILER_COUNTRY_CODE;
	}

	/**
	 * @param tRAILER_COUNTRY_CODE the tRAILER_COUNTRY_CODE to set
	 */
	public void setTRAILER_COUNTRY_CODE(String tRAILER_COUNTRY_CODE) {
		TRAILER_COUNTRY_CODE = tRAILER_COUNTRY_CODE;
	}

	/**
	 * @return the tRAILER_SHIP_STATE
	 */
	public String getTRAILER_SHIP_STATE() {
		return TRAILER_SHIP_STATE;
	}

	/**
	 * @param tRAILER_SHIP_STATE the tRAILER_SHIP_STATE to set
	 */
	public void setTRAILER_SHIP_STATE(String tRAILER_SHIP_STATE) {
		TRAILER_SHIP_STATE = tRAILER_SHIP_STATE;
	}

	/**
	 * @return the tRAILER_SHIP_CITY
	 */
	public String getTRAILER_SHIP_CITY() {
		return TRAILER_SHIP_CITY;
	}

	/**
	 * @param tRAILER_SHIP_CITY the tRAILER_SHIP_CITY to set
	 */
	public void setTRAILER_SHIP_CITY(String tRAILER_SHIP_CITY) {
		TRAILER_SHIP_CITY = tRAILER_SHIP_CITY;
	}

	public Float getCUST_PO_QTY() {
		return CUST_PO_QTY;
	}

	public void setCUST_PO_QTY(Float cUST_PO_QTY) {
		CUST_PO_QTY = cUST_PO_QTY;
	}

	public String getGI_TEMPLATE_DESC() {
		return GI_TEMPLATE_DESC;
	}

	public void setGI_TEMPLATE_DESC(String gI_TEMPLATE_DESC) {
		GI_TEMPLATE_DESC = gI_TEMPLATE_DESC;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getCUST_ADDR1() {
		return CUST_ADDR1;
	}

	public void setCUST_ADDR1(String cUST_ADDR1) {
		CUST_ADDR1 = cUST_ADDR1;
	}

	public String getCUST_ADDR2() {
		return CUST_ADDR2;
	}

	public void setCUST_ADDR2(String cUST_ADDR2) {
		CUST_ADDR2 = cUST_ADDR2;
	}

	public String getSUPPLIER_NAME() {
		return SUPPLIER_NAME;
	}

	public void setSUPPLIER_NAME(String sUPPLIER_NAME) {
		SUPPLIER_NAME = sUPPLIER_NAME;
	}

	public String getSUPPLIER_COUNTRY() {
		return SUPPLIER_COUNTRY;
	}

	public void setSUPPLIER_COUNTRY(String sUPPLIER_COUNTRY) {
		SUPPLIER_COUNTRY = sUPPLIER_COUNTRY;
	}

	public String getSUPPLIER_STATE() {
		return SUPPLIER_STATE;
	}

	public void setSUPPLIER_STATE(String sUPPLIER_STATE) {
		SUPPLIER_STATE = sUPPLIER_STATE;
	}

	public String getSUPPLIER_CITY() {
		return SUPPLIER_CITY;
	}

	public void setSUPPLIER_CITY(String sUPPLIER_CITY) {
		SUPPLIER_CITY = sUPPLIER_CITY;
	}

	public String getSUPPLIER_STREET1() {
		return SUPPLIER_STREET1;
	}

	public void setSUPPLIER_STREET1(String sUPPLIER_STREET1) {
		SUPPLIER_STREET1 = sUPPLIER_STREET1;
	}

	public String getSUPPLIER_STREET2() {
		return SUPPLIER_STREET2;
	}

	public void setSUPPLIER_STREET2(String sUPPLIER_STREET2) {
		SUPPLIER_STREET2 = sUPPLIER_STREET2;
	}

	public String getSUPPLIER_ZIP_CODE() {
		return SUPPLIER_ZIP_CODE;
	}

	public void setSUPPLIER_ZIP_CODE(String sUPPLIER_ZIP_CODE) {
		SUPPLIER_ZIP_CODE = sUPPLIER_ZIP_CODE;
	}

	public String getSHIP_TO_COUNTRY() {
		return SHIP_TO_COUNTRY;
	}

	public void setSHIP_TO_COUNTRY(String sHIP_TO_COUNTRY) {
		SHIP_TO_COUNTRY = sHIP_TO_COUNTRY;
	}

	public String getSHIP_TO_STATE() {
		return SHIP_TO_STATE;
	}

	public void setSHIP_TO_STATE(String sHIP_TO_STATE) {
		SHIP_TO_STATE = sHIP_TO_STATE;
	}

	public String getSHIP_TO_CITY() {
		return SHIP_TO_CITY;
	}

	public void setSHIP_TO_CITY(String sHIP_TO_CITY) {
		SHIP_TO_CITY = sHIP_TO_CITY;
	}

	public String getSHIP_TO_STREET1() {
		return SHIP_TO_STREET1;
	}

	public void setSHIP_TO_STREET1(String sHIP_TO_STREET1) {
		SHIP_TO_STREET1 = sHIP_TO_STREET1;
	}

	public String getSHIP_TO_STREET2() {
		return SHIP_TO_STREET2;
	}

	public void setSHIP_TO_STREET2(String sHIP_TO_STREET2) {
		SHIP_TO_STREET2 = sHIP_TO_STREET2;
	}

	public String getSHIP_TO_ZIP_CODE() {
		return SHIP_TO_ZIP_CODE;
	}

	public void setSHIP_TO_ZIP_CODE(String sHIP_TO_ZIP_CODE) {
		SHIP_TO_ZIP_CODE = sHIP_TO_ZIP_CODE;
	}

	@Override
	public String toString() {
		return "WMS_DYN_PICKING [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", SO_NO=" + SO_NO + ", SO_ITEM_NO=" + SO_ITEM_NO + ", PL_NO=" + PL_NO + ", POST_DATE=" + POST_DATE
				+ ", CUST_CODE=" + CUST_CODE + ", CUST_NAME=" + CUST_NAME + ", PLANT_CODE=" + PLANT_CODE
				+ ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_NAME=" + MATERIAL_NAME + ", DELIVERY_DATE="
				+ DELIVERY_DATE + ", ORDER_QTY=" + ORDER_QTY + ", UNIT=" + UNIT + ", CONV_QTY=" + CONV_QTY
				+ ", CONV_UNIT=" + CONV_UNIT + ", BARCODE_NO=" + BARCODE_NO + ", SCAN_QTY=" + SCAN_QTY + ", TOTAL_SCAN="
				+ TOTAL_SCAN + ", SALES_ORG_CODE=" + SALES_ORG_CODE + ", CONFIRM_USER=" + CONFIRM_USER
				+ ", CONFIRM_DATE=" + CONFIRM_DATE + ", STORAGE_LOC=" + STORAGE_LOC + ", BIN_LOC=" + BIN_LOC
				+ ", BIN_LEVEL=" + BIN_LEVEL + ", BIN_POSITION=" + BIN_POSITION + ", MOVEMENT_TYPE=" + MOVEMENT_TYPE
				+ ", STATUS=" + STATUS + ", STATUS_DESC=" + STATUS_DESC + ", DO_NO=" + DO_NO + ", HEADER_FLAG="
				+ HEADER_FLAG + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE="
				+ CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER="
				+ DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", DOC_NO=" + DOC_NO + ", SCAN_UNIT=" + SCAN_UNIT
				+ ", ROWID=" + ROWID + ", SALES_PRICE=" + SALES_PRICE + ", ITEM_AMOUNT=" + ITEM_AMOUNT
				+ ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", CURRENCY=" + CURRENCY + ", SGI_EACCOUNT_NO=" + SGI_EACCOUNT_NO
				+ ", GI_TEMPLATE_CODE=" + GI_TEMPLATE_CODE + ", GI_TEMPLATE_NAME=" + GI_TEMPLATE_NAME
				+ ", GI_TEMPLATE_DESC=" + GI_TEMPLATE_DESC + ", CUST_PO_NO=" + CUST_PO_NO + ", CUST_PO_QTY="
				+ CUST_PO_QTY + ", CUST_MATERIAL_CODE=" + CUST_MATERIAL_CODE + ", CUST_MATERIAL_NAME="
				+ CUST_MATERIAL_NAME + ", COMPANY_NAME=" + COMPANY_NAME + ", ADDRESS1=" + ADDRESS1 + ", ADDRESS2="
				+ ADDRESS2 + ", EO_NO=" + EO_NO + ", CUST_ADDRESS=" + CUST_ADDRESS + ", LINE_ITEM=" + LINE_ITEM
				+ ", DELIVERY_TIME=" + DELIVERY_TIME + ", CUST_PO_ITEM_NO=" + CUST_PO_ITEM_NO + ", CONTAINER_TYPE="
				+ CONTAINER_TYPE + ", CONTAINER_NO=" + CONTAINER_NO + ", CARRIER_NAME=" + CARRIER_NAME + ", DCR_NO="
				+ DCR_NO + ", EDI_CONFIRM_NO=" + EDI_CONFIRM_NO + ", CARRIER_CODE=" + CARRIER_CODE + ", TRAILER_NO="
				+ TRAILER_NO + ", TRAILER_COUNTRY_CODE=" + TRAILER_COUNTRY_CODE + ", TRAILER_SHIP_STATE="
				+ TRAILER_SHIP_STATE + ", TRAILER_SHIP_CITY=" + TRAILER_SHIP_CITY + ", PART_NO=" + PART_NO
				+ ", CUST_ADDR1=" + CUST_ADDR1 + ", CUST_ADDR2=" + CUST_ADDR2 + ", SUPPLIER_CODE=" + SUPPLIER_CODE
				+ ", SUPPLIER_NAME=" + SUPPLIER_NAME + ", SUPPLIER_COUNTRY=" + SUPPLIER_COUNTRY + ", SUPPLIER_STATE="
				+ SUPPLIER_STATE + ", SUPPLIER_CITY=" + SUPPLIER_CITY + ", SUPPLIER_STREET1=" + SUPPLIER_STREET1
				+ ", SUPPLIER_STREET2=" + SUPPLIER_STREET2 + ", SUPPLIER_ZIP_CODE=" + SUPPLIER_ZIP_CODE
				+ ", SHIP_TO_CODE=" + SHIP_TO_CODE + ", SHIP_TO_NAME=" + SHIP_TO_NAME + ", SHIP_TO_COUNTRY="
				+ SHIP_TO_COUNTRY + ", SHIP_TO_STATE=" + SHIP_TO_STATE + ", SHIP_TO_CITY=" + SHIP_TO_CITY
				+ ", SHIP_TO_STREET1=" + SHIP_TO_STREET1 + ", SHIP_TO_STREET2=" + SHIP_TO_STREET2
				+ ", SHIP_TO_ZIP_CODE=" + SHIP_TO_ZIP_CODE + "]";
	}
}
