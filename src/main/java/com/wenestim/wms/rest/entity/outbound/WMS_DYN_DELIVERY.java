/**
 *
 */
package com.wenestim.wms.rest.entity.outbound;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_DELIVERY
{
	public WMS_DYN_DELIVERY()
	{
	}

	private Integer WSID           ;
	private String  CLIENT_ID      ;
	private String  COMPANY_CODE   ;
    private String  DO_NO          ;
    private String  CONFIRM_USER   ;
    private String  CONFIRM_DATE   ;
    private String  EDI_CONFIRM_NO ;
    private String  EDI_DATE       ;
    private String  EDI_MESSAGE    ;
    private String  DCR_DATE       ;
    private String  DCR_NO         ;
    private String  DEPARTURE_DATE ;
    private String  DELIVERED_DATE ;
    private String  DEL_FLAG       ;
    private String  CREATE_USER    ;
    private String  CREATE_DATE    ;
    private String  UPDATE_USER    ;
    private String  UPDATE_DATE    ;
    private String  DELETE_USER    ;
    private String  DELETE_DATE    ;

    private String  MATERIAL_NAME  ;
    private String  CUST_NAME      ;
    private Double  DELIVERY_TIME  ;
    private String  STATUS         ;


    private String  SHIP_TO_CODE        ;
    private String  CARRIER_CODE        ;
    private String  TRAILER_NO          ;
    private String  TRAILER_COUNTRY_CODE;
    private String  TRAILER_SHIP_STATE  ;
    private String  TRAILER_SHIP_CITY   ;
    private String  GI_TEMPLATE_CODE    ;
    private String  GI_TEMPLATE_DESC    ;

    private String  PART_NO             ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCLIENT_ID() {
        return CLIENT_ID;
    }

    public void setCLIENT_ID(String cLIENT_ID) {
        CLIENT_ID = cLIENT_ID;
    }

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getDO_NO() {
		return DO_NO;
	}

	public void setDO_NO(String dO_NO) {
		DO_NO = dO_NO;
	}

	public String getCONFIRM_USER() {
		return CONFIRM_USER;
	}

	public void setCONFIRM_USER(String cONFIRM_USER) {
		CONFIRM_USER = cONFIRM_USER;
	}

	public String getCONFIRM_DATE() {
		return CONFIRM_DATE;
	}

	public void setCONFIRM_DATE(String cONFIRM_DATE) {
		CONFIRM_DATE = cONFIRM_DATE;
	}

	public String getEDI_DATE() {
		return EDI_DATE;
	}

	public void setEDI_DATE(String eDI_DATE) {
		EDI_DATE = eDI_DATE;
	}

	public String getEDI_MESSAGE() {
		return EDI_MESSAGE;
	}

	public void setEDI_MESSAGE(String eDI_MESSAGE) {
		EDI_MESSAGE = eDI_MESSAGE;
	}

	public String getDCR_DATE() {
		return DCR_DATE;
	}

	public void setDCR_DATE(String dCR_DATE) {
		DCR_DATE = dCR_DATE;
	}

	public String getDCR_NO() {
		return DCR_NO;
	}

	public void setDCR_NO(String dCR_NO) {
		DCR_NO = dCR_NO;
	}

	public String getDEPARTURE_DATE() {
		return DEPARTURE_DATE;
	}

	public void setDEPARTURE_DATE(String dEPARTURE_DATE) {
		DEPARTURE_DATE = dEPARTURE_DATE;
	}

	public String getDELIVERED_DATE() {
		return DELIVERED_DATE;
	}

	public void setDELIVERED_DATE(String dELIVERED_DATE) {
		DELIVERED_DATE = dELIVERED_DATE;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getCUST_NAME() {
		return CUST_NAME;
	}

	public void setCUST_NAME(String cUST_NAME) {
		CUST_NAME = cUST_NAME;
	}

	public Double getDELIVERY_TIME() {
		return DELIVERY_TIME;
	}

	public void setDELIVERY_TIME(Double dELIVERY_TIME) {
		DELIVERY_TIME = dELIVERY_TIME;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getSHIP_TO_CODE() {
		return SHIP_TO_CODE;
	}

	public void setSHIP_TO_CODE(String sHIP_TO_CODE) {
		SHIP_TO_CODE = sHIP_TO_CODE;
	}

	public String getCARRIER_CODE() {
		return CARRIER_CODE;
	}

	public void setCARRIER_CODE(String cARRIER_CODE) {
		CARRIER_CODE = cARRIER_CODE;
	}

	public String getTRAILER_NO() {
		return TRAILER_NO;
	}

	public void setTRAILER_NO(String tRAILER_NO) {
		TRAILER_NO = tRAILER_NO;
	}

	public String getTRAILER_COUNTRY_CODE() {
		return TRAILER_COUNTRY_CODE;
	}

	public void setTRAILER_COUNTRY_CODE(String tRAILER_COUNTRY_CODE) {
		TRAILER_COUNTRY_CODE = tRAILER_COUNTRY_CODE;
	}

	public String getTRAILER_SHIP_STATE() {
		return TRAILER_SHIP_STATE;
	}

	public void setTRAILER_SHIP_STATE(String tRAILER_SHIP_STATE) {
		TRAILER_SHIP_STATE = tRAILER_SHIP_STATE;
	}

	public String getTRAILER_SHIP_CITY() {
		return TRAILER_SHIP_CITY;
	}

	public void setTRAILER_SHIP_CITY(String tRAILER_SHIP_CITY) {
		TRAILER_SHIP_CITY = tRAILER_SHIP_CITY;
	}

	public String getGI_TEMPLATE_CODE() {
		return GI_TEMPLATE_CODE;
	}

	public void setGI_TEMPLATE_CODE(String gI_TEMPLATE_CODE) {
		GI_TEMPLATE_CODE = gI_TEMPLATE_CODE;
	}

	public String getEDI_CONFIRM_NO() {
		return EDI_CONFIRM_NO;
	}

	public void setEDI_CONFIRM_NO(String eDI_CONFIRM_NO) {
		EDI_CONFIRM_NO = eDI_CONFIRM_NO;
	}

	public String getGI_TEMPLATE_DESC() {
		return GI_TEMPLATE_DESC;
	}

	public void setGI_TEMPLATE_DESC(String gI_TEMPLATE_DESC) {
		GI_TEMPLATE_DESC = gI_TEMPLATE_DESC;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_DELIVERY [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", DO_NO=" + DO_NO + ", CONFIRM_USER=" + CONFIRM_USER + ", CONFIRM_DATE=" + CONFIRM_DATE
				+ ", EDI_CONFIRM_NO=" + EDI_CONFIRM_NO + ", EDI_DATE=" + EDI_DATE + ", EDI_MESSAGE=" + EDI_MESSAGE
				+ ", DCR_DATE=" + DCR_DATE + ", DCR_NO=" + DCR_NO + ", DEPARTURE_DATE=" + DEPARTURE_DATE
				+ ", DELIVERED_DATE=" + DELIVERED_DATE + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", MATERIAL_NAME=" + MATERIAL_NAME
				+ ", CUST_NAME=" + CUST_NAME + ", DELIVERY_TIME=" + DELIVERY_TIME + ", STATUS=" + STATUS
				+ ", SHIP_TO_CODE=" + SHIP_TO_CODE + ", CARRIER_CODE=" + CARRIER_CODE + ", TRAILER_NO=" + TRAILER_NO
				+ ", TRAILER_COUNTRY_CODE=" + TRAILER_COUNTRY_CODE + ", TRAILER_SHIP_STATE=" + TRAILER_SHIP_STATE
				+ ", TRAILER_SHIP_CITY=" + TRAILER_SHIP_CITY + ", GI_TEMPLATE_CODE=" + GI_TEMPLATE_CODE
				+ ", GI_TEMPLATE_DESC=" + GI_TEMPLATE_DESC + ", PART_NO=" + PART_NO + "]";
	}
}
