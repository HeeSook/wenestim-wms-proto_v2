/**
 *
 */
package com.wenestim.wms.rest.entity.inventory;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_LEDGER
{
	public WMS_DYN_LEDGER()
	{
	}

	private String COMPANY_CODE  ;
    private String PLANT_NAME    ;
    private String MATERIAL_CODE ;
    private String MATERIAL_NAME ;
    private String MATERIAL_TYPE ;
    private Double BOH           ;
    private Double PURCHASING_GR ;
    private Double PRODUCTION_GR ;
    private Double OTHER_GR      ;
    private Double PRODUCTION_GI ;
    private Double SALES_DELIVERY;
    private Double OTHER_GI      ;
    private Double GR_TOTAL      ;
    private Double GI_TOTAL      ;
    private Double EOH           ;

    private String  PART_NO      ;

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getPLANT_NAME() {
		return PLANT_NAME;
	}

	public void setPLANT_NAME(String pLANT_NAME) {
		PLANT_NAME = pLANT_NAME;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public Double getBOH() {
		return BOH;
	}

	public void setBOH(Double bOH) {
		BOH = bOH;
	}

	public Double getPURCHASING_GR() {
		return PURCHASING_GR;
	}

	public void setPURCHASING_GR(Double pURCHASING_GR) {
		PURCHASING_GR = pURCHASING_GR;
	}

	public Double getPRODUCTION_GR() {
		return PRODUCTION_GR;
	}

	public void setPRODUCTION_GR(Double pRODUCTION_GR) {
		PRODUCTION_GR = pRODUCTION_GR;
	}

	public Double getOTHER_GR() {
		return OTHER_GR;
	}

	public void setOTHER_GR(Double oTHER_GR) {
		OTHER_GR = oTHER_GR;
	}

	public Double getPRODUCTION_GI() {
		return PRODUCTION_GI;
	}

	public void setPRODUCTION_GI(Double pRODUCTION_GI) {
		PRODUCTION_GI = pRODUCTION_GI;
	}

	public Double getSALES_DELIVERY() {
		return SALES_DELIVERY;
	}

	public void setSALES_DELIVERY(Double sALES_DELIVERY) {
		SALES_DELIVERY = sALES_DELIVERY;
	}

	public Double getOTHER_GI() {
		return OTHER_GI;
	}

	public void setOTHER_GI(Double oTHER_GI) {
		OTHER_GI = oTHER_GI;
	}

	public Double getGR_TOTAL() {
		return GR_TOTAL;
	}

	public void setGR_TOTAL(Double gR_TOTAL) {
		GR_TOTAL = gR_TOTAL;
	}

	public Double getGI_TOTAL() {
		return GI_TOTAL;
	}

	public void setGI_TOTAL(Double gI_TOTAL) {
		GI_TOTAL = gI_TOTAL;
	}

	public Double getEOH() {
		return EOH;
	}

	public void setEOH(Double eOH) {
		EOH = eOH;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_LEDGER [COMPANY_CODE=" + COMPANY_CODE + ", PLANT_NAME=" + PLANT_NAME + ", MATERIAL_CODE="
				+ MATERIAL_CODE + ", MATERIAL_NAME=" + MATERIAL_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", BOH="
				+ BOH + ", PURCHASING_GR=" + PURCHASING_GR + ", PRODUCTION_GR=" + PRODUCTION_GR + ", OTHER_GR="
				+ OTHER_GR + ", PRODUCTION_GI=" + PRODUCTION_GI + ", SALES_DELIVERY=" + SALES_DELIVERY + ", OTHER_GI="
				+ OTHER_GI + ", GR_TOTAL=" + GR_TOTAL + ", GI_TOTAL=" + GI_TOTAL + ", EOH=" + EOH + ", PART_NO="
				+ PART_NO + "]";
	}
}

