/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_BACKLOG
{
    public WMS_DYN_BACKLOG()
    {
    }

    private int    WSID              ;
    private int    ROWID             ;
    private String CLIENT_ID         ;
    private String COMPANY_CODE      ;
    private int    DOC_NO            ;
    private String PLANT_CODE        ;
    private String MATERIAL_CODE     ;
    private String UNIT              ;
    private float  COMP_QTY          ;
    private float  DEMAND_QTY        ;
    private String STORAGE_LOC       ;
    private float  INV_QTY           ;
    private float  AVAIL_QTY         ;
    private float  ASSIGN_QTY        ;
    private float  BACKLOG_QTY       ;
    private String PROD_PLANT_CODE   ;
    private String PROD_MATERIAL_CODE;
    private float  PROD_ACT_QTY      ;
    private String CREATE_USER       ;
    private String CREATE_DATE       ;
    private String UPDATE_USER       ;
    private String UPDATE_DATE       ;

    private String MATERIAL_NAME     ;
    private String MATERIAL_TYPE     ;
    private String STORAGE_LOC_NAME  ;
    private float  CURRENT_INV_QTY   ;
    private String STATUS            ;
    private String PROD_MATERIAL_NAME;

    private String  PART_NO          ;
    private String  PROD_PART_NO     ;

	public int getWSID() {
		return WSID;
	}

	public void setWSID(int wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public int getDOC_NO() {
		return DOC_NO;
	}

	public void setDOC_NO(int dOC_NO) {
		DOC_NO = dOC_NO;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public float getCOMP_QTY() {
		return COMP_QTY;
	}

	public void setCOMP_QTY(float cOMP_QTY) {
		COMP_QTY = cOMP_QTY;
	}

	public float getDEMAND_QTY() {
		return DEMAND_QTY;
	}

	public void setDEMAND_QTY(float dEMAND_QTY) {
		DEMAND_QTY = dEMAND_QTY;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public float getINV_QTY() {
		return INV_QTY;
	}

	public void setINV_QTY(float iNV_QTY) {
		INV_QTY = iNV_QTY;
	}

	public float getAVAIL_QTY() {
		return AVAIL_QTY;
	}

	public void setAVAIL_QTY(float aVAIL_QTY) {
		AVAIL_QTY = aVAIL_QTY;
	}

	public float getASSIGN_QTY() {
		return ASSIGN_QTY;
	}

	public void setASSIGN_QTY(float aSSIGN_QTY) {
		ASSIGN_QTY = aSSIGN_QTY;
	}

	public float getBACKLOG_QTY() {
		return BACKLOG_QTY;
	}

	public void setBACKLOG_QTY(float bACKLOG_QTY) {
		BACKLOG_QTY = bACKLOG_QTY;
	}

	public String getPROD_PLANT_CODE() {
		return PROD_PLANT_CODE;
	}

	public void setPROD_PLANT_CODE(String pROD_PLANT_CODE) {
		PROD_PLANT_CODE = pROD_PLANT_CODE;
	}

	public String getPROD_MATERIAL_CODE() {
		return PROD_MATERIAL_CODE;
	}

	public void setPROD_MATERIAL_CODE(String pROD_MATERIAL_CODE) {
		PROD_MATERIAL_CODE = pROD_MATERIAL_CODE;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public float getPROD_ACT_QTY() {
		return PROD_ACT_QTY;
	}

	public void setPROD_ACT_QTY(float pROD_ACT_QTY) {
		PROD_ACT_QTY = pROD_ACT_QTY;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public String getSTORAGE_LOC_NAME() {
		return STORAGE_LOC_NAME;
	}

	public void setSTORAGE_LOC_NAME(String sTORAGE_LOC_NAME) {
		STORAGE_LOC_NAME = sTORAGE_LOC_NAME;
	}

	public float getCURRENT_INV_QTY() {
		return CURRENT_INV_QTY;
	}

	public void setCURRENT_INV_QTY(float cURRENT_INV_QTY) {
		CURRENT_INV_QTY = cURRENT_INV_QTY;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getPROD_MATERIAL_NAME() {
		return PROD_MATERIAL_NAME;
	}

	public void setPROD_MATERIAL_NAME(String pROD_MATERIAL_NAME) {
		PROD_MATERIAL_NAME = pROD_MATERIAL_NAME;
	}

	public int getROWID() {
		return ROWID;
	}

	public void setROWID(int rOWID) {
		ROWID = rOWID;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getPROD_PART_NO() {
		return PROD_PART_NO;
	}

	public void setPROD_PART_NO(String pROD_PART_NO) {
		PROD_PART_NO = pROD_PART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_BACKLOG [WSID=" + WSID + ", ROWID=" + ROWID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE="
				+ COMPANY_CODE + ", DOC_NO=" + DOC_NO + ", PLANT_CODE=" + PLANT_CODE + ", MATERIAL_CODE="
				+ MATERIAL_CODE + ", UNIT=" + UNIT + ", COMP_QTY=" + COMP_QTY + ", DEMAND_QTY=" + DEMAND_QTY
				+ ", STORAGE_LOC=" + STORAGE_LOC + ", INV_QTY=" + INV_QTY + ", AVAIL_QTY=" + AVAIL_QTY + ", ASSIGN_QTY="
				+ ASSIGN_QTY + ", BACKLOG_QTY=" + BACKLOG_QTY + ", PROD_PLANT_CODE=" + PROD_PLANT_CODE
				+ ", PROD_MATERIAL_CODE=" + PROD_MATERIAL_CODE + ", PROD_ACT_QTY=" + PROD_ACT_QTY + ", CREATE_USER="
				+ CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE="
				+ UPDATE_DATE + ", MATERIAL_NAME=" + MATERIAL_NAME + ", MATERIAL_TYPE=" + MATERIAL_TYPE
				+ ", STORAGE_LOC_NAME=" + STORAGE_LOC_NAME + ", CURRENT_INV_QTY=" + CURRENT_INV_QTY + ", STATUS="
				+ STATUS + ", PROD_MATERIAL_NAME=" + PROD_MATERIAL_NAME + ", PART_NO=" + PART_NO + ", PROD_PART_NO="
				+ PROD_PART_NO + "]";
	}
}

