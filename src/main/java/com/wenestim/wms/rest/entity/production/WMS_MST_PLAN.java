/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_MST_PLAN
{
	public WMS_MST_PLAN()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID    ;
	private String  COMPANY_CODE ;
    private String  PLAN_VER     ;
    private String  PLAN_DESC    ;
    private String  PLAN_CURRENT ;
    private String  PLAN_START   ;
    private String  PLAN_END     ;
    private Integer CURRENT_VER  ;
    private String  ACT_FLAG     ;
    private String  RUN_FLAG     ;
    private String  DEL_FLAG     ;
    private String  MES_IF_FLAG  ;
    private String  MRP_FLAG     ;
    private String  CONFIRM_FLAG ;
    private String  MRP_STATUS   ;
    private String  MRP_PLAN_TYPE;

    private String  CREATE_USER  ;
    private String  CREATE_DATE  ;
    private String  UPDATE_USER  ;
    private String  UPDATE_DATE  ;
    private String  DELETE_USER  ;
    private String  DELETE_DATE  ;
    private String  CONFIRM_USER ;
    private String  CONFIRM_DATE ;


	public Integer getWSID() {
		return WSID;
	}


	public void setWSID(Integer wSID) {
		WSID = wSID;
	}


	public String getCLIENT_ID() {
		return CLIENT_ID;
	}


	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}


	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}


	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}


	public String getPLAN_VER() {
		return PLAN_VER;
	}


	public void setPLAN_VER(String pLAN_VER) {
		PLAN_VER = pLAN_VER;
	}


	public String getPLAN_DESC() {
		return PLAN_DESC;
	}


	public void setPLAN_DESC(String pLAN_DESC) {
		PLAN_DESC = pLAN_DESC;
	}


	public String getPLAN_CURRENT() {
		return PLAN_CURRENT;
	}


	public void setPLAN_CURRENT(String pLAN_CURRENT) {
		PLAN_CURRENT = pLAN_CURRENT;
	}


	public String getPLAN_START() {
		return PLAN_START;
	}


	public void setPLAN_START(String pLAN_START) {
		PLAN_START = pLAN_START;
	}


	public String getPLAN_END() {
		return PLAN_END;
	}


	public void setPLAN_END(String pLAN_END) {
		PLAN_END = pLAN_END;
	}


	public Integer getCURRENT_VER() {
		return CURRENT_VER;
	}


	public void setCURRENT_VER(Integer cURRENT_VER) {
		CURRENT_VER = cURRENT_VER;
	}


	public String getACT_FLAG() {
		return ACT_FLAG;
	}


	public void setACT_FLAG(String aCT_FLAG) {
		ACT_FLAG = aCT_FLAG;
	}


	public String getRUN_FLAG() {
		return RUN_FLAG;
	}


	public void setRUN_FLAG(String rUN_FLAG) {
		RUN_FLAG = rUN_FLAG;
	}


	public String getDEL_FLAG() {
		return DEL_FLAG;
	}


	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}


	public String getMES_IF_FLAG() {
		return MES_IF_FLAG;
	}


	public void setMES_IF_FLAG(String mES_IF_FLAG) {
		MES_IF_FLAG = mES_IF_FLAG;
	}


	public String getCREATE_USER() {
		return CREATE_USER;
	}


	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}


	public String getCREATE_DATE() {
		return CREATE_DATE;
	}


	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}


	public String getUPDATE_USER() {
		return UPDATE_USER;
	}


	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}


	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}


	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}


	public String getDELETE_USER() {
		return DELETE_USER;
	}


	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}


	public String getDELETE_DATE() {
		return DELETE_DATE;
	}


	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}


	public String getCONFIRM_USER() {
		return CONFIRM_USER;
	}


	public void setCONFIRM_USER(String cONFIRM_USER) {
		CONFIRM_USER = cONFIRM_USER;
	}


	public String getCONFIRM_DATE() {
		return CONFIRM_DATE;
	}


	public void setCONFIRM_DATE(String cONFIRM_DATE) {
		CONFIRM_DATE = cONFIRM_DATE;
	}

	public String getMRP_FLAG() {
		return MRP_FLAG;
	}


	public void setMRP_FLAG(String mRP_FLAG) {
		MRP_FLAG = mRP_FLAG;
	}


	public String getCONFIRM_FLAG() {
		return CONFIRM_FLAG;
	}


	public void setCONFIRM_FLAG(String cONFIRM_FLAG) {
		CONFIRM_FLAG = cONFIRM_FLAG;
	}

	public String getMRP_STATUS() {
		return MRP_STATUS;
	}

	public void setMRP_STATUS(String mRP_STATUS) {
		MRP_STATUS = mRP_STATUS;
	}

	public String getMRP_PLAN_TYPE() {
		return MRP_PLAN_TYPE;
	}


	public void setMRP_PLAN_TYPE(String mRP_PLAN_TYPE) {
		MRP_PLAN_TYPE = mRP_PLAN_TYPE;
	}

	@Override
	public String toString()
	{
		return "WMS_MST_PLAN [ "
                + " WSID         = " + WSID          + ","
				+ " CLIENT_ID    = " + CLIENT_ID     + ","
                + " COMPANY_CODE = " + COMPANY_CODE  + ","
                + " PLAN_VER     = " + PLAN_VER      + ","
                + " PLAN_DESC    = " + PLAN_DESC     + ","
                + " PLAN_CURRENT = " + PLAN_CURRENT  + ","
                + " PLAN_START   = " + PLAN_START    + ","
                + " PLAN_END     = " + PLAN_END      + ","
                + " CURRENT_VER  = " + CURRENT_VER   + ","
                + " ACT_FLAG     = " + ACT_FLAG      + ","
                + " RUN_FLAG     = " + RUN_FLAG      + ","
                + " DEL_FLAG     = " + DEL_FLAG      + ","
                + " MES_IF_FLAG  = " + MES_IF_FLAG   + ","
                + " CREATE_USER  = " + CREATE_USER   + ","
                + " CREATE_DATE  = " + CREATE_DATE   + ","
                + " UPDATE_USER  = " + UPDATE_USER   + ","
                + " UPDATE_DATE  = " + UPDATE_DATE   + ","
                + " DELETE_USER  = " + DELETE_USER   + ","
                + " DELETE_DATE  = " + DELETE_DATE   + ","
                + " CONFIRM_USER = " + CONFIRM_USER  + ","
                + " CONFIRM_DATE = " + CONFIRM_DATE  +
            "]";
    }
}
