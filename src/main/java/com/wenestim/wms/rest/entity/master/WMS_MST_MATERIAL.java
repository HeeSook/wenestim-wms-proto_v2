/**
 *
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_MATERIAL
{
	public WMS_MST_MATERIAL()
	{
	}

	private Integer WSID             ;
	private String CLIENT_ID         ;
	private String COMPANY_CODE      ;
	private String MATERIAL_CODE     ;
	private String R_MATERIAL_CODE   ;
	private String MATERIAL_TYPE     ;
	private String MATERIAL_NAME     ;
	private String PLANT_CODE        ;
	private String MODEL_NO          ;
	private String PART_NO           ;
	private String SUB_CONTRACT      ;
	private String OLD_MATERIAL_CODE ;
	private Float  CONV_QTY          ;
	private String CONV_UNIT         ;
	private String STORAGE_LOC       ;
	private String DEL_FLAG          ;
	private String CREATE_USER       ;
	private String CREATE_DATE       ;
	private String UPDATE_USER       ;
	private String UPDATE_DATE       ;
    private String DELETE_USER       ;
	private String DELETE_DATE       ;

	// For DropDownList
	private String MATERIAL_DESC     ;

	// For Type
	private String TYPE 			 ;

	private Float  SALES_PRICE		 ;
	private String CURRENCY 		 ;
	private String SO_FLAG	         ;

	private Float  COST		         ;


	private String  VEN_CUST_MAT_CODE1 ;
	private String  VEN_CUST_MAT_CODE2 ;
	private String  VEN_CUST_MAT_CODE3 ;

	private String  VEN_CUST_MAT_NAME1 ;
	private String  VEN_CUST_MAT_NAME2 ;
	private String  VEN_CUST_MAT_NAME3 ;

	private String  DIE_NO             ;

	private String  PROD_TYPE          ;
	private String  HS_CODE            ;
    private String  CONTAINER_TYPE     ;
    private String  CONTAINER_NO       ;

	public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getR_MATERIAL_CODE() {
		return R_MATERIAL_CODE;
	}

	public void setR_MATERIAL_CODE(String r_MATERIAL_CODE) {
		R_MATERIAL_CODE = r_MATERIAL_CODE;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getMODEL_NO() {
		return MODEL_NO;
	}

	public void setMODEL_NO(String mODEL_NO) {
		MODEL_NO = mODEL_NO;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	public String getSUB_CONTRACT() {
		return SUB_CONTRACT;
	}

	public void setSUB_CONTRACT(String sUB_CONTRACT) {
		SUB_CONTRACT = sUB_CONTRACT;
	}

	public String getOLD_MATERIAL_CODE() {
		return OLD_MATERIAL_CODE;
	}

	public void setOLD_MATERIAL_CODE(String oLD_MATERIAL_CODE) {
		OLD_MATERIAL_CODE = oLD_MATERIAL_CODE;
	}

	public Float getCONV_QTY() {
		return CONV_QTY;
	}

	public void setCONV_QTY(Float cONV_QTY) {
		CONV_QTY = cONV_QTY;
	}

	public String getCONV_UNIT() {
		return CONV_UNIT;
	}

	public void setCONV_UNIT(String cONV_UNIT) {
		CONV_UNIT = cONV_UNIT;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getMATERIAL_DESC() {
		return MATERIAL_DESC;
	}

	public void setMATERIAL_DESC(String mATERIAL_DESC) {
		MATERIAL_DESC = mATERIAL_DESC;
	}

	public String getTYPE() {
		return TYPE;
	}

	public void setTYPE(String tYPE) {
		TYPE = tYPE;
	}

	public Float getSALES_PRICE() {
		return SALES_PRICE;
	}

	public void setSALES_PRICE(Float sALES_PRICE) {
		SALES_PRICE = sALES_PRICE;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public String getSO_FLAG() {
		return SO_FLAG;
	}

	public void setSO_FLAG(String sO_FLAG) {
		SO_FLAG = sO_FLAG;
	}

	public Float getCOST() {
		return COST;
	}

	public void setCOST(Float cOST) {
		COST = cOST;
	}

	public String getVEN_CUST_MAT_CODE1() {
		return VEN_CUST_MAT_CODE1;
	}

	public void setVEN_CUST_MAT_CODE1(String vEN_CUST_MAT_CODE1) {
		VEN_CUST_MAT_CODE1 = vEN_CUST_MAT_CODE1;
	}

	public String getVEN_CUST_MAT_CODE2() {
		return VEN_CUST_MAT_CODE2;
	}

	public void setVEN_CUST_MAT_CODE2(String vEN_CUST_MAT_CODE2) {
		VEN_CUST_MAT_CODE2 = vEN_CUST_MAT_CODE2;
	}

	public String getVEN_CUST_MAT_CODE3() {
		return VEN_CUST_MAT_CODE3;
	}

	public void setVEN_CUST_MAT_CODE3(String vEN_CUST_MAT_CODE3) {
		VEN_CUST_MAT_CODE3 = vEN_CUST_MAT_CODE3;
	}

	public String getVEN_CUST_MAT_NAME1() {
		return VEN_CUST_MAT_NAME1;
	}

	public void setVEN_CUST_MAT_NAME1(String vEN_CUST_MAT_NAME1) {
		VEN_CUST_MAT_NAME1 = vEN_CUST_MAT_NAME1;
	}

	public String getVEN_CUST_MAT_NAME2() {
		return VEN_CUST_MAT_NAME2;
	}

	public void setVEN_CUST_MAT_NAME2(String vEN_CUST_MAT_NAME2) {
		VEN_CUST_MAT_NAME2 = vEN_CUST_MAT_NAME2;
	}

	public String getVEN_CUST_MAT_NAME3() {
		return VEN_CUST_MAT_NAME3;
	}

	public void setVEN_CUST_MAT_NAME3(String vEN_CUST_MAT_NAME3) {
		VEN_CUST_MAT_NAME3 = vEN_CUST_MAT_NAME3;
	}

	public String getDIE_NO() {
		return DIE_NO;
	}

	public void setDIE_NO(String dIE_NO) {
		DIE_NO = dIE_NO;
	}


	public String getPROD_TYPE() {
		return PROD_TYPE;
	}

	public void setPROD_TYPE(String pROD_TYPE) {
		PROD_TYPE = pROD_TYPE;
	}

	public String getHS_CODE() {
		return HS_CODE;
	}

	public void setHS_CODE(String hS_CODE) {
		HS_CODE = hS_CODE;
	}

	public String getCONTAINER_TYPE() {
		return CONTAINER_TYPE;
	}

	public void setCONTAINER_TYPE(String cONTAINER_TYPE) {
		CONTAINER_TYPE = cONTAINER_TYPE;
	}

	public String getCONTAINER_NO() {
		return CONTAINER_NO;
	}

	public void setCONTAINER_NO(String cONTAINER_NO) {
		CONTAINER_NO = cONTAINER_NO;
	}


	@Override
	public String toString()
	{
		return "WMS_MST_MATERIAL [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", MATERIAL_CODE=" + MATERIAL_CODE + ", R_MATERIAL_CODE=" + R_MATERIAL_CODE + ", MATERIAL_TYPE="
				+ MATERIAL_TYPE + ", MATERIAL_NAME=" + MATERIAL_NAME + ", PLANT_CODE=" + PLANT_CODE + ", MODEL_NO="
				+ MODEL_NO + ", PART_NO=" + PART_NO + ", SUB_CONTRACT=" + SUB_CONTRACT + ", OLD_MATERIAL_CODE="
				+ OLD_MATERIAL_CODE + ", CONV_QTY=" + CONV_QTY + ", CONV_UNIT=" + CONV_UNIT + ", STORAGE_LOC="
				+ STORAGE_LOC + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE="
				+ CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER="
				+ DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", MATERIAL_DESC=" + MATERIAL_DESC + ", TYPE=" + TYPE
				+ ", SALES_PRICE=" + SALES_PRICE + ", CURRENCY=" + CURRENCY + ", SO_FLAG=" + SO_FLAG + ", COST=" + COST
				+ ", VEN_CUST_MAT_CODE1=" + VEN_CUST_MAT_CODE1 + ", VEN_CUST_MAT_CODE2=" + VEN_CUST_MAT_CODE2
				+ ", VEN_CUST_MAT_CODE3=" + VEN_CUST_MAT_CODE3 + ", VEN_CUST_MAT_NAME1=" + VEN_CUST_MAT_NAME1
				+ ", VEN_CUST_MAT_NAME2=" + VEN_CUST_MAT_NAME2 + ", VEN_CUST_MAT_NAME3=" + VEN_CUST_MAT_NAME3
				+ ", DIE_NO=" + DIE_NO + "]";
	}
}
