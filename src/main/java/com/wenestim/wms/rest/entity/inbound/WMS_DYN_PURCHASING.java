/**
 *
 */
package com.wenestim.wms.rest.entity.inbound;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PURCHASING
{
	public WMS_DYN_PURCHASING()
	{
	}

	private Integer WSID              ;
	private String  CLIENT_ID		  ;
	private String  COMPANY_CODE      ;
    private String  PO_NO             ;
    private Integer PO_ITEM_NO        ;
    private String  PO_TYPE           ;
    private String  PO_DATE           ;
    private String  VENDOR_CODE       ;
    private String  MATERIAL_CODE     ;
    private String  MATERIAL_NAME 	  ;
    private String  DELIVERY_DATE     ;
    private Double  PO_QTY            ;
    private String  PO_UNIT           ;
    private String  CONV_UNIT	      ;
    private Double  INFO_QTY	      ;
    private String  INFO_UNIT		  ;
    private Double  PER_QTY           ;
    private Double  ITEM_QTY		  ;
    private Double  UNIT_PRICE		  ;
    private String  INCOTERMS         ;
    private String  PAYMENT_TERM      ;
    private String  CURRENCY          ;
    private Double  ITEM_AMOUNT       ;
    private String  PURCHASE_ORG_CODE ;
    private String  PURCHASE_GRP_CODE ;
    private String  HEADER_FLAG       ;
    private String  DEL_FLAG          ;
    private String  CREATE_USER       ;
    private String  CREATE_DATE       ;
    private String  UPDATE_USER       ;
    private String  UPDATE_DATE       ;
    private String  DELETE_USER       ;
    private String  DELETE_DATE       ;

    private String  GR_FLAG			  ;
    private String  PLANT_NAME        ;
    private String  MAT_TYPE_NAME     ;
    private String  PO_TYPE_NAME      ;
    private String  INCOTERMS_NAME    ;
    private String  PAYMENT_TERM_NAME ;
    private String  PURCHASE_ORG_NAME ;
    private String  PURCHASE_GRP_NAME ;

    private String  VENDOR_NAME       ;

    private String  PROMO_NAME		  ;
    private String  PROMO_DESC		  ;
    private Double  PROMO_DISC		  ;

    private String  MST_UNIT		  ;
    private Double  MST_QTY		      ;
    private String  STATUS		      ;
    private String  SUB_CONTRACT      ;
    private String  CHANGE_DATE       ;

    private String  PLANNED_ORDER_NO  ;
    private String  INPUT_TYPE        ;
    private String  PART_NO           ;


    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

    public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getPO_NO() {
		return PO_NO;
	}

	public void setPO_NO(String pO_NO) {
		PO_NO = pO_NO;
	}

	public Integer getPO_ITEM_NO() {
		return PO_ITEM_NO;
	}

	public void setPO_ITEM_NO(Integer pO_ITEM_NO) {
		PO_ITEM_NO = pO_ITEM_NO;
	}

	public String getPO_TYPE() {
		return PO_TYPE;
	}

	public void setPO_TYPE(String pO_TYPE) {
		PO_TYPE = pO_TYPE;
	}

	public String getPO_DATE() {
		return PO_DATE;
	}

	public void setPO_DATE(String pO_DATE) {
		PO_DATE = pO_DATE;
	}

	public String getVENDOR_CODE() {
		return VENDOR_CODE;
	}

	public void setVENDOR_CODE(String vENDOR_CODE) {
		VENDOR_CODE = vENDOR_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getDELIVERY_DATE() {
		return DELIVERY_DATE;
	}

	public void setDELIVERY_DATE(String dELIVERY_DATE) {
		DELIVERY_DATE = dELIVERY_DATE;
	}

	public Double getPO_QTY() {
		return PO_QTY;
	}

	public void setPO_QTY(Double pO_QTY) {
		PO_QTY = pO_QTY;
	}

	public String getPO_UNIT() {
		return PO_UNIT;
	}

	public void setPO_UNIT(String pO_UNIT) {
		PO_UNIT = pO_UNIT;
	}

	public Double getPER_QTY() {
		return PER_QTY;
	}

	public void setPER_QTY(Double pER_QTY) {
		PER_QTY = pER_QTY;
	}

	public String getINCOTERMS() {
		return INCOTERMS;
	}

	public void setINCOTERMS(String iNCOTERMS) {
		INCOTERMS = iNCOTERMS;
	}

	public String getPAYMENT_TERM() {
		return PAYMENT_TERM;
	}

	public void setPAYMENT_TERM(String pAYMENT_TERM) {
		PAYMENT_TERM = pAYMENT_TERM;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public Double getITEM_AMOUNT() {
		return ITEM_AMOUNT;
	}

	public void setITEM_AMOUNT(Double iTEM_AMOUNT) {
		ITEM_AMOUNT = iTEM_AMOUNT;
	}

	public String getPURCHASE_ORG_CODE() {
		return PURCHASE_ORG_CODE;
	}

	public void setPURCHASE_ORG_CODE(String pURCHASE_ORG_CODE) {
		PURCHASE_ORG_CODE = pURCHASE_ORG_CODE;
	}

	public String getPURCHASE_GRP_CODE() {
		return PURCHASE_GRP_CODE;
	}

	public void setPURCHASE_GRP_CODE(String pURCHASE_GRP_CODE) {
		PURCHASE_GRP_CODE = pURCHASE_GRP_CODE;
	}

	public String getHEADER_FLAG() {
		return HEADER_FLAG;
	}

	public void setHEADER_FLAG(String hEADER_FLAG) {
		HEADER_FLAG = hEADER_FLAG;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}


	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getPLANT_NAME() {
		return PLANT_NAME;
	}

	public void setPLANT_NAME(String pLANT_NAME) {
		PLANT_NAME = pLANT_NAME;
	}

	public String getMAT_TYPE_NAME() {
		return MAT_TYPE_NAME;
	}

	public void setMAT_TYPE_NAME(String mAT_TYPE_NAME) {
		MAT_TYPE_NAME = mAT_TYPE_NAME;
	}

	public String getPO_TYPE_NAME() {
		return PO_TYPE_NAME;
	}

	public void setPO_TYPE_NAME(String pO_TYPE_NAME) {
		PO_TYPE_NAME = pO_TYPE_NAME;
	}

	public String getINCOTERMS_NAME() {
		return INCOTERMS_NAME;
	}

	public void setINCOTERMS_NAME(String iNCOTERMS_NAME) {
		INCOTERMS_NAME = iNCOTERMS_NAME;
	}

	public String getPAYMENT_TERM_NAME() {
		return PAYMENT_TERM_NAME;
	}

	public void setPAYMENT_TERM_NAME(String pAYMENT_TERM_NAME) {
		PAYMENT_TERM_NAME = pAYMENT_TERM_NAME;
	}

	public String getPURCHASE_ORG_NAME() {
		return PURCHASE_ORG_NAME;
	}

	public void setPURCHASE_ORG_NAME(String pURCHASE_ORG_NAME) {
		PURCHASE_ORG_NAME = pURCHASE_ORG_NAME;
	}

	public String getPURCHASE_GRP_NAME() {
		return PURCHASE_GRP_NAME;
	}

	public void setPURCHASE_GRP_NAME(String pURCHASE_GRP_NAME) {
		PURCHASE_GRP_NAME = pURCHASE_GRP_NAME;
	}

	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}

	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}
	public String getCONV_UNIT() {
		return CONV_UNIT;
	}

	public void setCONV_UNIT(String cONV_UNIT) {
		CONV_UNIT = cONV_UNIT;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getGR_FLAG() {
		return GR_FLAG;
	}

	public void setGR_FLAG(String gR_FLAG) {
		GR_FLAG = gR_FLAG;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public Double getITEM_QTY() {
		return ITEM_QTY;
	}

	public void setITEM_QTY(Double iTEM_QTY) {
		ITEM_QTY = iTEM_QTY;
	}

	public Double getINFO_QTY() {
		return INFO_QTY;
	}

	public void setINFO_QTY(Double iNFO_QTY) {
		INFO_QTY = iNFO_QTY;
	}

	public String getINFO_UNIT() {
		return INFO_UNIT;
	}

	public void setINFO_UNIT(String iNFO_UNIT) {
		INFO_UNIT = iNFO_UNIT;
	}

	public Double getUNIT_PRICE() {
		return UNIT_PRICE;
	}

	public void setUNIT_PRICE(Double uNIT_PRICE) {
		UNIT_PRICE = uNIT_PRICE;
	}

	public String getPROMO_NAME() {
		return PROMO_NAME;
	}

	public void setPROMO_NAME(String pROMO_NAME) {
		PROMO_NAME = pROMO_NAME;
	}

	public String getPROMO_DESC() {
		return PROMO_DESC;
	}

	public void setPROMO_DESC(String pROMO_DESC) {
		PROMO_DESC = pROMO_DESC;
	}

	public Double getPROMO_DISC() {
		return PROMO_DISC;
	}

	public void setPROMO_DISC(Double pROMO_DISC) {
		PROMO_DISC = pROMO_DISC;
	}

	public String getMST_UNIT() {
		return MST_UNIT;
	}

	public void setMST_UNIT(String mST_UNIT) {
		MST_UNIT = mST_UNIT;
	}

	public Double getMST_QTY() {
		return MST_QTY;
	}

	public void setMST_QTY(Double mST_QTY) {
		MST_QTY = mST_QTY;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}


	public String getSUB_CONTRACT() {
		return SUB_CONTRACT;
	}

	public void setSUB_CONTRACT(String sUB_CONTRACT) {
		SUB_CONTRACT = sUB_CONTRACT;
	}

	public String getCHANGE_DATE() {
		return CHANGE_DATE;
	}

	public void setCHANGE_DATE(String cHANGE_DATE) {
		CHANGE_DATE = cHANGE_DATE;
	}

	public String getINPUT_TYPE() {
		return INPUT_TYPE;
	}


	public String getPLANNED_ORDER_NO() {
		return PLANNED_ORDER_NO;
	}

	public void setPLANNED_ORDER_NO(String pLANNED_ORDER_NO) {
		PLANNED_ORDER_NO = pLANNED_ORDER_NO;
	}

	public void setINPUT_TYPE(String iNPUT_TYPE) {
		INPUT_TYPE = iNPUT_TYPE;
	}


	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_PURCHASING [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", PO_NO=" + PO_NO + ", PO_ITEM_NO=" + PO_ITEM_NO + ", PO_TYPE=" + PO_TYPE + ", PO_DATE=" + PO_DATE
				+ ", VENDOR_CODE=" + VENDOR_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_NAME="
				+ MATERIAL_NAME + ", DELIVERY_DATE=" + DELIVERY_DATE + ", PO_QTY=" + PO_QTY + ", PO_UNIT=" + PO_UNIT
				+ ", CONV_UNIT=" + CONV_UNIT + ", INFO_QTY=" + INFO_QTY + ", INFO_UNIT=" + INFO_UNIT + ", PER_QTY="
				+ PER_QTY + ", ITEM_QTY=" + ITEM_QTY + ", UNIT_PRICE=" + UNIT_PRICE + ", INCOTERMS=" + INCOTERMS
				+ ", PAYMENT_TERM=" + PAYMENT_TERM + ", CURRENCY=" + CURRENCY + ", ITEM_AMOUNT=" + ITEM_AMOUNT
				+ ", PURCHASE_ORG_CODE=" + PURCHASE_ORG_CODE + ", PURCHASE_GRP_CODE=" + PURCHASE_GRP_CODE
				+ ", HEADER_FLAG=" + HEADER_FLAG + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", GR_FLAG=" + GR_FLAG
				+ ", PLANT_NAME=" + PLANT_NAME + ", MAT_TYPE_NAME=" + MAT_TYPE_NAME + ", PO_TYPE_NAME=" + PO_TYPE_NAME
				+ ", INCOTERMS_NAME=" + INCOTERMS_NAME + ", PAYMENT_TERM_NAME=" + PAYMENT_TERM_NAME
				+ ", PURCHASE_ORG_NAME=" + PURCHASE_ORG_NAME + ", PURCHASE_GRP_NAME=" + PURCHASE_GRP_NAME
				+ ", VENDOR_NAME=" + VENDOR_NAME + ", PROMO_NAME=" + PROMO_NAME + ", PROMO_DESC=" + PROMO_DESC + ", MST_UNIT=" + MST_UNIT + ", MST_QTY=" + MST_QTY
				+ ", STATUS=" + STATUS + ", PROMO_DISC=" + PROMO_DISC + "]";
	}
}


