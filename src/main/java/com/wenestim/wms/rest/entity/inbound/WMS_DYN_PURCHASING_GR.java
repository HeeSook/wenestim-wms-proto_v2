/**
 *
 */
package com.wenestim.wms.rest.entity.inbound;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PURCHASING_GR
{
	public WMS_DYN_PURCHASING_GR()
	{
	}

	private int     WSID          ;
	private String  CLIENT_ID     ;
	private String  COMPANY_CODE  ;
    private int     DOC_NO        ;
    private String  PO_NO         ;
    private int     PO_ITEM_NO    ;
    private String  BARCODE_NO    ;
    private String  VENDOR_CODE   ;
    private String  MATERIAL_CODE ;
    private String  MATERIAL_NAME ;
    private String  VENDOR_LOT_NO ;
    private String  LOT_NO ;
    private String  GR_DATE       ;
    private float   GR_QTY        ;
    private String  GR_UNIT       ;
    private float   PER_QTY       ;
    private String  STORAGE_LOC   ;
    private String  BIN_LOC       ;
    private String  BIN_LEVEL     ;
    private String  BIN_POSITION  ;
    private String  MOVEMENT_TYPE ;
    private String  STATUS        ;
    private String  GR_STATUS     ;
    private String  DEL_FLAG      ;
    private String  CREATE_USER   ;
    private String  CREATE_DATE   ;
    private String  UPDATE_USER   ;
    private String  UPDATE_DATE   ;
    private String  DELETE_USER   ;
    private String  DELETE_DATE   ;

    private String  PO_TYPE           ;
    private String  PO_DATE           ;
    private String  DELIVERY_DATE     ;
    private float   PO_QTY            ;
    private String  PO_UNIT           ;
    private String  INCOTERMS         ;
    private String  PAYMENT_TERM      ;
    private String  CURRENCY          ;
    private float   ITEM_AMOUNT       ;
    private String  PURCHASE_ORG_CODE ;
    private String  PURCHASE_GRP_CODE ;
    private String  HEADER_FLAG       ;
    private String  PLANT_CODE        ;
    private String  POST_DATE         ;
    private String  ROWID             ;
    private float   MOVE_QTY          ;
    private String  EDIT_FLAG         ;
    private int     RANK_NO           ;

    private String  MATERIAL_TYPE     ;	/*20180618 Add By HHS*/
    private float   MST_QTY           ;
    private String  MST_UNIT          ;
    private float   PO_ITEM_QTY       ;
    private float   GR_ITEM_QTY       ;
    private float   ITEM_QTY          ;
    private float   CONV_QTY          ;
    private String  MST_STORAGE_LOC   ;
    private String  PART_NO           ;

    public int getWSID() {
		return WSID;
	}

	public void setWSID(int wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public int getDOC_NO() {
		return DOC_NO;
	}

	public void setDOC_NO(int dOC_NO) {
		DOC_NO = dOC_NO;
	}

	public String getPO_NO() {
		return PO_NO;
	}

	public void setPO_NO(String pO_NO) {
		PO_NO = pO_NO;
	}

	public int getPO_ITEM_NO() {
		return PO_ITEM_NO;
	}

	public void setPO_ITEM_NO(int pO_ITEM_NO) {
		PO_ITEM_NO = pO_ITEM_NO;
	}

	public String getBARCODE_NO() {
		return BARCODE_NO;
	}

	public void setBARCODE_NO(String bARCODE_NO) {
		BARCODE_NO = bARCODE_NO;
	}

	public String getVENDOR_CODE() {
		return VENDOR_CODE;
	}

	public void setVENDOR_CODE(String vENDOR_CODE) {
		VENDOR_CODE = vENDOR_CODE;
	}

	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getVENDOR_LOT_NO() {
		return VENDOR_LOT_NO;
	}

	public void setVENDOR_LOT_NO(String vENDOR_LOT_NO) {
		VENDOR_LOT_NO = vENDOR_LOT_NO;
	}

	public String getLOT_NO() {
		return LOT_NO;
	}

	public void setLOT_NO(String lOT_NO) {
		LOT_NO = lOT_NO;
	}

	public String getGR_DATE() {
		return GR_DATE;
	}

	public void setGR_DATE(String gR_DATE) {
		GR_DATE = gR_DATE;
	}

	public float getGR_QTY() {
		return GR_QTY;
	}

	public void setGR_QTY(float gR_QTY) {
		GR_QTY = gR_QTY;
	}

	public String getGR_UNIT() {
		return GR_UNIT;
	}

	public void setGR_UNIT(String gR_UNIT) {
		GR_UNIT = gR_UNIT;
	}

	public float getPER_QTY() {
		return PER_QTY;
	}

	public void setPER_QTY(float pER_QTY) {
		PER_QTY = pER_QTY;
	}

	public String getSTORAGE_LOC() {
		return STORAGE_LOC;
	}

	public void setSTORAGE_LOC(String sTORAGE_LOC) {
		STORAGE_LOC = sTORAGE_LOC;
	}

	public String getBIN_LOC() {
		return BIN_LOC;
	}

	public void setBIN_LOC(String bIN_LOC) {
		BIN_LOC = bIN_LOC;
	}

	public String getBIN_LEVEL() {
		return BIN_LEVEL;
	}

	public void setBIN_LEVEL(String bIN_LEVEL) {
		BIN_LEVEL = bIN_LEVEL;
	}

	public String getBIN_POSITION() {
		return BIN_POSITION;
	}

	public void setBIN_POSITION(String bIN_POSITION) {
		BIN_POSITION = bIN_POSITION;
	}

	public String getMOVEMENT_TYPE() {
		return MOVEMENT_TYPE;
	}

	public void setMOVEMENT_TYPE(String mOVEMENT_TYPE) {
		MOVEMENT_TYPE = mOVEMENT_TYPE;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getPO_TYPE() {
		return PO_TYPE;
	}

	public void setPO_TYPE(String pO_TYPE) {
		PO_TYPE = pO_TYPE;
	}

	public String getPO_DATE() {
		return PO_DATE;
	}

	public void setPO_DATE(String pO_DATE) {
		PO_DATE = pO_DATE;
	}

	public String getDELIVERY_DATE() {
		return DELIVERY_DATE;
	}

	public void setDELIVERY_DATE(String dELIVERY_DATE) {
		DELIVERY_DATE = dELIVERY_DATE;
	}

	public float getPO_QTY() {
		return PO_QTY;
	}

	public void setPO_QTY(float pO_QTY) {
		PO_QTY = pO_QTY;
	}

	public String getPO_UNIT() {
		return PO_UNIT;
	}

	public void setPO_UNIT(String pO_UNIT) {
		PO_UNIT = pO_UNIT;
	}

	public String getINCOTERMS() {
		return INCOTERMS;
	}

	public void setINCOTERMS(String iNCOTERMS) {
		INCOTERMS = iNCOTERMS;
	}

	public String getPAYMENT_TERM() {
		return PAYMENT_TERM;
	}

	public void setPAYMENT_TERM(String pAYMENT_TERM) {
		PAYMENT_TERM = pAYMENT_TERM;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public float getITEM_AMOUNT() {
		return ITEM_AMOUNT;
	}

	public void setITEM_AMOUNT(float iTEM_AMOUNT) {
		ITEM_AMOUNT = iTEM_AMOUNT;
	}

	public String getPURCHASE_ORG_CODE() {
		return PURCHASE_ORG_CODE;
	}

	public void setPURCHASE_ORG_CODE(String pURCHASE_ORG_CODE) {
		PURCHASE_ORG_CODE = pURCHASE_ORG_CODE;
	}

	public String getPURCHASE_GRP_CODE() {
		return PURCHASE_GRP_CODE;
	}

	public void setPURCHASE_GRP_CODE(String pURCHASE_GRP_CODE) {
		PURCHASE_GRP_CODE = pURCHASE_GRP_CODE;
	}

	public String getHEADER_FLAG() {
		return HEADER_FLAG;
	}

	public void setHEADER_FLAG(String hEADER_FLAG) {
		HEADER_FLAG = hEADER_FLAG;
	}

	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	public String getPOST_DATE() {
		return POST_DATE;
	}

	public void setPOST_DATE(String pOST_DATE) {
		POST_DATE = pOST_DATE;
	}

	public String getROWID() {
		return ROWID;
	}

	public void setROWID(String rOWID) {
		ROWID = rOWID;
	}

	public float getMOVE_QTY() {
		return MOVE_QTY;
	}

	public void setMOVE_QTY(float mOVE_QTY) {
		MOVE_QTY = mOVE_QTY;
	}

	public String getEDIT_FLAG() {
		return EDIT_FLAG;
	}

	public void setEDIT_FLAG(String eDIT_FLAG) {
		EDIT_FLAG = eDIT_FLAG;
	}

	public int getRANK_NO() {
		return RANK_NO;
	}

	public void setRANK_NO(int rANK_NO) {
		RANK_NO = rANK_NO;
	}

	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	public float getMST_QTY() {
		return MST_QTY;
	}

	public void setMST_QTY(float mST_QTY) {
		MST_QTY = mST_QTY;
	}

	public String getMST_UNIT() {
		return MST_UNIT;
	}

	public void setMST_UNIT(String mST_UNIT) {
		MST_UNIT = mST_UNIT;
	}

	public float getPO_ITEM_QTY() {
		return PO_ITEM_QTY;
	}

	public void setPO_ITEM_QTY(float pO_ITEM_QTY) {
		PO_ITEM_QTY = pO_ITEM_QTY;
	}

	public float getGR_ITEM_QTY() {
		return GR_ITEM_QTY;
	}

	public void setGR_ITEM_QTY(float gR_ITEM_QTY) {
		GR_ITEM_QTY = gR_ITEM_QTY;
	}

	public float getITEM_QTY() {
		return ITEM_QTY;
	}

	public void setITEM_QTY(float iTEM_QTY) {
		ITEM_QTY = iTEM_QTY;
	}

	public String getGR_STATUS() {
		return GR_STATUS;
	}

	public void setGR_STATUS(String gR_STATUS) {
		GR_STATUS = gR_STATUS;
	}

	public float getCONV_QTY() {
		return CONV_QTY;
	}

	public void setCONV_QTY(float cONV_QTY) {
		CONV_QTY = cONV_QTY;
	}

	public String getMST_STORAGE_LOC() {
		return MST_STORAGE_LOC;
	}

	public void setMST_STORAGE_LOC(String mST_STORAGE_LOC) {
		MST_STORAGE_LOC = mST_STORAGE_LOC;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}

	@Override
	public String toString() {
		return "WMS_DYN_PURCHASING_GR [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", DOC_NO=" + DOC_NO + ", PO_NO=" + PO_NO + ", PO_ITEM_NO=" + PO_ITEM_NO + ", BARCODE_NO="
				+ BARCODE_NO + ", VENDOR_CODE=" + VENDOR_CODE + ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_NAME="
				+ MATERIAL_NAME + ", VENDOR_LOT_NO=" + VENDOR_LOT_NO + ", LOT_NO=" + LOT_NO + ", GR_DATE=" + GR_DATE
				+ ", GR_QTY=" + GR_QTY + ", GR_UNIT=" + GR_UNIT + ", PER_QTY=" + PER_QTY + ", STORAGE_LOC="
				+ STORAGE_LOC + ", BIN_LOC=" + BIN_LOC + ", BIN_LEVEL=" + BIN_LEVEL + ", BIN_POSITION=" + BIN_POSITION
				+ ", MOVEMENT_TYPE=" + MOVEMENT_TYPE + ", STATUS=" + STATUS + ", GR_STATUS=" + GR_STATUS + ", DEL_FLAG="
				+ DEL_FLAG + ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER="
				+ UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE="
				+ DELETE_DATE + ", PO_TYPE=" + PO_TYPE + ", PO_DATE=" + PO_DATE + ", DELIVERY_DATE=" + DELIVERY_DATE
				+ ", PO_QTY=" + PO_QTY + ", PO_UNIT=" + PO_UNIT + ", INCOTERMS=" + INCOTERMS + ", PAYMENT_TERM="
				+ PAYMENT_TERM + ", CURRENCY=" + CURRENCY + ", ITEM_AMOUNT=" + ITEM_AMOUNT + ", PURCHASE_ORG_CODE="
				+ PURCHASE_ORG_CODE + ", PURCHASE_GRP_CODE=" + PURCHASE_GRP_CODE + ", HEADER_FLAG=" + HEADER_FLAG
				+ ", PLANT_CODE=" + PLANT_CODE + ", POST_DATE=" + POST_DATE + ", ROWID=" + ROWID + ", MOVE_QTY="
				+ MOVE_QTY + ", EDIT_FLAG=" + EDIT_FLAG + ", RANK_NO=" + RANK_NO + ", MATERIAL_TYPE=" + MATERIAL_TYPE
				+ ", MST_QTY=" + MST_QTY + ", MST_UNIT=" + MST_UNIT + ", PO_ITEM_QTY=" + PO_ITEM_QTY + ", GR_ITEM_QTY="
				+ GR_ITEM_QTY + ", ITEM_QTY=" + ITEM_QTY + ", CONV_QTY=" + CONV_QTY + ", MST_STORAGE_LOC="
				+ MST_STORAGE_LOC + ", PART_NO=" + PART_NO + "]";
	}
}


