/**
 *
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_CUSTOMER
{
	public WMS_MST_CUSTOMER()
	{
	}

	private Integer WSID              ;
	private String  CLIENT_ID         ;
	private String  COMPANY_CODE      ;
    private String  CUST_CODE         ;
    private String  CUST_NAME         ;
    private String  SALES_ORG_CODE    ;
    private String  SALES_DIV_CODE    ;
    private String  ADDRESS           ;
    private String  STREET1           ;
    private String  STREET2           ;
    private String  PO_BOX            ;
    private String  CITY              ;
    private String  STATE             ;
    private String  ZIP_CODE          ;
	private String  COUNTRY_CODE      ;
    private String  PHONE             ;
    private String  CONTACT_NAME      ;
    private String  CONTACT_EMAIL     ;
    private String  CONTACT_PHONE     ;
    private String  PAYMENT_TERM      ;
	private Float   DELIVERY_TIME     ;
    private String  CURRENCY          ;
    private Float   SALES_PRICE       ;
    private Float   PRICE_UNIT        ;
    private String  UNIT              ;
    private String  DEL_FLAG          ;
    private String  CREATE_USER       ;
    private String  CREATE_DATE       ;
    private String  UPDATE_USER       ;
    private String  UPDATE_DATE       ;
    private String  DELETE_USER       ;
    private String  DELETE_DATE       ;

    private String  GI_TEMPLATE_CODE  ;
    private String  EO_NO             ;

    private String  SUPPLIER_CODE     ;
    private String  SUPPLIER_NAME     ;
    private String  SUPPLIER_COUNTRY  ;
    private String  SUPPLIER_STATE    ;
    private String  SUPPLIER_CITY     ;
    private String  SUPPLIER_STREET1  ;
    private String  SUPPLIER_STREET2  ;
    private String  SUPPLIER_ZIP_CODE ;


    private String  SHIP_TO_CODE      ;
    private String  SHIP_TO_NAME      ;
    private String  SHIP_TO_COUNTRY   ;
    private String  SHIP_TO_STATE     ;
    private String  SHIP_TO_CITY      ;
    private String  SHIP_TO_STREET1   ;
    private String  SHIP_TO_STREET2   ;
    private String  SHIP_TO_ZIP_CODE  ;

    public Integer getWSID() {
		return WSID;
	}
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getCUST_CODE() {
		return CUST_CODE;
	}

	public void setCUST_CODE(String cUST_CODE) {
		CUST_CODE = cUST_CODE;
	}

	public String getCUST_NAME() {
		return CUST_NAME;
	}

	public void setCUST_NAME(String cUST_NAME) {
		CUST_NAME = cUST_NAME;
	}

	public String getSALES_ORG_CODE() {
		return SALES_ORG_CODE;
	}

	public void setSALES_ORG_CODE(String sALES_ORG_CODE) {
		SALES_ORG_CODE = sALES_ORG_CODE;
	}

	public String getSALES_DIV_CODE() {
		return SALES_DIV_CODE;
	}

	public void setSALES_DIV_CODE(String sALES_DIV_CODE) {
		SALES_DIV_CODE = sALES_DIV_CODE;
	}

	public String getADDRESS() {
		return ADDRESS;
	}

	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	public String getSTREET1() {
		return STREET1;
	}

	public void setSTREET1(String sTREET1) {
		STREET1 = sTREET1;
	}

	public String getSTREET2() {
		return STREET2;
	}

	public void setSTREET2(String sTREET2) {
		STREET2 = sTREET2;
	}

	public String getPO_BOX() {
		return PO_BOX;
	}

	public void setPO_BOX(String pO_BOX) {
		PO_BOX = pO_BOX;
	}

	public String getCITY() {
		return CITY;
	}

	public void setCITY(String cITY) {
		CITY = cITY;
	}

	public String getSTATE() {
		return STATE;
	}

	public void setSTATE(String sTATE) {
		STATE = sTATE;
	}

	public String getZIP_CODE() {
		return ZIP_CODE;
	}

	public void setZIP_CODE(String zIP_CODE) {
		ZIP_CODE = zIP_CODE;
	}

	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}

	public void setCOUNTRY_CODE(String cOUNTRY_CODE) {
		COUNTRY_CODE = cOUNTRY_CODE;
	}

	public String getPHONE() {
		return PHONE;
	}

	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}

	public String getCONTACT_NAME() {
		return CONTACT_NAME;
	}

	public void setCONTACT_NAME(String cONTACT_NAME) {
		CONTACT_NAME = cONTACT_NAME;
	}

	public String getCONTACT_EMAIL() {
		return CONTACT_EMAIL;
	}

	public void setCONTACT_EMAIL(String cONTACT_EMAIL) {
		CONTACT_EMAIL = cONTACT_EMAIL;
	}

	public String getCONTACT_PHONE() {
		return CONTACT_PHONE;
	}

	public void setCONTACT_PHONE(String cONTACT_PHONE) {
		CONTACT_PHONE = cONTACT_PHONE;
	}

	public String getPAYMENT_TERM() {
		return PAYMENT_TERM;
	}

	public void setPAYMENT_TERM(String pAYMENT_TERM) {
		PAYMENT_TERM = pAYMENT_TERM;
	}

	public Float getDELIVERY_TIME() {
		return DELIVERY_TIME;
	}

	public void setDELIVERY_TIME(Float dELIVERY_TIME) {
		DELIVERY_TIME = dELIVERY_TIME;
	}

	public String getCURRENCY() {
		return CURRENCY;
	}

	public void setCURRENCY(String cURRENCY) {
		CURRENCY = cURRENCY;
	}

	public Float getSALES_PRICE() {
		return SALES_PRICE;
	}

	public void setSALES_PRICE(Float sALES_PRICE) {
		SALES_PRICE = sALES_PRICE;
	}

	public Float getPRICE_UNIT() {
		return PRICE_UNIT;
	}

	public void setPRICE_UNIT(Float pRICE_UNIT) {
		PRICE_UNIT = pRICE_UNIT;
	}

	public String getUNIT() {
		return UNIT;
	}

	public void setUNIT(String uNIT) {
		UNIT = uNIT;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getGI_TEMPLATE_CODE() {
		return GI_TEMPLATE_CODE;
	}

	public void setGI_TEMPLATE_CODE(String gI_TEMPLATE_CODE) {
		GI_TEMPLATE_CODE = gI_TEMPLATE_CODE;
	}

	public String getSHIP_TO_CODE() {
		return SHIP_TO_CODE;
	}

	public void setSHIP_TO_CODE(String sHIP_TO_CODE) {
		SHIP_TO_CODE = sHIP_TO_CODE;
	}

	public String getSHIP_TO_NAME() {
		return SHIP_TO_NAME;
	}

	public void setSHIP_TO_NAME(String sHIP_TO_NAME) {
		SHIP_TO_NAME = sHIP_TO_NAME;
	}

	public String getSUPPLIER_CODE() {
		return SUPPLIER_CODE;
	}

	public void setSUPPLIER_CODE(String sUPPLIER_CODE) {
		SUPPLIER_CODE = sUPPLIER_CODE;
	}

	public String getEO_NO() {
		return EO_NO;
	}

	public void setEO_NO(String eO_NO) {
		EO_NO = eO_NO;
	}

	public String getSUPPLIER_NAME() {
		return SUPPLIER_NAME;
	}

	public void setSUPPLIER_NAME(String sUPPLIER_NAME) {
		SUPPLIER_NAME = sUPPLIER_NAME;
	}

	public String getSUPPLIER_COUNTRY() {
		return SUPPLIER_COUNTRY;
	}

	public void setSUPPLIER_COUNTRY(String sUPPLIER_COUNTRY) {
		SUPPLIER_COUNTRY = sUPPLIER_COUNTRY;
	}

	public String getSUPPLIER_STATE() {
		return SUPPLIER_STATE;
	}

	public void setSUPPLIER_STATE(String sUPPLIER_STATE) {
		SUPPLIER_STATE = sUPPLIER_STATE;
	}

	public String getSUPPLIER_CITY() {
		return SUPPLIER_CITY;
	}

	public void setSUPPLIER_CITY(String sUPPLIER_CITY) {
		SUPPLIER_CITY = sUPPLIER_CITY;
	}

	public String getSUPPLIER_STREET1() {
		return SUPPLIER_STREET1;
	}

	public void setSUPPLIER_STREET1(String sUPPLIER_STREET1) {
		SUPPLIER_STREET1 = sUPPLIER_STREET1;
	}
	public String getSUPPLIER_STREET2() {
		return SUPPLIER_STREET2;
	}

	public void setSUPPLIER_STREET2(String sUPPLIER_STREET2) {
		SUPPLIER_STREET2 = sUPPLIER_STREET2;
	}

	public String getSUPPLIER_ZIP_CODE() {
		return SUPPLIER_ZIP_CODE;
	}

	public void setSUPPLIER_ZIP_CODE(String sUPPLIER_ZIP_CODE) {
		SUPPLIER_ZIP_CODE = sUPPLIER_ZIP_CODE;
	}

	public String getSHIP_TO_COUNTRY() {
		return SHIP_TO_COUNTRY;
	}

	public void setSHIP_TO_COUNTRY(String sHIP_TO_COUNTRY) {
		SHIP_TO_COUNTRY = sHIP_TO_COUNTRY;
	}

	public String getSHIP_TO_STATE() {
		return SHIP_TO_STATE;
	}

	public void setSHIP_TO_STATE(String sHIP_TO_STATE) {
		SHIP_TO_STATE = sHIP_TO_STATE;
	}

	public String getSHIP_TO_CITY() {
		return SHIP_TO_CITY;
	}

	public void setSHIP_TO_CITY(String sHIP_TO_CITY) {
		SHIP_TO_CITY = sHIP_TO_CITY;
	}

	public String getSHIP_TO_STREET1() {
		return SHIP_TO_STREET1;
	}

	public void setSHIP_TO_STREET1(String sHIP_TO_STREET1) {
		SHIP_TO_STREET1 = sHIP_TO_STREET1;
	}

	public String getSHIP_TO_STREET2() {
		return SHIP_TO_STREET2;
	}

	public void setSHIP_TO_STREET2(String sHIP_TO_STREET2) {
		SHIP_TO_STREET2 = sHIP_TO_STREET2;
	}

	public String getSHIP_TO_ZIP_CODE() {
		return SHIP_TO_ZIP_CODE;
	}

	public void setSHIP_TO_ZIP_CODE(String sHIP_TO_ZIP_CODE) {
		SHIP_TO_ZIP_CODE = sHIP_TO_ZIP_CODE;
	}
	@Override
	public String toString() {
		return "WMS_MST_CUSTOMER [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", CUST_CODE=" + CUST_CODE + ", CUST_NAME=" + CUST_NAME + ", SALES_ORG_CODE=" + SALES_ORG_CODE
				+ ", SALES_DIV_CODE=" + SALES_DIV_CODE + ", ADDRESS=" + ADDRESS + ", STREET1=" + STREET1 + ", STREET2="
				+ STREET2 + ", PO_BOX=" + PO_BOX + ", CITY=" + CITY + ", STATE=" + STATE + ", ZIP_CODE=" + ZIP_CODE
				+ ", COUNTRY_CODE=" + COUNTRY_CODE + ", PHONE=" + PHONE + ", CONTACT_NAME=" + CONTACT_NAME
				+ ", CONTACT_EMAIL=" + CONTACT_EMAIL + ", CONTACT_PHONE=" + CONTACT_PHONE + ", PAYMENT_TERM="
				+ PAYMENT_TERM + ", DELIVERY_TIME=" + DELIVERY_TIME + ", CURRENCY=" + CURRENCY + ", SALES_PRICE="
				+ SALES_PRICE + ", PRICE_UNIT=" + PRICE_UNIT + ", UNIT=" + UNIT + ", DEL_FLAG=" + DEL_FLAG
				+ ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER
				+ ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE
				+ ", GI_TEMPLATE_CODE=" + GI_TEMPLATE_CODE + ", EO_NO=" + EO_NO + ", SUPPLIER_CODE=" + SUPPLIER_CODE
				+ ", SUPPLIER_NAME=" + SUPPLIER_NAME + ", SUPPLIER_COUNTRY=" + SUPPLIER_COUNTRY + ", SUPPLIER_STATE="
				+ SUPPLIER_STATE + ", SUPPLIER_CITY=" + SUPPLIER_CITY + ", SUPPLIER_STREET1=" + SUPPLIER_STREET1
				+ ", SUPPLIER_STREET2=" + SUPPLIER_STREET2 + ", SUPPLIER_ZIP_CODE=" + SUPPLIER_ZIP_CODE
				+ ", SHIP_TO_CODE=" + SHIP_TO_CODE + ", SHIP_TO_NAME=" + SHIP_TO_NAME + ", SHIP_TO_COUNTRY="
				+ SHIP_TO_COUNTRY + ", SHIP_TO_STATE=" + SHIP_TO_STATE + ", SHIP_TO_CITY=" + SHIP_TO_CITY
				+ ", SHIP_TO_STREET1=" + SHIP_TO_STREET1 + ", SHIP_TO_STREET2=" + SHIP_TO_STREET2
				+ ", SHIP_TO_ZIP_CODE=" + SHIP_TO_ZIP_CODE + "]";
	}
}