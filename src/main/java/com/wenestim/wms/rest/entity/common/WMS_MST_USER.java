/**
 *
 */
package com.wenestim.wms.rest.entity.common;

/**
 * @author Andrew
 *
 */
public class WMS_MST_USER 
{
	/**
	 *
	 */
	public WMS_MST_USER() {
		// TODO Auto-generated constructor stub
	}

	private Integer WSID         ;
	private String CLIENT_ID     ;
	private String COMPANY_CODE  ;
	private String USER_ID       ;
	private String USER_PW       ;
	private String USER_NAME     ;
	private String USER_GRP      ;
	private String USER_EMAIL    ;

	private String  DEL_FLAG     ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
    private String  DELETE_USER  ;
	private String  DELETE_DATE  ;

	private String  PRINTER_IP    ;
	private String  LOGO_NAME     ;
	private String  PRICE_FORMAT  ;
	private String  QTY_FORMAT    ;
	private String  DASHBOARD_REST;
	private String  DASHBOARD_DIR ;
	private String  COMPANY_NAME  ;
	private String  LOT_FLAG      ;

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	/**
	 * @return the uSER_ID
	 */
	public String getUSER_ID() {
		return USER_ID;
	}

	/**
	 * @param uSER_ID the uSER_ID to set
	 */
	public void setUSER_ID(String uSER_ID) {
		USER_ID = uSER_ID;
	}

	/**
	 * @return the uSER_PW
	 */
	public String getUSER_PW() {
		return USER_PW;
	}

	/**
	 * @param uSER_PW the uSER_PW to set
	 */
	public void setUSER_PW(String uSER_PW) {
		USER_PW = uSER_PW;
	}

	/**
	 * @return the uSER_NAME
	 */
	public String getUSER_NAME() {
		return USER_NAME;
	}

	/**
	 * @param uSER_NAME the uSER_NAME to set
	 */
	public void setUSER_NAME(String uSER_NAME) {
		USER_NAME = uSER_NAME;
	}

	/**
	 * @return the uSER_GRP
	 */
	public String getUSER_GRP() {
		return USER_GRP;
	}

	/**
	 * @param uSER_GRP the uSER_GRP to set
	 */
	public void setUSER_GRP(String uSER_GRP) {
		USER_GRP = uSER_GRP;
	}

	/**
	 * @return the uSER_EMAIL
	 */
	public String getUSER_EMAIL() {
		return USER_EMAIL;
	}

	/**
	 * @param uSER_EMAIL the uSER_EMAIL to set
	 */
	public void setUSER_EMAIL(String uSER_EMAIL) {
		USER_EMAIL = uSER_EMAIL;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	public String getPRINTER_IP() {
		return PRINTER_IP;
	}

	public void setPRINTER_IP(String pRINTER_IP) {
		PRINTER_IP = pRINTER_IP;
	}

	public String getLOGO_NAME() {
		return LOGO_NAME;
	}

	public void setLOGO_NAME(String lOGO_NAME) {
		LOGO_NAME = lOGO_NAME;
	}

	public String getPRICE_FORMAT() {
		return PRICE_FORMAT;
	}

	public void setPRICE_FORMAT(String pRICE_FORMAT) {
		PRICE_FORMAT = pRICE_FORMAT;
	}

	public String getQTY_FORMAT() {
		return QTY_FORMAT;
	}

	public void setQTY_FORMAT(String qTY_FORMAT) {
		QTY_FORMAT = qTY_FORMAT;
	}

	public String getDASHBOARD_REST() {
		return DASHBOARD_REST;
	}

	public void setDASHBOARD_REST(String dASHBOARD_REST) {
		DASHBOARD_REST = dASHBOARD_REST;
	}

	public String getDASHBOARD_DIR() {
		return DASHBOARD_DIR;
	}

	public void setDASHBOARD_DIR(String dASHBOARD_DIR) {
		DASHBOARD_DIR = dASHBOARD_DIR;
	}

	public String getCOMPANY_NAME() {
		return COMPANY_NAME;
	}

	public void setCOMPANY_NAME(String cOMPANY_NAME) {
		COMPANY_NAME = cOMPANY_NAME;
	}


	public String getLOT_FLAG() {
		return LOT_FLAG;
	}

	public void setLOT_FLAG(String lOT_FLAG) {
		LOT_FLAG = lOT_FLAG;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WMS_MST_USER [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", USER_ID=" + USER_ID + ", USER_PW=" + USER_PW + ", USER_NAME=" + USER_NAME + ", USER_GRP="
				+ USER_GRP + ", USER_EMAIL=" + USER_EMAIL + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", PRINTER_IP=" + PRINTER_IP
				+ ", LOGO_NAME=" + LOGO_NAME + "]";
	}

}
