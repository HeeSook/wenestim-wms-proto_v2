/**
 *
 */
package com.wenestim.wms.rest.entity.production;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PRODUCTION
{

	public WMS_DYN_PRODUCTION()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID    ;
	private String  COMPANY_CODE ;
	private String  PLANT_CODE   ;
	private String  PLAN_CURRENT ;
	private String  PLAN_VER     ;
	private String  PLAN_TYPE    ;
	private String  MATERIAL_CODE;
	private String  MATERIAL_TYPE;
	private String  CATEGORY     ;
	private String  PLAN_DATE    ;
	private Float   PLAN_QTY     ;
	private String  DEL_FLAG     ;
	private String  CREATE_USER  ;
	private String  CREATE_DATE  ;
	private String  UPDATE_USER  ;
	private String  UPDATE_DATE  ;
	private String  DELETE_USER  ;
	private String  DELETE_DATE  ;
	private Integer SORT_SEQ     ;

	private String  MATERIAL_NAME;
    private String  PART_NO      ;

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	/**
	 * @return the pLANT_CODE
	 */
	public String getPLANT_CODE() {
		return PLANT_CODE;
	}

	/**
	 * @param pLANT_CODE the pLANT_CODE to set
	 */
	public void setPLANT_CODE(String pLANT_CODE) {
		PLANT_CODE = pLANT_CODE;
	}

	/**
	 * @return the pLAN_DATE
	 */
	public String getPLAN_DATE() {
		return PLAN_DATE;
	}

	/**
	 * @param pLAN_DATE the pLAN_DATE to set
	 */
	public void setPLAN_DATE(String pLAN_DATE) {
		PLAN_DATE = pLAN_DATE;
	}

	public String getCATEGORY() {
		return CATEGORY;
	}

	public void setCATEGORY(String cATEGORY) {
		CATEGORY = cATEGORY;
	}

	/**
	 * @return the pLAN_VER
	 */
	public String getPLAN_VER() {
		return PLAN_VER;
	}

	/**
	 * @param pLAN_VER the pLAN_VER to set
	 */
	public void setPLAN_VER(String pLAN_VER) {
		PLAN_VER = pLAN_VER;
	}

	/**
	 * @return the mATERIAL_TYPE
	 */
	public String getMATERIAL_TYPE() {
		return MATERIAL_TYPE;
	}

	/**
	 * @param mATERIAL_TYPE the mATERIAL_TYPE to set
	 */
	public void setMATERIAL_TYPE(String mATERIAL_TYPE) {
		MATERIAL_TYPE = mATERIAL_TYPE;
	}

	/**
	 * @return the mATERIAL_CODE
	 */
	public String getMATERIAL_CODE() {
		return MATERIAL_CODE;
	}

	/**
	 * @param mATERIAL_CODE the mATERIAL_CODE to set
	 */
	public void setMATERIAL_CODE(String mATERIAL_CODE) {
		MATERIAL_CODE = mATERIAL_CODE;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	/**
	 * @return the mATERIAL_NAME
	 */
	public String getMATERIAL_NAME() {
		return MATERIAL_NAME;
	}

	/**
	 * @param mATERIAL_NAME the mATERIAL_NAME to set
	 */
	public void setMATERIAL_NAME(String mATERIAL_NAME) {
		MATERIAL_NAME = mATERIAL_NAME;
	}

	public String getPLAN_CURRENT() {
		return PLAN_CURRENT;
	}

	public void setPLAN_CURRENT(String pLAN_CURRENT) {
		PLAN_CURRENT = pLAN_CURRENT;
	}

	public Float getPLAN_QTY() {
		return PLAN_QTY;
	}

	public void setPLAN_QTY(Float pLAN_QTY) {
		PLAN_QTY = pLAN_QTY;
	}

	public Integer getSORT_SEQ() {
		return SORT_SEQ;
	}

	public void setSORT_SEQ(Integer sORT_SEQ) {
		SORT_SEQ = sORT_SEQ;
	}

	public String getPART_NO() {
		return PART_NO;
	}

	public void setPART_NO(String pART_NO) {
		PART_NO = pART_NO;
	}


	public String getPLAN_TYPE() {
		return PLAN_TYPE;
	}

	public void setPLAN_TYPE(String pLAN_TYPE) {
		PLAN_TYPE = pLAN_TYPE;
	}

	@Override
	public String toString() {
		return "WMS_DYN_PRODUCTION [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", PLANT_CODE=" + PLANT_CODE + ", PLAN_CURRENT=" + PLAN_CURRENT + ", PLAN_VER=" + PLAN_VER
				+ ", MATERIAL_CODE=" + MATERIAL_CODE + ", MATERIAL_TYPE=" + MATERIAL_TYPE + ", CATEGORY=" + CATEGORY
				+ ", PLAN_DATE=" + PLAN_DATE + ", PLAN_QTY=" + PLAN_QTY + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER="
				+ CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE="
				+ UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", SORT_SEQ="
				+ SORT_SEQ + ", MATERIAL_NAME=" + MATERIAL_NAME + ", PART_NO=" + PART_NO + "]";
	}
}
