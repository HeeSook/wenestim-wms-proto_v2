/**
 *
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_BOM
{
	public WMS_MST_BOM()
	{
	}

	private Integer WSID               ;
	private String  CLIENT_ID          ;
	private String  COMPANY_CODE       ;
	private String  PROD_MATERIAL_CODE ;
    private Float   PROD_QTY           ;
    private String  PROD_UNIT          ;
    private Integer COMP_NO            ;
    private String  COMP_MATERIAL_CODE ;
    private Float   COMP_QTY           ;
    private String  COMP_UNIT          ;
    private String  DEL_FLAG           ;
    private String  CREATE_USER        ;
    private String  CREATE_DATE        ;
    private String  UPDATE_USER        ;
    private String  UPDATE_DATE        ;
    private String  DELETE_USER        ;
    private String  DELETE_DATE        ;

    // For Description
    private String  PROD_MATERIAL_NAME ;
    private String  PROD_MATERIAL_PART ;
    private String  PROD_MATERIAL_TYPE ;
    private String  COMP_MATERIAL_NAME ;
    private String  COMP_MATERIAL_PART ;
    private String  COMP_MATERIAL_TYPE ;
    private String  COMP_STORAGE_LOC   ;

    private String  PROD_PLANT_CODE ;
    private String  COMP_PLANT_CODE ;

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	/**
	 * @return the pROD_MATERIAL_CODE
	 */
	public String getPROD_MATERIAL_CODE() {
		return PROD_MATERIAL_CODE;
	}

	/**
	 * @param pROD_MATERIAL_CODE the pROD_MATERIAL_CODE to set
	 */
	public void setPROD_MATERIAL_CODE(String pROD_MATERIAL_CODE) {
		PROD_MATERIAL_CODE = pROD_MATERIAL_CODE;
	}

	/**
	 * @return the pROD_QTY
	 */
	public Float getPROD_QTY() {
		return PROD_QTY;
	}

	/**
	 * @param pROD_QTY the pROD_QTY to set
	 */
	public void setPROD_QTY(Float pROD_QTY) {
		PROD_QTY = pROD_QTY;
	}

	/**
	 * @return the pROD_UNIT
	 */
	public String getPROD_UNIT() {
		return PROD_UNIT;
	}

	/**
	 * @param pROD_UNIT the pROD_UNIT to set
	 */
	public void setPROD_UNIT(String pROD_UNIT) {
		PROD_UNIT = pROD_UNIT;
	}

	/**
	 * @return the cOMP_NO
	 */
	public Integer getCOMP_NO() {
		return COMP_NO;
	}

	/**
	 * @param cOMP_NO the cOMP_NO to set
	 */
	public void setCOMP_NO(Integer cOMP_NO) {
		COMP_NO = cOMP_NO;
	}

	/**
	 * @return the cOMP_MATERIAL_CODE
	 */
	public String getCOMP_MATERIAL_CODE() {
		return COMP_MATERIAL_CODE;
	}

	/**
	 * @param cOMP_MATERIAL_CODE the cOMP_MATERIAL_CODE to set
	 */
	public void setCOMP_MATERIAL_CODE(String cOMP_MATERIAL_CODE) {
		COMP_MATERIAL_CODE = cOMP_MATERIAL_CODE;
	}

	/**
	 * @return the cOMP_QTY
	 */
	public Float getCOMP_QTY() {
		return COMP_QTY;
	}

	/**
	 * @param cOMP_QTY the cOMP_QTY to set
	 */
	public void setCOMP_QTY(Float cOMP_QTY) {
		COMP_QTY = cOMP_QTY;
	}

	/**
	 * @return the cOMP_UNIT
	 */
	public String getCOMP_UNIT() {
		return COMP_UNIT;
	}

	/**
	 * @param cOMP_UNIT the cOMP_UNIT to set
	 */
	public void setCOMP_UNIT(String cOMP_UNIT) {
		COMP_UNIT = cOMP_UNIT;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	/**
	 * @return the pROD_MATERIAL_NAME
	 */
	public String getPROD_MATERIAL_NAME() {
		return PROD_MATERIAL_NAME;
	}

	/**
	 * @param pROD_MATERIAL_NAME the pROD_MATERIAL_NAME to set
	 */
	public void setPROD_MATERIAL_NAME(String pROD_MATERIAL_NAME) {
		PROD_MATERIAL_NAME = pROD_MATERIAL_NAME;
	}

	/**
	 * @return the pROD_MATERIAL_TYPE
	 */
	public String getPROD_MATERIAL_TYPE() {
		return PROD_MATERIAL_TYPE;
	}

	/**
	 * @param pROD_MATERIAL_TYPE the pROD_MATERIAL_TYPE to set
	 */
	public void setPROD_MATERIAL_TYPE(String pROD_MATERIAL_TYPE) {
		PROD_MATERIAL_TYPE = pROD_MATERIAL_TYPE;
	}

	/**
	 * @return the cOMP_MATERIAL_NAME
	 */
	public String getCOMP_MATERIAL_NAME() {
		return COMP_MATERIAL_NAME;
	}

	/**
	 * @param cOMP_MATERIAL_NAME the cOMP_MATERIAL_NAME to set
	 */
	public void setCOMP_MATERIAL_NAME(String cOMP_MATERIAL_NAME) {
		COMP_MATERIAL_NAME = cOMP_MATERIAL_NAME;
	}

	/**
	 * @return the cOMP_MATERIAL_TYPE
	 */
	public String getCOMP_MATERIAL_TYPE() {
		return COMP_MATERIAL_TYPE;
	}

	/**
	 * @param cOMP_MATERIAL_TYPE the cOMP_MATERIAL_TYPE to set
	 */
	public void setCOMP_MATERIAL_TYPE(String cOMP_MATERIAL_TYPE) {
		COMP_MATERIAL_TYPE = cOMP_MATERIAL_TYPE;
	}

	public String getPROD_PLANT_CODE() {
		return PROD_PLANT_CODE;
	}

	public void setPROD_PLANT_CODE(String pROD_PLANT_CODE) {
		PROD_PLANT_CODE = pROD_PLANT_CODE;
	}

	public String getCOMP_PLANT_CODE() {
		return COMP_PLANT_CODE;
	}

	public void setCOMP_PLANT_CODE(String cOMP_PLANT_CODE) {
		COMP_PLANT_CODE = cOMP_PLANT_CODE;
	}

	public String getCOMP_STORAGE_LOC() {
		return COMP_STORAGE_LOC;
	}

	public void setCOMP_STORAGE_LOC(String cOMP_STORAGE_LOC) {
		COMP_STORAGE_LOC = cOMP_STORAGE_LOC;
	}

	public String getPROD_MATERIAL_PART() {
		return PROD_MATERIAL_PART;
	}

	public void setPROD_MATERIAL_PART(String pROD_MATERIAL_PART) {
		PROD_MATERIAL_PART = pROD_MATERIAL_PART;
	}

	public String getCOMP_MATERIAL_PART() {
		return COMP_MATERIAL_PART;
	}

	public void setCOMP_MATERIAL_PART(String cOMP_MATERIAL_PART) {
		COMP_MATERIAL_PART = cOMP_MATERIAL_PART;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WMS_MST_BOM [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", PROD_MATERIAL_CODE=" + PROD_MATERIAL_CODE + ", PROD_QTY=" + PROD_QTY + ", PROD_UNIT=" + PROD_UNIT
				+ ", COMP_NO=" + COMP_NO + ", COMP_MATERIAL_CODE=" + COMP_MATERIAL_CODE + ", COMP_QTY=" + COMP_QTY
				+ ", COMP_UNIT=" + COMP_UNIT + ", DEL_FLAG=" + DEL_FLAG + ", CREATE_USER=" + CREATE_USER
				+ ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER + ", UPDATE_DATE=" + UPDATE_DATE
				+ ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE + ", PROD_MATERIAL_NAME="
				+ PROD_MATERIAL_NAME + ", PROD_MATERIAL_TYPE=" + PROD_MATERIAL_TYPE + ", COMP_MATERIAL_NAME="
				+ COMP_MATERIAL_NAME + ", COMP_MATERIAL_TYPE=" + COMP_MATERIAL_TYPE + ", COMP_PLANT_CODE=" + COMP_PLANT_CODE + ", PROD_PLANT_CODE=" + PROD_PLANT_CODE + "]";
	}


}

