/**
 *
 */
package com.wenestim.wms.rest.entity.master;

/**
 * @author Andrew
 *
 */
public class WMS_MST_VENDOR
{
	public WMS_MST_VENDOR()
	{
	}

	private Integer WSID             ;
	private String CLIENT_ID         ;
	private String COMPANY_CODE      ;
	private String VENDOR_CODE       ;
	private String VENDOR_NAME       ;
	private String PURCHASE_ORG_CODE ;
	private String PURCHASE_GRP_CODE ;
	private String ADDRESS           ;
	private String COUNTRY_CODE      ;
	private String PHONE             ;
	private String CONTACT_NAME      ;
	private String CONTACT_EMAIL     ;
	private String CONTACT_PHONE     ;
	private String INCOTERMS         ;
	private String PAYMENT_TERM      ;
	private Float  DELIVERY_TIME     ;
	private String DEL_FLAG          ;
	private String CREATE_USER       ;
	private String CREATE_DATE       ;
	private String UPDATE_USER       ;
	private String UPDATE_DATE       ;
    private String DELETE_USER       ;
	private String DELETE_DATE       ;

	// For DropDownList
	private String VENDOR_DESC       ;

	/**
	 * @return the wSID
	 */
	public Integer getWSID() {
		return WSID;
	}

	/**
	 * @param wSID the wSID to set
	 */
	public void setWSID(Integer wSID) {
		WSID = wSID;
	}

	/**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	/**
	 * @return the cOMPANY_CODE
	 */
	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	/**
	 * @param cOMPANY_CODE the cOMPANY_CODE to set
	 */
	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	/**
	 * @return the vENDOR_CODE
	 */
	public String getVENDOR_CODE() {
		return VENDOR_CODE;
	}

	/**
	 * @param vENDOR_CODE the vENDOR_CODE to set
	 */
	public void setVENDOR_CODE(String vENDOR_CODE) {
		VENDOR_CODE = vENDOR_CODE;
	}

	/**
	 * @return the vENDOR_NAME
	 */
	public String getVENDOR_NAME() {
		return VENDOR_NAME;
	}

	/**
	 * @param vENDOR_NAME the vENDOR_NAME to set
	 */
	public void setVENDOR_NAME(String vENDOR_NAME) {
		VENDOR_NAME = vENDOR_NAME;
	}

	/**
	 * @return the pURCHASE_ORG_CODE
	 */
	public String getPURCHASE_ORG_CODE() {
		return PURCHASE_ORG_CODE;
	}

	/**
	 * @param pURCHASE_ORG_CODE the pURCHASE_ORG_CODE to set
	 */
	public void setPURCHASE_ORG_CODE(String pURCHASE_ORG_CODE) {
		PURCHASE_ORG_CODE = pURCHASE_ORG_CODE;
	}

	/**
	 * @return the pURCHASE_GRP_CODE
	 */
	public String getPURCHASE_GRP_CODE() {
		return PURCHASE_GRP_CODE;
	}

	/**
	 * @param pURCHASE_GRP_CODE the pURCHASE_GRP_CODE to set
	 */
	public void setPURCHASE_GRP_CODE(String pURCHASE_GRP_CODE) {
		PURCHASE_GRP_CODE = pURCHASE_GRP_CODE;
	}

	/**
	 * @return the aDDRESS
	 */
	public String getADDRESS() {
		return ADDRESS;
	}

	/**
	 * @param aDDRESS the aDDRESS to set
	 */
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}

	/**
	 * @return the cOUNTRY_CODE
	 */
	public String getCOUNTRY_CODE() {
		return COUNTRY_CODE;
	}

	/**
	 * @param cOUNTRY_CODE the cOUNTRY_CODE to set
	 */
	public void setCOUNTRY_CODE(String cOUNTRY_CODE) {
		COUNTRY_CODE = cOUNTRY_CODE;
	}

	/**
	 * @return the pHONE
	 */
	public String getPHONE() {
		return PHONE;
	}

	/**
	 * @param pHONE the pHONE to set
	 */
	public void setPHONE(String pHONE) {
		PHONE = pHONE;
	}

	/**
	 * @return the cONTACT_NAME
	 */
	public String getCONTACT_NAME() {
		return CONTACT_NAME;
	}

	/**
	 * @param cONTACT_NAME the cONTACT_NAME to set
	 */
	public void setCONTACT_NAME(String cONTACT_NAME) {
		CONTACT_NAME = cONTACT_NAME;
	}

	/**
	 * @return the cONTACT_EMAIL
	 */
	public String getCONTACT_EMAIL() {
		return CONTACT_EMAIL;
	}

	/**
	 * @param cONTACT_EMAIL the cONTACT_EMAIL to set
	 */
	public void setCONTACT_EMAIL(String cONTACT_EMAIL) {
		CONTACT_EMAIL = cONTACT_EMAIL;
	}

	/**
	 * @return the cONTACT_PHONE
	 */
	public String getCONTACT_PHONE() {
		return CONTACT_PHONE;
	}

	/**
	 * @param cONTACT_PHONE the cONTACT_PHONE to set
	 */
	public void setCONTACT_PHONE(String cONTACT_PHONE) {
		CONTACT_PHONE = cONTACT_PHONE;
	}

	/**
	 * @return the iNCOTERMS
	 */
	public String getINCOTERMS() {
		return INCOTERMS;
	}

	/**
	 * @param iNCOTERMS the iNCOTERMS to set
	 */
	public void setINCOTERMS(String iNCOTERMS) {
		INCOTERMS = iNCOTERMS;
	}

	/**
	 * @return the pAYMENT_TERM
	 */
	public String getPAYMENT_TERM() {
		return PAYMENT_TERM;
	}

	/**
	 * @param pAYMENT_TERM the pAYMENT_TERM to set
	 */
	public void setPAYMENT_TERM(String pAYMENT_TERM) {
		PAYMENT_TERM = pAYMENT_TERM;
	}

	/**
	 * @return the dELIVERY_TIME
	 */
	public Float getDELIVERY_TIME() {
		return DELIVERY_TIME;
	}

	/**
	 * @param dELIVERY_TIME the dELIVERY_TIME to set
	 */
	public void setDELIVERY_TIME(Float dELIVERY_TIME) {
		DELIVERY_TIME = dELIVERY_TIME;
	}

	/**
	 * @return the dEL_FLAG
	 */
	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	/**
	 * @param dEL_FLAG the dEL_FLAG to set
	 */
	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	/**
	 * @return the cREATE_USER
	 */
	public String getCREATE_USER() {
		return CREATE_USER;
	}

	/**
	 * @param cREATE_USER the cREATE_USER to set
	 */
	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	/**
	 * @return the cREATE_DATE
	 */
	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	/**
	 * @param cREATE_DATE the cREATE_DATE to set
	 */
	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	/**
	 * @return the uPDATE_USER
	 */
	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	/**
	 * @param uPDATE_USER the uPDATE_USER to set
	 */
	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	/**
	 * @return the uPDATE_DATE
	 */
	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	/**
	 * @param uPDATE_DATE the uPDATE_DATE to set
	 */
	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	/**
	 * @return the dELETE_USER
	 */
	public String getDELETE_USER() {
		return DELETE_USER;
	}

	/**
	 * @param dELETE_USER the dELETE_USER to set
	 */
	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	/**
	 * @return the dELETE_DATE
	 */
	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	/**
	 * @param dELETE_DATE the dELETE_DATE to set
	 */
	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	/**
	 * @return the vENDOR_DESC
	 */
	public String getVENDOR_DESC() {
		return VENDOR_DESC;
	}

	/**
	 * @param vENDOR_DESC the vENDOR_DESC to set
	 */
	public void setVENDOR_DESC(String vENDOR_DESC) {
		VENDOR_DESC = vENDOR_DESC;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WMS_MST_VENDOR [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", VENDOR_CODE=" + VENDOR_CODE + ", VENDOR_NAME=" + VENDOR_NAME + ", PURCHASE_ORG_CODE="
				+ PURCHASE_ORG_CODE + ", PURCHASE_GRP_CODE=" + PURCHASE_GRP_CODE + ", ADDRESS=" + ADDRESS
				+ ", COUNTRY_CODE=" + COUNTRY_CODE + ", PHONE=" + PHONE + ", CONTACT_NAME=" + CONTACT_NAME
				+ ", CONTACT_EMAIL=" + CONTACT_EMAIL + ", CONTACT_PHONE=" + CONTACT_PHONE + ", INCOTERMS=" + INCOTERMS
				+ ", PAYMENT_TERM=" + PAYMENT_TERM + ", DELIVERY_TIME=" + DELIVERY_TIME + ", DEL_FLAG=" + DEL_FLAG
				+ ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER
				+ ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE
				+ ", VENDOR_DESC=" + VENDOR_DESC + "]";
	}

}

