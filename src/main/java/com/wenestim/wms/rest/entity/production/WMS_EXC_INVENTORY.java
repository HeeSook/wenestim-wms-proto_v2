/**
 *
 */
package com.wenestim.wms.rest.entity.production;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_INVENTORY;

/**
 * @author Andrew
 *
 */
/**
 * @author hsheo
 *
 */
public class WMS_EXC_INVENTORY extends WMS_DYN_INVENTORY
{
	public WMS_EXC_INVENTORY()
	{
	}

	private String ORDER_NO       ;
	private int    PLAN_SEQ       ;

	private float  AVAIL_QTY      ;
    private float  ASSIGN_QTY     ;
    private float  REQ_QTY        ;
    private float  BACKLOG_QTY    ;
    private float  TOT_ASSIGN_QTY ;

    private String DEMAND_TYPE ;

	public float getAVAIL_QTY() {
		return AVAIL_QTY;
	}

	public void setAVAIL_QTY(float aVAIL_QTY) {
		AVAIL_QTY = aVAIL_QTY;
	}

	public float getBACKLOG_QTY() {
		return BACKLOG_QTY;
	}

	public void setBACKLOG_QTY(float bACKLOG_QTY) {
		BACKLOG_QTY = bACKLOG_QTY;
	}

	public float getASSIGN_QTY() {
		return ASSIGN_QTY;
	}

	public void setASSIGN_QTY(float aSSIGN_QTY) {
		ASSIGN_QTY = aSSIGN_QTY;
	}

	public int getPLAN_SEQ() {
		return PLAN_SEQ;
	}

	public void setPLAN_SEQ(int pLAN_SEQ) {
		PLAN_SEQ = pLAN_SEQ;
	}

	public float getREQ_QTY() {
		return REQ_QTY;
	}

	public void setREQ_QTY(float rEQ_QTY) {
		REQ_QTY = rEQ_QTY;
	}

	public String getDEMAND_TYPE() {
		return DEMAND_TYPE;
	}

	public void setDEMAND_TYPE(String dEMAND_TYPE) {
		DEMAND_TYPE = dEMAND_TYPE;
	}

	public String getORDER_NO() {
		return ORDER_NO;
	}

	public void setORDER_NO(String oRDER_NO) {
		ORDER_NO = oRDER_NO;
	}

	public float getTOT_ASSIGN_QTY() {
		return TOT_ASSIGN_QTY;
	}

	public void setTOT_ASSIGN_QTY(float tOT_ASSIGN_QTY) {
		TOT_ASSIGN_QTY = tOT_ASSIGN_QTY;
	}

	@Override
	public String toString()
	{
		return "WMS_EXC_INVENTORY [ "
                + " WSID           = " + super.getWSID()           + ","
				+ " COMPANY_CODE   = " + super.getCOMPANY_CODE()   + ","
                + " PLANT_CODE     = " + super.getPLANT_CODE()     + ","
				+ " STORAGE_LOC    = " + super.getSTORAGE_LOC()    + ","
                + " MATERIAL_CODE  = " + super.getMATERIAL_CODE()  + ","
                + " INV_QTY        = " + super.getINV_QTY()        + ","
                + " UNIT           = " + super.getUNIT()           + ","
                + " BIN_LOC        = " + super.getBIN_LOC()        + ","
                + " BIN_LEVEL      = " + super.getBIN_LEVEL()      + ","
                + " BIN_POSITION   = " + super.getBIN_POSITION()   + ","
                + " AVAIL_QTY      = " + AVAIL_QTY   + ","
                + " BACKLOG_QTY    = " + BACKLOG_QTY + ","
                + " ASSIGN_QTY     = " + ASSIGN_QTY  +
            "]";
    }
}

