/**
 *
 */
package com.wenestim.wms.rest.entity.common;

/**
 * @author Andrew
 *
 */
public class WMS_MST_CODERULE
{
	public WMS_MST_CODERULE()
	{
	}

	private Integer WSID         ;
	private String  CLIENT_ID    ;
	private String  COMPANY_CODE ;
    private String  CATEGORY1    ;
    private String  CATEGORY2    ;
    private String  CODE         ;
    private String  CODE_DESC    ;
    private String  CODE_TYPE    ;
    private Integer START_RANGE  ;
    private Integer END_RANGE    ;
    private String  RULE         ;
    private String  COMMENT      ;
    private Integer CURR_VALUE   ;
    private String  DEL_FLAG     ;
    private String  CREATE_USER  ;
    private String  CREATE_DATE  ;
    private String  UPDATE_USER  ;
    private String  UPDATE_DATE  ;
    private String  DELETE_USER  ;
    private String  DELETE_DATE  ;

    public Integer getWSID() {
		return WSID;
	}

	public void setWSID(Integer wSID) {
		WSID = wSID;
	}
	
    /**
	 * @return the cLIENT_ID
	 */
	public String getCLIENT_ID() {
		return CLIENT_ID;
	}

	/**
	 * @param cLIENT_ID the cLIENT_ID to set
	 */
	public void setCLIENT_ID(String cLIENT_ID) {
		CLIENT_ID = cLIENT_ID;
	}

	public String getCOMPANY_CODE() {
		return COMPANY_CODE;
	}

	public void setCOMPANY_CODE(String cOMPANY_CODE) {
		COMPANY_CODE = cOMPANY_CODE;
	}

	public String getCATEGORY1() {
		return CATEGORY1;
	}

	public void setCATEGORY1(String cATEGORY1) {
		CATEGORY1 = cATEGORY1;
	}

	public String getCATEGORY2() {
		return CATEGORY2;
	}

	public void setCATEGORY2(String cATEGORY2) {
		CATEGORY2 = cATEGORY2;
	}

	public String getCODE() {
		return CODE;
	}

	public void setCODE(String cODE) {
		CODE = cODE;
	}

	public String getCODE_DESC() {
		return CODE_DESC;
	}

	public void setCODE_DESC(String cODE_DESC) {
		CODE_DESC = cODE_DESC;
	}

	public String getCODE_TYPE() {
		return CODE_TYPE;
	}

	public void setCODE_TYPE(String cODE_TYPE) {
		CODE_TYPE = cODE_TYPE;
	}

	public Integer getSTART_RANGE() {
		return START_RANGE;
	}

	public void setSTART_RANGE(Integer sTART_RANGE) {
		START_RANGE = sTART_RANGE;
	}

	public Integer getEND_RANGE() {
		return END_RANGE;
	}

	public void setEND_RANGE(Integer eND_RANGE) {
		END_RANGE = eND_RANGE;
	}

	public String getRULE() {
		return RULE;
	}

	public void setRULE(String rULE) {
		RULE = rULE;
	}

	public String getCOMMENT() {
		return COMMENT;
	}

	public void setCOMMENT(String cOMMENT) {
		COMMENT = cOMMENT;
	}

	public Integer getCURR_VALUE() {
		return CURR_VALUE;
	}

	public void setCURR_VALUE(Integer cURR_VALUE) {
		CURR_VALUE = cURR_VALUE;
	}

	public String getDEL_FLAG() {
		return DEL_FLAG;
	}

	public void setDEL_FLAG(String dEL_FLAG) {
		DEL_FLAG = dEL_FLAG;
	}

	public String getCREATE_USER() {
		return CREATE_USER;
	}

	public void setCREATE_USER(String cREATE_USER) {
		CREATE_USER = cREATE_USER;
	}

	public String getCREATE_DATE() {
		return CREATE_DATE;
	}

	public void setCREATE_DATE(String cREATE_DATE) {
		CREATE_DATE = cREATE_DATE;
	}

	public String getUPDATE_USER() {
		return UPDATE_USER;
	}

	public void setUPDATE_USER(String uPDATE_USER) {
		UPDATE_USER = uPDATE_USER;
	}

	public String getUPDATE_DATE() {
		return UPDATE_DATE;
	}

	public void setUPDATE_DATE(String uPDATE_DATE) {
		UPDATE_DATE = uPDATE_DATE;
	}

	public String getDELETE_USER() {
		return DELETE_USER;
	}

	public void setDELETE_USER(String dELETE_USER) {
		DELETE_USER = dELETE_USER;
	}

	public String getDELETE_DATE() {
		return DELETE_DATE;
	}

	public void setDELETE_DATE(String dELETE_DATE) {
		DELETE_DATE = dELETE_DATE;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "WMS_MST_CODERULE [WSID=" + WSID + ", CLIENT_ID=" + CLIENT_ID + ", COMPANY_CODE=" + COMPANY_CODE
				+ ", CATEGORY1=" + CATEGORY1 + ", CATEGORY2=" + CATEGORY2 + ", CODE=" + CODE + ", CODE_DESC="
				+ CODE_DESC + ", CODE_TYPE=" + CODE_TYPE + ", START_RANGE=" + START_RANGE + ", END_RANGE=" + END_RANGE
				+ ", RULE=" + RULE + ", COMMENT=" + COMMENT + ", CURR_VALUE=" + CURR_VALUE + ", DEL_FLAG=" + DEL_FLAG
				+ ", CREATE_USER=" + CREATE_USER + ", CREATE_DATE=" + CREATE_DATE + ", UPDATE_USER=" + UPDATE_USER
				+ ", UPDATE_DATE=" + UPDATE_DATE + ", DELETE_USER=" + DELETE_USER + ", DELETE_DATE=" + DELETE_DATE
				+ "]";
	}
	
}
