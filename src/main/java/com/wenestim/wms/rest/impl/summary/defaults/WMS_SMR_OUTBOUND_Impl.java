/**
 * 
 */
package com.wenestim.wms.rest.impl.summary.defaults;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.defaults.WMS_SMR_OUTBOUND_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;



/**
 * @author Andrew
 *
 */
public class WMS_SMR_OUTBOUND_Impl {

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_OUTBOUND_Impl.class);
	
	/**
	 * GET OUTBOUND ORDER DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getOutboundOrderDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundOrderDailyDetail(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundOrderTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundOrderTop5ByCustomer(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundOrderDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundOrderDailyTotalQty(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * GET OUTBOUND PICKING DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param POST_DATE
	 * @return List<HashMap<?, ?>> 
	 */
	public List<HashMap<?, ?>> getOutboundPickingDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundPickingDailyDetail(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundPickingTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundPickingTop5ByCustomer(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundPickingDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("POST_DATE") String POST_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundPickingDailyTotalQty(CLIENT_ID, COMPANY_CODE, POST_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	/**
	 * GET OUTBOUND DELIVERY DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getOutboundDeliveryDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundDeliveryDailyDetail(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundDeliveryTop5ByCustomer(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundDeliveryTop5ByCustomer(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getOutboundDeliveryDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DELIVERY_DATE") String DELIVERY_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getOutboundDeliveryDailyTotalQty(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);
		} finally {
			sqlSession.close();
		}
	}
}
