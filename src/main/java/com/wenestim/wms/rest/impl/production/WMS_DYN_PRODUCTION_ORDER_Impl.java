package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.production.WMS_DYN_PRODUCTION_ORDER_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_DYN_PRODUCTION_ORDER_Impl
{
	private static final Logger LOGGER = Logger.getLogger(WMS_DYN_PRODUCTION_ORDER_Impl.class);

	public List<HashMap<String,Object>> getProductionOrderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_DYN_PRODUCTION_ORDER_Mapper mWMS_DYN_PRODUCTION_ORDER_Mapper = sqlSession.getMapper(WMS_DYN_PRODUCTION_ORDER_Mapper.class);
			return mWMS_DYN_PRODUCTION_ORDER_Mapper.getProductionOrderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateProductionOrder(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_PRODUCTION_ORDER_Mapper mWMS_DYN_PRODUCTION_ORDER_Mapper = sqlSession.getMapper(WMS_DYN_PRODUCTION_ORDER_Mapper.class);
			mWMS_DYN_PRODUCTION_ORDER_Mapper.updateProductionOrder(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
