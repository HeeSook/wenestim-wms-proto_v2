/**
 *
 */
package com.wenestim.wms.rest.impl.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_SALES;
import com.wenestim.wms.rest.mapper.outbound.WMS_DYN_SALES_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_SALES_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_SALES_Impl.class);

	public List<WMS_DYN_SALES> getAllSales() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			return mWMS_DYN_SALES_Mapper.getAllSales();
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_DYN_SALES> getAllSalesByCompany(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PL_NO") String PL_NO,
			@Param("MAIN_FLAG") String MAIN_FLAG, @Param("SALES_ORG_CODE") String SALES_ORG_CODE,
			@Param("SO_NO") String SO_NO, @Param("SO_TYPE") String SO_TYPE, @Param("SO_ITEM_NO") String SO_ITEM_NO, @Param("CUST_CODE") String CUST_CODE,
			@Param("DELIVERY_DATE1") String DELIVERY_DATE1, @Param("DELIVERY_DATE2") String DELIVERY_DATE2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			return mWMS_DYN_SALES_Mapper.getAllSalesByCompany(CLIENT_ID,COMPANY_CODE, PL_NO, MAIN_FLAG, SALES_ORG_CODE, SO_NO, SO_TYPE, SO_ITEM_NO,
					CUST_CODE, DELIVERY_DATE1, DELIVERY_DATE2);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertSales(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			mWMS_DYN_SALES_Mapper.insertSales(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateSales(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			mWMS_DYN_SALES_Mapper.updateSales(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteSales(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			mWMS_DYN_SALES_Mapper.deleteSales(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteAllSales(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("SO_NO") String SO_NO, @Param("usedId") String userId) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			LOGGER.info(COMPANY_CODE);
			LOGGER.info(SO_NO);
			mWMS_DYN_SALES_Mapper.deleteAllSales(CLIENT_ID,COMPANY_CODE,SO_NO,userId);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateHeader(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			mWMS_DYN_SALES_Mapper.updateHeader(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}
