/**
 *
 */
package com.wenestim.wms.rest.impl.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;
import com.wenestim.wms.rest.mapper.outbound.WMS_DYN_PICKING_Mapper;
import com.wenestim.wms.rest.mapper.outbound.WMS_DYN_SALES_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PICKING_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PICKING_Impl.class);

	public List<WMS_DYN_PICKING> getAllPicking() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getAllPicking();
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_DYN_PICKING> getAllPickingByCompany(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PL_NO") String PL_NO,
			@Param("CUST_CODE") String CUST_CODE,@Param("HEADER_FLAG") String HEADER_FLAG,
			@Param("CREATE_DATE1") String CREATE_DATE1, @Param("CREATE_DATE2") String CREATE_DATE2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getAllPickingByCompany(CLIENT_ID,COMPANY_CODE, PL_NO,CUST_CODE, HEADER_FLAG, CREATE_DATE1, CREATE_DATE2);
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_DYN_PICKING> getAllPickingByCompanyGI(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("PL_NO") String PL_NO,
			@Param("CUST_CODE") String CUST_CODE,@Param("HEADER_FLAG") String HEADER_FLAG,
			@Param("CREATE_DATE1") String CREATE_DATE1, @Param("CREATE_DATE2") String CREATE_DATE2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getAllPickingByCompanyGI(CLIENT_ID,COMPANY_CODE, PL_NO,CUST_CODE, HEADER_FLAG, CREATE_DATE1, CREATE_DATE2);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertSales(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_SALES_Mapper mWMS_DYN_SALES_Mapper = sqlSession.getMapper(WMS_DYN_SALES_Mapper.class);
			mWMS_DYN_SALES_Mapper.insertSales(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_DYN_PICKING> getCurrentTodayPickingNo(@Param("CREATE_DATE") String CREATE_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getCurrentTodayPickingNo(CREATE_DATE);
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_DYN_PICKING> getPickingStatusList(
			@Param("CLIENT_ID"        ) String CLIENT_ID	    ,
			@Param("COMPANY_CODE"     ) String COMPANY_CODE	    ,
			@Param("PL_NO"            ) String PL_NO			,
			@Param("CUST_CODE"        ) String CUST_CODE		,
			@Param("STATUS"           ) String STATUS           ,
			@Param("FROM_PICKING_DATE") String FROM_PICKING_DATE,
			@Param("TO_PICKING_DATE"  ) String TO_PICKING_DATE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getPickingStatusList(
					CLIENT_ID   ,
					COMPANY_CODE,
					PL_NO       ,
					CUST_CODE   ,
					STATUS      ,
					FROM_PICKING_DATE, TO_PICKING_DATE
					);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertPicking(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.insertPicking(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean insertPickingPDA(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.insertPickingPDA(oWMS_DYN_PICKING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePicking(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.updatePicking(oWMS_DYN_PICKING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean confirmPackingStatus(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.confirmPackingStatus(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}


	// GWANGHYUN KIM 4/8/2018
	public Boolean scanUpdatePicking(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.scanUpdatePicking(oWMS_DYN_PICKING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deletePicking(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.deletePicking(oWMS_DYN_PICKING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	// GWANGHYUN KIM 8/20/2018
	public List<WMS_DYN_PICKING> getSOItem(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("SO_NO"        ) String SO_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) String SO_ITEM_NO   ,
			@Param("ISSUE_DATE"   ) String ISSUE_DATE   ,
			@Param("GET_BARCODE"  ) String GET_BARCODE
			) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getSOItem(CLIENT_ID, COMPANY_CODE, SO_NO, MATERIAL_CODE, SO_ITEM_NO, ISSUE_DATE, GET_BARCODE);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean updateSOBarCode(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.updateSOBarCode(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>>  getSoItemNo(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("SO_NO"        ) String SO_NO

			) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getSoItemNo(CLIENT_ID, COMPANY_CODE, SO_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public float getOrderQty(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getOrderQty(CLIENT_ID, COMPANY_CODE, PL_NO, MATERIAL_CODE, SO_ITEM_NO, SO_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public float getScanQty(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getScanQty(CLIENT_ID, COMPANY_CODE, PL_NO, MATERIAL_CODE, SO_ITEM_NO, SO_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public Boolean chkExistsPicking(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PL_NO"       ) String PL_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkExistsPicking(CLIENT_ID, COMPANY_CODE, PL_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public Boolean chkExistsPickingByBarcode_NO(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkExistsPickingByBarcode_NO(CLIENT_ID, COMPANY_CODE, BARCODE_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public Boolean chkExistsPickingMaterial(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PL_NO"        ) String PL_NO       ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkExistsPickingMaterial(CLIENT_ID, COMPANY_CODE, PL_NO, MATERIAL_CODE);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public Boolean chkExistsItemNo(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO     ,
			@Param("SO_NO"        ) String SO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkExistsItemNo(CLIENT_ID, COMPANY_CODE, PL_NO, MATERIAL_CODE, SO_ITEM_NO, SO_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	public Boolean chkItemScanStatus(
			@Param("CLIENT_ID"    ) String CLIENT_ID    ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE ,
			@Param("PL_NO"        ) String PL_NO        ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,
			@Param("SO_ITEM_NO"   ) int SO_ITEM_NO      ,
			@Param("SO_NO"        ) String SO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkItemScanStatus(CLIENT_ID, COMPANY_CODE, PL_NO, MATERIAL_CODE, SO_ITEM_NO, SO_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 5/30/2018
	public Boolean chkPickingScanStatus(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PL_NO"       ) String PL_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.chkPickingScanStatus(CLIENT_ID, COMPANY_CODE, PL_NO);
		} finally {
			sqlSession.close();
		}
	}

	// GWANGHYUN KIM 5/30/2018
	public Boolean updateScanQty(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.updateScanQty(oWMS_DYN_PICKING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	// GWANGHYUN KIM 5/29/2018
	public WMS_DYN_PICKING getCurrentPickingData(WMS_DYN_PICKING oWMS_DYN_PICKING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			return mWMS_DYN_PICKING_Mapper.getCurrentPickingData(oWMS_DYN_PICKING);
		} finally {
			sqlSession.close();
		}
	};

	public Boolean updateSgiEaccountNo(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_DYN_PICKING_Mapper mWMS_DYN_PICKING_Mapper = sqlSession.getMapper(WMS_DYN_PICKING_Mapper.class);
			mWMS_DYN_PICKING_Mapper.updateSgiEaccountNo(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}


}
