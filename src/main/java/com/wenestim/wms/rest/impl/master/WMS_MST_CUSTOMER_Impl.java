/**
 *
 */
package com.wenestim.wms.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_CUSTOMER;
import com.wenestim.wms.rest.mapper.master.WMS_MST_CUSTOMER_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_CUSTOMER_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_CUSTOMER_Impl.class);

	public List<WMS_MST_CUSTOMER> getAllCustomer() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			return mWMS_MST_CUSTOMER_Mapper.getAllCustomer();
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_MST_CUSTOMER> getAllCustomerByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			return mWMS_MST_CUSTOMER_Mapper.getAllCustomerByCompany(CLIENT_ID, COMPANY_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.insertCustomer(oWMS_MST_CUSTOMER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.updateCustomer(oWMS_MST_CUSTOMER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCustomer(WMS_MST_CUSTOMER oWMS_MST_CUSTOMER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.deleteCustomer(oWMS_MST_CUSTOMER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_CUSTOMER> getCustomerList(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CUST_CODE") String CUST_CODE,
			@Param("SALES_ORG_CODE") String SALES_ORG_CODE,@Param("SALES_DIV_CODE") String SALES_DIV_CODE,@Param("CUST_NAME") String CUST_NAME, @Param("DEL_FLAG") String DEL_FLAG) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			return mWMS_MST_CUSTOMER_Mapper.getCustomerList(CLIENT_ID, COMPANY_CODE, CUST_CODE, SALES_ORG_CODE, SALES_DIV_CODE, CUST_NAME, DEL_FLAG);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertCustomerList(HashMap<String, Object> customerList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.insertCustomerList(customerList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCustomerList(HashMap<String, Object> customerList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.updateCustomerList(customerList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCustomerList(HashMap<String, Object> customerList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.deleteCustomerList(customerList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			return mWMS_MST_CUSTOMER_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_CUSTOMER_Mapper mWMS_MST_CUSTOMER_Mapper = sqlSession.getMapper(WMS_MST_CUSTOMER_Mapper.class);
			mWMS_MST_CUSTOMER_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
