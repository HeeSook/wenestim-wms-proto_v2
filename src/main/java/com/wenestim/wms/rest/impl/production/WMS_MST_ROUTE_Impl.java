/**
 *
 */
package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.production.WMS_MST_ROUTE;
import com.wenestim.wms.rest.mapper.production.WMS_MST_ROUTE_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_ROUTE_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_MST_ROUTE_Impl.class);

	public List<WMS_MST_ROUTE> getRouteList(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_ROUTE_Mapper mWMS_MST_ROUTE_Mapper = sqlSession.getMapper(WMS_MST_ROUTE_Mapper.class);
			return mWMS_MST_ROUTE_Mapper.getRouteList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateRouteList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_MST_ROUTE_Mapper mWMS_MST_ROUTE_Mapper = sqlSession.getMapper(WMS_MST_ROUTE_Mapper.class);
			mWMS_MST_ROUTE_Mapper.updateRouteList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteRouteList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_MST_ROUTE_Mapper mWMS_MST_ROUTE_Mapper = sqlSession.getMapper(WMS_MST_ROUTE_Mapper.class);
			mWMS_MST_ROUTE_Mapper.deleteRouteList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
