/**
 *
 */
package com.wenestim.wms.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.mapper.master.WMS_MST_BOM_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_BOM_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_BOM_Impl.class);

	public List<WMS_MST_BOM> getAllBom(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			return mWMS_MST_BOM_Mapper.getAllBom(CLIENT_ID, COMPANY_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertBom(WMS_MST_BOM oWMS_MST_BOM) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.insertBom(oWMS_MST_BOM);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateBom(WMS_MST_BOM oWMS_MST_BOM) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.updateBom(oWMS_MST_BOM);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteBom(WMS_MST_BOM oWMS_MST_BOM) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.deleteBom(oWMS_MST_BOM);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_BOM> getBomProdList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			return mWMS_MST_BOM_Mapper.getBomProdList(CLIENT_ID, COMPANY_CODE, PROD_MATERIAL_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertBomProdList(HashMap<String, Object> bomProdList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.insertBomProdList(bomProdList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateBomProdList(HashMap<String, Object> bomProdList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.updateBomProdList(bomProdList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteBomProdList(HashMap<String, Object> bomProdList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.deleteBomProdList(bomProdList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_BOM> getBomCompList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			return mWMS_MST_BOM_Mapper.getBomCompList(CLIENT_ID, COMPANY_CODE, PROD_MATERIAL_CODE);
		} finally {
			sqlSession.close();
		}
	}


	public Boolean insertBomCompList(HashMap<String, Object> bomCompList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			result = mWMS_MST_BOM_Mapper.insertBomCompList(bomCompList);
			LOGGER.debug("-->>>>>>>>>>>>> RESULT:" + result);
			sqlSession.commit();
//			result = Boolean.valueOf(true);
		} catch (Exception e) {
//			result = Boolean.valueOf(false);
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateBomCompList(HashMap<String, Object> bomCompList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.updateBomCompList(bomCompList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteBomCompList(HashMap<String, Object> bomCompList) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.deleteBomCompList(bomCompList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			return mWMS_MST_BOM_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_BOM_Mapper mWMS_MST_BOM_Mapper = sqlSession.getMapper(WMS_MST_BOM_Mapper.class);
			mWMS_MST_BOM_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}
