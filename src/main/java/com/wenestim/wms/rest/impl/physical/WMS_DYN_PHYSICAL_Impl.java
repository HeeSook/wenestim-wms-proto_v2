/**
 *
 */
package com.wenestim.wms.rest.impl.physical;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.physical.WMS_DYN_PHYSICAL;
import com.wenestim.wms.rest.mapper.physical.WMS_DYN_PHYSICAL_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PHYSICAL_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PHYSICAL_Impl.class);

	public List<WMS_DYN_PHYSICAL> getAllPhysical(
            @Param("CLIENT_ID"    ) String CLIENT_ID    ,
    		@Param("COMPANY_CODE" ) String COMPANY_CODE ,
            @Param("PLANT_CODE"   ) String PLANT_CODE   ,
            @Param("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @Param("MATERIAL_TYPE") String MATERIAL_TYPE,
            @Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PHYSICAL_Mapper mWMS_DYN_PHYSICAL_Mapper = sqlSession.getMapper(WMS_DYN_PHYSICAL_Mapper.class);
			return mWMS_DYN_PHYSICAL_Mapper.getAllPhysical(
            		CLIENT_ID    ,
            		COMPANY_CODE ,
            		PLANT_CODE   ,
            		STORAGE_LOC  ,
            		MATERIAL_TYPE,
            		MATERIAL_CODE
					);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertPhysical(WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PHYSICAL_Mapper mWMS_DYN_PHYSICAL_Mapper = sqlSession.getMapper(WMS_DYN_PHYSICAL_Mapper.class);
			mWMS_DYN_PHYSICAL_Mapper.insertPhysical(oWMS_DYN_PHYSICAL);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePhysical(HashMap<String,Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PHYSICAL_Mapper mWMS_DYN_PHYSICAL_Mapper = sqlSession.getMapper(WMS_DYN_PHYSICAL_Mapper.class);
			mWMS_DYN_PHYSICAL_Mapper.updatePhysical(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deletePhysical(WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PHYSICAL_Mapper mWMS_DYN_PHYSICAL_Mapper = sqlSession.getMapper(WMS_DYN_PHYSICAL_Mapper.class);
			mWMS_DYN_PHYSICAL_Mapper.deletePhysical(oWMS_DYN_PHYSICAL);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	// GWANGHYUN KIM 4/7/2018
	public Boolean chkExistsPhysicalByBarcode_NO(
    		@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PHYSICAL_Mapper mWMS_DYN_PHYSICAL_Mapper = sqlSession.getMapper(WMS_DYN_PHYSICAL_Mapper.class);
			return mWMS_DYN_PHYSICAL_Mapper.chkExistsPhysicalByBarcode_NO(CLIENT_ID,COMPANY_CODE,BARCODE_NO);
		} finally {
			sqlSession.close();
		}
	}


}
