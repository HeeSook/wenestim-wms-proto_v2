package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.production.WMS_MST_PLAN;
import com.wenestim.wms.rest.mapper.production.WMS_MST_PLAN_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_MST_PLAN_Impl
{
	private static final Logger LOGGER = Logger.getLogger(WMS_MST_PLAN_Impl.class);

	public List<WMS_MST_PLAN> getPlanVerList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_PLAN_Mapper mWMS_MST_PLAN_Mapper = sqlSession.getMapper(WMS_MST_PLAN_Mapper.class);
			return mWMS_MST_PLAN_Mapper.getPlanVerList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public String createPlan(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_PLAN_Mapper mWMS_MST_PLAN_Mapper = sqlSession.getMapper(WMS_MST_PLAN_Mapper.class);
			sqlSession.commit();
			return mWMS_MST_PLAN_Mapper.createPlan(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updatePlan(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_PLAN_Mapper mWMS_MST_PLAN_Mapper = sqlSession.getMapper(WMS_MST_PLAN_Mapper.class);
			mWMS_MST_PLAN_Mapper.updatePlan(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean confirmPlan(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_PLAN_Mapper mWMS_MST_PLAN_Mapper = sqlSession.getMapper(WMS_MST_PLAN_Mapper.class);
			mWMS_MST_PLAN_Mapper.confirmPlan(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
