/**
 *
 */
package com.wenestim.wms.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_INFOCODE;
import com.wenestim.wms.rest.mapper.master.WMS_MST_INFOCODE_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_INFOCODE_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_INFOCODE_Impl.class);

	public List<WMS_MST_INFOCODE> getAllInfoCode() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			return mWMS_MST_INFOCODE_Mapper.getAllInfoCode();
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.insertInfoCode(oWMS_MST_INFOCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.updateInfoCode(oWMS_MST_INFOCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteInfoCode(WMS_MST_INFOCODE oWMS_MST_INFOCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.deleteInfoCode(oWMS_MST_INFOCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_INFOCODE> getInfoCodeList(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@Param("INFO_NO") String INFO_NO, @Param("CATEGORY") String CATEGORY, @Param("FROM_DATE") String FROM_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			return mWMS_MST_INFOCODE_Mapper.getInfoCodeList(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, MATERIAL_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, INFO_NO, CATEGORY, FROM_DATE);
		} finally {
			sqlSession.close();
		}

	}

	public List<WMS_MST_INFOCODE> getInfoCodeByValidDate(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("MATERIAL_CODE") String MATERIAL_CODE,@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@Param("VALID_DATE") String VALID_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			return mWMS_MST_INFOCODE_Mapper.getInfoCodeByValidDate(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, MATERIAL_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, VALID_DATE);
		} finally {
			sqlSession.close();
		}

	}

	public Boolean insertInfoCodeList(HashMap<String, Object> infoCodeList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.insertInfoCodeList(infoCodeList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateInfoCodeList(HashMap<String, Object> infoCodeList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.updateInfoCodeList(infoCodeList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteInfoCodeList(HashMap<String, Object> infoCodeList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.deleteInfoCodeList(infoCodeList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			return mWMS_MST_INFOCODE_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_INFOCODE_Mapper mWMS_MST_INFOCODE_Mapper = sqlSession.getMapper(WMS_MST_INFOCODE_Mapper.class);
			mWMS_MST_INFOCODE_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}
