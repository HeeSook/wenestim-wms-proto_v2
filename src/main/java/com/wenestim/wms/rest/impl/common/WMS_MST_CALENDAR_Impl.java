/**
 *
 */
package com.wenestim.wms.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.common.WMS_MST_CALENDAR_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_CALENDAR_Impl
{

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_CALENDAR_Impl.class);

	public List<HashMap<String,Object>> getDateList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_CALENDAR_Mapper mWMS_MST_CALENDAR_Mapper = sqlSession.getMapper(WMS_MST_CALENDAR_Mapper.class);
			return mWMS_MST_CALENDAR_Mapper.getDateList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getWeekList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_CALENDAR_Mapper mWMS_MST_CALENDAR_Mapper = sqlSession.getMapper(WMS_MST_CALENDAR_Mapper.class);
			return mWMS_MST_CALENDAR_Mapper.getWeekList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
