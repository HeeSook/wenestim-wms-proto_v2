package com.wenestim.wms.rest.impl.common;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.QueryParam;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.common.WMS_MST_COMPANY;
import com.wenestim.wms.rest.mapper.common.WMS_MST_COMPANY_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_COMPANY_Impl
{
	private final static Logger LOGGER = Logger.getLogger(WMS_MST_COMPANY_Impl.class);

	public List<WMS_MST_COMPANY> getCompanyList(
			@QueryParam("CLIENT_ID")    String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_COMPANY_Mapper mWMS_MST_COMPANY_Mapper = sqlSession.getMapper(WMS_MST_COMPANY_Mapper.class);
			return mWMS_MST_COMPANY_Mapper.getCompanyList(CLIENT_ID,COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateCompany(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_COMPANY_Mapper mWMS_MST_COMPANY_Mapper = sqlSession.getMapper(WMS_MST_COMPANY_Mapper.class);
			mWMS_MST_COMPANY_Mapper.updateCompany(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
