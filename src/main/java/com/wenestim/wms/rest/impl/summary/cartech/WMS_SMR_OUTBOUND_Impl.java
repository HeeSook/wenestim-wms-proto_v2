/**
 *
 */
package com.wenestim.wms.rest.impl.summary.cartech;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.cartech.WMS_SMR_OUTBOUND_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;



/**
 * @author Andrew
 *
 */
public class WMS_SMR_OUTBOUND_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_OUTBOUND_Impl.class);

	public HashMap<String, Object> getTotalSalesCount(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getTotalSalesCount(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getTotalSalesList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getTotalSalesList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDetailSalesList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_OUTBOUND_Mapper mWMS_SMR_OUTBOUND_Mapper = sqlSession.getMapper(WMS_SMR_OUTBOUND_Mapper.class);
			return mWMS_SMR_OUTBOUND_Mapper.getDetailSalesList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
