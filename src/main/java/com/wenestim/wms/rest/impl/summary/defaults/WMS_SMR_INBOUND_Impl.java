package com.wenestim.wms.rest.impl.summary.defaults;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.defaults.WMS_SMR_INBOUND_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_SMR_INBOUND_Impl {
	
	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_INBOUND_Impl.class);
	
	/**
	 * GET INBOUND ORDER DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundOrderDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundOrderDailyDetail(CLIENT_ID, COMPANY_CODE, PO_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getInboundOrderTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundOrderTop5ByVendor(CLIENT_ID, COMPANY_CODE, PO_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getInboundOrderDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundOrderDailyTotalQty(CLIENT_ID, COMPANY_CODE, PO_DATE);
		} finally {
			sqlSession.close();
		}
	}

	/**
	 * GET INBOUND RECEIVE DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundReceiveDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundReceiveDailyDetail(CLIENT_ID, COMPANY_CODE, GR_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getInboundReceiveTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundReceiveTop5ByVendor(CLIENT_ID, COMPANY_CODE, GR_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getInboundReceiveDailyTotalQty(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("GR_DATE") String GR_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundReceiveDailyTotalQty(CLIENT_ID, COMPANY_CODE, GR_DATE);
		} finally {
			sqlSession.close();
		}
	}

	/**
	 * GET INBOUND REMAIN DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return List<HashMap<?, ?>>
	 */
	public List<HashMap<?, ?>> getInboundRemainDailyDetail(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundRemainDailyDetail(CLIENT_ID, COMPANY_CODE, PO_DATE);
		} finally {
			sqlSession.close();
		}
	}
	
	public List<HashMap<?, ?>> getInboundRemainTop5ByVendor(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_DATE") String PO_DATE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getInboundRemainTop5ByVendor(CLIENT_ID, COMPANY_CODE, PO_DATE);
		} finally {
			sqlSession.close();
		}
	}
}
