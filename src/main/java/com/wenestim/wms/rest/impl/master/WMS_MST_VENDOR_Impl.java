/**
 *
 */
package com.wenestim.wms.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_VENDOR;
import com.wenestim.wms.rest.mapper.master.WMS_MST_VENDOR_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_VENDOR_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_VENDOR_Impl.class);

	public List<WMS_MST_VENDOR> getAllVendor() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			return mWMS_MST_VENDOR_Mapper.getAllVendor();
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertVendor(WMS_MST_VENDOR oWMS_MST_VENDOR) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.insertVendor(oWMS_MST_VENDOR);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateVendor(WMS_MST_VENDOR oWMS_MST_VENDOR) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.updateVendor(oWMS_MST_VENDOR);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteVendor(WMS_MST_VENDOR oWMS_MST_VENDOR) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.deleteVendor(oWMS_MST_VENDOR);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_VENDOR> getVendorList(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE,@Param("VENDOR_CODE") String VENDOR_CODE,
			@Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE, @Param("VENDOR_NAME") String VENDOR_NAME, @Param("DEL_FLAG") String DEL_FLAG) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			return mWMS_MST_VENDOR_Mapper.getVendorList(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, VENDOR_NAME, DEL_FLAG);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertVendorList(HashMap<String, Object> vendorList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.insertVendorList(vendorList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateVendorList(HashMap<String, Object> vendorList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.updateVendorList(vendorList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteVendorList(HashMap<String, Object> vendorList){
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.deleteVendorList(vendorList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public List<WMS_MST_VENDOR> getVendorCodeByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE, @Param("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			return mWMS_MST_VENDOR_Mapper.getVendorCodeByCompany(CLIENT_ID, COMPANY_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			return mWMS_MST_VENDOR_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_VENDOR_Mapper mWMS_MST_VENDOR_Mapper = sqlSession.getMapper(WMS_MST_VENDOR_Mapper.class);
			mWMS_MST_VENDOR_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
