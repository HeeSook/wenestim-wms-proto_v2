package com.wenestim.wms.rest.impl.summary.cartech;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.cartech.WMS_SMR_INBOUND_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_SMR_INBOUND_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_INBOUND_Impl.class);

	public HashMap<String, Object> getTotalOrderCount(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getTotalOrderCount(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getTotalOrderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getTotalOrderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDetailOrderList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INBOUND_Mapper mWMS_SMR_INBOUND_Mapper = sqlSession.getMapper(WMS_SMR_INBOUND_Mapper.class);
			return mWMS_SMR_INBOUND_Mapper.getDetailOrderList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

}
