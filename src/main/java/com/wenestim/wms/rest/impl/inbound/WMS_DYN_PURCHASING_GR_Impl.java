/**
 *
 */
package com.wenestim.wms.rest.impl.inbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING_GR;
import com.wenestim.wms.rest.mapper.inbound.WMS_DYN_PURCHASING_GR_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PURCHASING_GR_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PURCHASING_GR_Impl.class);

	public List<WMS_DYN_PURCHASING_GR> getAllPurchasingGr(
            @Param("CLIENT_ID"         ) String CLIENT_ID         ,
            @Param("COMPANY_CODE"      ) String COMPANY_CODE      ,
            @Param("PLANT_CODE"        ) String PLANT_CODE        ,
            @Param("PO_NO"             ) String PO_NO             ,
            @Param("VENDOR_CODE"       ) String VENDOR_CODE       ,
            @Param("FROM_PO_DATE"      ) String FROM_PO_DATE      ,
            @Param("TO_PO_DATE"        ) String TO_PO_DATE        ,
            @Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
            @Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE  ,
            @Param("POST_DATE"         ) String POST_DATE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			return mWMS_DYN_PURCHASING_GR_Mapper.getAllPurchasingGr(
					CLIENT_ID    ,
					COMPANY_CODE ,
					PLANT_CODE   ,
					PO_NO        ,
					VENDOR_CODE  ,
					FROM_PO_DATE ,     TO_PO_DATE      ,
					FROM_DELIVERY_DATE,TO_DELIVERY_DATE,
					POST_DATE
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertPurchasingGr(WMS_DYN_PURCHASING_GR oWMS_DYN_PURCHASING_GR) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			mWMS_DYN_PURCHASING_GR_Mapper.insertPurchasingGr(oWMS_DYN_PURCHASING_GR);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePurchasingGr(HashMap dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			mWMS_DYN_PURCHASING_GR_Mapper.updatePurchasingGr(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deletePurchasingGr(HashMap dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			mWMS_DYN_PURCHASING_GR_Mapper.deletePurchasingGr(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}


	// GWANGHYUN KIM 4/7/2018
	public Boolean chkExistsPurchasingByBarcode_NO(
			@Param("CLIENT_ID"   ) String CLIENT_ID	  ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("BARCODE_NO"  ) String BARCODE_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			return mWMS_DYN_PURCHASING_GR_Mapper.chkExistsPurchasingByBarcode_NO(CLIENT_ID,COMPANY_CODE,BARCODE_NO);
		} finally {
			sqlSession.close();
		}
	}

	public float getGR_Qty(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		float grQty = 0;
		try {
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			grQty = mWMS_DYN_PURCHASING_GR_Mapper.getGR_Qty(CLIENT_ID, COMPANY_CODE, PO_NO, PO_ITEM_NO, MATERIAL_CODE);
			LOGGER.info("GR_QTY1 is "+grQty+" from implement");
			return grQty;
		}catch (Exception e) {
			return grQty;
		}finally {
			sqlSession.close();
		}
	}

	public Boolean updateGRstatus(
			@Param("STATUS"       ) String STATUS      ,
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_GR_Mapper mWMS_DYN_PURCHASING_GR_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_GR_Mapper.class);
			mWMS_DYN_PURCHASING_GR_Mapper.updateGRstatus(STATUS, CLIENT_ID, COMPANY_CODE, PO_NO, PO_ITEM_NO, MATERIAL_CODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

}

