/**
 *
 */
package com.wenestim.wms.rest.impl.interfaces;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.interfaces.WMS_IFS_RECEIVE_MES_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_IFS_RECEIVE_MES_Impl
{

	private final static Logger LOGGER = Logger.getLogger(WMS_IFS_RECEIVE_MES_Impl.class);

	public String insertMesActualData(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		String result = "";

		try
		{
			WMS_IFS_RECEIVE_MES_Mapper mWMS_IFS_RECEIVE_MES_Mapper = sqlSession.getMapper(WMS_IFS_RECEIVE_MES_Mapper.class);
			mWMS_IFS_RECEIVE_MES_Mapper.insertMesActualData(dataMap);
			sqlSession.commit();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
			result = e.getMessage();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public String insertMesSpmData(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		String result = "";

		try
		{
			WMS_IFS_RECEIVE_MES_Mapper mWMS_IFS_RECEIVE_MES_Mapper = sqlSession.getMapper(WMS_IFS_RECEIVE_MES_Mapper.class);
			mWMS_IFS_RECEIVE_MES_Mapper.insertMesSpmData(dataMap);
			sqlSession.commit();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
			result = e.getMessage();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
