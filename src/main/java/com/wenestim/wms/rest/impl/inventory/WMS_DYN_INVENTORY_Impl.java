/**
 *
 */
package com.wenestim.wms.rest.impl.inventory;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_INVENTORY;
import com.wenestim.wms.rest.mapper.inventory.WMS_DYN_INVENTORY_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_INVENTORY_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_INVENTORY_Impl.class);

	public List<WMS_DYN_INVENTORY> getAllInventory(
            @Param("CLIENT_ID"    ) String CLIENT_ID    ,
            @Param("COMPANY_CODE" ) String COMPANY_CODE ,
            @Param("PLANT_CODE"   ) String PLANT_CODE   ,
            @Param("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @Param("MATERIAL_TYPE") String MATERIAL_TYPE,
            @Param("MATERIAL_CODE") String MATERIAL_CODE,
            @Param("INCLUDE_ZERO" ) String INCLUDE_ZERO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			return mWMS_DYN_INVENTORY_Mapper.getAllInventory(
            		CLIENT_ID    ,
            		COMPANY_CODE ,
            		PLANT_CODE   ,
            		STORAGE_LOC  ,
            		MATERIAL_TYPE,
            		MATERIAL_CODE,
            		INCLUDE_ZERO
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			return mWMS_DYN_INVENTORY_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			mWMS_DYN_INVENTORY_Mapper.insertInventory(oWMS_DYN_INVENTORY);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			mWMS_DYN_INVENTORY_Mapper.updateInventory(oWMS_DYN_INVENTORY);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteInventory(WMS_DYN_INVENTORY oWMS_DYN_INVENTORY) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			mWMS_DYN_INVENTORY_Mapper.deleteInventory(oWMS_DYN_INVENTORY);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_INVENTORY_Mapper mWMS_DYN_INVENTORY_Mapper = sqlSession.getMapper(WMS_DYN_INVENTORY_Mapper.class);
			mWMS_DYN_INVENTORY_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}


}
