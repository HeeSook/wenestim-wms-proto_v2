package com.wenestim.wms.rest.impl.inventory;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_LEDGER;
import com.wenestim.wms.rest.mapper.inventory.WMS_DYN_LEDGER_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_DYN_LEDGER_Impl
{
	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_LEDGER_Impl.class);

	public List<WMS_DYN_LEDGER> getMaterialLedgerList(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_LEDGER_Mapper mWMS_DYN_LEDGER_Mapper = sqlSession.getMapper(WMS_DYN_LEDGER_Mapper.class);
			return mWMS_DYN_LEDGER_Mapper.getMaterialLedgerList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
