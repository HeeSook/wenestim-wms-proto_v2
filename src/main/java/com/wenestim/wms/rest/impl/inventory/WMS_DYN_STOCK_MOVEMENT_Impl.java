/**
 *
 */
package com.wenestim.wms.rest.impl.inventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.wenestim.wms.rest.entity.inventory.WMS_DYN_INVENTORY;
import com.wenestim.wms.rest.entity.inventory.WMS_DYN_STOCK_MOVEMENT;
import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.entity.production.WMS_DYN_BACKLOG;
import com.wenestim.wms.rest.entity.production.WMS_EXC_INVENTORY;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.impl.production.WMS_EXC_MRP_Impl;
import com.wenestim.wms.rest.mapper.inventory.WMS_DYN_STOCK_MOVEMENT_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_STOCK_MOVEMENT_Impl {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_STOCK_MOVEMENT_Impl.class);

    static List<WMS_MST_BOM>       bomList = null;
	static List<WMS_EXC_INVENTORY> invList = null;
	static List<WMS_DYN_BACKLOG>   backlogList = null;

    public List<WMS_DYN_STOCK_MOVEMENT> getInventoryList(
            @Param("CLIENT_ID"       ) String CLIENT_ID       ,
            @Param("COMPANY_CODE"    ) String COMPANY_CODE    ,
            @Param("PLANT_CODE"      ) String PLANT_CODE      ,
            @Param("MOVEMENT_TYPE"   ) String MOVEMENT_TYPE   ,
            @Param("CURR_STORAGE_LOC") String CURR_STORAGE_LOC,
            @Param("MOVE_STORAGE_LOC") String MOVE_STORAGE_LOC,
            @Param("MATERIAL_CODE"   ) String MATERIAL_CODE   ,
            @Param("POST_DATE"       ) String POST_DATE
            )
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try
        {
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
            return mWMS_DYN_STOCK_MOVEMENT_Mapper.getInventoryList(
            		CLIENT_ID        ,
            		COMPANY_CODE     ,
            		PLANT_CODE       ,
            		MOVEMENT_TYPE    ,
            		CURR_STORAGE_LOC ,
            		MOVE_STORAGE_LOC ,
            		MATERIAL_CODE    ,
            		POST_DATE
                    );
        }
        finally
        {
            sqlSession.close();
        }
    }

//	public List<WMS_DYN_STOCK_MOVEMENT> getLatestMovementList(HashMap<String, Object> paramMap)
//	{
//        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
//        try
//        {
//            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
//            return mWMS_DYN_STOCK_MOVEMENT_Mapper.getLatestMovementList( paramMap );
//        }
//        finally
//        {
//            sqlSession.close();
//        }
//	}


    public List<WMS_DYN_STOCK_MOVEMENT> getMovementList(
            @Param("CLIENT_ID"     ) String CLIENT_ID     ,
            @Param("COMPANY_CODE"  ) String COMPANY_CODE  ,
            @Param("PLANT_CODE"    ) String PLANT_CODE    ,
            @Param("MOVEMENT_TYPE" ) String MOVEMENT_TYPE ,
            @Param("STORAGE_LOC"   ) String STORAGE_LOC   ,
            @Param("MATERIAL_TYPE" ) String MATERIAL_TYPE ,
            @Param("MATERIAL_CODE" ) String MATERIAL_CODE ,
            @Param("FROM_POST_DATE") String FROM_POST_DATE,
            @Param("TO_POST_DATE"  ) String TO_POST_DATE
            )
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        try
        {
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
            return mWMS_DYN_STOCK_MOVEMENT_Mapper.getMovementList(
                    CLIENT_ID     ,
                    COMPANY_CODE  ,
                    PLANT_CODE    ,
                    MOVEMENT_TYPE ,
                    STORAGE_LOC   ,
                    MATERIAL_TYPE ,
                    MATERIAL_CODE ,
                    FROM_POST_DATE, TO_POST_DATE
                    );
        }
        finally
        {
            sqlSession.close();
        }
    }

    public Boolean insertStockMovement(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
            mWMS_DYN_STOCK_MOVEMENT_Mapper.insertStockMovement(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean updateStockMovement(WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try {
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
            mWMS_DYN_STOCK_MOVEMENT_Mapper.updateStockMovement(oWMS_DYN_STOCK_MOVEMENT);
            sqlSession.commit();
            result = true;
        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

    public Boolean deleteStockMovement(WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT) {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try {
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
            mWMS_DYN_STOCK_MOVEMENT_Mapper.deleteStockMovement(oWMS_DYN_STOCK_MOVEMENT);
            sqlSession.commit();
            result = true;
        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        } finally {
            sqlSession.close();
        }
        return result;
    }

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
            WMS_DYN_STOCK_MOVEMENT_Mapper mWMS_DYN_STOCK_MOVEMENT_Mapper = sqlSession.getMapper(WMS_DYN_STOCK_MOVEMENT_Mapper.class);
			return mWMS_DYN_STOCK_MOVEMENT_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}


    public boolean insertStockMovement(String inputType, JSONArray jsArray, String userId, String clientId, String companyCode) throws Exception
    {
        return insertStockMovement(inputType,jsArray,userId,clientId,companyCode,false);
    }

    public boolean insertStockMovement(String inputType, JSONArray jsArray, String userId, String clientId, String companyCode, boolean mergeDocNo) throws Exception
    {
    	Boolean backFlush = false;
    	Boolean result    = true;
        try
        {
        	bomList     = new ArrayList<WMS_MST_BOM>()      ;
        	invList     = new ArrayList<WMS_EXC_INVENTORY>();
        	backlogList = new ArrayList<WMS_DYN_BACKLOG>();
        	int DOC_NO  = -1;
            String movementType = "";

            if(jsArray.length() > 0)
            {
            	movementType = jsArray.getJSONObject(0).getString("MOVEMENT_TYPE");
            }


        	if( inputType.equals("MOVEMENT") || inputType.equals("PROD_ORDER") || inputType.equals("MES_IF"))
        	{
        		if( movementType.equals("100") || movementType.equals("101") )
        		{
        			bomList   = getBomList(clientId,companyCode);
    	        	invList   = getInvList(clientId,companyCode);
    	    		backFlush = true;
        		}
        	}

            List<WMS_DYN_STOCK_MOVEMENT> dataList = new ArrayList<WMS_DYN_STOCK_MOVEMENT>();
            if( mergeDocNo )
            {
            	WMS_MST_CODERULE_Impl  iWMS_MST_CODERULE_Impl  = new WMS_MST_CODERULE_Impl();
            	DOC_NO = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(clientId, companyCode, "INV", "DOC_NO", 1));
            }


            for (int i=0; i<jsArray.length(); i++)
            {

                String CLIENT_ID          = jsArray.getJSONObject(i).has("CLIENT_ID"         ) ? jsArray.getJSONObject(i).getString("CLIENT_ID"         ) : "";
                String COMPANY_CODE       = jsArray.getJSONObject(i).has("COMPANY_CODE"      ) ? jsArray.getJSONObject(i).getString("COMPANY_CODE"      ) : "";
                String PLANT_CODE         = jsArray.getJSONObject(i).has("PLANT_CODE"        ) ? jsArray.getJSONObject(i).getString("PLANT_CODE"        ) : "";
                String MATERIAL_CODE      = jsArray.getJSONObject(i).has("MATERIAL_CODE"     ) ? jsArray.getJSONObject(i).getString("MATERIAL_CODE"     ) : "";
                String POST_DATE          = jsArray.getJSONObject(i).has("POST_DATE"         ) ? jsArray.getJSONObject(i).getString("POST_DATE"         ) : "";
                String MOVEMENT_TYPE      = jsArray.getJSONObject(i).has("MOVEMENT_TYPE"     ) ? jsArray.getJSONObject(i).getString("MOVEMENT_TYPE"     ) : "";
                String CURR_STORAGE_LOC   = jsArray.getJSONObject(i).has("CURR_STORAGE_LOC"  ) ? jsArray.getJSONObject(i).getString("CURR_STORAGE_LOC"  ) : "";
                Double CURR_QTY           = jsArray.getJSONObject(i).has("CURR_QTY"          ) ? jsArray.getJSONObject(i).getDouble("CURR_QTY"          ) : 0;
                String MOVE_MATERIAL_CODE = jsArray.getJSONObject(i).has("MOVE_MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MOVE_MATERIAL_CODE") : "";
                String MOVE_STORAGE_LOC   = jsArray.getJSONObject(i).has("MOVE_STORAGE_LOC"  ) ? jsArray.getJSONObject(i).getString("MOVE_STORAGE_LOC"  ) : "";
                Double MOVE_QTY           = jsArray.getJSONObject(i).has("MOVE_QTY"          ) ? jsArray.getJSONObject(i).getDouble("MOVE_QTY"          ) : 0;
                String UNIT               = jsArray.getJSONObject(i).has("UNIT"              ) ? jsArray.getJSONObject(i).getString("UNIT"              ) : "";
                String USER_ID            = jsArray.getJSONObject(i).has("USER_ID"           ) ? jsArray.getJSONObject(i).getString("USER_ID"           ) : "";
                String EDIT_FLAG          = jsArray.getJSONObject(i).has("EDIT_FLAG"         ) ? jsArray.getJSONObject(i).getString("EDIT_FLAG"         ) : "Y";
                String SHIFT              = jsArray.getJSONObject(i).has("SHIFT"             ) ? jsArray.getJSONObject(i).getString("SHIFT"             ) : "1";

                if( CURR_QTY == 0 && MOVE_QTY == 0 )
                {
                	continue;
                }
                if( clientId != null && clientId.length()>0)
                {
                	CLIENT_ID = clientId;
                }
                if( companyCode != null && companyCode.length()>0)
                {
                	COMPANY_CODE = companyCode;
                }
                if( userId != null && userId.length()>0)
                {
                	USER_ID = userId;
                }
                if(MOVE_MATERIAL_CODE == null || MOVE_MATERIAL_CODE.equals(""))
                {
                	MOVE_MATERIAL_CODE = MATERIAL_CODE;
                }

                WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT =
                		setStockMovement(
                				dataList,
                				CLIENT_ID         ,
                				COMPANY_CODE      ,
                				DOC_NO            ,
                				PLANT_CODE        ,
                				MATERIAL_CODE     ,
                				POST_DATE         ,
                                SHIFT             ,
                				MOVEMENT_TYPE     ,
                				CURR_STORAGE_LOC  ,
                				CURR_QTY          ,
                				MOVE_MATERIAL_CODE,
                				MOVE_STORAGE_LOC  ,
                				MOVE_QTY          ,
                				UNIT              ,
                                0                 ,
                				inputType         ,
                				USER_ID
                				);
                if( backFlush )
                {
                	createProdBackFlush(inputType, dataList, oWMS_DYN_STOCK_MOVEMENT);
                }
            }

            if( dataList.size() > 0 )
            {
                HashMap<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("CLIENT_ID"   , dataList.get(0).getCLIENT_ID()   );
                dataMap.put("COMPANY_CODE", dataList.get(0).getCOMPANY_CODE());
                dataMap.put("dataList"    , dataList   );
                dataMap.put("backlogList" , backlogList);
                result = insertStockMovement(dataMap);
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            result = false;
        }
        return result;
    }


    private WMS_DYN_STOCK_MOVEMENT setStockMovement(
    		List<WMS_DYN_STOCK_MOVEMENT> dataList,
    		String CLIENT_ID          ,
    		String COMPANY_CODE       ,
    		int    DOC_NO             ,
    		String PLANT_CODE         ,
    		String MATERIAL_CODE      ,
    		String POST_DATE          ,
    		String SHIFT              ,
    		String MOVEMENT_TYPE      ,
    		String CURR_STORAGE_LOC   ,
    		Double CURR_QTY           ,
    		String MOVE_MATERIAL_CODE ,
    		String MOVE_STORAGE_LOC   ,
    		Double MOVE_QTY           ,
    		String UNIT               ,
    		int    REF_DOC_NO         ,
    		String INPUT_TYPE         ,
    		String USER_ID
    		) throws Exception
    {
    	WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT = new WMS_DYN_STOCK_MOVEMENT();
        WMS_MST_CODERULE_Impl  iWMS_MST_CODERULE_Impl  = new WMS_MST_CODERULE_Impl();

        int docNo = DOC_NO;
        if( docNo == -1 )
        {
        	docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID, COMPANY_CODE, "INV", "DOC_NO", 1));
        }

        oWMS_DYN_STOCK_MOVEMENT.setCLIENT_ID(CLIENT_ID)                  ;
        oWMS_DYN_STOCK_MOVEMENT.setCOMPANY_CODE(COMPANY_CODE)            ;
        oWMS_DYN_STOCK_MOVEMENT.setDOC_NO(docNo)                         ;
        oWMS_DYN_STOCK_MOVEMENT.setPLANT_CODE(PLANT_CODE)                ;
        oWMS_DYN_STOCK_MOVEMENT.setMATERIAL_CODE(MATERIAL_CODE)          ;
        oWMS_DYN_STOCK_MOVEMENT.setPOST_DATE(POST_DATE)                  ;
        oWMS_DYN_STOCK_MOVEMENT.setSHIFT(SHIFT)                          ;
        oWMS_DYN_STOCK_MOVEMENT.setMOVEMENT_TYPE(MOVEMENT_TYPE)          ;
        oWMS_DYN_STOCK_MOVEMENT.setCURR_STORAGE_LOC(CURR_STORAGE_LOC)    ;
        oWMS_DYN_STOCK_MOVEMENT.setCURR_QTY(CURR_QTY)                    ;
        oWMS_DYN_STOCK_MOVEMENT.setMOVE_MATERIAL_CODE(MOVE_MATERIAL_CODE);
        oWMS_DYN_STOCK_MOVEMENT.setMOVE_STORAGE_LOC(MOVE_STORAGE_LOC)    ;
        oWMS_DYN_STOCK_MOVEMENT.setMOVE_QTY(MOVE_QTY)                    ;
        oWMS_DYN_STOCK_MOVEMENT.setUNIT(UNIT)                            ;
        oWMS_DYN_STOCK_MOVEMENT.setINPUT_TYPE(INPUT_TYPE)                ;
        oWMS_DYN_STOCK_MOVEMENT.setCREATE_USER(USER_ID)                  ;
        oWMS_DYN_STOCK_MOVEMENT.setREF_DOC_NO(REF_DOC_NO)                ;
        dataList.add(oWMS_DYN_STOCK_MOVEMENT);

        return oWMS_DYN_STOCK_MOVEMENT;
    }

    private List<WMS_MST_BOM> getBomList(String clientId,String companyCode)
	{
		WMS_EXC_MRP_Impl implName = new WMS_EXC_MRP_Impl();
		return implName.getBomList(clientId,companyCode);
	}

	private List<WMS_EXC_INVENTORY> getInvList(String clientId,String companyCode)
	{
		WMS_EXC_MRP_Impl implName = new WMS_EXC_MRP_Impl();
		return implName.getInvList(clientId,companyCode);
	}

	private void createProdBackFlush(String inputType, List<WMS_DYN_STOCK_MOVEMENT> dataList, WMS_DYN_STOCK_MOVEMENT stockMovement)
	{
		try
		{
			int    docNo        = stockMovement.getDOC_NO()        ;
			String plantCode    = stockMovement.getPLANT_CODE()    ;
			String materialCode = stockMovement.getMATERIAL_CODE() ;
			String postDate     = stockMovement.getPOST_DATE()     ;
			String shift        = stockMovement.getSHIFT()         ;
			float  prodActQty   = Math.abs(new Float(stockMovement.getCURR_QTY()));
			String movementType = stockMovement.getMOVEMENT_TYPE() ;
			String userId       = stockMovement.getCREATE_USER()   ;

			List<WMS_MST_BOM> myBomList = bomList.stream()
		                .filter(item ->	 item.getPROD_PLANT_CODE().equals(plantCode)       &&
		                				 item.getPROD_MATERIAL_CODE().equals(materialCode) &&
		                				!item.getCOMP_MATERIAL_CODE().equals("-")
		                				)
		                .collect(Collectors.toList());

//			System.out.println("myBomList.size():"+myBomList.size());
			for(int i=0; i<myBomList.size(); i++)
			{
	        	WMS_MST_BOM bomData      = myBomList.get(i);
	        	String  actMoveType      = "200";
	        	String  clientId         = bomData.getCLIENT_ID()         ;
	            String  companyCode      = bomData.getCOMPANY_CODE()      ;
	            String  compPlantCode    = bomData.getCOMP_PLANT_CODE()   ;
	            String  compMaterialCode = bomData.getCOMP_MATERIAL_CODE();
	            float   compQty          = bomData.getCOMP_QTY()          ;
	            String  compUnit         = bomData.getCOMP_UNIT()         ;
	            String  compStorageLoc   = bomData.getCOMP_STORAGE_LOC()  ;
	            float   compDemandQty    = prodActQty * compQty           ;

	            WMS_DYN_BACKLOG backlogData    = new WMS_DYN_BACKLOG();
	            if(movementType.equals("100"))
	            {
	            	assignInvQty(backlogData,compPlantCode,compMaterialCode,compStorageLoc,compDemandQty);
	            }

	            float invQty    = backlogData.getINV_QTY()    ;
	            float availQty  = backlogData.getAVAIL_QTY()  ;
	            float assignQty = backlogData.getASSIGN_QTY() ;
	            float backlog   = backlogData.getBACKLOG_QTY();
	            Double moveQty = new Double(assignQty) * -1;
	            if(movementType.equals("101"))
	            {
	            	actMoveType = "201";
	            	moveQty     = new Double(compDemandQty);
	            }

//	            System.out.println("docNo:"+docNo+",comp:"+compMaterialCode+",qtyper:"+compQty+",demand:"+compDemandQty+",inv:"+invQty+",assign:"+assignQty+",avail:"+availQty+",backlog:"+backlog);
	            if( moveQty != 0 )
	            {
	            	WMS_DYN_STOCK_MOVEMENT oWMS_DYN_STOCK_MOVEMENT =
	            			setStockMovement(
	            				dataList        ,           // dataList,
	            				clientId        ,           // CLIENT_ID          ,
	            				companyCode     ,           // COMPANY_CODE       ,
	            				-1              ,           // DOC_NO             ,
	            				compPlantCode   ,           // PLANT_CODE         ,
	            				compMaterialCode,           // MATERIAL_CODE      ,
	            				postDate        ,           // POST_DATE          ,
	            				shift           ,           // SHIFT              ,
	            				actMoveType     ,           // MOVEMENT_TYPE      ,
	            				compStorageLoc  ,           // CURR_STORAGE_LOC   ,
	            				moveQty         ,           // CURR_QTY           ,
	            				compMaterialCode,           // MOVE_MATERIAL_CODE ,
	            				compStorageLoc  ,           // MOVE_STORAGE_LOC   ,
	            				moveQty         ,           // MOVE_QTY           ,
	            				compUnit        ,           // UNIT               ,
	            				docNo           ,           // REF_DOC_NO         ,
	            				inputType + "-PROD_ACTUAL", // INPUT_TYPE         ,
	            				userId                      // USER_ID
	            				);
	            }
	            if(movementType.equals("100"))
	            {
	            	createBacklog(backlogData,bomData,userId,docNo,prodActQty);
	            }
	        }
		}
		catch(Exception e){
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
		}
	}

	private void assignInvQty(WMS_DYN_BACKLOG assignData,String plantCode,String materialCode,String compStorageLoc,float demandQty)
	{
		try
		{
			float invQty    = 0;
			float availQty  = 0;
			float assignQty = 0;
			float remainQty = 0;

			List<WMS_DYN_INVENTORY> dataList = invList.stream()
	                .filter(item ->
	                				item.getPLANT_CODE().equals(plantCode)       &&
	                				item.getMATERIAL_CODE().equals(materialCode) &&
	                				item.getSTORAGE_LOC().equals(compStorageLoc) &&
	                				item.getREMAIN_QTY()>0
	                				)
	                .collect(Collectors.toList());

			for(int i=0; i<dataList.size(); i++)
			{
				WMS_DYN_INVENTORY resultData = dataList.get(i);
				invQty    = resultData.getINV_QTY()   ;
				availQty  = resultData.getREMAIN_QTY();
				remainQty = availQty-demandQty;
				assignQty = demandQty;
				if( availQty < demandQty)
				{
					assignQty = availQty;
					remainQty =	0;
				}
				resultData.setREMAIN_QTY(remainQty);
			}
			assignData.setDEMAND_QTY(demandQty);
			assignData.setINV_QTY(invQty)      ;
			assignData.setAVAIL_QTY(availQty)  ;
			assignData.setASSIGN_QTY(assignQty);
			assignData.setBACKLOG_QTY(demandQty-assignQty) ;
//	        System.out.println("demandQty:"+demandQty+",invQty:"+assignData.getINV_QTY()+",availQty:"+assignData.getAVAIL_QTY()+",assignQty:"+assignData.getASSIGN_QTY());
		}
		catch(Exception e)
		{
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
		}
	}

	private void createBacklog(WMS_DYN_BACKLOG backlogData,WMS_MST_BOM bomData, String userId, int docNo, float prodActQty)
	{
		float backlog = backlogData.getBACKLOG_QTY();
		if( backlog == 0)
		{
			return;
		}

		try
		{
			backlogData.setCLIENT_ID(bomData.getCLIENT_ID())                  ;
			backlogData.setCOMPANY_CODE(bomData.getCOMPANY_CODE())            ;
			backlogData.setDOC_NO(docNo)                                      ;
			backlogData.setPROD_PLANT_CODE(bomData.getPROD_PLANT_CODE())      ;
			backlogData.setPROD_MATERIAL_CODE(bomData.getPROD_MATERIAL_CODE());
			backlogData.setPLANT_CODE(bomData.getCOMP_PLANT_CODE())           ;
			backlogData.setMATERIAL_CODE(bomData.getCOMP_MATERIAL_CODE())     ;
			backlogData.setSTORAGE_LOC(bomData.getCOMP_STORAGE_LOC())         ;
			backlogData.setCOMP_QTY(bomData.getCOMP_QTY())                    ;
			backlogData.setUNIT(bomData.getCOMP_UNIT())                       ;
			backlogData.setPROD_ACT_QTY(prodActQty)                           ;
			backlogData.setCREATE_USER(userId)                                ;

	        backlogList.add(backlogData);
		}
		catch(Exception e)
		{
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
		}
	}
}