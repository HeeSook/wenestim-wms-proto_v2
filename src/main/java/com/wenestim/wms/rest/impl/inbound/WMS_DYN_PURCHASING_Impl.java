/**
 *
 */
package com.wenestim.wms.rest.impl.inbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING;
import com.wenestim.wms.rest.mapper.inbound.WMS_DYN_PURCHASING_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_PURCHASING_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PURCHASING_Impl.class);

	public List<WMS_DYN_PURCHASING> getAllPurchasing(
            @Param("CLIENT_ID"         ) String CLIENT_ID          ,
            @Param("COMPANY_CODE"      ) String COMPANY_CODE       ,
            @Param("PLANT_CODE"        ) String PLANT_CODE         ,
            @Param("PURCHASE_ORG"      ) String PURCHASE_ORG       ,
            @Param("PURCHASE_GRP"      ) String PURCHASE_GRP       ,
            @Param("PO_TYPE"           ) String PO_TYPE            ,
            @Param("MATERIAL_CODE"     ) String MATERIAL_CODE      ,
            @Param("VENDOR_CODE"       ) String VENDOR_CODE        ,
            @Param("PO_NO"             ) String PO_NO              ,
            @Param("FROM_PO_DATE"      ) String FROM_PO_DATE       ,
            @Param("TO_PO_DATE"        ) String TO_PO_DATE         ,
            @Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE ,
            @Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.getAllPurchasing(
            		CLIENT_ID    ,
            		COMPANY_CODE ,
            		PLANT_CODE   ,
            		PURCHASE_ORG ,
                    PURCHASE_GRP ,
                    PO_TYPE      ,
                    MATERIAL_CODE,
                    VENDOR_CODE  ,
                    PO_NO        ,
                    FROM_PO_DATE ,      TO_PO_DATE      ,
                    FROM_DELIVERY_DATE, TO_DELIVERY_DATE
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_DYN_PURCHASING> getPurchasingByCompany(
			@Param("CLIENT_ID"         ) String CLIENT_ID	   	   ,
			@Param("COMPANY_CODE"      ) String COMPANY_CODE	   ,
			@Param("MAIN_FLAG"         ) String MAIN_FLAG          ,
			@Param("PURCHASE_ORG"      ) String PURCHASE_ORG	   ,
			@Param("PURCHASE_GRP"      ) String PURCHASE_GRP	   ,
			@Param("PLANT"             ) String PLANT		       ,
			@Param("PO_TYPE"           ) String PO_TYPE		       ,
			@Param("FROM_PO_NO"        ) String FROM_PO_NO	       ,
			@Param("TO_PO_NO"          ) String TO_PO_NO		   ,
			@Param("FROM_PO_DATE"      ) String FROM_PO_DATE	   ,
			@Param("TO_PO_DATE"        ) String TO_PO_DATE	       ,
			@Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE ,
			@Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE   ,
			@Param("FROM_VENDOR_CODE"  ) String FROM_VENDOR_CODE   ,
			@Param("TO_VENDOR_CODE"    ) String TO_VENDOR_CODE	   ,
			@Param("MATERIAL_CODE"     ) String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.getPurchasingByCompany(
					CLIENT_ID		  ,
					COMPANY_CODE	  ,
					MAIN_FLAG		  ,
					PURCHASE_ORG	  ,
					PURCHASE_GRP	  ,
					PLANT			  ,
					PO_TYPE		      ,
					FROM_PO_NO		  , TO_PO_NO		,
					FROM_PO_DATE	  , TO_PO_DATE		,
					FROM_DELIVERY_DATE,	TO_DELIVERY_DATE,
					FROM_VENDOR_CODE  ,	TO_VENDOR_CODE	,
					MATERIAL_CODE
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public WMS_DYN_PURCHASING getPurchasing(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.getPurchasing(CLIENT_ID, COMPANY_CODE, PO_NO, PO_ITEM_NO, MATERIAL_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertPurchasing(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			mWMS_DYN_PURCHASING_Mapper.insertPurchasing(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePurchasing(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			mWMS_DYN_PURCHASING_Mapper.updatePurchasing(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deletePurchasing(HashMap<String, Object> dataMap) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			mWMS_DYN_PURCHASING_Mapper.deletePurchasing(dataMap);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

//	public Boolean deletePurchasing(WMS_DYN_PURCHASING oWMS_DYN_PURCHASING) {
//		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
//		Boolean result = false;
//		try {
//			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
//			mWMS_DYN_PURCHASING_Mapper.deletePurchasing(oWMS_DYN_PURCHASING);
//			sqlSession.commit();
//			result = true;
//		} catch (Exception e) {
//			LOGGER.error("Exception:" + e.getMessage());
//			e.printStackTrace();
//		} finally {
//			sqlSession.close();
//		}
//		return result;
//	}

	// GWANGHYUN KIM 4/7/2018
	public Boolean chkExistsPoNumber(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PO_NO"       ) String PO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.chkExistsPoNumber(CLIENT_ID,COMPANY_CODE,PO_NO);
		} finally {
			sqlSession.close();
		}
	}


	// GWANGHYUN KIM 4/7/2018
	public Boolean chkExistsMaterial(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.chkExistsMaterial(CLIENT_ID, COMPANY_CODE, PO_NO, MATERIAL_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean deleteAllPO(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("PO_NO") String PO_NO, @Param("INPUT_TYPE") String INPUT_TYPE, @Param("userId") String userId) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			LOGGER.info(COMPANY_CODE);
			LOGGER.info(PO_NO);
			mWMS_DYN_PURCHASING_Mapper.deleteAllPO(CLIENT_ID,COMPANY_CODE,PO_NO,INPUT_TYPE,userId);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePurchasingFromPDA(WMS_DYN_PURCHASING oWMS_DYN_PURCHASING) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			mWMS_DYN_PURCHASING_Mapper.updatePurchasingFromPDA(oWMS_DYN_PURCHASING);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean chkGRStauts(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			return mWMS_DYN_PURCHASING_Mapper.chkGRStauts(CLIENT_ID, COMPANY_CODE, PO_NO, PO_ITEM_NO, MATERIAL_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public float getPO_Qty(
			@Param("CLIENT_ID"    ) String CLIENT_ID   ,
			@Param("COMPANY_CODE" ) String COMPANY_CODE,
			@Param("PO_NO"        ) String PO_NO       ,
			@Param("PO_ITEM_NO"   ) int PO_ITEM_NO     ,
			@Param("MATERIAL_CODE") String MATERIAL_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		float poQty = 0;
		try {
			WMS_DYN_PURCHASING_Mapper mWMS_DYN_PURCHASING_Mapper = sqlSession.getMapper(WMS_DYN_PURCHASING_Mapper.class);
			poQty = mWMS_DYN_PURCHASING_Mapper.getPO_Qty(CLIENT_ID, COMPANY_CODE, PO_NO, PO_ITEM_NO, MATERIAL_CODE);
			LOGGER.info("PO_QTY1 is "+poQty+" from implement");
			return poQty;
		}catch (Exception e) {
			return poQty;
		}finally {
			sqlSession.close();
		}
	}

}
