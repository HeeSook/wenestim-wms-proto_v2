package com.wenestim.wms.rest.impl.master;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_PROMOTION;
import com.wenestim.wms.rest.mapper.master.WMS_MST_PROMOTION_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_MST_PROMOTION_Impl {

	private static final Logger LOGGER = Logger.getLogger(WMS_MST_PROMOTION_Impl.class);

	public List<WMS_MST_PROMOTION> getAllPromotionByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("DEL_FLAG") String DEL_FLAG) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_PROMOTION_Mapper mWMS_MST_PROMOTION_Mapper = sqlSession.getMapper(WMS_MST_PROMOTION_Mapper.class);
			return mWMS_MST_PROMOTION_Mapper.getAllPromotionByCompany(CLIENT_ID, COMPANY_CODE, DEL_FLAG);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertPromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_PROMOTION_Mapper mWMS_MST_PROMOTION_Mapper = sqlSession.getMapper(WMS_MST_PROMOTION_Mapper.class);
			mWMS_MST_PROMOTION_Mapper.insertPromotion(oWMS_MST_PROMOTION);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updatePromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_PROMOTION_Mapper mWMS_MST_PROMOTION_Mapper = sqlSession.getMapper(WMS_MST_PROMOTION_Mapper.class);
			mWMS_MST_PROMOTION_Mapper.updatePromotion(oWMS_MST_PROMOTION);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deletePromotion(WMS_MST_PROMOTION oWMS_MST_PROMOTION) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_PROMOTION_Mapper mWMS_MST_PROMOTION_Mapper = sqlSession.getMapper(WMS_MST_PROMOTION_Mapper.class);
			mWMS_MST_PROMOTION_Mapper.deletePromotion(oWMS_MST_PROMOTION);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public int getMaxPromoCode() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_PROMOTION_Mapper mWMS_MST_PROMOTION_Mapper = sqlSession.getMapper(WMS_MST_PROMOTION_Mapper.class);
			return mWMS_MST_PROMOTION_Mapper.getMaxPromoCode();
		} finally {
			sqlSession.close();
		}
	}
}