package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.entity.production.WMS_EXC_DEMAND;
import com.wenestim.wms.rest.entity.production.WMS_EXC_INVENTORY;
import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;
import com.wenestim.wms.rest.mapper.production.WMS_EXC_MRP_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_EXC_MRP_Impl
{
	private final static Logger LOGGER = Logger.getLogger(WMS_EXC_MRP_Impl.class);

	public List<WMS_EXC_DEMAND> getDemandList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("PLAN_VER"    ) String PLAN_VER,
			@Param("PLAN_TYPE"   ) String PLAN_TYPE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			return mWMS_EXC_MRP_Mapper.getDemandList(CLIENT_ID, COMPANY_CODE, PLAN_VER, PLAN_TYPE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_BOM> getBomList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			return mWMS_EXC_MRP_Mapper.getBomList(CLIENT_ID, COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_EXC_INVENTORY> getInvList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			return mWMS_EXC_MRP_Mapper.getInvList(CLIENT_ID, COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_EXC_INVENTORY> getInvDetailList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			return mWMS_EXC_MRP_Mapper.getInvDetailList(CLIENT_ID, COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_MRP> getMrpMasterList(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			return mWMS_EXC_MRP_Mapper.getMrpMasterList(CLIENT_ID, COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertMasterList(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertMasterList(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

	public Boolean insertDemandList(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertDemandList(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean insertInvList(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertInvList(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

	public Boolean insertPeggingList(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertPeggingList(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean insertReqItemList(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertReqItemList(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean insertDynMrp(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.insertDynMrp(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }


    public Boolean updateStartMrp(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.updateStartMrp(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean updateEndMrp(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.updateEndMrp(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }

    public Boolean deleteMrpData(HashMap<String, Object> dataMap)
    {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
        Boolean result = false;
        try
        {
			WMS_EXC_MRP_Mapper mWMS_EXC_MRP_Mapper = sqlSession.getMapper(WMS_EXC_MRP_Mapper.class);
			mWMS_EXC_MRP_Mapper.deleteMrpData(dataMap);
            sqlSession.commit();
            result = true;
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
        finally
        {
            sqlSession.close();
        }
        return result;
    }
}
