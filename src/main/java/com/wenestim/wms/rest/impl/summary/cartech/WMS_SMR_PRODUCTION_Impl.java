/**
 *
 */
package com.wenestim.wms.rest.impl.summary.cartech;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.cartech.WMS_SMR_PRODUCTION_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_SMR_PRODUCTION_Impl
{
	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_PRODUCTION_Impl.class);

	public HashMap<String,Object> getTotalProductionQty(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_PRODUCTION_Mapper mWMS_SMR_PRODUCTION_Mapper = sqlSession.getMapper(WMS_SMR_PRODUCTION_Mapper.class);
			return mWMS_SMR_PRODUCTION_Mapper.getTotalProductionQty(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getTotalProductionList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_PRODUCTION_Mapper mWMS_SMR_PRODUCTION_Mapper = sqlSession.getMapper(WMS_SMR_PRODUCTION_Mapper.class);
			return mWMS_SMR_PRODUCTION_Mapper.getTotalProductionList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getDetailProductionList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_PRODUCTION_Mapper mWMS_SMR_PRODUCTION_Mapper = sqlSession.getMapper(WMS_SMR_PRODUCTION_Mapper.class);
			return mWMS_SMR_PRODUCTION_Mapper.getDetailProductionList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getProductionResultList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_PRODUCTION_Mapper mWMS_SMR_PRODUCTION_Mapper = sqlSession.getMapper(WMS_SMR_PRODUCTION_Mapper.class);
			return mWMS_SMR_PRODUCTION_Mapper.getProductionResultList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getProductionRunTimeList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_PRODUCTION_Mapper mWMS_SMR_PRODUCTION_Mapper = sqlSession.getMapper(WMS_SMR_PRODUCTION_Mapper.class);
			return mWMS_SMR_PRODUCTION_Mapper.getProductionRunTimeList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}


