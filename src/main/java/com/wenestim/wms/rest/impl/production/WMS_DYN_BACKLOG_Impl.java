/**
 *
 */
package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.production.WMS_DYN_BACKLOG;
import com.wenestim.wms.rest.mapper.production.WMS_DYN_BACKLOG_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_BACKLOG_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_DYN_BACKLOG_Impl.class);

	public List<WMS_DYN_BACKLOG> getBacklogList(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_DYN_BACKLOG_Mapper mWMS_DYN_BACKLOG_Mapper = sqlSession.getMapper(WMS_DYN_BACKLOG_Mapper.class);
			return mWMS_DYN_BACKLOG_Mapper.getBacklogList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateBacklogList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_BACKLOG_Mapper mWMS_DYN_BACKLOG_Mapper = sqlSession.getMapper(WMS_DYN_BACKLOG_Mapper.class);
			mWMS_DYN_BACKLOG_Mapper.updateBacklogList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
