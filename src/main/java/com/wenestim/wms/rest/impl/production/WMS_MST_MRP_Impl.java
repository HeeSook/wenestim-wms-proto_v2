package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;
import com.wenestim.wms.rest.mapper.production.WMS_MST_MRP_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_MST_MRP_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_MRP_Impl.class);

	public List<WMS_MST_MRP> getMrpMasterList(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MRP_Mapper mWMS_MST_MRP_Mapper = sqlSession.getMapper(WMS_MST_MRP_Mapper.class);
			return mWMS_MST_MRP_Mapper.getMrpMasterList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean updateMrpMasterList(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_MST_MRP_Mapper mWMS_MST_MRP_Mapper = sqlSession.getMapper(WMS_MST_MRP_Mapper.class);
			mWMS_MST_MRP_Mapper.updateMrpMasterList(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
