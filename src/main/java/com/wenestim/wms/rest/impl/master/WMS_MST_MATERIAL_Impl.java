package com.wenestim.wms.rest.impl.master;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.master.WMS_MST_MATERIAL;
import com.wenestim.wms.rest.mapper.master.WMS_MST_MATERIAL_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_MST_MATERIAL_Impl
{
	private static final Logger LOGGER = Logger.getLogger(WMS_MST_MATERIAL_Impl.class);

	public List<WMS_MST_MATERIAL> getAllMaterial()
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getAllMaterial();
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_MATERIAL> getMaterialListByPlanMaterialCodeType(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("MATERIAL_CODE") String MATERIAL_CODE, @Param("PLANT_CODE") String PLANT_CODE, @Param("MATERIAL_TYPE") String MATERIAL_TYPE, @Param("DEL_FLAG") String DEL_FLAG)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getMaterialListByPlanMaterialCodeType(CLIENT_ID, COMPANY_CODE, MATERIAL_CODE, PLANT_CODE, MATERIAL_TYPE, DEL_FLAG);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_MATERIAL> getAllMaterialByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getAllMaterialByCompany(CLIENT_ID, COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_MATERIAL> getMaterial(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("MATERIAL_CODE") String MATERIAL_CODE)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getMaterial(CLIENT_ID, COMPANY_CODE, MATERIAL_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_MST_MATERIAL> getMaterialByMaterialType(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("TYPE") String TYPE)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getMaterialByMaterialType(CLIENT_ID, COMPANY_CODE, TYPE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public Boolean insertMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.insertMaterial(oWMS_MST_MATERIAL);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean insertMaterialList(HashMap<String, Object> materialList)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.insertMaterialList(materialList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.updateMaterial(oWMS_MST_MATERIAL);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateMaterialList(HashMap<String, Object> materialList)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.updateMaterialList(materialList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteMaterial(WMS_MST_MATERIAL oWMS_MST_MATERIAL)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.deleteMaterial(oWMS_MST_MATERIAL);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteMaterialList(HashMap<String, Object> materialList)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = Boolean.valueOf(false);
		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.deleteMaterialList(materialList);
			sqlSession.commit();
			result = Boolean.valueOf(true);
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}


	public List<WMS_MST_MATERIAL> getMaterialCodeByCompany(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getMaterialCodeByCompany(CLIENT_ID, COMPANY_CODE);
		} finally {
			sqlSession.close();
		}
	}

	public List<HashMap<String,Object>> getExcelUploadCheckList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			return mWMS_MST_MATERIAL_Mapper.getExcelUploadCheckList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}


	public Boolean excelUpload(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_MST_MATERIAL_Mapper mWMS_MST_MATERIAL_Mapper = sqlSession.getMapper(WMS_MST_MATERIAL_Mapper.class);
			mWMS_MST_MATERIAL_Mapper.excelUpload(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

}
