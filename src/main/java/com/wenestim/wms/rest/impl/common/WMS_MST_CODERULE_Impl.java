/**
 *
 */
package com.wenestim.wms.rest.impl.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.common.WMS_MST_CODERULE;
import com.wenestim.wms.rest.mapper.common.WMS_MST_CODERULE_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_CODERULE_Impl {

private final static Logger LOGGER = Logger.getLogger(WMS_MST_CODERULE_Impl.class);

	public List<WMS_MST_CODERULE> getAllCodeRule() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			return mWMS_MST_CODERULE_Mapper.getAllCodeRule();
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			mWMS_MST_CODERULE_Mapper.insertCodeRule(oWMS_MST_CODERULE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			mWMS_MST_CODERULE_Mapper.updateCodeRule(oWMS_MST_CODERULE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteCodeRule(WMS_MST_CODERULE oWMS_MST_CODERULE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			mWMS_MST_CODERULE_Mapper.deleteCodeRule(oWMS_MST_CODERULE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public String getDocNo(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("CATEGORY2") String CATEGORY2, @Param("CODE") String CODE, @Param("NEED_CNT") Integer NEED_CNT) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			return mWMS_MST_CODERULE_Mapper.getDocNo(CLIENT_ID, COMPANY_CODE, CATEGORY2, CODE, NEED_CNT);
		} finally {
			sqlSession.close();
		}
	}
	
	public String getAvailableMstCode(@Param("CLIENT_ID") String CLIENT_ID, @Param("COMPANY_CODE") String COMPANY_CODE, @Param("CATEGORY2") String CATEGORY2, @Param("CODE") String CODE, @Param("NEED_CNT") Integer NEED_CNT) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_CODERULE_Mapper mWMS_MST_CODERULE_Mapper = sqlSession.getMapper(WMS_MST_CODERULE_Mapper.class);
			return mWMS_MST_CODERULE_Mapper.getAvailableMstCode(CLIENT_ID, COMPANY_CODE, CATEGORY2, CODE, NEED_CNT);
		} finally {
			sqlSession.close();
		}
	}

}
