/**
 *
 */
package com.wenestim.wms.rest.impl.outbound;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.outbound.WMS_DYN_DELIVERY;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;
import com.wenestim.wms.rest.mapper.outbound.WMS_DYN_DELIVERY_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_DELIVERY_Impl {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_DELIVERY_Impl.class);

	public List<WMS_DYN_DELIVERY> getAllDeliveryHeader(
			@Param("CLIENT_ID"         ) String CLIENT_ID         ,
			@Param("COMPANY_CODE"      ) String COMPANY_CODE      ,
			@Param("DO_NO"             ) String DO_NO             ,
			@Param("CUST_CODE"         ) String CUST_CODE         ,
			@Param("STATUS"            ) String STATUS            ,
			@Param("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
			@Param("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getAllDeliveryHeader(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO       ,
                    CUST_CODE   ,
                    STATUS      ,
                    FROM_DELIVERY_DATE, TO_DELIVERY_DATE
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<WMS_DYN_PICKING> getAllDeliveryDetail(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getAllDeliveryDetail(
					CLIENT_ID   ,
					COMPANY_CODE,
					DO_NO
					);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public WMS_DYN_PICKING getDcrInfo(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getDcrInfo(
					CLIENT_ID   ,
					COMPANY_CODE,
					DO_NO
					);
		}
		finally
		{
			sqlSession.close();
		}
	}
	
	
	
	public List<WMS_DYN_PICKING> getAllDeliveryDetailPdf(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getAllDeliveryDetailPdf(
					CLIENT_ID   ,
					COMPANY_CODE,
					DO_NO
					);
		}
		finally
		{
			sqlSession.close();
		}
	}
	
	public List<WMS_DYN_PICKING> getAllDeliveryDetailPdf_1000(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getAllDeliveryDetailPdf_1000(
					CLIENT_ID   ,
					COMPANY_CODE,
					DO_NO
					);
		}
		finally
		{
			sqlSession.close();
		}
	}
	
	
	public Boolean insertDelivery(WMS_DYN_DELIVERY oWMS_DYN_DELIVERY) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			mWMS_DYN_DELIVERY_Mapper.insertDelivery(oWMS_DYN_DELIVERY);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDelivery(HashMap dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			mWMS_DYN_DELIVERY_Mapper.updateDelivery(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateDeliveryInfo(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			mWMS_DYN_DELIVERY_Mapper.updateDeliveryInfo(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteDelivery(WMS_DYN_DELIVERY oWMS_DYN_DELIVERY) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			mWMS_DYN_DELIVERY_Mapper.deleteDelivery(oWMS_DYN_DELIVERY);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	/* Noah Kim 4/16/2018
	 * */
	public String getCUSTOM_NUM(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getCUSTOM_NUM(CLIENT_ID, COMPANY_CODE, DO_NO);
		} finally {
			sqlSession.close();
		}
	}

	/* Noah Kim 4/16/2018
	 * */
	public String getDeliveryTime(
			@Param("CUST_CODE"   ) String CUST_CODE   ,
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.getDeliveryTime(CUST_CODE, CLIENT_ID, COMPANY_CODE);
		} finally {
			sqlSession.close();
		}
	}

	/* Noah Kim 4/16/2018
	 * */
	public Boolean chkDeliveryNum(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("DO_NO"       ) String DO_NO
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_DYN_DELIVERY_Mapper mWMS_DYN_DELIVERY_Mapper = sqlSession.getMapper(WMS_DYN_DELIVERY_Mapper.class);
			return mWMS_DYN_DELIVERY_Mapper.chkDeliveryNum(CLIENT_ID, COMPANY_CODE, DO_NO);
		} finally {
			sqlSession.close();
		}
	}


}