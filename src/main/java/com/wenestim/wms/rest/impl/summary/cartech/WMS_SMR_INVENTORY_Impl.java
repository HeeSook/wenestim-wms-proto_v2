/**
 *
 */
package com.wenestim.wms.rest.impl.summary.cartech;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.summary.cartech.WMS_SMR_INVENTORY_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_SMR_INVENTORY_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_INVENTORY_Impl.class);

	public HashMap<String, Object> getTotalInventoryQty(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INVENTORY_Mapper mWMS_SMR_INVENTORY_Mapper = sqlSession.getMapper(WMS_SMR_INVENTORY_Mapper.class);
			return mWMS_SMR_INVENTORY_Mapper.getTotalInventoryQty(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String, Object>> getTotalInventoryList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INVENTORY_Mapper mWMS_SMR_INVENTORY_Mapper = sqlSession.getMapper(WMS_SMR_INVENTORY_Mapper.class);
			return mWMS_SMR_INVENTORY_Mapper.getTotalInventoryList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String, Object>> getDetailInventoryQty(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INVENTORY_Mapper mWMS_SMR_INVENTORY_Mapper = sqlSession.getMapper(WMS_SMR_INVENTORY_Mapper.class);
			return mWMS_SMR_INVENTORY_Mapper.getDetailInventoryQty(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public List<HashMap<String, Object>> getDetailInventoryList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_SMR_INVENTORY_Mapper mWMS_SMR_INVENTORY_Mapper = sqlSession.getMapper(WMS_SMR_INVENTORY_Mapper.class);
			return mWMS_SMR_INVENTORY_Mapper.getDetailInventoryList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}

}
