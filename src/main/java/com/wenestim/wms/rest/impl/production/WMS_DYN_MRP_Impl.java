/**
 *
 */
package com.wenestim.wms.rest.impl.production;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.production.WMS_DYN_MRP_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_DYN_MRP_Impl
{

	private static final Logger LOGGER = Logger.getLogger(WMS_DYN_MRP_Impl.class);

	public List<HashMap<String,Object>> getMrpList(HashMap<String,Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_DYN_MRP_Mapper mWMS_DYN_MRP_Mapper = sqlSession.getMapper(WMS_DYN_MRP_Mapper.class);
			return mWMS_DYN_MRP_Mapper.getMrpList(paramMap);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
