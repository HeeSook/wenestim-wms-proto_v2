/**
 *
 */
package com.wenestim.wms.rest.impl.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.common.WMS_MST_USER;
import com.wenestim.wms.rest.mapper.common.WMS_MST_USER_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_USER_Impl {

	/**
	 *
	 */

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_USER_Impl.class);

	public WMS_MST_USER_Impl() {
		// TODO Auto-generated constructor stub
	}

	public List<WMS_MST_USER> getAllUser(
			@Param("CLIENT_ID"   ) String CLIENT_ID,
			@Param("COMPANY_CODE") String COMPANY_CODE
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.getAllUser(CLIENT_ID,COMPANY_CODE);
		}
		finally
		{
			sqlSession.close();
		}
	}

	public WMS_MST_USER getUser(@Param("COMPANY_CODE") String COMPANY_CODE, @Param("WSID") int WSID) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.getUser(COMPANY_CODE, WSID);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertUser(WMS_MST_USER oWMS_MST_USER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			mWMS_MST_USER_Mapper.insertUser(oWMS_MST_USER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateUser(WMS_MST_USER oWMS_MST_USER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			mWMS_MST_USER_Mapper.updateUser(oWMS_MST_USER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteUser(WMS_MST_USER oWMS_MST_USER) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			mWMS_MST_USER_Mapper.deleteUser(oWMS_MST_USER);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean checkExitUserID(@Param("USER_ID") String USER_ID) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.checkExitUserID(USER_ID);
		}  finally {
			sqlSession.close();
		}
	}

	/**
	 * User Login Mapper
	 * @param USER_ID
	 * @param USER_PW
	 * @return
	 */
	public WMS_MST_USER getUser(@Param("USER_ID") String USER_ID,@Param("USER_PW") String USER_PW) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.getUser(USER_ID, USER_PW);
		}  finally {
			sqlSession.close();
		}
	}

	public WMS_MST_USER getCompanyCode(String USER_ID) throws Exception {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.getCompanyCode(USER_ID);
		}  finally {
			sqlSession.close();
		}
	}

	public WMS_MST_USER getLogin(
			@Param("CLIENT_ID"   ) String CLIENT_ID   ,
			@Param("COMPANY_CODE") String COMPANY_CODE,
			@Param("USER_ID"     ) String USER_ID,
			@Param("USER_PW"     ) String USER_PW
			)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try
		{
			WMS_MST_USER_Mapper mWMS_MST_USER_Mapper = sqlSession.getMapper(WMS_MST_USER_Mapper.class);
			return mWMS_MST_USER_Mapper.getLogin(CLIENT_ID, COMPANY_CODE, USER_ID, USER_PW);
		}
		finally
		{
			sqlSession.close();
		}
	}
}
