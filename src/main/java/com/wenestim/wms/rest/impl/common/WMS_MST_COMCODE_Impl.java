package com.wenestim.wms.rest.impl.common;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.common.WMS_MST_COMCODE;
import com.wenestim.wms.rest.mapper.common.WMS_MST_COMCODE_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

/**
 * @author Andrew
 *
 */
public class WMS_MST_COMCODE_Impl {

private final static Logger LOGGER = Logger.getLogger(WMS_MST_COMCODE_Impl.class);

	public List<WMS_MST_COMCODE> getAllComCode() {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			return mWMS_MST_COMCODE_Mapper.getAllComCode();
		} finally {
			sqlSession.close();
		}
	}
	
	public List<WMS_MST_COMCODE> getAllComCodeByCategory(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CATEGORY1") String CATEGORY1,@Param("CATEGORY2") String CATEGORY2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			return mWMS_MST_COMCODE_Mapper.getAllComCodeByCategory(CLIENT_ID,COMPANY_CODE,CATEGORY1,CATEGORY2);
		} finally {
			sqlSession.close();
		}
	}

	public List<WMS_MST_COMCODE> getComCodeByCategory(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE,@Param("CATEGORY1") String CATEGORY1,@Param("CATEGORY2") String CATEGORY2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			return mWMS_MST_COMCODE_Mapper.getComCodeByCategory(CLIENT_ID,COMPANY_CODE,CATEGORY1,CATEGORY2);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean insertComCode(WMS_MST_COMCODE oWMS_MST_COMCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			mWMS_MST_COMCODE_Mapper.insertComCode(oWMS_MST_COMCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean updateComCode(WMS_MST_COMCODE oWMS_MST_COMCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			mWMS_MST_COMCODE_Mapper.updateComCode(oWMS_MST_COMCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}

	public Boolean deleteComCode(WMS_MST_COMCODE oWMS_MST_COMCODE) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			mWMS_MST_COMCODE_Mapper.deleteComCode(oWMS_MST_COMCODE);
			sqlSession.commit();
			result = true;
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		} finally {
			sqlSession.close();
		}
		return result;
	}
	
	public int countSORT1(@Param("CATEGORY1") String CATEGORY1, @Param("CATEGORY2") String CATEGORY2) {
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		try {
			WMS_MST_COMCODE_Mapper mWMS_MST_COMCODE_Mapper = sqlSession.getMapper(WMS_MST_COMCODE_Mapper.class);
			return mWMS_MST_COMCODE_Mapper.countSORT1(CATEGORY1, CATEGORY2);
		} finally {
			sqlSession.close();
		}
	}
}
