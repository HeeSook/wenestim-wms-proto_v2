package com.wenestim.wms.rest.impl.admin;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.mapper.admin.WMS_ADM_MENU_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_ADM_MENU_Impl
{
	private static final Logger LOGGER = Logger.getLogger(WMS_ADM_MENU_Impl.class);

	public List<HashMap<String,Object>> getMenuList(HashMap<String, Object> paramMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();

		try
		{
			WMS_ADM_MENU_Mapper mWMS_ADM_MENU_Mapper = sqlSession.getMapper(WMS_ADM_MENU_Mapper.class);
			return mWMS_ADM_MENU_Mapper.getMenuList(paramMap);
		} finally {
			sqlSession.close();
		}
	}

	public Boolean updateMenu(HashMap<String, Object> dataMap)
	{
		SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
		Boolean result = false;

		try
		{
			WMS_ADM_MENU_Mapper mWMS_ADM_MENU_Mapper = sqlSession.getMapper(WMS_ADM_MENU_Mapper.class);
			mWMS_ADM_MENU_Mapper.updateMenu(dataMap);
			sqlSession.commit();
			result = true;
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			sqlSession.close();
		}
		return result;
	}
}
