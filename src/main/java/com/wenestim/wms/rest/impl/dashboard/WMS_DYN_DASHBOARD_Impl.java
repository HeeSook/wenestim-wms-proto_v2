package com.wenestim.wms.rest.impl.dashboard;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;

import com.wenestim.wms.rest.entity.dashboard.WMS_DYN_DASHBOARD;
import com.wenestim.wms.rest.mapper.dashboard.WMS_DYN_DASHBOARD_Mapper;
import com.wenestim.wms.rest.util.MyBatisUtil;

public class WMS_DYN_DASHBOARD_Impl {

private final static Logger LOGGER = Logger.getLogger(WMS_DYN_DASHBOARD_Impl.class);

public List<WMS_DYN_DASHBOARD> getSupplierState1(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE) {
	SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
	try {
		WMS_DYN_DASHBOARD_Mapper mWMS_DYN_DASHBOARD_Mapper = sqlSession.getMapper(WMS_DYN_DASHBOARD_Mapper.class);
		return mWMS_DYN_DASHBOARD_Mapper.getSupplierState1(CLIENT_ID, COMPANY_CODE);
	} finally {
		sqlSession.close();
	}
}

public List<WMS_DYN_DASHBOARD> getSupplierState2(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE) {
	SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
	try {
		WMS_DYN_DASHBOARD_Mapper mWMS_DYN_DASHBOARD_Mapper = sqlSession.getMapper(WMS_DYN_DASHBOARD_Mapper.class);
		return mWMS_DYN_DASHBOARD_Mapper.getSupplierState2(CLIENT_ID, COMPANY_CODE);
	} finally {
		sqlSession.close();
	}
}

public List<WMS_DYN_DASHBOARD> getCustomerState1(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE) {
	SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
	try {
		WMS_DYN_DASHBOARD_Mapper mWMS_DYN_DASHBOARD_Mapper = sqlSession.getMapper(WMS_DYN_DASHBOARD_Mapper.class);
		return mWMS_DYN_DASHBOARD_Mapper.getCustomerState1(CLIENT_ID, COMPANY_CODE);
	} finally {
		sqlSession.close();
	}
}

public List<WMS_DYN_DASHBOARD> getCustomerState2(@Param("CLIENT_ID") String CLIENT_ID,@Param("COMPANY_CODE") String COMPANY_CODE) {
	SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory().openSession();
	try {
		WMS_DYN_DASHBOARD_Mapper mWMS_DYN_DASHBOARD_Mapper = sqlSession.getMapper(WMS_DYN_DASHBOARD_Mapper.class);
		return mWMS_DYN_DASHBOARD_Mapper.getCustomerState2(CLIENT_ID, COMPANY_CODE);
	} finally {
		sqlSession.close();
	}
}

}
