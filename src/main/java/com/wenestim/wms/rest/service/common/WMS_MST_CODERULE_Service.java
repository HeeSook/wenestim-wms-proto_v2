/**
 *
 */
package com.wenestim.wms.rest.service.common;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.common.WMS_MST_CODERULE;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.util.StringUtil;


/**
 * @author
 *
 */
@Path("/Common/CodeRule")
public class WMS_MST_CODERULE_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_CODERULE_Service.class);

	@GET
	@Path("/All")
	public Response getAllCodeRule() {
		LOGGER.info("WMS_MST_CODERULE_Service.getAllCodeRule() called");

		String result = "";
		try {
			WMS_MST_CODERULE_Impl mWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
			List<WMS_MST_CODERULE> allList = mWMS_MST_CODERULE_Impl.getAllCodeRule();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Mst/Check")
	public Response getAvailableMstCode(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("CATEGORY2") String CATEGORY2,
			@QueryParam("CODE") String CODE, @QueryParam("NEED_CNT") Integer NEED_CNT) throws Exception {
		LOGGER.info("WMS_MST_CODERULE_Service.getAvailableMstCode() called");
		LOGGER.debug("\nCLIENT_ID:"+CLIENT_ID+",COMPANY_CODE:" + COMPANY_CODE + ",CATEGORY2:" + CATEGORY2 + ",CODE:" + CODE + ",NEED_CNT:" + NEED_CNT);

		String rtnData,rtnMsg;
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			CATEGORY2    = StringUtil.getEmptyNullPrevString(CATEGORY2);
			CODE         = StringUtil.getEmptyNullPrevString(CODE);

			WMS_MST_CODERULE_Impl mWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
			rtnData = mWMS_MST_CODERULE_Impl.getAvailableMstCode(CLIENT_ID,COMPANY_CODE, CATEGORY2, CODE, NEED_CNT);

			JsonObject jsonObject = new JsonObject();
			if (rtnData != null) {
				jsonObject.addProperty("ret", Integer.valueOf(0));
				jsonObject.addProperty("data", rtnData);
				jsonObject.addProperty("msg", "");
			} else {
				rtnMsg = "could not load data";
				jsonObject.addProperty("ret" , Integer.valueOf(1));
				jsonObject.addProperty("data", "");
				jsonObject.addProperty("msg" , rtnMsg);
			}

			return Response.status(200).entity(jsonObject.toString()).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/DocNo")
	public Response getDocNo(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("CATEGORY2") String CATEGORY2,
			@QueryParam("CODE") String CODE, @QueryParam("NEED_CNT") Integer NEED_CNT) throws Exception {
		LOGGER.info("WMS_MST_CODERULE_Service.getDocNo() called");
		LOGGER.debug("COMPANY_CODE : " + COMPANY_CODE + ",CATEGORY2 :" + CATEGORY2 + ",CODE : " + CODE + ",NEED_CNT : " + NEED_CNT);

		String rtnData,rtnMsg;
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			CATEGORY2    = StringUtil.getEmptyNullPrevString(CATEGORY2);
			CODE         = StringUtil.getEmptyNullPrevString(CODE);

			WMS_MST_CODERULE_Impl mWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
			rtnData = mWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID,COMPANY_CODE, CATEGORY2, CODE, NEED_CNT);
			LOGGER.info("DocNo : " + rtnData);

			Gson gson = new Gson();
			JsonObject jsonObject = new JsonObject();
			if (rtnData != null) {
				jsonObject.addProperty("ret", Integer.valueOf(0));
				jsonObject.addProperty("data", rtnData);
				jsonObject.addProperty("msg", "");
			} else {
				rtnMsg = "could not load data";
				jsonObject.addProperty("ret" , Integer.valueOf(1));
				jsonObject.addProperty("data", "");
				jsonObject.addProperty("msg" , rtnMsg);
			}
			rtnData = gson.toJson(jsonObject);

			return Response.status(200).entity(rtnData).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
}
