/**
 *
 */
package com.wenestim.wms.rest.service.inbound;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING;
import com.wenestim.wms.rest.impl.inbound.WMS_DYN_PURCHASING_Impl;
import com.wenestim.wms.rest.util.StringUtil;


/**
 * @author
 *
 */
@Path("/Inbound/Purchasing")
public class WMS_DYN_PURCHASING_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PURCHASING_Service.class);

	@GET
	@Path("/All")
	public Response getAllPurchasing(
            @QueryParam("CLIENT_ID"         ) String CLIENT_ID          ,
            @QueryParam("COMPANY_CODE"      ) String COMPANY_CODE       ,
            @QueryParam("PLANT_CODE"        ) String PLANT_CODE         ,
            @QueryParam("PURCHASE_ORG"      ) String PURCHASE_ORG       ,
            @QueryParam("PURCHASE_GRP"      ) String PURCHASE_GRP       ,
            @QueryParam("PO_TYPE"           ) String PO_TYPE            ,
            @QueryParam("MATERIAL_CODE"     ) String MATERIAL_CODE      ,
            @QueryParam("VENDOR_CODE"       ) String VENDOR_CODE        ,
            @QueryParam("PO_NO"             ) String PO_NO              ,
            @QueryParam("FROM_PO_DATE"      ) String FROM_PO_DATE       ,
            @QueryParam("TO_PO_DATE"        ) String TO_PO_DATE         ,
            @QueryParam("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE ,
            @QueryParam("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
			)
	{
		LOGGER.info("WMS_DYN_PURCHASING_Service.getAllPurchasing() called");

		String result = "";
		try
		{
            CLIENT_ID          = StringUtil.getEmptyNullPrevString(CLIENT_ID         ) ;
            COMPANY_CODE       = StringUtil.getEmptyNullPrevString(COMPANY_CODE      ) ;
            PLANT_CODE         = StringUtil.getEmptyNullPrevString(PLANT_CODE        ) ;
            PURCHASE_ORG       = StringUtil.getEmptyNullPrevString(PURCHASE_ORG      ) ;
            PURCHASE_GRP       = StringUtil.getEmptyNullPrevString(PURCHASE_GRP      ) ;
            PO_TYPE            = StringUtil.getEmptyNullPrevString(PO_TYPE           ) ;
            PO_NO              = StringUtil.getEmptyNullPrevString(PO_NO             ) ;
            MATERIAL_CODE      = StringUtil.getEmptyNullPrevString(MATERIAL_CODE     ) ;
            VENDOR_CODE        = StringUtil.getEmptyNullPrevString(VENDOR_CODE       ) ;
            FROM_PO_DATE       = StringUtil.getEmptyNullPrevString(FROM_PO_DATE      ) ;
            TO_PO_DATE         = StringUtil.getEmptyNullPrevString(TO_PO_DATE        ) ;
            FROM_DELIVERY_DATE = StringUtil.getEmptyNullPrevString(FROM_DELIVERY_DATE) ;
            TO_DELIVERY_DATE   = StringUtil.getEmptyNullPrevString(TO_DELIVERY_DATE  ) ;

			WMS_DYN_PURCHASING_Impl mWMS_DYN_PURCHASING_Impl = new WMS_DYN_PURCHASING_Impl();
			List<WMS_DYN_PURCHASING> allList = mWMS_DYN_PURCHASING_Impl.getAllPurchasing(
            		CLIENT_ID    ,
            		COMPANY_CODE ,
            		PLANT_CODE   ,
            		PURCHASE_ORG ,
                    PURCHASE_GRP ,
                    PO_TYPE      ,
                    MATERIAL_CODE,
                    VENDOR_CODE  ,
                    PO_NO        ,
                    FROM_PO_DATE ,      TO_PO_DATE      ,
                    FROM_DELIVERY_DATE, TO_DELIVERY_DATE
					);

			Gson gson = new Gson();
			if (allList != null)
			{
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/List")
	public Response getPurchasingByCompany(
			@QueryParam("CLIENT_ID") 			String CLIENT_ID	,
			@QueryParam("COMPANY_CODE") 		String COMPANY_CODE	,
			@QueryParam("MAIN_FLAG")			String MAIN_FLAG	,
			@QueryParam("PURCHASE_ORG") 		String PURCHASE_ORG	,
			@QueryParam("PURCHASE_GRP")			String PURCHASE_GRP	,

			@QueryParam("PLANT")				String PLANT		,
			@QueryParam("PO_TYPE") 				String PO_TYPE		,

			@QueryParam("FROM_PO_NO") 			String FROM_PO_NO	,
			@QueryParam("TO_PO_NO") 			String TO_PO_NO		,

			@QueryParam("FROM_PO_DATE") 		String FROM_PO_DATE	,
			@QueryParam("TO_PO_DATE") 			String TO_PO_DATE	,

			@QueryParam("FROM_DELIVERY_DATE")	String FROM_DELIVERY_DATE	,
			@QueryParam("TO_DELIVERY_DATE") 	String TO_DELIVERY_DATE		,

			@QueryParam("FROM_VENDOR_CODE") 	String FROM_VENDOR_CODE		,
			@QueryParam("TO_VENDOR_CODE") 		String TO_VENDOR_CODE		,
			@QueryParam("MATERIAL_CODE") 		String MATERIAL_CODE
			)
	{
		LOGGER.info("WMS_DYN_PURCHASING_Service.getPurchasingByCompany() called");

		String result = "";
		try
		{
			CLIENT_ID    		= StringUtil.getEmptyNullPrevString(CLIENT_ID) 			;
			COMPANY_CODE		= StringUtil.getEmptyNullPrevString(COMPANY_CODE) 		;
			MAIN_FLAG			= StringUtil.getEmptyNullPrevString(MAIN_FLAG) 			;

			PURCHASE_ORG		= StringUtil.getEmptyNullPrevString(PURCHASE_ORG)		;
			PURCHASE_GRP 		= StringUtil.getEmptyNullPrevString(PURCHASE_GRP)		;
			PLANT 				= StringUtil.getEmptyNullPrevString(PLANT)				;
			PO_TYPE				= StringUtil.getEmptyNullPrevString(PO_TYPE)			;

			FROM_PO_NO			= StringUtil.getEmptyNullPrevString(FROM_PO_NO)			;
			TO_PO_NO			= StringUtil.getEmptyNullPrevString(TO_PO_NO)			;

			FROM_PO_DATE		= StringUtil.getEmptyNullPrevString(FROM_PO_DATE)		;
			TO_PO_DATE			= StringUtil.getEmptyNullPrevString(TO_PO_DATE)			;

			FROM_DELIVERY_DATE	= StringUtil.getEmptyNullPrevString(FROM_DELIVERY_DATE)	;
			TO_DELIVERY_DATE	= StringUtil.getEmptyNullPrevString(TO_DELIVERY_DATE)	;

			FROM_VENDOR_CODE	= StringUtil.getEmptyNullPrevString(FROM_VENDOR_CODE)	;
			TO_VENDOR_CODE		= StringUtil.getEmptyNullPrevString(TO_VENDOR_CODE)		;

			MATERIAL_CODE		= StringUtil.getEmptyNullPrevString(MATERIAL_CODE)		;

			WMS_DYN_PURCHASING_Impl mWMS_DYN_PURCHASING_Impl = new WMS_DYN_PURCHASING_Impl();
			List<WMS_DYN_PURCHASING> allList = mWMS_DYN_PURCHASING_Impl.getPurchasingByCompany(
					CLIENT_ID		,
					COMPANY_CODE	,
					MAIN_FLAG		,
					PURCHASE_ORG	,
					PURCHASE_GRP	,
					PLANT			,
					PO_TYPE			,
					FROM_PO_NO		,
					TO_PO_NO		,
					FROM_PO_DATE	,
					TO_PO_DATE		,
					FROM_DELIVERY_DATE,
					TO_DELIVERY_DATE,
					FROM_VENDOR_CODE,
					TO_VENDOR_CODE	,
					MATERIAL_CODE
					);

			Gson gson = new Gson();
			if (allList != null)
			{
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/GRBarcode")
	public Response getPOItem(
            @QueryParam("CLIENT_ID"    ) String CLIENT_ID   ,
            @QueryParam("COMPANY_CODE" ) String COMPANY_CODE,
			@QueryParam("PO_NO"        ) String PO_NO       ,
			@QueryParam("PO_ITEM_NO"   ) String PO_ITEM_NO  ,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE
			)
	{

		String result = "";
		int rPO_ITEM_NO = Integer.parseInt(PO_ITEM_NO);
		try {
			WMS_DYN_PURCHASING_Impl mWMS_DYN_PURCHASING_Impl = new WMS_DYN_PURCHASING_Impl();
			WMS_DYN_PURCHASING oWMS_DYN_PURCHASING = mWMS_DYN_PURCHASING_Impl.getPurchasing(CLIENT_ID, COMPANY_CODE, PO_NO, rPO_ITEM_NO, MATERIAL_CODE);

			Gson gson = new Gson();
			if (oWMS_DYN_PURCHASING != null) {
				result = gson.toJson(oWMS_DYN_PURCHASING);
				return Response.status(200).entity(result).build();
			}else {
				JsonObject rtnJson = new JsonObject();
				rtnJson.addProperty("MSG", "Not existing entered data. Please check you PO number or Item number or Part No.");
				return Response.status(500).entity(rtnJson.toString()).build();
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createPurchasing(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_DYN_PURCHASING_Service.createPurchasing() called");

		Boolean rtnData = false;
		String rtnMsg = "";
		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			int splitPos = getString.indexOf("^");
			String dataString = getString.substring(splitPos + 1);

			dataString = "[" + dataString.substring(1, dataString.length()) + "]";

			JSONArray saveList = new JSONArray(dataString);
			rtnData = setPurchasingObj("INSERT", "PO", saveList, getString, "");

			JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("Method", "CREATE");
			rtnJson.addProperty("Object", "WMS_DYN_PURCHASING");

			JsonObject jsonObject = new JsonObject();

			jsonObject = setTableEventResultHandler(rtnData, "");
			return Response.status(200).entity(jsonObject.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateSelectedPO(InputStream inputStream, @PathParam("userId") String userId) throws Exception {
		LOGGER.info("WMS_DYN_PURCHASING_Service.updateSelectedSales() called");

		Boolean rtnData = false;
		String rtnMsg = "";
		try {
			String getString = StringUtil.getInputStreamToString(inputStream);

			int splitPos = getString.indexOf("^");
			String dataString = getString.substring(splitPos + 1);

			dataString = "[" + dataString.substring(1, dataString.length()) + "]";

			JSONArray saveList = new JSONArray(dataString);
			rtnData = setPurchasingObj("UPDATE", "PO", saveList, getString, userId);

			JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("Method", "UPDATE");
			rtnJson.addProperty("Object", "WMS_DYN_PURCHASING");

			JsonObject jsonObject = new JsonObject();

			jsonObject = setTableEventResultHandler(rtnData, "");

			return Response.status(200).entity(jsonObject.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Delete/{userId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteSelectedSales(InputStream inputStream, @PathParam("userId") String userId) throws Exception {
		LOGGER.info("oWMS_DYN_PURCHASING_Service.deleteSelectedSales() called");

		Boolean rtnData = false;
		String rtnMsg = "";
		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			int splitPos = getString.indexOf("^");
			String dataString = getString.substring(splitPos + 1);

			dataString = "[" + dataString.substring(1, dataString.length()) + "]";

			JSONArray saveList = new JSONArray(dataString);
			rtnData = setPurchasingObj("DELETE", "PO", saveList, getString, userId);

			JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("Method", "DELETE");
			rtnJson.addProperty("Object", "oWMS_DYN_PURCHASING");

			JsonObject jsonObject = new JsonObject();

			jsonObject = setTableEventResultHandler(rtnData, "");

			return Response.status(200).entity(jsonObject.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


	@POST
	@Path("/Delete/AllPO/{CLIENT_ID}/{COMPANY_CODE}/{PO_NO}/{INPUT_TYPE}/{userId}")
	public Response deleteAllPO(@PathParam("CLIENT_ID") String CLIENT_ID,@PathParam("COMPANY_CODE") String COMPANY_CODE, @PathParam("PO_NO") String PO_NO, @PathParam("INPUT_TYPE") String INPUT_TYPE,@PathParam("userId") String userId) throws Exception {
		LOGGER.info("WMS_DYN_PURCHASING_Service.deleteAllPO() called");

		Boolean rtnData = false;
		String rtnMsg = "";
		try {
			CLIENT_ID	= StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_NO = StringUtil.getEmptyNullPrevString(PO_NO);
			INPUT_TYPE = StringUtil.getEmptyNullPrevString(INPUT_TYPE);


			WMS_DYN_PURCHASING_Impl iWMS_DYN_PURCHASING_Impl = new WMS_DYN_PURCHASING_Impl();
			LOGGER.info(COMPANY_CODE);
			LOGGER.info(PO_NO);
			rtnData = iWMS_DYN_PURCHASING_Impl.deleteAllPO(CLIENT_ID,COMPANY_CODE, PO_NO,INPUT_TYPE,userId);

			JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("Method", "DELETE");
			rtnJson.addProperty("Object", "WMS_DYN_PURCHASING");

			JsonObject jsonObject = new JsonObject();

			jsonObject = setTableEventResultHandler(rtnData, "");

			return Response.status(200).entity(jsonObject.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	public boolean setPurchasingObj(String evtHandler, String inputType, JSONArray saveList, String getString, String userId)
			throws Exception {
		Boolean result = true;
		try {
			HashMap<String, Object> paramMap = StringUtil.getParamMap(getString);

			String CLIENT_ID = (String) paramMap.get("CLIENT_ID");
			String COMPANY_CODE = (String) paramMap.get("COMPANY_CODE");
			String PO_NO = (String) paramMap.get("PO_NO");
			String PO_TYPE = (String) paramMap.get("PO_TYPE");
			String PO_DATE = (String) paramMap.get("PO_DATE");
			String DELIVERY_DATE = (String) paramMap.get("DELIVERY_DATE");
			String PURCHASE_ORG_CODE = (String) paramMap.get("PURCHASE_ORG_CODE");
			String PURCHASE_GRP_CODE = (String) paramMap.get("PURCHASE_GRP_CODE");
//			String VENDOR_CODE = (String) paramMap.get("VENDOR_CODE");
			String CREATE_USER =(String) paramMap.get("USER_ID");
			String CREATE_DATE = (String) paramMap.get("CREATE_DATE");
			String UPDATE_USER = (String) paramMap.get("UPDATE_USER");
			String UPDATE_DATE = (String) paramMap.get("UPDATE_DATE");
			String DELETE_USER = (String) paramMap.get("DELETE_USER");
			String DELETE_DATE = (String) paramMap.get("DELETE_DATE");
			String DEL_FLAG = (String) paramMap.get("DEL_FLAG");
			String INPUT_TYPE = "";

			Integer PO_ITEM_NO = 0 ;

			WMS_DYN_PURCHASING_Impl iWMS_DYN_PURCHASING_Impl = new WMS_DYN_PURCHASING_Impl();
			List<WMS_DYN_PURCHASING> dataList = new ArrayList<WMS_DYN_PURCHASING>();

			for (int i = 0; i < saveList.length(); i++) {
				Integer WSID 				= saveList.getJSONObject(i).has("WSID") 				? saveList.getJSONObject(i).getInt("WSID") 				: 0;
				String VENDOR_CODE 			= saveList.getJSONObject(i).has("VENDOR_CODE") 			? saveList.getJSONObject(i).getString("VENDOR_CODE") 	: "";
				String MATERIAL_CODE 		= saveList.getJSONObject(i).has("MATERIAL_CODE") 		? saveList.getJSONObject(i).getString("MATERIAL_CODE") 	: "";
				Double PO_QTY 				= saveList.getJSONObject(i).has("PO_QTY") 				? saveList.getJSONObject(i).getDouble("PO_QTY") 		: 0;
				String PO_UNIT 				= saveList.getJSONObject(i).has("PO_UNIT") 				? saveList.getJSONObject(i).getString("PO_UNIT") 		: "";
//				Double PER_QTY 				= saveList.getJSONObject(i).has("PER_QTY") 				? saveList.getJSONObject(i).getDouble("PER_QTY") 		: 0;
				String INCOTERMS 			= saveList.getJSONObject(i).has("INCOTERMS") 			? saveList.getJSONObject(i).getString("INCOTERMS") 		: "";
				String PAYMENT_TERM 		= saveList.getJSONObject(i).has("PAYMENT_TERM") 		? saveList.getJSONObject(i).getString("PAYMENT_TERM") 	: "";
				String CURRENCY 			= saveList.getJSONObject(i).has("CURRENCY") 			? saveList.getJSONObject(i).getString("CURRENCY") 		: "";
				String PROMO_NAME 			= saveList.getJSONObject(i).has("PROMO_NAME") 			? saveList.getJSONObject(i).getString("PROMO_NAME") 	: "";
				String PROMO_DESC 			= saveList.getJSONObject(i).has("PROMO_DESC") 			? saveList.getJSONObject(i).getString("PROMO_DESC") 	: "";
				Double PROMO_DISC 			= saveList.getJSONObject(i).has("PROMO_DISC") 			? saveList.getJSONObject(i).getDouble("PROMO_DISC") 	: 0;
				String ITEM_AMOUNT 			= saveList.getJSONObject(i).has("ITEM_AMOUNT") 			? saveList.getJSONObject(i).getString("ITEM_AMOUNT") 	: "0";
				if( ITEM_AMOUNT == null || ITEM_AMOUNT.equals(""))
				{
					ITEM_AMOUNT = "0";
				}

				Double ITEM_QTY 			= saveList.getJSONObject(i).has("ITEM_QTY") 			? saveList.getJSONObject(i).getDouble("ITEM_QTY") 		: 0;
				Double MST_QTY 				= saveList.getJSONObject(i).has("MST_QTY") 				? saveList.getJSONObject(i).getDouble("MST_QTY") 		: 0;
				String MST_UNIT 		    = saveList.getJSONObject(i).has("MST_UNIT") 		    ? saveList.getJSONObject(i).getString("MST_UNIT") 		: "";

				String GR_FLAG 				= saveList.getJSONObject(i).has("GR_FLAG") 				? saveList.getJSONObject(i).getString("GR_FLAG") 		  : "";
				String PLANNED_ORDER_NO 	= saveList.getJSONObject(i).has("PLANNED_ORDER_NO") 	? saveList.getJSONObject(i).getString("PLANNED_ORDER_NO") : "";

				String HEADER_FLAG;

				if (i == 0) {HEADER_FLAG = "Y";} else {HEADER_FLAG = "N";}

				if (evtHandler.equals("UPDATE") || evtHandler.equals("DELETE")) {
					CLIENT_ID 				= saveList.getJSONObject(i).has("CLIENT_ID") 			? saveList.getJSONObject(i).getString("CLIENT_ID") 		: "";
					COMPANY_CODE 			= saveList.getJSONObject(i).has("COMPANY_CODE") 		? saveList.getJSONObject(i).getString("COMPANY_CODE") 	: "";
					PO_NO 					= saveList.getJSONObject(i).has("PO_NO") 				? saveList.getJSONObject(i).getString("PO_NO") 			: "";
					PO_ITEM_NO 				= saveList.getJSONObject(i).has("PO_ITEM_NO") 			? saveList.getJSONObject(i).getInt("PO_ITEM_NO") 		: 0;
					PO_TYPE 				= saveList.getJSONObject(i).has("PO_TYPE") 				? saveList.getJSONObject(i).getString("PO_TYPE") 		: "";
					PO_DATE 				= saveList.getJSONObject(i).has("PO_DATE") 				? saveList.getJSONObject(i).getString("PO_DATE") 		: "";
					DELIVERY_DATE 			= saveList.getJSONObject(i).has("DELIVERY_DATE") 		? saveList.getJSONObject(i).getString("DELIVERY_DATE") 	: "";
					PURCHASE_GRP_CODE 		= saveList.getJSONObject(i).has("PURCHASE_GRP_CODE") 	? saveList.getJSONObject(i).getString("PURCHASE_GRP_CODE") : "";
					PURCHASE_ORG_CODE 		= saveList.getJSONObject(i).has("PURCHASE_ORG_CODE") 	? saveList.getJSONObject(i).getString("PURCHASE_ORG_CODE") : "";
					HEADER_FLAG 			= saveList.getJSONObject(i).has("HEADER_FLAG") 			? saveList.getJSONObject(i).getString("HEADER_FLAG") 	: "";
					CREATE_USER 			= saveList.getJSONObject(i).has("CREATE_USER") 			? saveList.getJSONObject(i).getString("CREATE_USER") 	: "";
					CREATE_DATE 			= saveList.getJSONObject(i).has("CREATE_DATE") 			? saveList.getJSONObject(i).getString("CREATE_DATE") 	: "";
					UPDATE_USER 			= saveList.getJSONObject(i).has("UPDATE_USER") 			? saveList.getJSONObject(i).getString("UPDATE_USER")	: "";
					UPDATE_DATE 			= saveList.getJSONObject(i).has("UPDATE_DATE") 			? saveList.getJSONObject(i).getString("UPDATE_DATE") 	: "";
					DELETE_USER 			= saveList.getJSONObject(i).has("DELETE_USER") 			? saveList.getJSONObject(i).getString("DELETE_USER")	: "";
					DELETE_DATE 			= saveList.getJSONObject(i).has("DELETE_DATE") 			? saveList.getJSONObject(i).getString("DELETE_DATE") 	: "";

					PLANNED_ORDER_NO		= saveList.getJSONObject(i).has("PLANNED_ORDER_NO") 	? saveList.getJSONObject(i).getString("PLANNED_ORDER_NO") 		: "";
					INPUT_TYPE		= saveList.getJSONObject(i).has("INPUT_TYPE") 	? saveList.getJSONObject(i).getString("INPUT_TYPE") 		: "";

				}

				WMS_DYN_PURCHASING oWMS_DYN_PURCHASING = new WMS_DYN_PURCHASING();

				oWMS_DYN_PURCHASING.setCLIENT_ID(CLIENT_ID);
				oWMS_DYN_PURCHASING.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_DYN_PURCHASING.setPO_NO(PO_NO);

				oWMS_DYN_PURCHASING.setWSID(WSID);
				oWMS_DYN_PURCHASING.setPO_TYPE(PO_TYPE);
				oWMS_DYN_PURCHASING.setPO_DATE(PO_DATE);
				oWMS_DYN_PURCHASING.setVENDOR_CODE(VENDOR_CODE);
				oWMS_DYN_PURCHASING.setMATERIAL_CODE(MATERIAL_CODE);
				oWMS_DYN_PURCHASING.setDELIVERY_DATE(DELIVERY_DATE);
				oWMS_DYN_PURCHASING.setPO_QTY(PO_QTY);
				oWMS_DYN_PURCHASING.setPO_UNIT(PO_UNIT);
//				oWMS_DYN_PURCHASING.setPER_QTY(PER_QTY);
				oWMS_DYN_PURCHASING.setINCOTERMS(INCOTERMS);
				oWMS_DYN_PURCHASING.setPAYMENT_TERM(PAYMENT_TERM);
				oWMS_DYN_PURCHASING.setCURRENCY(CURRENCY);

				oWMS_DYN_PURCHASING.setPROMO_NAME(PROMO_NAME);
				oWMS_DYN_PURCHASING.setPROMO_DESC(PROMO_DESC);
				oWMS_DYN_PURCHASING.setPROMO_DISC(PROMO_DISC);
				oWMS_DYN_PURCHASING.setITEM_AMOUNT(new Double(ITEM_AMOUNT));
				oWMS_DYN_PURCHASING.setITEM_QTY(ITEM_QTY);
				oWMS_DYN_PURCHASING.setMST_QTY(MST_QTY);
				oWMS_DYN_PURCHASING.setMST_UNIT(MST_UNIT);
				oWMS_DYN_PURCHASING.setPURCHASE_GRP_CODE(PURCHASE_GRP_CODE);
				oWMS_DYN_PURCHASING.setPURCHASE_ORG_CODE(PURCHASE_ORG_CODE);

				oWMS_DYN_PURCHASING.setHEADER_FLAG(HEADER_FLAG);
				oWMS_DYN_PURCHASING.setCREATE_USER(CREATE_USER);
				oWMS_DYN_PURCHASING.setCREATE_DATE(CREATE_DATE);
				oWMS_DYN_PURCHASING.setUPDATE_USER(UPDATE_USER);
				oWMS_DYN_PURCHASING.setUPDATE_DATE(UPDATE_DATE);
				oWMS_DYN_PURCHASING.setDELETE_USER(DELETE_USER);
				oWMS_DYN_PURCHASING.setDELETE_DATE(DELETE_DATE);
				oWMS_DYN_PURCHASING.setGR_FLAG(GR_FLAG);
				oWMS_DYN_PURCHASING.setDEL_FLAG(DEL_FLAG);
				oWMS_DYN_PURCHASING.setPLANNED_ORDER_NO(PLANNED_ORDER_NO);

				if (evtHandler.equals("INSERT")) {
					oWMS_DYN_PURCHASING.setPO_ITEM_NO(10 * (i + 1));
				} else {
					if(evtHandler.equals("UPDATE")){
						oWMS_DYN_PURCHASING.setUPDATE_USER(userId);
					}else if(evtHandler.equals("DELETE")){
						oWMS_DYN_PURCHASING.setDELETE_USER(userId);
					}
					oWMS_DYN_PURCHASING.setPO_ITEM_NO(PO_ITEM_NO);
				}



				dataList.add(oWMS_DYN_PURCHASING);
			}

			if (dataList.size() > 0) {
				HashMap<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("dataList"      , dataList      );
				dataMap.put("CLIENT_ID"     , CLIENT_ID     );
				dataMap.put("COMPANY_CODE"  , COMPANY_CODE  );
				dataMap.put("PO_NO"         , PO_NO         );
				dataMap.put("INPUT_TYPE"    , INPUT_TYPE    );

				LOGGER.info(dataMap);
				LOGGER.info(dataList);
				if (evtHandler.equals("INSERT")) {
					result = iWMS_DYN_PURCHASING_Impl.insertPurchasing(dataMap);
				} else if (evtHandler.equals("UPDATE")) {
					result = iWMS_DYN_PURCHASING_Impl.updatePurchasing(dataMap);
				} else if (evtHandler.equals("DELETE")) {
					result = iWMS_DYN_PURCHASING_Impl.deletePurchasing(dataMap);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			result = false;
		}
		return result;
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData) {
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue()) {
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		} else {
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
