/**
 *
 */
package com.wenestim.wms.rest.service.outbound;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_DELIVERY;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;
import com.wenestim.wms.rest.impl.outbound.WMS_DYN_DELIVERY_Impl;
import com.wenestim.wms.rest.util.StringUtil;




/**
 * @author
 *
 */
@Path("/Outbound/Delivery")
public class WMS_DYN_DELIVERY_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_DELIVERY_Service.class);

    @GET
    @Path("/Header")
    public Response getAllDeliveryHeader(
            @QueryParam("CLIENT_ID"         ) String CLIENT_ID         ,
            @QueryParam("COMPANY_CODE"      ) String COMPANY_CODE      ,
            @QueryParam("DO_NO"             ) String DO_NO             ,
            @QueryParam("CUST_CODE"         ) String CUST_CODE         ,
            @QueryParam("STATUS"            ) String STATUS            ,
            @QueryParam("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
            @QueryParam("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE
            )
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.getAllDeliveryHeader() called");
        LOGGER.info("COMPANY_CODE:"      +COMPANY_CODE      +",DO_NO:"           +DO_NO+"  ,CUST_CODE:"+CUST_CODE);
        LOGGER.info("FROM_DELIVERY_DATE:"+FROM_DELIVERY_DATE+",TO_DELIVERY_DATE:"+TO_DELIVERY_DATE);
        LOGGER.info("STATUS:"            +STATUS            +",CLIENT_ID:"       +CLIENT_ID);


        String result = "";
        try
        {
        	CLIENT_ID          = StringUtil.getEmptyNullPrevString(CLIENT_ID         );
        	COMPANY_CODE       = StringUtil.getEmptyNullPrevString(COMPANY_CODE      );
            DO_NO              = StringUtil.getEmptyNullPrevString(DO_NO             );
            CUST_CODE          = StringUtil.getEmptyNullPrevString(CUST_CODE         );
            STATUS             = StringUtil.getEmptyNullPrevString(STATUS            );
            FROM_DELIVERY_DATE = StringUtil.getEmptyNullPrevString(FROM_DELIVERY_DATE);
            TO_DELIVERY_DATE   = StringUtil.getEmptyNullPrevString(TO_DELIVERY_DATE  );

            WMS_DYN_DELIVERY_Impl mWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
            List<WMS_DYN_DELIVERY> allList = mWMS_DYN_DELIVERY_Impl.getAllDeliveryHeader(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO       ,
                    CUST_CODE   ,
                    STATUS      ,
                    FROM_DELIVERY_DATE, TO_DELIVERY_DATE
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @GET
    @Path("/Detail")
    public Response getAllDeliveryDetail(
            @QueryParam("CLIENT_ID"   ) String CLIENT_ID   ,
    		@QueryParam("COMPANY_CODE") String COMPANY_CODE,
            @QueryParam("DO_NO"       ) String DO_NO
            )
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.getAllDeliveryDetail() called");
        LOGGER.info("COMPANY_CODE:"+COMPANY_CODE+",DO_NO:"+DO_NO);


        String result = "";
        try
        {
        	CLIENT_ID   = StringUtil.getEmptyNullPrevString(CLIENT_ID   );
        	COMPANY_CODE= StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            DO_NO       = StringUtil.getEmptyNullPrevString(DO_NO       );

            WMS_DYN_DELIVERY_Impl mWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
            List<WMS_DYN_PICKING> allList = mWMS_DYN_DELIVERY_Impl.getAllDeliveryDetail(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @GET
    @Path("/DcrInfo")
    public Response getDcrInfo(
            @QueryParam("CLIENT_ID"   ) String CLIENT_ID   ,
    		@QueryParam("COMPANY_CODE") String COMPANY_CODE,
            @QueryParam("DO_NO"       ) String DO_NO
            )
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.getDcrInfo() called");
        LOGGER.info("COMPANY_CODE:"+COMPANY_CODE+",DO_NO:"+DO_NO);


        String result = "";
        try
        {
        	CLIENT_ID   = StringUtil.getEmptyNullPrevString(CLIENT_ID   );
        	COMPANY_CODE= StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            DO_NO       = StringUtil.getEmptyNullPrevString(DO_NO       );

            WMS_DYN_DELIVERY_Impl mWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
            WMS_DYN_PICKING list = mWMS_DYN_DELIVERY_Impl.getDcrInfo(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO
                    );

            Gson gson = new Gson();
            if (list != null)
            {
                result = gson.toJson(list);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
    
    @GET
    @Path("/DetailPdf")
    public Response getAllDeliveryDetailPdf(
            @QueryParam("CLIENT_ID"   ) String CLIENT_ID   ,
    		@QueryParam("COMPANY_CODE") String COMPANY_CODE,
            @QueryParam("DO_NO"       ) String DO_NO
            )
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.getAllDeliveryDetailPdf() called");
        LOGGER.info("COMPANY_CODE:"+COMPANY_CODE+",DO_NO:"+DO_NO);


        String result = "";
        try
        {
        	CLIENT_ID   = StringUtil.getEmptyNullPrevString(CLIENT_ID   );
        	COMPANY_CODE= StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            DO_NO       = StringUtil.getEmptyNullPrevString(DO_NO       );

            WMS_DYN_DELIVERY_Impl mWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
            List<WMS_DYN_PICKING> allList = mWMS_DYN_DELIVERY_Impl.getAllDeliveryDetailPdf(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
    
    @GET
    @Path("/DetailPdf_1000")
    public Response getAllDeliveryDetailPdf_1000(
            @QueryParam("CLIENT_ID"   ) String CLIENT_ID   ,
    		@QueryParam("COMPANY_CODE") String COMPANY_CODE,
            @QueryParam("DO_NO"       ) String DO_NO
            )
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.getAllDeliveryDetailPdf() called");
        LOGGER.info("COMPANY_CODE:"+COMPANY_CODE+",DO_NO:"+DO_NO);


        String result = "";
        try
        {
        	CLIENT_ID   = StringUtil.getEmptyNullPrevString(CLIENT_ID   );
        	COMPANY_CODE= StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            DO_NO       = StringUtil.getEmptyNullPrevString(DO_NO       );

            WMS_DYN_DELIVERY_Impl mWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
            List<WMS_DYN_PICKING> allList = mWMS_DYN_DELIVERY_Impl.getAllDeliveryDetailPdf_1000(
            		CLIENT_ID   ,
            		COMPANY_CODE,
                    DO_NO
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
    
    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDelivery(InputStream inputStream,
            @PathParam("userId"     ) String userId    ,
            @PathParam("clientId"   ) String clientId  ,
            @PathParam("companyCode") String companyCode
    		) throws Exception
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.updateDelivery() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsDataListArray = new JSONArray(getString);

            HashMap rtnMap = setDataObjList(jsDataListArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", rtnMap.get("dataList"));

            WMS_DYN_DELIVERY_Impl iWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();

            rtnBoolResult = iWMS_DYN_DELIVERY_Impl.updateDelivery(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/UpdateDeliveryInfo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateDeliveryInfo(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_DELIVERY_Service.updateDeliveryInfo() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

            WMS_DYN_DELIVERY_Impl iWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();
        	rtnBoolResult = iWMS_DYN_DELIVERY_Impl.updateDeliveryInfo(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }



	/* Noah Kim 4/16/2018
	 * */
	@POST
	@Path("/PDA/Update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response scanUpdateDelivery(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_DYN_DELIVERY_Service.scanUpdateDelivery() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONObject jsonObject = new JSONObject(getString);

			WMS_DYN_DELIVERY oWMS_DYN_DELIVERY = new WMS_DYN_DELIVERY();

			String  CLIENT_ID  	    = jsonObject.has("CLIENT_ID"     )  ? jsonObject.getString ("CLIENT_ID"    ) : "";
			String  COMPANY_CODE  	= jsonObject.has("COMPANY_CODE"  )  ? jsonObject.getString ("COMPANY_CODE" ) : "";
			String  DO_NO  	        = jsonObject.has("DO_NO"         )  ? jsonObject.getString ("DO_NO"        ) : "";

			oWMS_DYN_DELIVERY.setCLIENT_ID(CLIENT_ID);
			oWMS_DYN_DELIVERY.setCOMPANY_CODE(COMPANY_CODE);
			oWMS_DYN_DELIVERY.setDO_NO(DO_NO);

			WMS_DYN_DELIVERY_Impl iWMS_DYN_DELIVERY_Impl = new WMS_DYN_DELIVERY_Impl();

			Boolean chkDeliveryNum = iWMS_DYN_DELIVERY_Impl.chkDeliveryNum(
					oWMS_DYN_DELIVERY.getCLIENT_ID()   ,
					oWMS_DYN_DELIVERY.getCOMPANY_CODE(),
					oWMS_DYN_DELIVERY.getDO_NO()
					);
			JsonObject rtnJson = new JsonObject();

			if(chkDeliveryNum) {

				String test = iWMS_DYN_DELIVERY_Impl.getCUSTOM_NUM(
						oWMS_DYN_DELIVERY.getCLIENT_ID()   ,
						oWMS_DYN_DELIVERY.getCOMPANY_CODE(),
						oWMS_DYN_DELIVERY.getDO_NO()
						);
				//LOGGER.info("CUST_CODE ====" + test);
				String deliveryTime = iWMS_DYN_DELIVERY_Impl.getDeliveryTime(
						test,
						oWMS_DYN_DELIVERY.getCLIENT_ID()   ,
						oWMS_DYN_DELIVERY.getCOMPANY_CODE()
						);

				oWMS_DYN_DELIVERY.setDELIVERY_TIME(Double.parseDouble(deliveryTime));

				List<WMS_DYN_DELIVERY> dataList = new ArrayList<WMS_DYN_DELIVERY>();
				dataList.add(oWMS_DYN_DELIVERY);

				HashMap<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("dataList", dataList);

				rtnBoolResult = iWMS_DYN_DELIVERY_Impl.updateDelivery(dataMap);
				//LOGGER.info("DELIVERY_TIME ====" + deliveryTime);

				if(rtnBoolResult) {
					rtnJson.addProperty("ret", Integer.valueOf(0));
					rtnJson.addProperty("msg", "Delivery Number is successfully scanned : " + oWMS_DYN_DELIVERY.getDO_NO());
				} else {
					rtnJson.addProperty("ret", Integer.valueOf(1));
					rtnJson.addProperty("msg", "Delivery Number is not successfully scanned : " + oWMS_DYN_DELIVERY.getDO_NO());
				}
			} else {
				rtnJson.addProperty("ret", Integer.valueOf(1));
				rtnJson.addProperty("msg", "Delivery Number does not exist : " + oWMS_DYN_DELIVERY.getDO_NO());
			}


			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	public HashMap setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
	{
		HashMap rtnMap = new HashMap();

		List<WMS_DYN_DELIVERY> dataList = new ArrayList<WMS_DYN_DELIVERY>();
		JSONArray moveList = new JSONArray();
		try
		{
			for (int i=0; i<jsArray.length(); i++)
			{
				String WSID			= jsArray.getJSONObject(i).has("WSID"   	  ) ? jsArray.getJSONObject(i).getString("WSID"   	  	) : "";
				String DO_NO		= jsArray.getJSONObject(i).has("DO_NO"   	  ) ? jsArray.getJSONObject(i).getString("DO_NO"   	    ) : "";
				Double DELIVERY_TIME= jsArray.getJSONObject(i).has("DELIVERY_TIME") ? jsArray.getJSONObject(i).getDouble("DELIVERY_TIME") : 0 ;

				WMS_DYN_DELIVERY oWMS_DYN_DELIVERY = new WMS_DYN_DELIVERY();

				oWMS_DYN_DELIVERY.setCLIENT_ID(clientId         );
				oWMS_DYN_DELIVERY.setCOMPANY_CODE(comapnyCode   );
				oWMS_DYN_DELIVERY.setDO_NO(DO_NO				);
				oWMS_DYN_DELIVERY.setDELIVERY_TIME(DELIVERY_TIME);
				oWMS_DYN_DELIVERY.setUPDATE_USER(userId			);

				dataList.add(oWMS_DYN_DELIVERY);
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
		}
		rtnMap.put("dataList", dataList);
		rtnMap.put("moveList", moveList);
		return rtnMap;
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not save data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
