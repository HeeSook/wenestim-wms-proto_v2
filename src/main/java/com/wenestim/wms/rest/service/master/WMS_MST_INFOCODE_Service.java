/**
 *
 */
package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_INFOCODE;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.impl.master.WMS_MST_INFOCODE_Impl;
import com.wenestim.wms.rest.util.StringUtil;



/**
 * @author
 *
 */
@Path("/Master/InfoCode")
public class WMS_MST_INFOCODE_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_INFOCODE_Service.class);

	@GET
	@Path("/All")
	public Response getAllInfoCode() {
		LOGGER.info("WMS_MST_INFOCODE_Service.getAllInfoCode() called");

		String result = "";
		try {
			WMS_MST_INFOCODE_Impl mWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			List<WMS_MST_INFOCODE> allList = mWMS_MST_INFOCODE_Impl.getAllInfoCode();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


	@GET
	@Path("/List")
	public Response getInfoCodeList(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,@QueryParam("VENDOR_CODE") String VENDOR_CODE,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE, @QueryParam("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE, @QueryParam("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@QueryParam("INFO_NO") String INFO_NO,     @QueryParam("CATEGORY") String CATEGORY, @QueryParam("FROM_DATE") String FROM_DATE ) {

		LOGGER.info("WMS_MST_INFOCODE_Serviec.getInfoCodeList() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID +", COMPANY_CODE:" + COMPANY_CODE + ", VENDOR_CODE:" + VENDOR_CODE
				+ ",MATERIAL_CODE:" + MATERIAL_CODE + ",PURCHASE_ORG_CODE:" + PURCHASE_ORG_CODE
				+ ",PURCHASE_GRP_CODE:" + PURCHASE_GRP_CODE + ",INFO_NO" + INFO_NO + ",CATEGORY:" + CATEGORY);

		String result = "";
		try
		{
			CLIENT_ID         = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE      = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			VENDOR_CODE       = StringUtil.getEmptyNullPrevString(VENDOR_CODE);
			MATERIAL_CODE     = StringUtil.getEmptyNullPrevString(MATERIAL_CODE);
			PURCHASE_ORG_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_ORG_CODE);
			PURCHASE_GRP_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_GRP_CODE);
			INFO_NO           = StringUtil.getEmptyNullPrevString(INFO_NO);
			CATEGORY          = StringUtil.getEmptyNullPrevString(CATEGORY);

			FROM_DATE         = StringUtil.getEmptyNullPrevString(FROM_DATE);

			WMS_MST_INFOCODE_Impl mWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			List<WMS_MST_INFOCODE> infoCodeList = mWMS_MST_INFOCODE_Impl.getInfoCodeList(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, MATERIAL_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, INFO_NO, CATEGORY, FROM_DATE);

			Gson gson = new Gson();
			if (infoCodeList != null) {
				result = gson.toJson(infoCodeList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/InfoValidList")
	public Response getInfoCodeByValidDate(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,@QueryParam("VENDOR_CODE") String VENDOR_CODE,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE, @QueryParam("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE, @QueryParam("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE,
			@QueryParam("VALID_DATE") String VALID_DATE) {

		LOGGER.info("WMS_MST_INFOCODE_Serviec.getInfoCodeList() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID +", COMPANY_CODE:" + COMPANY_CODE + ", VENDOR_CODE:" + VENDOR_CODE
				+ ",MATERIAL_CODE:" + MATERIAL_CODE + ",PURCHASE_ORG_CODE:" + PURCHASE_ORG_CODE
				+ ",PURCHASE_GRP_CODE:" + PURCHASE_GRP_CODE + ",VALID_DATE:" + VALID_DATE);

		String result = "";
		try
		{
			CLIENT_ID         = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE      = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			VENDOR_CODE       = StringUtil.getEmptyNullPrevString(VENDOR_CODE);
			MATERIAL_CODE     = StringUtil.getEmptyNullPrevString(MATERIAL_CODE);
			PURCHASE_ORG_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_ORG_CODE);
			PURCHASE_GRP_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_GRP_CODE);
			VALID_DATE           = StringUtil.getEmptyNullPrevString(VALID_DATE);

			WMS_MST_INFOCODE_Impl mWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			List<WMS_MST_INFOCODE> infoCodeList = mWMS_MST_INFOCODE_Impl.getInfoCodeByValidDate(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, MATERIAL_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, VALID_DATE);

			Gson gson = new Gson();
			if (infoCodeList != null) {
				result = gson.toJson(infoCodeList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@Deprecated
	@POST
	@Path("/Create/List/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertInfoCodeList(InputStream inputStream, @PathParam("userId") String userId) throws Exception
	{
		LOGGER.info("WMS_MST_INFOCODE_Serviec.insertInfoCodeList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsInfoCodeListArray = new JSONArray(getString);

			HashMap<String, Object> infoCodeListMap = new HashMap<String, Object>();
			infoCodeListMap.put("infoCodeList", setInfoCodeObjList(jsInfoCodeListArray, "INSERT", userId));

			WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			rtnBoolResult = iWMS_MST_INFOCODE_Impl.insertInfoCodeList(infoCodeListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertInfoCode(InputStream inputStream, @PathParam("userId") String userId) throws Exception
	{
		LOGGER.info("WMS_MST_INFOCODE_Serviec.insertInfoCodeList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsInfoCodeListArray = new JSONArray(getString);

			for (int i=0; i<jsInfoCodeListArray.length(); i++) {
				WMS_MST_INFOCODE oWMS_MST_INFOCODE = new WMS_MST_INFOCODE();
				oWMS_MST_INFOCODE = setInfoCodeObj("INSERT", jsInfoCodeListArray.getJSONObject(i), userId);

				WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
				rtnBoolResult = iWMS_MST_INFOCODE_Impl.insertInfoCode(oWMS_MST_INFOCODE);
			}

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateInfoCodeList(InputStream inputStream, @PathParam("userId") String userId) throws Exception
	{
		LOGGER.info("WMS_MST_INFOCODE_Serviec.updateInfoCode() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsInfoCodeListArray = new JSONArray(getString);

			HashMap<String, Object> infoCodeListMap = new HashMap<String, Object>();
			infoCodeListMap.put("infoCodeList", setInfoCodeObjList(jsInfoCodeListArray, "UPDATE", userId));

			WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			rtnBoolResult = iWMS_MST_INFOCODE_Impl.updateInfoCodeList(infoCodeListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteInfoCodeList(InputStream inputStream, @PathParam("userId") String userId) throws Exception
	{
		LOGGER.info("WMS_MST_INFOCODE_Serviec.deleteInfoCodeList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsInfoCodeListArray = new JSONArray(getString);

			HashMap<String, Object> infoCodeListMap = new HashMap<String, Object>();
			infoCodeListMap.put("infoCodeList", setInfoCodeObjList(jsInfoCodeListArray, "DELETE", userId));

			WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
			rtnBoolResult = iWMS_MST_INFOCODE_Impl.deleteInfoCodeList(infoCodeListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_INFOCODE_Serviec.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

        	WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
        	List<HashMap<String,Object>> allList = iWMS_MST_INFOCODE_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_INFOCODE_Serviec.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	WMS_MST_INFOCODE_Impl iWMS_MST_INFOCODE_Impl = new WMS_MST_INFOCODE_Impl();
        	rtnBoolResult = iWMS_MST_INFOCODE_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	private WMS_MST_INFOCODE setInfoCodeObj(String evtHandler, JSONObject jsonObj, String userId) throws Exception
	{
		LOGGER.info("setInfoCodeObj() jsonObj:" + jsonObj);

		WMS_MST_INFOCODE oWMS_MST_INFOCODE = new WMS_MST_INFOCODE();
		try {
			if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
			{
				Integer WSID = Integer.valueOf(jsonObj.has("WSID") ? Integer.parseInt(jsonObj.getString("WSID")) : 0);
				LOGGER.debug("setInfoCodeObjList() WSID: " + WSID);
				oWMS_MST_INFOCODE.setWSID(WSID);
			}

			String CLIENT_ID     = jsonObj.has("CLIENT_ID")     ? jsonObj.getString("CLIENT_ID")     : "";
			String COMPANY_CODE  = jsonObj.has("COMPANY_CODE")  ? jsonObj.getString("COMPANY_CODE")  : "";
			String VENDOR_CODE   = jsonObj.has("VENDOR_CODE")   ? jsonObj.getString("VENDOR_CODE")   : "";
			String MATERIAL_CODE = jsonObj.has("MATERIAL_CODE") ? jsonObj.getString("MATERIAL_CODE") : "";
			String FROM_DATE     = jsonObj.has("FROM_DATE")     ? StringUtil.getConversionDateFormat(jsonObj.getString("FROM_DATE")) : "";
			String CATEGORY      = jsonObj.has("CATEGORY")      ? jsonObj.getString("CATEGORY") : "";
//			Float  UNIT_PRICE    = Float.valueOf(jsonObj.has("UNIT_PRICE") ? Float.parseFloat(jsonObj.getString("UNIT_PRICE")) : 0.0F);
			String UNIT_PRICE    = jsonObj.has("UNIT_PRICE") ? jsonObj.getString("UNIT_PRICE"):"0";
			if( UNIT_PRICE == null || UNIT_PRICE.equals(""))
			{
				UNIT_PRICE = "0";
			}

			String CURRENCY      = jsonObj.has("CURRENCY")      ? jsonObj.getString("CURRENCY") : "";
			Float  INFO_QTY      = Float.valueOf(jsonObj.has("INFO_QTY") ? Float.parseFloat(jsonObj.getString("INFO_QTY")) : 0.0F);
            String INFO_UNIT     = jsonObj.has("INFO_UNIT")     ? jsonObj.getString("INFO_UNIT") : "";

			Integer INFO_NO      = 0;
			if (evtHandler.equals("INSERT")) {
				WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
				INFO_NO = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID, COMPANY_CODE, "INFO_CODE", "INFO_CODE", 1));
			} else {
				INFO_NO = Integer.valueOf(jsonObj.has("INFO_NO") ? Integer.parseInt(jsonObj.getString("INFO_NO")) : 0);
			}

			String ACT_FLAG      = jsonObj.has("ACT_FLAG") ? jsonObj.getString("ACT_FLAG") : "";
            String DEL_FLAG      = jsonObj.has("DEL_FLAG") ? jsonObj.getString("DEL_FLAG") : "";

			String CREATE_USER = jsonObj.has("CREATE_USER") ? jsonObj.getString("CREATE_USER") : "";
			String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
			String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

			oWMS_MST_INFOCODE.setCLIENT_ID(CLIENT_ID);
			oWMS_MST_INFOCODE.setCOMPANY_CODE(COMPANY_CODE);
			oWMS_MST_INFOCODE.setVENDOR_CODE(VENDOR_CODE);
			oWMS_MST_INFOCODE.setMATERIAL_CODE(MATERIAL_CODE);
			oWMS_MST_INFOCODE.setFROM_DATE(FROM_DATE);
			oWMS_MST_INFOCODE.setCATEGORY(CATEGORY);
			oWMS_MST_INFOCODE.setUNIT_PRICE(new Float(UNIT_PRICE));
			oWMS_MST_INFOCODE.setCURRENCY(CURRENCY);
			oWMS_MST_INFOCODE.setINFO_QTY(INFO_QTY);
			oWMS_MST_INFOCODE.setINFO_UNIT(INFO_UNIT);
			oWMS_MST_INFOCODE.setINFO_NO(INFO_NO);
			oWMS_MST_INFOCODE.setACT_FLAG(ACT_FLAG);
			oWMS_MST_INFOCODE.setDEL_FLAG(DEL_FLAG);
			oWMS_MST_INFOCODE.setCREATE_USER(CREATE_USER);
			oWMS_MST_INFOCODE.setUPDATE_USER(UPDATE_USER);
			oWMS_MST_INFOCODE.setDELETE_USER(DELETE_USER);

		} catch (Exception e) {
			LOGGER.error("Exception :"+e.getMessage()+"#");
			e.printStackTrace();
		}
		return oWMS_MST_INFOCODE;

	}

	private List<WMS_MST_INFOCODE> setInfoCodeObjList(JSONArray jsArray, String evtHandler, String userId) throws Exception
	{
		List<WMS_MST_INFOCODE> infoCodeList = new ArrayList<WMS_MST_INFOCODE>();
		try
		{
			WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
			for (int i = 0; i < jsArray.length(); i++)
			{
				JSONObject jsInfoCode = jsArray.getJSONObject(i);
				LOGGER.debug("setInfoCodeObjList() jsonObj:" + jsInfoCode.toString());

				String CLIENT_ID     = jsInfoCode.has("CLIENT_ID")     ? jsInfoCode.getString("CLIENT_ID")     : "";
				String COMPANY_CODE  = jsInfoCode.has("COMPANY_CODE")  ? jsInfoCode.getString("COMPANY_CODE")  : "";
				String VENDOR_CODE   = jsInfoCode.has("VENDOR_CODE")   ? jsInfoCode.getString("VENDOR_CODE")   : "";
				String MATERIAL_CODE = jsInfoCode.has("MATERIAL_CODE") ? jsInfoCode.getString("MATERIAL_CODE") : "";
				String FROM_DATE     = jsInfoCode.has("FROM_DATE") ? StringUtil.getConversionDateFormat(jsInfoCode.getString("FROM_DATE")) : "";
				String CATEGORY      = jsInfoCode.has("CATEGORY") ? jsInfoCode.getString("CATEGORY") : "";
//				Float  UNIT_PRICE    = Float.valueOf(jsInfoCode.has("UNIT_PRICE") ? Float.parseFloat(jsInfoCode.getString("UNIT_PRICE")) : 0.0F);
				String UNIT_PRICE    = jsInfoCode.has("UNIT_PRICE") ? jsInfoCode.getString("UNIT_PRICE"):"0";
				if( UNIT_PRICE == null || UNIT_PRICE.equals(""))
				{
					UNIT_PRICE = "0";
				}


				String CURRENCY      = jsInfoCode.has("CURRENCY") ? jsInfoCode.getString("CURRENCY") : "";
				Float  INFO_QTY      = Float.valueOf(jsInfoCode.has("INFO_QTY") ? Float.parseFloat(jsInfoCode.getString("INFO_QTY")) : 0.0F);
                String INFO_UNIT     = jsInfoCode.has("INFO_UNIT") ? jsInfoCode.getString("INFO_UNIT") : "";

				Integer INFO_NO      = 0;
				if (evtHandler.equals("INSERT")) {
					INFO_NO = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID, COMPANY_CODE, "INFO_CODE", "INFO_CODE", 1));
				} else {
					INFO_NO = Integer.valueOf(jsInfoCode.has("INFO_NO") ? Integer.parseInt(jsInfoCode.getString("INFO_NO")) : 0);
				}

				String ACT_FLAG      = jsInfoCode.has("ACT_FLAG") ? jsInfoCode.getString("ACT_FLAG") : "";
                String DEL_FLAG      = jsInfoCode.has("DEL_FLAG") ? jsInfoCode.getString("DEL_FLAG") : "";

				String CREATE_USER = jsInfoCode.has("CREATE_USER") ? jsInfoCode.getString("CREATE_USER") : "";
				String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
				String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

				WMS_MST_INFOCODE oWMS_MST_INFOCODE = new WMS_MST_INFOCODE();
				if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
				{
					Integer WSID = Integer.valueOf(jsInfoCode.has("WSID") ? Integer.parseInt(jsInfoCode.getString("WSID")) : 0);
					LOGGER.debug("setInfoCodeObjList() WSID: " + WSID);
					oWMS_MST_INFOCODE.setWSID(WSID);
				}
				oWMS_MST_INFOCODE.setCLIENT_ID(CLIENT_ID);
				oWMS_MST_INFOCODE.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_MST_INFOCODE.setVENDOR_CODE(VENDOR_CODE);
				oWMS_MST_INFOCODE.setMATERIAL_CODE(MATERIAL_CODE);
				oWMS_MST_INFOCODE.setFROM_DATE(FROM_DATE);
				oWMS_MST_INFOCODE.setCATEGORY(CATEGORY);
				oWMS_MST_INFOCODE.setUNIT_PRICE(new Float(UNIT_PRICE));
				oWMS_MST_INFOCODE.setCURRENCY(CURRENCY);
				oWMS_MST_INFOCODE.setINFO_QTY(INFO_QTY);
				oWMS_MST_INFOCODE.setINFO_UNIT(INFO_UNIT);
				oWMS_MST_INFOCODE.setINFO_NO(INFO_NO);
				oWMS_MST_INFOCODE.setACT_FLAG(ACT_FLAG);
				oWMS_MST_INFOCODE.setDEL_FLAG(DEL_FLAG);
				oWMS_MST_INFOCODE.setCREATE_USER(CREATE_USER);
				oWMS_MST_INFOCODE.setUPDATE_USER(UPDATE_USER);
				oWMS_MST_INFOCODE.setDELETE_USER(DELETE_USER);
				infoCodeList.add(oWMS_MST_INFOCODE);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("WMS_MST_INFOCODE_Serviec.setInfoCodeObjList() Exception:" + e.toString() + " #");
		}
		return infoCodeList;
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
