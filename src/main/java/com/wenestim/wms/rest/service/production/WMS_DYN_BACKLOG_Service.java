/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.production.WMS_DYN_BACKLOG;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.impl.production.WMS_DYN_BACKLOG_Impl;
import com.wenestim.wms.rest.util.StringUtil;
/**
 * @author Andrew
 *
 */
@Path("/Production/Actual/BackLog")
public class WMS_DYN_BACKLOG_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_DYN_BACKLOG_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getBacklogList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_BACKLOG_Service.getBacklogList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

//        	String materialCode = (String)paramMap.get("MATERIAL_CODE");
//        	paramMap.put("MATERIAL_CODE", StringUtil.getJsonToObjectList(materialCode));

        	WMS_DYN_BACKLOG_Impl mWMS_DYN_BACKLOG_Impl = new WMS_DYN_BACKLOG_Impl();
        	List<WMS_DYN_BACKLOG> allList = mWMS_DYN_BACKLOG_Impl.getBacklogList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateBacklogList(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
    		) throws Exception
    {
        LOGGER.info("WMS_DYN_BACKLOG_Service.updateBacklogList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {

            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsonArray = new JSONArray(getString);

            List dataList = setDataObjList(jsonArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList"    , dataList   );
            dataMap.put("CLIENT_ID"   , clientId   );
            dataMap.put("COMPANY_CODE", companyCode);
            dataMap.put("USER_ID"     , userId     );

            WMS_DYN_BACKLOG_Impl iWMS_DYN_BACKLOG_Impl = new WMS_DYN_BACKLOG_Impl();
            rtnBoolResult = iWMS_DYN_BACKLOG_Impl.updateBacklogList(dataMap);
            if(rtnBoolResult)
            {
                WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("PROD_BACKLOG", jsonArray, userId, clientId, companyCode);
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public List setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
    {
        List<WMS_DYN_BACKLOG> dataList = null;

        try
        {
            dataList = new ArrayList<WMS_DYN_BACKLOG>();

            for (int i=0; i<jsArray.length(); i++)
            {
            	int WSID             = jsArray.getJSONObject(i).has("WSID"       ) ? jsArray.getJSONObject(i).getInt("WSID"            ):0;
            	String STORAGE_LOC   = jsArray.getJSONObject(i).has("STORAGE_LOC") ? jsArray.getJSONObject(i).getString("STORAGE_LOC") : "";
            	Double BACKLOG_QTY   = jsArray.getJSONObject(i).has("BACKLOG_QTY") ? jsArray.getJSONObject(i).getDouble("BACKLOG_QTY") : 0;
            	Double GI_QTY        = BACKLOG_QTY*-1;

            	WMS_DYN_BACKLOG oWMS_DYN_PROD_BACKLOG = new WMS_DYN_BACKLOG();

            	oWMS_DYN_PROD_BACKLOG.setCLIENT_ID    (clientId    );
            	oWMS_DYN_PROD_BACKLOG.setCOMPANY_CODE (comapnyCode );
            	oWMS_DYN_PROD_BACKLOG.setWSID         (WSID        );
            	oWMS_DYN_PROD_BACKLOG.setUPDATE_USER  (userId      );

            	dataList.add(oWMS_DYN_PROD_BACKLOG);

				jsArray.getJSONObject(i).put("MOVEMENT_TYPE"   , "200"      );
				jsArray.getJSONObject(i).put("CURR_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("CURR_QTY"        , GI_QTY     );
				jsArray.getJSONObject(i).put("MOVE_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("MOVE_QTY"        , GI_QTY     );
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        return dataList;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
