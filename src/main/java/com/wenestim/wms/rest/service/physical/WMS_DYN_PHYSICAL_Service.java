/**
 *
 */
package com.wenestim.wms.rest.service.physical;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.physical.WMS_DYN_PHYSICAL;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.impl.physical.WMS_DYN_PHYSICAL_Impl;
import com.wenestim.wms.rest.util.StringUtil;


/**
 * @author
 *
 */
@Path("/Physical")
public class WMS_DYN_PHYSICAL_Service {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PHYSICAL_Service.class);

    @GET
    @Path("/All")
    public Response getAllPhysical(
            @QueryParam("CLIENT_ID"    ) String CLIENT_ID    ,
    		@QueryParam("COMPANY_CODE" ) String COMPANY_CODE ,
            @QueryParam("PLANT_CODE"   ) String PLANT_CODE   ,
            @QueryParam("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @QueryParam("MATERIAL_TYPE") String MATERIAL_TYPE,
            @QueryParam("MATERIAL_CODE") String MATERIAL_CODE
            )
    {
        LOGGER.info("WMS_DYN_PHYSICAL_Service.getAllPhysical() called");

        String result = "";
        try
        {
        	CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID    );
        	COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE );
        	PLANT_CODE    = StringUtil.getEmptyNullPrevString(PLANT_CODE   );
        	STORAGE_LOC   = StringUtil.getEmptyNullPrevString(STORAGE_LOC  );
        	MATERIAL_TYPE = StringUtil.getEmptyNullPrevString(MATERIAL_TYPE);
        	MATERIAL_CODE = StringUtil.getEmptyNullPrevString(MATERIAL_CODE);
            LOGGER.info("CLIENT_ID:"+CLIENT_ID+"COMPANY_CODE:"+COMPANY_CODE+",PLANT_CODE:"+PLANT_CODE+",MATERIAL_TYPE:"+MATERIAL_TYPE+",STORAGE_LOC:"+STORAGE_LOC+",MATERIAL_CODE:"+MATERIAL_CODE);

            WMS_DYN_PHYSICAL_Impl mWMS_DYN_PHYSICAL_Impl = new WMS_DYN_PHYSICAL_Impl();
            List<WMS_DYN_PHYSICAL> allList = mWMS_DYN_PHYSICAL_Impl.getAllPhysical(
            		CLIENT_ID    ,
            		COMPANY_CODE ,
            		PLANT_CODE   ,
            		STORAGE_LOC  ,
            		MATERIAL_TYPE,
            		MATERIAL_CODE
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePhysical(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PHYSICAL_Service.updatePhysical() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String    getString = StringUtil.getInputStreamToString(inputStream);
            JSONArray jsArray   = new JSONArray(getString);
            HashMap   rtnMap    = setDataObjList(jsArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", rtnMap.get("dataList"));

            WMS_DYN_PHYSICAL_Impl iWMS_DYN_PHYSICAL_Impl = new WMS_DYN_PHYSICAL_Impl();

            rtnBoolResult = iWMS_DYN_PHYSICAL_Impl.updatePhysical(dataMap);
            if(rtnBoolResult)
            {
                WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("PHYSICAL", jsArray, userId, clientId, companyCode);
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    // GWANGHYUN KIM 4/7/2018
    @POST
    @Path("PDA/Create/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response insertPhysical(InputStream inputStream,
            @PathParam("userId"     ) String userId   ,
            @PathParam("clientId"   ) String clientId ,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PHYSICAL_Service.insertPhysical() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONObject jsonObject = new JSONObject(getString);



            WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL = new WMS_DYN_PHYSICAL();
            oWMS_DYN_PHYSICAL = setObjectByJson("Insert", jsonObject, userId, clientId, companyCode);

            WMS_DYN_PHYSICAL_Impl iWMS_DYN_PHYSICAL_Impl = new WMS_DYN_PHYSICAL_Impl();

            Boolean chkExist = iWMS_DYN_PHYSICAL_Impl.chkExistsPhysicalByBarcode_NO(
            		oWMS_DYN_PHYSICAL.getCLIENT_ID()   ,
            		oWMS_DYN_PHYSICAL.getCOMPANY_CODE(),
            		oWMS_DYN_PHYSICAL.getBARCODE_NO()
            		);

            JsonObject rtnJson = new JsonObject();

            if(!chkExist) {

                rtnBoolResult = iWMS_DYN_PHYSICAL_Impl.insertPhysical(oWMS_DYN_PHYSICAL);

                if(rtnBoolResult)
                {
                    rtnJson.addProperty("ret", Integer.valueOf(0));
                    rtnJson.addProperty("msg", "Insert Physical Data Success : " + oWMS_DYN_PHYSICAL.getBARCODE_NO() + " ");
                }
            } else {

                rtnJson.addProperty("ret", Integer.valueOf(1));
                rtnJson.addProperty("msg", "Already Scanned : " + oWMS_DYN_PHYSICAL.getBARCODE_NO() + " ");
            }

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public HashMap setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
    {
        HashMap rtnMap = new HashMap();
        List<WMS_DYN_PHYSICAL> dataList = null;

        try
        {
            WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
            dataList = new ArrayList<WMS_DYN_PHYSICAL>();

            for (int i=0; i<jsArray.length(); i++)
            {
                int docNo               = jsArray.getJSONObject(i).has("DOC_NO"          ) ? jsArray.getJSONObject(i).getInt("DOC_NO"             ) : 0;
                String MATERIAL_CODE    = jsArray.getJSONObject(i).has("MATERIAL_CODE"   ) ? jsArray.getJSONObject(i).getString("MATERIAL_CODE"   ) : "";
                String POST_DATE        = jsArray.getJSONObject(i).has("POST_DATE"       ) ? jsArray.getJSONObject(i).getString("POST_DATE"       ) : "";
                String MOVEMENT_TYPE    = jsArray.getJSONObject(i).has("MOVEMENT_TYPE"   ) ? jsArray.getJSONObject(i).getString("MOVEMENT_TYPE"   ) : "";
                String CURR_STORAGE_LOC = jsArray.getJSONObject(i).has("CURR_STORAGE_LOC") ? jsArray.getJSONObject(i).getString("CURR_STORAGE_LOC") : "";
                Double CURR_QTY         = jsArray.getJSONObject(i).has("CURR_QTY"        ) ? jsArray.getJSONObject(i).getDouble("CURR_QTY"        ) : 0;
                String MOVE_STORAGE_LOC = jsArray.getJSONObject(i).has("MOVE_STORAGE_LOC") ? jsArray.getJSONObject(i).getString("MOVE_STORAGE_LOC") : "";
                Double DIFF_QTY         = jsArray.getJSONObject(i).has("DIFF_QTY"        ) ? jsArray.getJSONObject(i).getDouble("DIFF_QTY"        ) : 0;
                Double COUNT_QTY        = jsArray.getJSONObject(i).has("COUNT_QTY"       ) ? jsArray.getJSONObject(i).getDouble("COUNT_QTY"       ) : 0;
                String UNIT             = jsArray.getJSONObject(i).has("UNIT"            ) ? jsArray.getJSONObject(i).getString("UNIT"            ) : "";
                Double INV_QTY          = jsArray.getJSONObject(i).has("INV_QTY"         ) ? jsArray.getJSONObject(i).getDouble("INV_QTY"         ) : 0;
                String EDIT_FLAG        = jsArray.getJSONObject(i).has("EDIT_FLAG"       ) ? jsArray.getJSONObject(i).getString("EDIT_FLAG"       ) : "N";


                if(docNo == 0)
                {
                    docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(clientId, comapnyCode, "INV", "DOC_NO", 1));
                }

                WMS_DYN_PHYSICAL oWMS_DYN_PHYSICAL = new WMS_DYN_PHYSICAL();
                oWMS_DYN_PHYSICAL.setCLIENT_ID(clientId          );
                oWMS_DYN_PHYSICAL.setCOMPANY_CODE(comapnyCode    );
                oWMS_DYN_PHYSICAL.setDOC_NO(docNo                );
                oWMS_DYN_PHYSICAL.setSTORAGE_LOC(CURR_STORAGE_LOC);
                oWMS_DYN_PHYSICAL.setMATERIAL_CODE(MATERIAL_CODE );
                oWMS_DYN_PHYSICAL.setPOST_DATE(POST_DATE         );
                oWMS_DYN_PHYSICAL.setINV_QTY(CURR_QTY            );
                oWMS_DYN_PHYSICAL.setCOUNT_QTY(COUNT_QTY         );
                oWMS_DYN_PHYSICAL.setDIFF_QTY(DIFF_QTY           );
                oWMS_DYN_PHYSICAL.setINV_QTY(INV_QTY             );
                oWMS_DYN_PHYSICAL.setUNIT(UNIT                   );
                oWMS_DYN_PHYSICAL.setCREATE_USER(userId          );
                oWMS_DYN_PHYSICAL.setUPDATE_USER(userId          );

                dataList.add(oWMS_DYN_PHYSICAL);

                if( !MOVEMENT_TYPE.equals("") && DIFF_QTY != 0)
                {
                    jsArray.getJSONObject(i).put("CURR_QTY", DIFF_QTY*-1);
                    jsArray.getJSONObject(i).put("MOVE_QTY", DIFF_QTY*-1);
                }
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        rtnMap.put("dataList", dataList);

        return rtnMap;
    }

    //GWANGHYUN 4/7/2018
    private WMS_DYN_PHYSICAL setObjectByJson(String tblProc, JSONObject jsonObj, String userId, String clientId, String comapnyCode) throws Exception {

        WMS_DYN_PHYSICAL mWMS_DYN_PHYSICAL = new WMS_DYN_PHYSICAL();

        try {
            LOGGER.debug("setObjectByJson() jsonObj:" + jsonObj.toString());

            WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();

            int docNo            = jsonObj.has("DOC_NO"          )   ? jsonObj.getInt   ("DOC_NO"        ) : 0;
            String STORAGE_LOC   = jsonObj.has("STORAGE_LOC"     )   ? jsonObj.getString("STORAGE_LOC"   ) : "";
            String MATERIAL_CODE = jsonObj.has("MATERIAL_CODE"   )   ? jsonObj.getString("MATERIAL_CODE" ) : "";
            String POST_DATE     = jsonObj.has("POST_DATE"       )   ? jsonObj.getString("POST_DATE"     ) : "";
            String BARCODE_NO    = jsonObj.has("BARCODE_NO"      )   ? jsonObj.getString("BARCODE_NO"    ) : "";
            Double INV_QTY       = jsonObj.has("INV_QTY"         )   ? jsonObj.getDouble("INV_QTY"       ) : 0;
            Double COUNT_QTY     = jsonObj.has("COUNT_QTY"       )   ? jsonObj.getDouble("COUNT_QTY"     ) : 0;
            Double DIFF_QTY      = jsonObj.has("DIFF_QTY"        )   ? jsonObj.getDouble("DIFF_QTY"      ) : 0;
            String UNIT          = jsonObj.has("UNIT"            )   ? jsonObj.getString("UNIT"          ) : "";

            if(docNo == 0)
            {
                docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(clientId,comapnyCode, "INV", "DOC_NO", 1));
            }

            mWMS_DYN_PHYSICAL.setDOC_NO(docNo);
            mWMS_DYN_PHYSICAL.setSTORAGE_LOC(STORAGE_LOC);
            mWMS_DYN_PHYSICAL.setMATERIAL_CODE(MATERIAL_CODE);
            mWMS_DYN_PHYSICAL.setPOST_DATE(POST_DATE);
            mWMS_DYN_PHYSICAL.setBARCODE_NO(BARCODE_NO);
            mWMS_DYN_PHYSICAL.setINV_QTY(INV_QTY);
            mWMS_DYN_PHYSICAL.setCOUNT_QTY(COUNT_QTY);
            mWMS_DYN_PHYSICAL.setDIFF_QTY(DIFF_QTY);
            mWMS_DYN_PHYSICAL.setUNIT(UNIT);
            mWMS_DYN_PHYSICAL.setCOMPANY_CODE(comapnyCode);
            mWMS_DYN_PHYSICAL.setCREATE_USER(userId);
            mWMS_DYN_PHYSICAL.setCLIENT_ID(clientId);

        } catch (Exception e) {
            LOGGER.error("Exception :"+e.getMessage()+"#");
            e.printStackTrace();
        }
        return mWMS_DYN_PHYSICAL;
    }


    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "confirm");
        }
        else
        {
            String rtnMsg = "could not save data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
