package com.wenestim.wms.rest.service.summary.defaults;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.impl.summary.defaults.WMS_SMR_INBOUND_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Default/Summary/Inbound")
public class WMS_SMR_INBOUND_Service {

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_INBOUND_Service.class);

	/**
	 * GET INBOUND ORDER DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return INBOUND ORDER DAILY DETAIL
	 */
	@GET
	@Path("/Order/DailyDetail")
	public Response getInboundOrderDailyDetail(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PO_DATE") String PO_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundOrderDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", PO_DATE: " + PO_DATE);

		String result = "";
		try {
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_DATE      = StringUtil.getEmptyNullPrevString(PO_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_INBOUND_Impl.getInboundOrderDailyDetail(CLIENT_ID, COMPANY_CODE, PO_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Order/Vendor/Top5")
	public Response getInboundOrderTop5ByVendor(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PO_DATE") String PO_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundOrderTop5ByVendor() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", PO_DATE: " + PO_DATE);

		String result = "";
		try {
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_DATE      = StringUtil.getEmptyNullPrevString(PO_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> vendorMapList = iWMS_SMR_INBOUND_Impl.getInboundOrderTop5ByVendor(CLIENT_ID, COMPANY_CODE, PO_DATE);

			Gson gson = new Gson();
			if (vendorMapList != null) {
				result = gson.toJson(vendorMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	@GET
	@Path("/Order/TotalQty")
	public Response getInboundOrderDailyTotalQty(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PO_DATE") String PO_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundOrderTop5ByVendor() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", PO_DATE: " + PO_DATE);

		String result = "";
		try {
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_DATE      = StringUtil.getEmptyNullPrevString(PO_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> orderedTotalQty = iWMS_SMR_INBOUND_Impl.getInboundOrderDailyTotalQty(CLIENT_ID, COMPANY_CODE, PO_DATE);

			Gson gson = new Gson();
			if (orderedTotalQty != null) {
				result = gson.toJson(orderedTotalQty);
			}

			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	/**
	 * GET INBOUND RECEIVE DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return INBOUND RECEIVE DAILY DETAIL
	 */
	@GET
	@Path("/Receive/DailyDetail")
	public Response getInboundReceiveDailyDetail(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("GR_DATE") String GR_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundReceiveDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", GR_DATE: " + GR_DATE);

		String result = "";
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			GR_DATE      = StringUtil.getEmptyNullPrevString(GR_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_INBOUND_Impl.getInboundReceiveDailyDetail(CLIENT_ID, COMPANY_CODE, GR_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Receive/Vendor/Top5")
	public Response getInboundReceiveTop5ByVendor(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("GR_DATE") String GR_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundReceiveTop5ByVendor() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", GR_DATE: " + GR_DATE);

		String result = "";
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			GR_DATE      = StringUtil.getEmptyNullPrevString(GR_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> vendorMapList = iWMS_SMR_INBOUND_Impl.getInboundReceiveTop5ByVendor(CLIENT_ID, COMPANY_CODE, GR_DATE);

			Gson gson = new Gson();
			if (vendorMapList != null) {
				result = gson.toJson(vendorMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	@GET
	@Path("/Receive/TotalQty")
	public Response getInboundReceiveDailyTotalQty(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("GR_DATE") String GR_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundReceiveTop5ByVendor() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", GR_DATE: " + GR_DATE);

		String result = "";
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			GR_DATE      = StringUtil.getEmptyNullPrevString(GR_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> receivedTotalQty = iWMS_SMR_INBOUND_Impl.getInboundReceiveDailyTotalQty(CLIENT_ID, COMPANY_CODE, GR_DATE);

			Gson gson = new Gson();
			if (receivedTotalQty != null) {
				result = gson.toJson(receivedTotalQty);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	/**
	 * GET INBOUND REMAIN DAILY DETAIL
	 * @param COMPANY_CODE
	 * @param PO_DATE
	 * @return INBOUND REMAIN DAILY DETAIL
	 */
	@GET
	@Path("/Remain/DailyDetail")
	public Response getInboundRemainDailyDetail(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PO_DATE") String PO_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundRemainDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", PO_DATE: " + PO_DATE);

		String result = "";
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_DATE      = StringUtil.getEmptyNullPrevString(PO_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_INBOUND_Impl.getInboundRemainDailyDetail(CLIENT_ID, COMPANY_CODE, PO_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Remain/Vendor/Top5")
	public Response getInboundRemainTop5ByVendor(
			@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PO_DATE") String PO_DATE) {

		LOGGER.info("WMS_SMR_INBOUND_Service.getInboundRemainTop5ByVendor() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", PO_DATE: " + PO_DATE);

		String result = "";
		try {
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PO_DATE      = StringUtil.getEmptyNullPrevString(PO_DATE);

			WMS_SMR_INBOUND_Impl iWMS_SMR_INBOUND_Impl = new WMS_SMR_INBOUND_Impl();
			List<HashMap<?, ?>> vendorMapList = iWMS_SMR_INBOUND_Impl.getInboundRemainTop5ByVendor(CLIENT_ID, COMPANY_CODE, PO_DATE);

			Gson gson = new Gson();
			if (vendorMapList != null) {
				result = gson.toJson(vendorMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

}
