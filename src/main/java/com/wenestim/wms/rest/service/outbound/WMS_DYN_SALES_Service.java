/**
 *
 */
package com.wenestim.wms.rest.service.outbound;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_SALES;
import com.wenestim.wms.rest.impl.outbound.WMS_DYN_SALES_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Outbound/Sales")
public class WMS_DYN_SALES_Service {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_SALES_Service.class);

    @GET
    @Path("/All")
    public Response getAllSales() {
        LOGGER.info("WMS_DYN_SALES_Service.getAllSales() called");

        String result = "";
        try {
            WMS_DYN_SALES_Impl mWMS_DYN_SALES_Impl = new WMS_DYN_SALES_Impl();
            List<WMS_DYN_SALES> allList = mWMS_DYN_SALES_Impl.getAllSales();

            Gson gson = new Gson();
            if (allList != null) {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @GET
    @Path("/List")
    public Response getAllSalesByCompany(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("PL_NO") String PL_NO,
            @QueryParam("MAIN_FLAG") String MAIN_FLAG, @QueryParam("SALES_ORG_CODE") String SALES_ORG_CODE,
            @QueryParam("SO_NO") String SO_NO, @QueryParam("SO_TYPE") String SO_TYPE, @QueryParam("SO_ITEM_NO") String SO_ITEM_NO,
            @QueryParam("CUST_CODE") String CUST_CODE, @QueryParam("DELIVERY_DATE1") String DELIVERY_DATE1,
            @QueryParam("DELIVERY_DATE2") String DELIVERY_DATE2) {
        LOGGER.info("WMS_DYN_SALES_Service.getAllSalesByCompany() called");

        String result = "";
        try {
            CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
            COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            PL_NO = StringUtil.getEmptyNullPrevString(PL_NO);
            MAIN_FLAG = StringUtil.getEmptyNullPrevString(MAIN_FLAG);
            SALES_ORG_CODE = StringUtil.getEmptyNullPrevString(SALES_ORG_CODE);
            SO_NO = StringUtil.getEmptyNullPrevString(SO_NO);
            SO_TYPE = StringUtil.getEmptyNullPrevString(SO_TYPE);
            SO_ITEM_NO = StringUtil.getEmptyNullPrevString(SO_ITEM_NO);
            CUST_CODE = StringUtil.getEmptyNullPrevString(CUST_CODE);
            DELIVERY_DATE1 = StringUtil.getEmptyNullPrevString(DELIVERY_DATE1);
            DELIVERY_DATE2 = StringUtil.getEmptyNullPrevString(DELIVERY_DATE2);

            WMS_DYN_SALES_Impl iWMS_DYN_SALES_Impl = new WMS_DYN_SALES_Impl();
            List<WMS_DYN_SALES> dataList = iWMS_DYN_SALES_Impl.getAllSalesByCompany(CLIENT_ID, COMPANY_CODE, PL_NO, MAIN_FLAG,
                    SALES_ORG_CODE, SO_NO, SO_TYPE, SO_ITEM_NO, CUST_CODE, DELIVERY_DATE1, DELIVERY_DATE2);

            Gson gson = new Gson();
            if (dataList != null) {
                result = gson.toJson(dataList);
            }
            return Response.status(200).entity(result).build();

        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createSales(InputStream inputStream) throws Exception {
        LOGGER.info("WMS_DYN_SALES_Service.createSales() called");

        Boolean rtnData = false;
        String rtnMsg = "";
        try {
            String getString = StringUtil.getInputStreamToString(inputStream);
            int splitPos = getString.indexOf("^");
            String dataString = getString.substring(splitPos + 1);

            dataString = "[" + dataString.substring(1, dataString.length()) + "]";

            JSONArray saveList = new JSONArray(dataString);
            rtnData = setSalesOrderObj("INSERT", "SO", saveList, getString,"");

            JsonObject rtnJson = new JsonObject();
            rtnJson.addProperty("Method", "CREATE");
            rtnJson.addProperty("Object", "WMS_DYN_SALES");

            JsonObject jsonObject = new JsonObject();

            jsonObject = setTableEventResultHandler(rtnData, "");
            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (Exception e) {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Delete/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteSelectedSales(InputStream inputStream, @PathParam("userId") String userId) throws Exception {
        LOGGER.info("WMS_DYN_SALES_Service.deleteSelectedSales() called");

        Boolean rtnData = false;
        String rtnMsg = "";
        try {
            String getString = StringUtil.getInputStreamToString(inputStream);
            int splitPos = getString.indexOf("^");
            String dataString = getString.substring(splitPos + 1);

            dataString = "[" + dataString.substring(1, dataString.length()) + "]";

            JSONArray saveList = new JSONArray(dataString);
            rtnData = setSalesOrderObj("DELETE", "SO", saveList, getString, userId);

            JsonObject rtnJson = new JsonObject();
            rtnJson.addProperty("Method", "DELETE");
            rtnJson.addProperty("Object", "WMS_DYN_SALES");

            JsonObject jsonObject = new JsonObject();

            jsonObject = setTableEventResultHandler(rtnData, "");

            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (Exception e) {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Delete/AllSales/{CLIENT_ID}/{COMPANY_CODE}/{SO_NO}/{userId}")
    public Response deleteAllSales(@PathParam("CLIENT_ID") String CLIENT_ID, @PathParam("COMPANY_CODE") String COMPANY_CODE, @PathParam("SO_NO") String SO_NO,@PathParam("userId") String userId) throws Exception {
        LOGGER.info("WMS_DYN_SALES_Service.deleteAllSales() called");

        Boolean rtnData = false;
        String rtnMsg = "";
        try {
            CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
            COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
            SO_NO = StringUtil.getEmptyNullPrevString(SO_NO);


            WMS_DYN_SALES_Impl iWMS_DYN_SALES_Impl = new WMS_DYN_SALES_Impl();
//            LOGGER.info(COMPANY_CODE);
//            LOGGER.info(SO_NO);
            rtnData = iWMS_DYN_SALES_Impl.deleteAllSales(CLIENT_ID, COMPANY_CODE, SO_NO, userId);

            JsonObject rtnJson = new JsonObject();
            rtnJson.addProperty("Method", "DELETE");
            rtnJson.addProperty("Object", "WMS_DYN_SALES");

            JsonObject jsonObject = new JsonObject();

            jsonObject = setTableEventResultHandler(rtnData, "");

            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (Exception e) {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update/{userId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateSelectedSales(InputStream inputStream, @PathParam("userId") String userId) throws Exception {
        LOGGER.info("WMS_DYN_SALES_Service.updateSelectedSales() called");

        Boolean rtnData = false;
        String rtnMsg = "";
        try {
            String getString = StringUtil.getInputStreamToString(inputStream);
            int splitPos = getString.indexOf("^");
            String dataString = getString.substring(splitPos + 1);

            dataString = "[" + dataString.substring(1, dataString.length()) + "]";

            JSONArray saveList = new JSONArray(dataString);
            rtnData = setSalesOrderObj("UPDATE", "SO", saveList, getString, userId);

            JsonObject rtnJson = new JsonObject();
            rtnJson.addProperty("Method", "UPDATE");
            rtnJson.addProperty("Object", "WMS_DYN_SALES");

            JsonObject jsonObject = new JsonObject();

            jsonObject = setTableEventResultHandler(rtnData, "");

            return Response.status(200).entity(jsonObject.toString()).build();
        } catch (Exception e) {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/UpdateHeader")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateHeader(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_SALES_Service.updateHeader() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

//          String dataItem = (String)paramMap.get("dataItem") ;
//          HashMap itemMap = StringUtil.getJsonToHashMapList(dataItem).get(0);
//          StringUtil.copyHashMap(itemMap,paramMap);

            WMS_DYN_SALES_Impl iWMS_DYN_SALES_Impl = new WMS_DYN_SALES_Impl();
            rtnBoolResult = iWMS_DYN_SALES_Impl.updateHeader(paramMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public boolean setSalesOrderObj(String evtHandler, String inputType, JSONArray saveList, String getString, String userId)
            throws Exception {
        Boolean result = true;
        try {
            HashMap<String, Object> paramMap = StringUtil.getParamMap(getString);
            LOGGER.info(paramMap.get("SALES_ORG_CODE"));
            String CLIENT_ID        = (String) paramMap.get("CLIENT_ID");
            String COMPANY_CODE     = (String) paramMap.get("COMPANY_CODE");
            String SALES_ORG_CODE   = (String) paramMap.get("SALES_ORG_CODE");
            String SO_NO            = (String) paramMap.get("SO_NO");
            String SO_TYPE          = (String) paramMap.get("SO_TYPE");
            String CUST_CODE        = (String) paramMap.get("CUST_CODE");
            String DELIVERY_DATE    = (String) paramMap.get("DELIVERY_DATE");
            String CUST_PO_NO       = (String) paramMap.get("CUST_PO_NO");
            String USER_ID          = (String) paramMap.get("USER_ID");
            String CREATE_DATE      = (String) paramMap.get("CREATE_DATE");
            String UPDATE_USER      = (String) paramMap.get("UPDATE_USER");
            String UPDATE_DATE      = (String) paramMap.get("UPDATE_DATE");
            String DELETE_USER      = (String) paramMap.get("DELETE_USER");
            String DELETE_DATE      = (String) paramMap.get("DELETE_DATE");
            String DEL_FLAG         = (String) paramMap.get("DEL_FLAG");

            Integer SO_ITEM_NO = 0 ;

            WMS_DYN_SALES_Impl iWMS_DYN_SALES_Impl = new WMS_DYN_SALES_Impl();
            List<WMS_DYN_SALES> dataList = new ArrayList<WMS_DYN_SALES>();

            System.out.println("saveList.length():"+saveList.length());

            for (int i = 0; i < saveList.length(); i++) {
                Integer WSID            = saveList.getJSONObject(i).has("WSID"           ) ? saveList.getJSONObject(i).getInt   ("WSID"           ) : 0  ;
                String PL_NO            = saveList.getJSONObject(i).has("PL_NO"          ) ? saveList.getJSONObject(i).getString("PL_NO"          ) : "" ;
                String MATERIAL_CODE    = saveList.getJSONObject(i).has("MATERIAL_CODE"  ) ? saveList.getJSONObject(i).getString("MATERIAL_CODE"  ) : "" ;
                Double ORDER_QTY        = saveList.getJSONObject(i).has("ORDER_QTY"      ) ? saveList.getJSONObject(i).getDouble("ORDER_QTY"      ) : 0  ;
                String UNIT             = saveList.getJSONObject(i).has("UNIT"           ) ? saveList.getJSONObject(i).getString("UNIT"           ) : "" ;
                Double CONV_QTY         = saveList.getJSONObject(i).has("CONV_QTY"       ) ? saveList.getJSONObject(i).getDouble("CONV_QTY"       ) : 0  ;
                String CONV_UNIT        = saveList.getJSONObject(i).has("CONV_UNIT"      ) ? saveList.getJSONObject(i).getString("CONV_UNIT"      ) : "" ;
                Double SALES_PRICE      = saveList.getJSONObject(i).has("SALES_PRICE"    ) ? saveList.getJSONObject(i).getDouble("SALES_PRICE"    ) : 0  ;
                Double ITEM_AMOUNT      = saveList.getJSONObject(i).has("ITEM_AMOUNT"    ) ? saveList.getJSONObject(i).getDouble("ITEM_AMOUNT"    ) : 0  ;
                String PROMO_NAME       = saveList.getJSONObject(i).has("PROMO_NAME"     ) ? saveList.getJSONObject(i).getString("PROMO_NAME"     ) : "" ;
                String PROMO_DESC       = saveList.getJSONObject(i).has("PROMO_DESC"     ) ? saveList.getJSONObject(i).getString("PROMO_DESC"     ) : "" ;
                Double PROMO_DISC       = saveList.getJSONObject(i).has("PROMO_DISC"     ) ? saveList.getJSONObject(i).getDouble("PROMO_DISC"     ) : 0  ;
                String CURRENCY         = saveList.getJSONObject(i).has("CURRENCY"       ) ? saveList.getJSONObject(i).getString("CURRENCY"       ) : "" ;
                String EO_NO            = saveList.getJSONObject(i).has("EO_NO"          ) ? saveList.getJSONObject(i).getString("EO_NO"          ) : "" ;
                String CUST_PO_ITEM_NO  = saveList.getJSONObject(i).has("CUST_PO_ITEM_NO") ? saveList.getJSONObject(i).getString("CUST_PO_ITEM_NO") : "" ;
                String CUST_PO_QTY      = saveList.getJSONObject(i).has("CUST_PO_QTY"    ) ? saveList.getJSONObject(i).getString("CUST_PO_QTY"    ) : "" ;
                String ITEM_RANK        = saveList.getJSONObject(i).has("ITEM_RANK"      ) ? saveList.getJSONObject(i).getString("ITEM_RANK"      ) : "" ;
                String ITEM_INDEX       = saveList.getJSONObject(i).has("ITEM_INDEX"     ) ? saveList.getJSONObject(i).getString("ITEM_INDEX"     ) : "" ;
                String CONV_COUNT       = saveList.getJSONObject(i).has("CONV_COUNT"     ) ? saveList.getJSONObject(i).getString("CONV_COUNT"     ) : "" ;

                if( CUST_PO_ITEM_NO.equals(""))
                {
                    CUST_PO_ITEM_NO = null;
                }

                if( CUST_PO_QTY == null || CUST_PO_QTY.equals(""))
                {
                    CUST_PO_QTY = "0";
                }

                if( ITEM_RANK == null || ITEM_RANK.equals(""))
                {
                    ITEM_RANK = "0";
                }

                if( ITEM_INDEX == null || ITEM_INDEX.equals(""))
                {
                    ITEM_INDEX = "0";
                }

                if( CONV_COUNT == null || CONV_COUNT.equals(""))
                {
                    CONV_COUNT = "0";
                }

                String HEADER_FLAG;

                if (i == 0) {HEADER_FLAG = "Y";} else {HEADER_FLAG = "N";}

                if (evtHandler.equals("UPDATE") || evtHandler.equals("DELETE")) {
                    WSID                = saveList.getJSONObject(i).has("WSID")             ? saveList.getJSONObject(i).getInt("WSID")              : 0;
                    CLIENT_ID           = saveList.getJSONObject(i).has("CLIENT_ID")        ? saveList.getJSONObject(i).getString("CLIENT_ID")      : "";
                    COMPANY_CODE        = saveList.getJSONObject(i).has("COMPANY_CODE")     ? saveList.getJSONObject(i).getString("COMPANY_CODE")   : "";
                    USER_ID             = saveList.getJSONObject(i).has("CREATE_USER")      ? saveList.getJSONObject(i).getString("CREATE_USER")    : "";
                    SO_ITEM_NO          = saveList.getJSONObject(i).has("SO_ITEM_NO")       ? saveList.getJSONObject(i).getInt("SO_ITEM_NO")        : 0;
                    SO_NO               = saveList.getJSONObject(i).has("SO_NO")            ? saveList.getJSONObject(i).getString("SO_NO")          : "";
                    SO_TYPE             = saveList.getJSONObject(i).has("SO_TYPE")          ? saveList.getJSONObject(i).getString("SO_TYPE")        : "";
                    CUST_CODE           = saveList.getJSONObject(i).has("CUST_CODE")        ? saveList.getJSONObject(i).getString("CUST_CODE")      : "";
                    DELIVERY_DATE       = saveList.getJSONObject(i).has("DELIVERY_DATE")    ? saveList.getJSONObject(i).getString("DELIVERY_DATE")  : "";
                    SALES_ORG_CODE      = saveList.getJSONObject(i).has("SALES_ORG_CODE")   ? saveList.getJSONObject(i).getString("SALES_ORG_CODE") : "";
                    CREATE_DATE         = saveList.getJSONObject(i).has("CREATE_DATE")      ? saveList.getJSONObject(i).getString("CREATE_DATE")    : "";
                    UPDATE_USER         = saveList.getJSONObject(i).has("UPDATE_USER")      ? saveList.getJSONObject(i).getString("UPDATE_USER")    : "";
                    UPDATE_DATE         = saveList.getJSONObject(i).has("UPDATE_DATE")      ? saveList.getJSONObject(i).getString("UPDATE_DATE")    : "";
                    DELETE_USER         = saveList.getJSONObject(i).has("DELETE_USER")      ? saveList.getJSONObject(i).getString("DELETE_USER")    : "";
                    DELETE_DATE         = saveList.getJSONObject(i).has("DELETE_DATE")      ? saveList.getJSONObject(i).getString("DELETE_DATE")    : "";

                }

                WMS_DYN_SALES oWMS_DYN_SALES = new WMS_DYN_SALES();

                oWMS_DYN_SALES.setCLIENT_ID(CLIENT_ID);
                oWMS_DYN_SALES.setCOMPANY_CODE(COMPANY_CODE);
                oWMS_DYN_SALES.setSALES_ORG_CODE(SALES_ORG_CODE);
                oWMS_DYN_SALES.setSO_NO(SO_NO);
                oWMS_DYN_SALES.setSO_TYPE(SO_TYPE);
                oWMS_DYN_SALES.setCUST_CODE(CUST_CODE);
                oWMS_DYN_SALES.setDELIVERY_DATE(DELIVERY_DATE);
                oWMS_DYN_SALES.setCREATE_USER(USER_ID);
                oWMS_DYN_SALES.setCREATE_DATE(CREATE_DATE);
                oWMS_DYN_SALES.setUPDATE_USER(UPDATE_USER);
                oWMS_DYN_SALES.setUPDATE_DATE(UPDATE_DATE);
                oWMS_DYN_SALES.setDELETE_USER(DELETE_USER);
                oWMS_DYN_SALES.setDELETE_DATE(DELETE_DATE);
                oWMS_DYN_SALES.setDEL_FLAG(DEL_FLAG);
                oWMS_DYN_SALES.setCUST_PO_NO(CUST_PO_NO);
                oWMS_DYN_SALES.setCUST_PO_QTY(new Float(CUST_PO_QTY));
                oWMS_DYN_SALES.setITEM_RANK(new Integer(ITEM_RANK));
                oWMS_DYN_SALES.setITEM_INDEX(new Integer(ITEM_INDEX));
                oWMS_DYN_SALES.setCONV_COUNT(new Float(CONV_COUNT));



                if (evtHandler.equals("INSERT")) {
                    oWMS_DYN_SALES.setSO_ITEM_NO(10 * (i + 1));
                } else {
                    if(evtHandler.equals("UPDATE")){
                        oWMS_DYN_SALES.setUPDATE_USER(userId);
                    }else if(evtHandler.equals("DELETE")){
                        oWMS_DYN_SALES.setDELETE_USER(userId);
                    }
                    oWMS_DYN_SALES.setSO_ITEM_NO(SO_ITEM_NO);
                }

                oWMS_DYN_SALES.setMATERIAL_CODE(MATERIAL_CODE);
                oWMS_DYN_SALES.setORDER_QTY(new Float(ORDER_QTY));
                oWMS_DYN_SALES.setUNIT(UNIT);
                oWMS_DYN_SALES.setCONV_QTY(new Float(CONV_QTY));
                oWMS_DYN_SALES.setCONV_UNIT(CONV_UNIT);
                oWMS_DYN_SALES.setHEADER_FLAG(HEADER_FLAG);
                oWMS_DYN_SALES.setWSID(WSID);
                oWMS_DYN_SALES.setPL_NO(PL_NO);
                oWMS_DYN_SALES.setCURRENCY(CURRENCY);
                oWMS_DYN_SALES.setITEM_AMOUNT(new Float(ITEM_AMOUNT));
                oWMS_DYN_SALES.setPROMO_DESC(PROMO_DESC);
                oWMS_DYN_SALES.setPROMO_NAME(PROMO_NAME);
                oWMS_DYN_SALES.setPROMO_DISC(new Float(PROMO_DISC));
                oWMS_DYN_SALES.setSALES_PRICE(new Float(SALES_PRICE));
                oWMS_DYN_SALES.setCUST_PO_ITEM_NO(CUST_PO_ITEM_NO);
                oWMS_DYN_SALES.setEO_NO(EO_NO);
                dataList.add(oWMS_DYN_SALES);
            }

            if (dataList.size() > 0) {
                HashMap<String, Object> dataMap = new HashMap<String, Object>();
                dataMap.put("CLIENT_ID"   , CLIENT_ID   );
                dataMap.put("COMPANY_CODE", COMPANY_CODE);
                dataMap.put("SO_NO"       , SO_NO       );
                dataMap.put("USER_ID"     , USER_ID     );
                dataMap.put("dataList", dataList);
//                LOGGER.info(dataMap);
//                LOGGER.info(dataList);
                if (evtHandler.equals("INSERT")) {
                    result = iWMS_DYN_SALES_Impl.insertSales(dataMap);
                } else if (evtHandler.equals("UPDATE")) {
                    result = iWMS_DYN_SALES_Impl.updateSales(dataMap);
                } else if (evtHandler.equals("DELETE")) {
                    result = iWMS_DYN_SALES_Impl.deleteSales(dataMap);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            result = false;
        }
        return result;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData) {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue()) {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "confirm");
        } else {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }

}
