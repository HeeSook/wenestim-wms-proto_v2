package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_PROMOTION;
import com.wenestim.wms.rest.impl.master.WMS_MST_PROMOTION_Impl;
import com.wenestim.wms.rest.util.StringUtil;

@Path("/Master/Promotion")
public class WMS_MST_PROMOTION_Service {

	private static final Logger LOGGER = Logger.getLogger(WMS_MST_PROMOTION_Service.class);


	@GET
	@Path("/List")
	public Response getAllPromotionByCompany(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("DEL_FLAG") String DEL_FLAG) {

		String result = "";
		try
		{
			DEL_FLAG =  StringUtil.getEmptyNullPrevString(DEL_FLAG);
			WMS_MST_PROMOTION_Impl iWMS_MST_PROMOTION_Impl = new WMS_MST_PROMOTION_Impl();
			List<WMS_MST_PROMOTION> dataList = iWMS_MST_PROMOTION_Impl.getAllPromotionByCompany(CLIENT_ID, COMPANY_CODE, DEL_FLAG);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createPromotion(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_PROMOTION_Service.createPromotion() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings Insert Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_PROMOTION_Impl iWMS_MST_PROMOTION_Impl = new WMS_MST_PROMOTION_Impl();
			WMS_MST_PROMOTION oWMS_MST_PROMOTION  = new WMS_MST_PROMOTION();
			int maxCode = iWMS_MST_PROMOTION_Impl.getMaxPromoCode();
			maxCode = maxCode + 1;

			oWMS_MST_PROMOTION = setObjectByJson(jsonArray, "CREATE");
			oWMS_MST_PROMOTION.setPROMO_CODE(maxCode);
			oWMS_MST_PROMOTION.setDEL_FLAG("N");

			boolean result = iWMS_MST_PROMOTION_Impl.insertPromotion(oWMS_MST_PROMOTION);
			LOGGER.info("Promotion Insert Result: " + result);

			JsonObject rtnJson = new JsonObject();
			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Promotion is instered");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updatePromotion(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_PROMOTION_Service.updatePromotion() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings Update Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_PROMOTION_Impl iWMS_MST_PROMOTION_Impl = new WMS_MST_PROMOTION_Impl();
			WMS_MST_PROMOTION oWMS_MST_PROMOTION  = new WMS_MST_PROMOTION();

			oWMS_MST_PROMOTION = setObjectByJson(jsonArray, "UPDATE");

			boolean result = iWMS_MST_PROMOTION_Impl.updatePromotion(oWMS_MST_PROMOTION);
			LOGGER.info("Promotion Update Result: " + result);

			JsonObject rtnJson = new JsonObject();
			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Promotion is Updated");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deletePromotion(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_PROMOTION_Service.deletePromotion() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings Delete Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_PROMOTION_Impl iWMS_MST_PROMOTION_Impl = new WMS_MST_PROMOTION_Impl();
			WMS_MST_PROMOTION oWMS_MST_PROMOTION  = new WMS_MST_PROMOTION();

			oWMS_MST_PROMOTION = setObjectByJson(jsonArray, "UPDATE");

			boolean result = iWMS_MST_PROMOTION_Impl.deletePromotion(oWMS_MST_PROMOTION);
			LOGGER.info("Promotion Delete Result: " + result);

			JsonObject rtnJson = new JsonObject();
			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Promotion is Deleted");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	private WMS_MST_PROMOTION setObjectByJson(JSONArray jsArray, String evtHandler) {
		WMS_MST_PROMOTION oWMS_MST_PROMOTION  = new WMS_MST_PROMOTION();

		try {
			LOGGER.debug("setObjectByJson() jsArray:" + jsArray.toString());

			JSONObject jsonObj = jsArray.getJSONObject(0);

			if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
			{
				Integer WSID = Integer.valueOf(jsonObj.has("WSID") ? Integer.parseInt(jsonObj.getString("WSID")) : 0);
				LOGGER.debug("setMaterialObjList() WSID: " + WSID);
				oWMS_MST_PROMOTION.setWSID(WSID);
			}

			String  CLIENT_ID     = jsonObj.has("CLIENT_ID"     )   ? jsonObj.getString ("CLIENT_ID"     ) : "";
			String  COMPANY_CODE  = jsonObj.has("COMPANY_CODE"  )   ? jsonObj.getString ("COMPANY_CODE"  ) : "";
			String  PROMO_DESC    = jsonObj.has("PROMO_DESC"    )   ? jsonObj.getString ("PROMO_DESC"    ) : "";
			Float   PROMO_DISC    = Float.valueOf(jsonObj.has("PROMO_DISC") ? Float.parseFloat(jsonObj.getString("PROMO_DISC")) : jsonObj.getString("PROMO_DISC") == "" ? 0.0F : 0.0F);
			String  CREATE_USER   = jsonObj.has("CREATE_USER"   )   ? jsonObj.getString ("CREATE_USER"   ) : "";
			String  UPDATE_USER   = jsonObj.has("UPDATE_USER"   )   ? jsonObj.getString ("UPDATE_USER"   ) : "";
			String  DELETE_USER   = jsonObj.has("DELETE_USER"   )   ? jsonObj.getString ("DELETE_USER"   ) : "";

			oWMS_MST_PROMOTION.setCLIENT_ID(CLIENT_ID);
			oWMS_MST_PROMOTION.setCOMPANY_CODE(COMPANY_CODE);
			oWMS_MST_PROMOTION.setPROMO_DESC(PROMO_DESC);
			oWMS_MST_PROMOTION.setPROMO_DISC(PROMO_DISC);
			oWMS_MST_PROMOTION.setCREATE_USER(CREATE_USER);
			oWMS_MST_PROMOTION.setUPDATE_USER(UPDATE_USER);
			oWMS_MST_PROMOTION.setDELETE_USER(DELETE_USER);

		}
		catch (Exception e) {
			LOGGER.error("Exception :"+e.getMessage()+"#");
			e.printStackTrace();
		}
		return oWMS_MST_PROMOTION;
	}

}