/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.impl.production.WMS_DYN_PLANNED_ORDER_Impl;
import com.wenestim.wms.rest.util.StringUtil;
/**
 * @author Andrew
 *
 */
@Path("/Production/PlannedOrder")
public class WMS_DYN_PLANNED_ORDER_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_DYN_PLANNED_ORDER_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPlannedOrderList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_PLANNED_ORDER_Service.getPlannedOrderList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String saveData    = (String)paramMap.get("saveData") ;
        	ArrayList saveList = StringUtil.getJsonToHashMapList(saveData);
        	paramMap.put("saveList", saveList);

        	WMS_DYN_PLANNED_ORDER_Impl mWMS_DYN_PLANNED_ORDER_Impl = new WMS_DYN_PLANNED_ORDER_Impl();
        	List<HashMap<String,Object>> allList = mWMS_DYN_PLANNED_ORDER_Impl.getPlannedOrderList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePlannedOrder(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_PLANNED_ORDER_Service.updatePlannedOrder() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String saveData    = (String)paramMap.get("saveData") ;
        	ArrayList saveList = StringUtil.getJsonToHashMapList(saveData);
        	paramMap.put("saveList", saveList);

        	WMS_DYN_PLANNED_ORDER_Impl mWMS_DYN_PLANNED_ORDER_Impl = new WMS_DYN_PLANNED_ORDER_Impl();
        	rtnBoolResult = mWMS_DYN_PLANNED_ORDER_Impl.updatePlannedOrder(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
