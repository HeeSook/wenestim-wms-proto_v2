package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_MATERIAL;
import com.wenestim.wms.rest.impl.master.WMS_MST_MATERIAL_Impl;
import com.wenestim.wms.rest.util.StringUtil;




/**
 * @author Andrew
 *
 */
@Path("/Master/Material")
public class WMS_MST_MATERIAL_Service
{
	private static final Logger LOGGER = Logger.getLogger(WMS_MST_MATERIAL_Service.class);

	@GET
	@Path("/Test")
	public Response getTest()
	{
		LOGGER.debug("WMS_MST_MATERIAL_Serviec.getTest called()");
		try
		{
			String result = "{ test: 1, test: 2 }";

			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/List")
	public Response getMaterialListByPlanMaterialCodeType(
			@QueryParam("CLIENT_ID"    ) String CLIENT_ID    ,
			@QueryParam("COMPANY_CODE" ) String COMPANY_CODE ,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE,
			@QueryParam("PLANT_CODE"   ) String PLANT_CODE   ,
			@QueryParam("MATERIAL_TYPE") String MATERIAL_TYPE,
			@QueryParam("DEL_FLAG"     ) String DEL_FLAG
			)
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.getMaterialListByPlanMaterialCodeType() called");
		LOGGER.debug("[PARAM] CLIENT_ID:" + CLIENT_ID + ", COMPANY_CODE:" + COMPANY_CODE + ",MATERIAL_CODE:" + MATERIAL_CODE +
				     "PLANT_CODE:" + PLANT_CODE + ",MATERIAL_TYPE:" + MATERIAL_TYPE + ",DEL_FLAG:" + DEL_FLAG);

		String result = "";
		try
		{
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID    );
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE );
			MATERIAL_CODE = StringUtil.getEmptyNullPrevString(MATERIAL_CODE);
			PLANT_CODE    = StringUtil.getEmptyNullPrevString(PLANT_CODE   );
			MATERIAL_TYPE = StringUtil.getEmptyNullPrevString(MATERIAL_TYPE);
			DEL_FLAG      = StringUtil.getEmptyNullPrevString(DEL_FLAG     );

			WMS_MST_MATERIAL_Impl mWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();

			List<WMS_MST_MATERIAL> materialList = mWMS_MST_MATERIAL_Impl.
					getMaterialListByPlanMaterialCodeType(CLIENT_ID, COMPANY_CODE, MATERIAL_CODE, PLANT_CODE, MATERIAL_TYPE, DEL_FLAG);

			Gson gson = new Gson();
			if (materialList != null)
			{
				result = gson.toJson(materialList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Part/Info")
	public Response getAllMaterialByCompany(
			@QueryParam("CLIENT_ID"   ) String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE
			)
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.getAllMaterialByCompany() called");
		LOGGER.debug("CLIENT_ID    : " + CLIENT_ID);
        LOGGER.debug("COMPANY_CODE : " + COMPANY_CODE);

		String result = "";
		try
		{
			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			List<WMS_MST_MATERIAL> dataList = iWMS_MST_MATERIAL_Impl.getAllMaterialByCompany(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (dataList != null)
			{
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Part/{MATERIAL_CODE}")
	public Response getMaterial(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@PathParam("MATERIAL_CODE") String MATERIAL_CODE)
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.getMaterial() called");
		LOGGER.debug("CLIENT_ID     : " + CLIENT_ID);
		LOGGER.debug("COMPANY_CODE  : " + COMPANY_CODE);

        LOGGER.debug("MATERIAL_CODE : " + MATERIAL_CODE);

		String result = "";
		try
		{
			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			List<WMS_MST_MATERIAL> dataList = iWMS_MST_MATERIAL_Impl.getMaterial(CLIENT_ID, COMPANY_CODE, MATERIAL_CODE);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Type/{TYPE}")
	public Response getMaterialByMaterialType(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@PathParam("TYPE") String TYPE)
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.getMaterial() called");
		LOGGER.debug("CLIENT_ID     : " + CLIENT_ID);
		LOGGER.debug("COMPANY_CODE  : " + COMPANY_CODE);
		LOGGER.debug("TYPE : " + TYPE);

		String result = "";
		try
		{
			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			List<WMS_MST_MATERIAL> dataList = iWMS_MST_MATERIAL_Impl.getMaterialByMaterialType(CLIENT_ID, COMPANY_CODE, TYPE);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Code/List")
	public Response getMaterialCodeByCompany(@QueryParam("CLIENT_CODE") String CLIENT_CODE, @QueryParam("COMPANY_CODE") String COMPANY_CODE)
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.getMaterialCodeByCompany() called");
		LOGGER.debug("CLIENT_CODE  : " + CLIENT_CODE);
        LOGGER.debug("COMPANY_CODE : " + COMPANY_CODE);

		String result = "";
		try
		{
			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			List<WMS_MST_MATERIAL> dataList = iWMS_MST_MATERIAL_Impl.getMaterialCodeByCompany(CLIENT_CODE, COMPANY_CODE);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertMaterialList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.insertMaterialList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsMaterialListArray = new JSONArray(getString);

			HashMap<String, Object> materialListMap = new HashMap<String, Object>();
			materialListMap.put("materialList", setMaterialObjList(jsMaterialListArray, "INSERT", userId));

			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			rtnBoolResult = iWMS_MST_MATERIAL_Impl.insertMaterialList(materialListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateMaterialList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.updateMaterial() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsMaterialListArray = new JSONArray(getString);

			HashMap<String, Object> materialListMap = new HashMap<String, Object>();
			materialListMap.put("materialList", setMaterialObjList(jsMaterialListArray, "UPDATE", userId));

			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			rtnBoolResult = iWMS_MST_MATERIAL_Impl.updateMaterialList(materialListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteMaterialList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_MATERIAL_Serviec.deleteMaterialList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsMaterialListArray = new JSONArray(getString);

			HashMap<String, Object> materialListMap = new HashMap<String, Object>();
			materialListMap.put("materialList", setMaterialObjList(jsMaterialListArray, "DELETE", userId));

			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
			rtnBoolResult = iWMS_MST_MATERIAL_Impl.deleteMaterialList(materialListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_MATERIAL_Serviec.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
        	List<HashMap<String,Object>> allList = iWMS_MST_MATERIAL_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_MATERIAL_Serviec.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

			WMS_MST_MATERIAL_Impl iWMS_MST_MATERIAL_Impl = new WMS_MST_MATERIAL_Impl();
        	rtnBoolResult = iWMS_MST_MATERIAL_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private List<WMS_MST_MATERIAL> setMaterialObjList(JSONArray jsArray, String evtHandler, String userId)
			throws Exception
	{
		List<WMS_MST_MATERIAL> materialList = new ArrayList<WMS_MST_MATERIAL>();
		try
		{
			for (int i = 0; i < jsArray.length(); i++)
			{
				JSONObject jsMaterial = jsArray.getJSONObject(i);
				LOGGER.debug("setMaterialObjList() jsonObj:" + jsMaterial.toString());

				String CLIENT_ID     = jsMaterial.has("CLIENT_ID")     ? jsMaterial.getString("CLIENT_ID")     : "";
				String COMPANY_CODE  = jsMaterial.has("COMPANY_CODE")  ? jsMaterial.getString("COMPANY_CODE")  : "";
				String MATERIAL_CODE = jsMaterial.has("MATERIAL_CODE") ? jsMaterial.getString("MATERIAL_CODE") : "";
				String MATERIAL_TYPE = jsMaterial.has("MATERIAL_TYPE") ? jsMaterial.getString("MATERIAL_TYPE") : "";
				String MATERIAL_NAME = jsMaterial.has("MATERIAL_NAME") ? jsMaterial.getString("MATERIAL_NAME") : "";
				String PLANT_CODE    = jsMaterial.has("PLANT_CODE")    ? jsMaterial.getString("PLANT_CODE")    : "";
				String MODEL_NO      = jsMaterial.has("MODEL_NO")      ? jsMaterial.getString("MODEL_NO")      : "";
				String PART_NO       = jsMaterial.has("PART_NO")       ? jsMaterial.getString("PART_NO")       : "";
				String DIE_NO        = jsMaterial.has("DIE_NO")        ? jsMaterial.getString("DIE_NO")        : "";

				String SUB_CONTRACT      = jsMaterial.has("SUB_CONTRACT"     ) ? jsMaterial.getString("SUB_CONTRACT") : "";
				String OLD_MATERIAL_CODE = jsMaterial.has("OLD_MATERIAL_CODE") ? jsMaterial.getString("OLD_MATERIAL_CODE") : "";
				Float  CONV_QTY          = Float.valueOf(jsMaterial.has("CONV_QTY") ? Float.parseFloat(jsMaterial.getString("CONV_QTY")) : jsMaterial.getString("CONV_QTY") == "" ? 0.0F : 0.0F);
				String CONV_UNIT         = jsMaterial.has("CONV_UNIT"  ) ? jsMaterial.getString("CONV_UNIT") : "";
				String STORAGE_LOC       = jsMaterial.has("STORAGE_LOC") ? jsMaterial.getString("STORAGE_LOC") : "";
				String DEL_FLAG          = jsMaterial.has("DEL_FLAG"   ) ? jsMaterial.getString("DEL_FLAG") : "";

				String COST           = jsMaterial.has("COST"          ) ? jsMaterial.getString("COST"          ) : "0";
				String SALES_PRICE    = jsMaterial.has("SALES_PRICE"   ) ? jsMaterial.getString("SALES_PRICE"   ) : "0";
				String PROD_TYPE      = jsMaterial.has("PROD_TYPE"     ) ? jsMaterial.getString("PROD_TYPE"     ) : "";
				String HS_CODE        = jsMaterial.has("HS_CODE"       ) ? jsMaterial.getString("HS_CODE"       ) : "";
				String CONTAINER_TYPE = jsMaterial.has("CONTAINER_TYPE") ? jsMaterial.getString("CONTAINER_TYPE") : "";
				String CONTAINER_NO   = jsMaterial.has("CONTAINER_NO"  ) ? jsMaterial.getString("CONTAINER_NO"  ) : "";

				if( COST == null || COST.equals("") )
				{
					COST = "0";
				}
				if( SALES_PRICE == null || SALES_PRICE.equals(""))
				{
					SALES_PRICE = "0";
				}

				String CURRENCY    = jsMaterial.has("CURRENCY")   ? jsMaterial.getString("CURRENCY")   : "";
				String SO_FLAG     = jsMaterial.has("SO_FLAG")    ? jsMaterial.getString("SO_FLAG")    : "";
				String VEN_CUST_MAT_CODE1     = jsMaterial.has("VEN_CUST_MAT_CODE1")    ? jsMaterial.getString("VEN_CUST_MAT_CODE1")    : "";
				String VEN_CUST_MAT_CODE2     = jsMaterial.has("VEN_CUST_MAT_CODE2")    ? jsMaterial.getString("VEN_CUST_MAT_CODE2")    : "";
				String VEN_CUST_MAT_CODE3     = jsMaterial.has("VEN_CUST_MAT_CODE3")    ? jsMaterial.getString("VEN_CUST_MAT_CODE3")    : "";
				String VEN_CUST_MAT_NAME1     = jsMaterial.has("VEN_CUST_MAT_NAME1")    ? jsMaterial.getString("VEN_CUST_MAT_NAME1")    : "";
				String VEN_CUST_MAT_NAME2     = jsMaterial.has("VEN_CUST_MAT_NAME2")    ? jsMaterial.getString("VEN_CUST_MAT_NAME2")    : "";
				String VEN_CUST_MAT_NAME3     = jsMaterial.has("VEN_CUST_MAT_NAME3")    ? jsMaterial.getString("VEN_CUST_MAT_NAME3")    : "";


				String CREATE_USER = evtHandler.equals("INSERT") ? userId : "";
				String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
				String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

				WMS_MST_MATERIAL oWMS_MST_MATERIAL = new WMS_MST_MATERIAL();
				if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
				{
					Integer WSID = Integer.valueOf(jsMaterial.has("WSID") ? Integer.parseInt(jsMaterial.getString("WSID")) : 0);
					LOGGER.debug("setMaterialObjList() WSID: " + WSID);
					oWMS_MST_MATERIAL.setWSID(WSID);
				}
				oWMS_MST_MATERIAL.setCLIENT_ID(CLIENT_ID);
				oWMS_MST_MATERIAL.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_MST_MATERIAL.setMATERIAL_CODE(MATERIAL_CODE);
				oWMS_MST_MATERIAL.setMATERIAL_TYPE(MATERIAL_TYPE);
				oWMS_MST_MATERIAL.setMATERIAL_NAME(MATERIAL_NAME);
				oWMS_MST_MATERIAL.setPLANT_CODE(PLANT_CODE);
				oWMS_MST_MATERIAL.setMODEL_NO(MODEL_NO);
				oWMS_MST_MATERIAL.setPART_NO(PART_NO);
				oWMS_MST_MATERIAL.setDIE_NO(DIE_NO);
				oWMS_MST_MATERIAL.setSUB_CONTRACT(SUB_CONTRACT);
				oWMS_MST_MATERIAL.setOLD_MATERIAL_CODE(OLD_MATERIAL_CODE);
				oWMS_MST_MATERIAL.setCONV_QTY(CONV_QTY);
				oWMS_MST_MATERIAL.setCONV_UNIT(CONV_UNIT);
				oWMS_MST_MATERIAL.setSTORAGE_LOC(STORAGE_LOC);
				oWMS_MST_MATERIAL.setDEL_FLAG(DEL_FLAG);
				oWMS_MST_MATERIAL.setCREATE_USER(CREATE_USER);
				oWMS_MST_MATERIAL.setUPDATE_USER(UPDATE_USER);
				oWMS_MST_MATERIAL.setDELETE_USER(DELETE_USER);

				oWMS_MST_MATERIAL.setCOST(new Float(COST));
				oWMS_MST_MATERIAL.setSALES_PRICE(new Float(SALES_PRICE));
				oWMS_MST_MATERIAL.setCURRENCY(CURRENCY);
				oWMS_MST_MATERIAL.setSO_FLAG(SO_FLAG);

				oWMS_MST_MATERIAL.setVEN_CUST_MAT_CODE1(VEN_CUST_MAT_CODE1);
				oWMS_MST_MATERIAL.setVEN_CUST_MAT_CODE2(VEN_CUST_MAT_CODE2);
				oWMS_MST_MATERIAL.setVEN_CUST_MAT_CODE3(VEN_CUST_MAT_CODE3);

				oWMS_MST_MATERIAL.setVEN_CUST_MAT_NAME1(VEN_CUST_MAT_NAME1);
				oWMS_MST_MATERIAL.setVEN_CUST_MAT_NAME2(VEN_CUST_MAT_NAME2);
				oWMS_MST_MATERIAL.setVEN_CUST_MAT_NAME3(VEN_CUST_MAT_NAME3);


				oWMS_MST_MATERIAL.setPROD_TYPE(PROD_TYPE)          ;
				oWMS_MST_MATERIAL.setHS_CODE(HS_CODE)              ;
				oWMS_MST_MATERIAL.setCONTAINER_NO(CONTAINER_NO)    ;
				oWMS_MST_MATERIAL.setCONTAINER_TYPE(CONTAINER_TYPE);
				materialList.add(oWMS_MST_MATERIAL);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("WMS_MST_CUSTOMER_Serviec.setMaterialObjList() Exception:" + e.toString() + " #");
		}
		return materialList;
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}
}
