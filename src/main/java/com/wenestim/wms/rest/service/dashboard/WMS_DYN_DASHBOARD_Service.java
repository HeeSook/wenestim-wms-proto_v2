/**
 *
 */
package com.wenestim.wms.rest.service.dashboard;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.entity.dashboard.WMS_DYN_DASHBOARD;
import com.wenestim.wms.rest.impl.dashboard.WMS_DYN_DASHBOARD_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Dashboard")
public class WMS_DYN_DASHBOARD_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_DASHBOARD_Service.class);

	@GET
	@Path("/SupplierState1")
	public Response getSupplierState1(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_DYN_DASHBOARD_Service.getSupplierState1() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			
			WMS_DYN_DASHBOARD_Impl mWMS_DYN_DASHBOARD_Impl = new WMS_DYN_DASHBOARD_Impl();
			List<WMS_DYN_DASHBOARD> allList = mWMS_DYN_DASHBOARD_Impl.getSupplierState1(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
	
	@GET
	@Path("/SupplierState2")
	public Response getSupplierState2(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_DYN_DASHBOARD_Service.getSupplierState2() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			
			WMS_DYN_DASHBOARD_Impl mWMS_DYN_DASHBOARD_Impl = new WMS_DYN_DASHBOARD_Impl();
			List<WMS_DYN_DASHBOARD> allList = mWMS_DYN_DASHBOARD_Impl.getSupplierState2(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
	
	@GET
	@Path("/CustomerState1")
	public Response getCustomerState1(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_DYN_DASHBOARD_Service.getCustomerState1() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			
			WMS_DYN_DASHBOARD_Impl mWMS_DYN_DASHBOARD_Impl = new WMS_DYN_DASHBOARD_Impl();
			List<WMS_DYN_DASHBOARD> allList = mWMS_DYN_DASHBOARD_Impl.getCustomerState1(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}
	
	@GET
	@Path("/CustomerState2")
	public Response getCustomerState2(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_DYN_DASHBOARD_Service.getCustomerState2() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			
			WMS_DYN_DASHBOARD_Impl mWMS_DYN_DASHBOARD_Impl = new WMS_DYN_DASHBOARD_Impl();
			List<WMS_DYN_DASHBOARD> allList = mWMS_DYN_DASHBOARD_Impl.getCustomerState2(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

}
