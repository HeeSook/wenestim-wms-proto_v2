/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;
import com.wenestim.wms.rest.impl.production.WMS_MST_MRP_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Production/Master/Mrp")
public class WMS_MST_MRP_Service {

    private static final Logger LOGGER = Logger.getLogger(WMS_MST_MRP_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getMrpMasterList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_MRP_Service.getMrpMasterList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
//        	String materialCode = (String)paramMap.get("MATERIAL_CODE");
//        	paramMap.put("MATERIAL_CODE", StringUtil.getJsonToObjectList(materialCode));

//        	String test = (String)paramMap.get("TEST");
//        	ArrayList testList = StringUtil.getJsonToHashMapList(test);


        	WMS_MST_MRP_Impl mWMS_MST_MRP_Impl = new WMS_MST_MRP_Impl();
        	List<WMS_MST_MRP> allList = mWMS_MST_MRP_Impl.getMrpMasterList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateMrpMasterList(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
    		) throws Exception
    {
        LOGGER.info("WMS_MST_MRP_Service.updateMrpMasterList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {

            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsonArray = new JSONArray(getString);

            List dataList = setDataObjList(jsonArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", dataList);

            WMS_MST_MRP_Impl iWMS_MST_MRP_Impl = new WMS_MST_MRP_Impl();
            rtnBoolResult = iWMS_MST_MRP_Impl.updateMrpMasterList(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    public List setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
    {

        List<WMS_MST_MRP> dataList = null;
        try
        {
            dataList = new ArrayList<WMS_MST_MRP>();

            for (int i=0; i<jsArray.length(); i++)
            {

            	String PLANT_CODE    = jsArray.getJSONObject(i).has("PLANT_CODE"   ) ? jsArray.getJSONObject(i).getString("PLANT_CODE"   ) : "";
            	String MATERIAL_CODE = jsArray.getJSONObject(i).has("MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MATERIAL_CODE") : "";
            	String LEAD_TIME     = jsArray.getJSONObject(i).has("LEAD_TIME"    ) ? jsArray.getJSONObject(i).getString("LEAD_TIME"    ) : "-1";
            	String LOT_SIZE      = jsArray.getJSONObject(i).has("LOT_SIZE"     ) ? jsArray.getJSONObject(i).getString("LOT_SIZE"     ) : "-1";
            	String SAFETY_STOCK  = jsArray.getJSONObject(i).has("SAFETY_STOCK" ) ? jsArray.getJSONObject(i).getString("SAFETY_STOCK" ) : "-1";
            	String PROD_QTY      = jsArray.getJSONObject(i).has("PROD_QTY"     ) ? jsArray.getJSONObject(i).getString("PROD_QTY"     ) : "-1";
            	String PROD_UNIT     = jsArray.getJSONObject(i).has("PROD_UNIT"    ) ? jsArray.getJSONObject(i).getString("PROD_UNIT"    ) : "";

                WMS_MST_MRP oWMS_MST_MRP = new WMS_MST_MRP();

                oWMS_MST_MRP.setCLIENT_ID    (clientId               );
                oWMS_MST_MRP.setCOMPANY_CODE (comapnyCode            );
                oWMS_MST_MRP.setPLANT_CODE   (PLANT_CODE             );
                oWMS_MST_MRP.setMATERIAL_CODE(MATERIAL_CODE          );
                oWMS_MST_MRP.setLEAD_TIME    (new Float(LEAD_TIME)   );
                oWMS_MST_MRP.setLOT_SIZE     (new Float(LOT_SIZE )   );
                oWMS_MST_MRP.setSAFETY_STOCK (new Float(SAFETY_STOCK));
                oWMS_MST_MRP.setPROD_QTY     (new Float(PROD_QTY )   );
                oWMS_MST_MRP.setPROD_UNIT    (PROD_UNIT              );
                oWMS_MST_MRP.setCREATE_USER  (userId                 );
                oWMS_MST_MRP.setUPDATE_USER  (userId                 );

                dataList.add(oWMS_MST_MRP);
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        return dataList;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
