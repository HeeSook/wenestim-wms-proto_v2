/**
 *
 */
package com.wenestim.wms.rest.service.inventory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.inventory.WMS_DYN_INVENTORY;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_INVENTORY_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Inventory/Inventory")
public class WMS_DYN_INVENTORY_Service {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_INVENTORY_Service.class);

    @GET
    @Path("/All")
    public Response getAllInventory(
            @QueryParam("CLIENT_ID"    ) String CLIENT_ID    ,
            @QueryParam("COMPANY_CODE" ) String COMPANY_CODE ,
            @QueryParam("PLANT_CODE"   ) String PLANT_CODE   ,
            @QueryParam("STORAGE_LOC"  ) String STORAGE_LOC  ,
            @QueryParam("MATERIAL_TYPE") String MATERIAL_TYPE,
            @QueryParam("MATERIAL_CODE") String MATERIAL_CODE,
            @QueryParam("INCLUDE_ZERO" ) String INCLUDE_ZERO
            )
    {
        LOGGER.info("WMS_DYN_INVENTORY_Service.getAllInventory() called");

        String result = "";
        try
        {
        	CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID    ) ;
        	COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE ) ;
        	PLANT_CODE    = StringUtil.getEmptyNullPrevString(PLANT_CODE   ) ;
        	STORAGE_LOC   = StringUtil.getEmptyNullPrevString(STORAGE_LOC  ) ;
        	MATERIAL_TYPE = StringUtil.getEmptyNullPrevString(MATERIAL_TYPE) ;
        	MATERIAL_CODE = StringUtil.getEmptyNullPrevString(MATERIAL_CODE) ;
        	INCLUDE_ZERO  = StringUtil.getEmptyNullPrevString(INCLUDE_ZERO ) ;

            LOGGER.info("PLANT_CODE:"+PLANT_CODE+",STORAGE_LOC:"+STORAGE_LOC+",MATERIAL_TYPE:"+MATERIAL_TYPE+",MATERIAL_CODE:"+MATERIAL_CODE+",INCLUDE_ZERO:"+INCLUDE_ZERO);

            WMS_DYN_INVENTORY_Impl mWMS_DYN_INVENTORY_Impl = new WMS_DYN_INVENTORY_Impl();
            List<WMS_DYN_INVENTORY> allList = mWMS_DYN_INVENTORY_Impl.getAllInventory(
            		CLIENT_ID     ,
            		COMPANY_CODE  ,
            		PLANT_CODE    ,
            		STORAGE_LOC   ,
            		MATERIAL_TYPE ,
            		MATERIAL_CODE ,
            		INCLUDE_ZERO
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_INVENTORY_Service.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

            WMS_DYN_INVENTORY_Impl mWMS_DYN_INVENTORY_Impl = new WMS_DYN_INVENTORY_Impl();
        	List<HashMap<String,Object>> allList = mWMS_DYN_INVENTORY_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_INVENTORY_Service.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

            WMS_DYN_INVENTORY_Impl mWMS_DYN_INVENTORY_Impl = new WMS_DYN_INVENTORY_Impl();
        	rtnBoolResult = mWMS_DYN_INVENTORY_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }


}
