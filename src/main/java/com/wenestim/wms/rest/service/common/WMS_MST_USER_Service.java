package com.wenestim.wms.rest.service.common;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.common.WMS_MST_USER;
import com.wenestim.wms.rest.impl.common.WMS_MST_USER_Impl;
import com.wenestim.wms.rest.util.StringUtil;



@Path("/Common/User")
public class WMS_MST_USER_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_USER_Service.class.getName());
	WMS_MST_USER oWMS_MST_USER = new WMS_MST_USER();

	@GET
	@Path("/List")
	public Response getAllUser(
			@QueryParam("CLIENT_ID"   ) String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE
			)
	{
		String result = "";

		try {
			WMS_MST_USER_Impl iWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
			List<WMS_MST_USER> userList = iWMS_MST_USER_Impl.getAllUser(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (userList != null)
			{
				result = gson.toJson(userList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/User")
	public Response getUser(@QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("WSID") String WSID) {

		String result = "";
		int rWSID = Integer.parseInt(WSID);
		try {
			WMS_MST_USER_Impl iWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
			WMS_MST_USER oWMS_MST_USER = iWMS_MST_USER_Impl.getUser(COMPANY_CODE, rWSID);

			Gson gson = new Gson();
			if (oWMS_MST_USER != null) {
				result = gson.toJson(oWMS_MST_USER);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createUser(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_USER_Service.createUser() called");

		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("User Insert Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_USER_Impl iWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
			WMS_MST_USER oWMS_MST_USER  = new WMS_MST_USER();

			oWMS_MST_USER = setObjectByJson("Create", jsonArray.getJSONObject(0));

			JsonObject rtnJson = new JsonObject();

			boolean existUser = iWMS_MST_USER_Impl.checkExitUserID(oWMS_MST_USER.getUSER_ID());

			if(!existUser) {
				boolean result = iWMS_MST_USER_Impl.insertUser(oWMS_MST_USER);
				if(result) {
					rtnJson.addProperty("ret", Integer.valueOf(0));
					rtnJson.addProperty("msg", "New user is instered");
				}
				return Response.status(200).entity(rtnJson.toString()).build();

			} else {
				rtnJson.addProperty("err", "User ID is already existed");
				return Response.status(500).entity(rtnJson.toString()).build();
			}
		}
		catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateUser(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_USER_Service.updateUser() called");

		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("User Update Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_USER_Impl iWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
			WMS_MST_USER oWMS_MST_USER  = new WMS_MST_USER();

			oWMS_MST_USER = setObjectByJson("UPDATE", jsonArray.getJSONObject(0));

			JsonObject rtnJson = new JsonObject();

			boolean result = iWMS_MST_USER_Impl.updateUser(oWMS_MST_USER);
			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", ""+oWMS_MST_USER.getUSER_NAME()+" is updated");
			}
			return Response.status(200).entity(rtnJson.toString()).build();

		}
		catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteUser(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_USER_Service.deleteUser() called");

		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("User Delete Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			WMS_MST_USER_Impl iWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
			WMS_MST_USER oWMS_MST_USER  = new WMS_MST_USER();

			oWMS_MST_USER = setObjectByJson("DELETE", jsonArray.getJSONObject(0));

			JsonObject rtnJson = new JsonObject();

			boolean result = iWMS_MST_USER_Impl.deleteUser(oWMS_MST_USER);
			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", ""+oWMS_MST_USER.getUSER_NAME()+" is deleted");
			}
			return Response.status(200).entity(rtnJson.toString()).build();

		}
		catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Auth")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response getAuth(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_USER_Service.getLogin() called");

		String result = "";
		try {
			String getString = StringUtil.getInputStreamToString(inputStream);

			JSONObject jsonObject = new JSONObject(getString);
			oWMS_MST_USER         = setObjectByJson("LOGIN", jsonObject);
			WMS_MST_USER userInfo = null;


			WMS_MST_USER rWMS_MST_USER = getCompanyCode(oWMS_MST_USER.getUSER_ID());
			if( rWMS_MST_USER != null )
			{
				String CLIENT_ID    = rWMS_MST_USER.getCLIENT_ID()   ;
				String COMPANY_CODE = rWMS_MST_USER.getCOMPANY_CODE();
				String USER_ID      = oWMS_MST_USER.getUSER_ID();
				String USER_PW      = oWMS_MST_USER.getUSER_PW();

				WMS_MST_USER_Impl oWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
				userInfo = oWMS_MST_USER_Impl.getLogin(CLIENT_ID, COMPANY_CODE, USER_ID, USER_PW);
			}

			JsonObject rsltJsonObject = new JsonObject();
			String MSG = "";
			if (userInfo != null) {
				rsltJsonObject.addProperty("RET"               , Integer.valueOf(0))          ;
				rsltJsonObject.addProperty("APP_USER_ID"       , userInfo.getUSER_ID())       ;
				rsltJsonObject.addProperty("APP_USER_NAME"     , userInfo.getUSER_NAME())     ;
				rsltJsonObject.addProperty("APP_USER_GRP"      , userInfo.getUSER_GRP())      ;
				rsltJsonObject.addProperty("APP_CLIENT_ID"     , userInfo.getCLIENT_ID())     ;
				rsltJsonObject.addProperty("APP_COMPANY_CODE"  , userInfo.getCOMPANY_CODE())  ;
				rsltJsonObject.addProperty("APP_COMPANY_NAME"  , userInfo.getCOMPANY_NAME())  ;
				rsltJsonObject.addProperty("APP_WSID"          , userInfo.getWSID())          ;
				rsltJsonObject.addProperty("APP_LOGO_NAME"     , userInfo.getLOGO_NAME())     ;
				rsltJsonObject.addProperty("APP_PRICE_FORMAT"  , userInfo.getPRICE_FORMAT())  ;
				rsltJsonObject.addProperty("APP_QTY_FORMAT"    , userInfo.getQTY_FORMAT())    ;
				rsltJsonObject.addProperty("APP_DASHBOARD_REST", userInfo.getDASHBOARD_REST());
				rsltJsonObject.addProperty("APP_DASHBOARD_DIR" , userInfo.getDASHBOARD_DIR() );
				rsltJsonObject.addProperty("APP_PRINTER_IP"    , userInfo.getPRINTER_IP() == null ? "" : userInfo.getPRINTER_IP());
				rsltJsonObject.addProperty("APP_LOT_FLAG"      , userInfo.getLOT_FLAG());
				rsltJsonObject.addProperty("MSG"               , MSG);

			} else {
				MSG = "The username or password you entered is incorrect.";
				rsltJsonObject.addProperty("RET"               , Integer.valueOf(1));
				rsltJsonObject.addProperty("APP_USER_ID"       , "");
				rsltJsonObject.addProperty("APP_USER_NAME"     , "");
				rsltJsonObject.addProperty("APP_USER_GRP"      , "");
				rsltJsonObject.addProperty("APP_CLIENT_ID"     , "");
				rsltJsonObject.addProperty("APP_COMPANY_CODE"  , "");
				rsltJsonObject.addProperty("APP_COMPANY_NAME"  , "");
				rsltJsonObject.addProperty("APP_WSID"          , "");
				rsltJsonObject.addProperty("APP_LOGO_NAME "    , "");
				rsltJsonObject.addProperty("APP_PRICE_FORMAT"  , "{0:c2}");
				rsltJsonObject.addProperty("APP_QTY_FORMAT"    , "{0:##,#}");
				rsltJsonObject.addProperty("APP_DASHBOARD_REST","Default");
				rsltJsonObject.addProperty("APP_DASHBOARD_DIR" ,"default");
				rsltJsonObject.addProperty("APP_PRINTER_IP"    , "");
				rsltJsonObject.addProperty("APP_LOT_FLAG"      , "");
				rsltJsonObject.addProperty("MSG"               ,MSG);
			}
			result = rsltJsonObject.toString();

			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	/**
	 * SelectHandler by MINJIOH(11.14.2017)
	 * @param inputStream
	 * @return
	 * @throws Exception
	 */
	private WMS_MST_USER setSelectHandler(InputStream inputStream) throws Exception {

		try {
			String getString = StringUtil.getInputStreamToString(inputStream);

			JSONObject jsonObject = new JSONObject(getString);
			oWMS_MST_USER = setObjectByJson("SELECT", jsonObject);

		} catch (Exception e) {
			LOGGER.error("Exception :"+e.getMessage()+"#");
			e.printStackTrace();
		}

		return oWMS_MST_USER;
	}

	private WMS_MST_USER getCompanyCode(String USER_ID) throws Exception {
		WMS_MST_USER_Impl mWMS_MST_USER_Impl = new WMS_MST_USER_Impl();
		return mWMS_MST_USER_Impl.getCompanyCode(USER_ID);
	}

    private WMS_MST_USER setObjectByJson(String evtHandler, JSONObject jsonObj) throws Exception {

		try {
			LOGGER.info("setObjectByJson() jsonObj:" + jsonObj.toString());
			if (evtHandler.equals("UPDATE") || evtHandler.equals("DELETE"))
			{
				Integer WSID = jsonObj.has("WSID") ? Integer.parseInt(jsonObj.getString("WSID")) : 0;
				LOGGER.info("setObjectByJson() WSID: "+WSID);
				oWMS_MST_USER.setWSID(WSID);
			}

			String CLIENT_ID     = jsonObj.has("CLIENT_ID"   ) ? jsonObj.getString("CLIENT_ID"   ) : "";
			String COMPANY_CODE  = jsonObj.has("COMPANY_CODE") ? jsonObj.getString("COMPANY_CODE") : "";
			String USER_ID       = jsonObj.has("USER_ID"     ) ? jsonObj.getString("USER_ID"     ) : "";
			String USER_PW       = jsonObj.has("USER_PW"     ) ? jsonObj.getString("USER_PW"     ) : "";
			String USER_GRP      = jsonObj.has("USER_GRP"    ) ? jsonObj.getString("USER_GRP"    ) : "";
			String USER_NAME     = jsonObj.has("USER_NAME"   ) ? jsonObj.getString("USER_NAME"   ) : "";
			String USER_EMAIL    = jsonObj.has("USER_EMAIL"  ) ? jsonObj.getString("USER_EMAIL"  ) : "";
			String DEL_FLAG      = jsonObj.has("DEL_FLAG"    ) ? jsonObj.getString("DEL_FLAG"    ) : "";
			String CREATE_USER   = jsonObj.has("CREATE_USER" ) ? jsonObj.getString("CREATE_USER" ) : "";
			String CREATE_DATE   = jsonObj.has("CREATE_DATE" ) ? jsonObj.getString("CREATE_DATE" ) : "";
			String UPDATE_USER   = jsonObj.has("UPDATE_USER" ) ? jsonObj.getString("UPDATE_USER" ) : "";
			String UPDATE_DATE   = jsonObj.has("UPDATE_DATE" ) ? jsonObj.getString("UPDATE_DATE" ) : "";
			String DELETE_USER   = jsonObj.has("DELETE_USER" ) ? jsonObj.getString("DELETE_USER" ) : "";
			String DELETE_DATE   = jsonObj.has("DELETE_DATE" ) ? jsonObj.getString("DELETE_DATE" ) : "";

			String PRINTER_IP    = jsonObj.has("PRINTER_IP" ) ? jsonObj.getString("PRINTER_IP" ) : "";


			oWMS_MST_USER.setCLIENT_ID(CLIENT_ID);
			oWMS_MST_USER.setCOMPANY_CODE(COMPANY_CODE);
			if(USER_ID != "") {
				oWMS_MST_USER.setUSER_ID(USER_ID);
			}
			if(USER_PW != "") {
				oWMS_MST_USER.setUSER_PW(USER_PW);
			}
			if(USER_GRP != "") {
				oWMS_MST_USER.setUSER_GRP(USER_GRP);
			}
			if(USER_NAME != "") {
				oWMS_MST_USER.setUSER_NAME(USER_NAME);
			}
			if(USER_EMAIL != "") {
				oWMS_MST_USER.setUSER_EMAIL(USER_EMAIL);
			}
			if(PRINTER_IP != "") {
				oWMS_MST_USER.setPRINTER_IP(PRINTER_IP);
			}
			oWMS_MST_USER.setDEL_FLAG(DEL_FLAG);
			oWMS_MST_USER.setCREATE_USER(CREATE_USER);
			oWMS_MST_USER.setCREATE_DATE(CREATE_DATE);
			oWMS_MST_USER.setUPDATE_USER(UPDATE_USER);
			oWMS_MST_USER.setUPDATE_DATE(UPDATE_DATE);
			oWMS_MST_USER.setDELETE_USER(DELETE_USER);
			oWMS_MST_USER.setDELETE_DATE(DELETE_DATE);

		} catch (Exception e) {
			LOGGER.error("Exception :"+e.getMessage()+"#");
			e.printStackTrace();
		}
		return oWMS_MST_USER;
	}

}
