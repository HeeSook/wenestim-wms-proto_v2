/**
 *
 */
package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_CUSTOMER;
import com.wenestim.wms.rest.impl.master.WMS_MST_CUSTOMER_Impl;
import com.wenestim.wms.rest.util.StringUtil;



/**
 * @author Andrew
 *
 */
@Path("/Master/Customer")
public class WMS_MST_CUSTOMER_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_CUSTOMER_Service.class);

	@GET
	@Path("/All")
	public Response getAllCustomer() {
		LOGGER.info("WMS_MST_CUSTOMER_Service.getAllCustomer() called");

		String result = "";
		try {
			WMS_MST_CUSTOMER_Impl mWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
			List<WMS_MST_CUSTOMER> allList = mWMS_MST_CUSTOMER_Impl.getAllCustomer();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Info")
	public Response getAllCustomerByCompany(
			@QueryParam("CLIENT_ID")    String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_MST_CUSTOMER_Service.getAllCustomerByCompany() called");
		LOGGER.info("CLIENT_ID : " + CLIENT_ID +",COMPANY_CODE : " + COMPANY_CODE);

		String result = "";
		try {
			WMS_MST_CUSTOMER_Impl mWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
			List<WMS_MST_CUSTOMER> allList = mWMS_MST_CUSTOMER_Impl.getAllCustomerByCompany(CLIENT_ID, COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

    @GET
    @Path("/List")
    public Response getCustomerList(
            @QueryParam("CLIENT_ID")      String CLIENT_ID     ,
            @QueryParam("COMPANY_CODE")   String COMPANY_CODE  ,
            @QueryParam("CUST_CODE")      String CUST_CODE     ,
            @QueryParam("SALES_ORG_CODE") String SALES_ORG_CODE,
            @QueryParam("SALES_DIV_CODE") String SALES_DIV_CODE,
            @QueryParam("CUST_NAME")      String CUST_NAME     ,
            @QueryParam("DEL_FLAG")       String DEL_FLAG) {
        LOGGER.info("WMS_MST_CUSTOMER_Serviec.getCustomerList() called");
        LOGGER.debug("[PARAM] CLIENT_ID:" + CLIENT_ID + ", COMPANY_CODE:" + COMPANY_CODE + ", CUST_CODE:" + CUST_CODE
                + ",SALES_ORG_CODE:" + SALES_ORG_CODE + ",SALES_DIV_CODE:" + SALES_DIV_CODE + ",CUST_NAME:" + CUST_NAME + ",DEL_FLAG:" + DEL_FLAG);

        String result = "";
        try
        {
            CLIENT_ID      = StringUtil.getEmptyNullPrevString(CLIENT_ID     );
            COMPANY_CODE   = StringUtil.getEmptyNullPrevString(COMPANY_CODE  );
            CUST_CODE      = StringUtil.getEmptyNullPrevString(CUST_CODE     );
            SALES_ORG_CODE = StringUtil.getEmptyNullPrevString(SALES_ORG_CODE);
            SALES_DIV_CODE = StringUtil.getEmptyNullPrevString(SALES_DIV_CODE);
            CUST_NAME      = StringUtil.getEmptyNullPrevString(CUST_NAME     );
            DEL_FLAG       = StringUtil.getEmptyNullPrevString(DEL_FLAG      );

            WMS_MST_CUSTOMER_Impl mWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();

            List<WMS_MST_CUSTOMER> customerList = mWMS_MST_CUSTOMER_Impl.getCustomerList(CLIENT_ID, COMPANY_CODE, CUST_CODE, SALES_ORG_CODE, SALES_DIV_CODE, CUST_NAME, DEL_FLAG);

            Gson gson = new Gson();
            if (customerList != null) {
                result = gson.toJson(customerList);
            }
            return Response.status(200).entity(result).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
	@Path("/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertCustomerList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_CUSTOMER_Serviec.insertCustomerList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsCustomerListArray = new JSONArray(getString);

			HashMap<String, Object> customerListMap = new HashMap<String, Object>();
			customerListMap.put("customerList", setCustomerObjList(jsCustomerListArray, "INSERT", userId));

			WMS_MST_CUSTOMER_Impl iWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
			rtnBoolResult = iWMS_MST_CUSTOMER_Impl.insertCustomerList(customerListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateCustomerList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_CUSTOMER_Serviec.updateCustomer() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsCustomerListArray = new JSONArray(getString);

			HashMap<String, Object> customerListMap = new HashMap<String, Object>();
			customerListMap.put("customerList", setCustomerObjList(jsCustomerListArray, "UPDATE", userId));

			WMS_MST_CUSTOMER_Impl iWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
			rtnBoolResult = iWMS_MST_CUSTOMER_Impl.updateCustomerList(customerListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteCustomerList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_CUSTOMER_Serviec.deleteCustomerList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsCustomerListArray = new JSONArray(getString);

			HashMap<String, Object> customerListMap = new HashMap<String, Object>();
			customerListMap.put("customerList", setCustomerObjList(jsCustomerListArray, "DELETE", userId));

			WMS_MST_CUSTOMER_Impl iWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
			rtnBoolResult = iWMS_MST_CUSTOMER_Impl.deleteCustomerList(customerListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_CUSTOMER_Serviec.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

        	WMS_MST_CUSTOMER_Impl iWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
        	List<HashMap<String,Object>> allList = iWMS_MST_CUSTOMER_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_CUSTOMER_Serviec.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	WMS_MST_CUSTOMER_Impl iWMS_MST_CUSTOMER_Impl = new WMS_MST_CUSTOMER_Impl();
        	rtnBoolResult = iWMS_MST_CUSTOMER_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private List<WMS_MST_CUSTOMER> setCustomerObjList(JSONArray jsArray, String evtHandler, String userId)
            throws Exception
    {
        List<WMS_MST_CUSTOMER> customerList = new ArrayList<WMS_MST_CUSTOMER>();
        try
        {
            for (int i = 0; i < jsArray.length(); i++)
            {
                JSONObject jsCustomer = jsArray.getJSONObject(i);
                LOGGER.debug("setCustomerObjList() jsonObj:" + jsCustomer.toString());

                String CLIENT_ID         = jsCustomer.has("CLIENT_ID"        ) ? jsCustomer.getString("CLIENT_ID"        ) : "";
                String COMPANY_CODE      = jsCustomer.has("COMPANY_CODE"     ) ? jsCustomer.getString("COMPANY_CODE"     ) : "";
                String CUST_CODE         = jsCustomer.has("CUST_CODE"        ) ? jsCustomer.getString("CUST_CODE"        ) : "";
                String CUST_NAME         = jsCustomer.has("CUST_NAME"        ) ? jsCustomer.getString("CUST_NAME"        ) : "";
                String SALES_ORG_CODE    = jsCustomer.has("SALES_ORG_CODE"   ) ? jsCustomer.getString("SALES_ORG_CODE"   ) : "";
                String SALES_DIV_CODE    = jsCustomer.has("SALES_DIV_CODE"   ) ? jsCustomer.getString("SALES_DIV_CODE"   ) : "";
                String ADDRESS           = jsCustomer.has("ADDRESS"          ) ? jsCustomer.getString("ADDRESS"          ) : "";
                String STREET1           = jsCustomer.has("STREET1"          ) ? jsCustomer.getString("STREET1"          ) : "";
                String STREET2           = jsCustomer.has("STREET2"          ) ? jsCustomer.getString("STREET2"          ) : "";
                String PO_BOX            = jsCustomer.has("PO_BOX"           ) ? jsCustomer.getString("PO_BOX"           ) : "";
                String CITY              = jsCustomer.has("CITY"             ) ? jsCustomer.getString("CITY"             ) : "";
                String STATE             = jsCustomer.has("STATE"            ) ? jsCustomer.getString("STATE"            ) : "";
                String ZIP_CODE          = jsCustomer.has("ZIP_CODE"         ) ? jsCustomer.getString("ZIP_CODE"         ) : "";
                String COUNTRY_CODE      = jsCustomer.has("COUNTRY_CODE"     ) ? jsCustomer.getString("COUNTRY_CODE"     ) : "";
                String PHONE             = jsCustomer.has("PHONE"            ) ? jsCustomer.getString("PHONE"            ) : "";
                String CONTACT_NAME      = jsCustomer.has("CONTACT_NAME"     ) ? jsCustomer.getString("CONTACT_NAME"     ) : "";
                String CONTACT_EMAIL     = jsCustomer.has("CONTACT_EMAIL"    ) ? jsCustomer.getString("CONTACT_EMAIL"    ) : "";
                String CONTACT_PHONE     = jsCustomer.has("CONTACT_PHONE"    ) ? jsCustomer.getString("CONTACT_PHONE"    ) : "";
                String PAYMENT_TERM      = jsCustomer.has("PAYMENT_TERM"     ) ? jsCustomer.getString("PAYMENT_TERM"     ) : "";
                String CURRENCY          = jsCustomer.has("CURRENCY"         ) ? jsCustomer.getString("CURRENCY"         ) : "";
                String UNIT              = jsCustomer.has("UNIT"             ) ? jsCustomer.getString("UNIT"             ) : "";
                String DEL_FLAG          = jsCustomer.has("DEL_FLAG"         ) ? jsCustomer.getString("DEL_FLAG"         ) : "";

                String EO_NO             = jsCustomer.has("EO_NO"            ) ? jsCustomer.getString("EO_NO"            ) : "";
                String GI_TEMPLATE_CODE  = jsCustomer.has("GI_TEMPLATE_CODE" ) ? jsCustomer.getString("GI_TEMPLATE_CODE" ) : "";

                String SUPPLIER_CODE     = jsCustomer.has("SUPPLIER_CODE"    ) ? jsCustomer.getString("SUPPLIER_CODE"    ) : "";
                String SUPPLIER_NAME     = jsCustomer.has("SUPPLIER_NAME"    ) ? jsCustomer.getString("SUPPLIER_NAME"    ) : "";
                String SUPPLIER_COUNTRY  = jsCustomer.has("SUPPLIER_COUNTRY" ) ? jsCustomer.getString("SUPPLIER_COUNTRY" ) : "";
                String SUPPLIER_STATE    = jsCustomer.has("SUPPLIER_STATE"   ) ? jsCustomer.getString("SUPPLIER_STATE"   ) : "";
                String SUPPLIER_CITY     = jsCustomer.has("SUPPLIER_CITY"    ) ? jsCustomer.getString("SUPPLIER_CITY"    ) : "";
                String SUPPLIER_STREET1  = jsCustomer.has("SUPPLIER_STREET1" ) ? jsCustomer.getString("SUPPLIER_STREET1" ) : "";
                String SUPPLIER_STREET2  = jsCustomer.has("SUPPLIER_STREET2" ) ? jsCustomer.getString("SUPPLIER_STREET2" ) : "";
                String SUPPLIER_ZIP_CODE = jsCustomer.has("SUPPLIER_ZIP_CODE") ? jsCustomer.getString("SUPPLIER_ZIP_CODE") : "";

                String SHIP_TO_CODE      = jsCustomer.has("SHIP_TO_CODE"     ) ? jsCustomer.getString("SHIP_TO_CODE"     ) : "";
                String SHIP_TO_NAME      = jsCustomer.has("SHIP_TO_NAME"     ) ? jsCustomer.getString("SHIP_TO_NAME"     ) : "";
                String SHIP_TO_COUNTRY   = jsCustomer.has("SHIP_TO_COUNTRY"  ) ? jsCustomer.getString("SHIP_TO_COUNTRY"  ) : "";
                String SHIP_TO_STATE     = jsCustomer.has("SHIP_TO_STATE"    ) ? jsCustomer.getString("SHIP_TO_STATE"    ) : "";
                String SHIP_TO_CITY      = jsCustomer.has("SHIP_TO_CITY"     ) ? jsCustomer.getString("SHIP_TO_CITY"     ) : "";
                String SHIP_TO_STREET1   = jsCustomer.has("SHIP_TO_STREET1"  ) ? jsCustomer.getString("SHIP_TO_STREET1"  ) : "";
                String SHIP_TO_STREET2   = jsCustomer.has("SHIP_TO_STREET2"  ) ? jsCustomer.getString("SHIP_TO_STREET2"  ) : "";
                String SHIP_TO_ZIP_CODE  = jsCustomer.has("SHIP_TO_ZIP_CODE" ) ? jsCustomer.getString("SHIP_TO_ZIP_CODE" ) : "";

                Float  DELIVERY_TIME  = Float.valueOf(jsCustomer.has("DELIVERY_TIME") ? Float.parseFloat(jsCustomer.getString("DELIVERY_TIME")) : jsCustomer.getString("DELIVERY_TIME") == "" ? 0.0F : 0.0F);
                Float  SALES_PRICE    = Float.valueOf(jsCustomer.has("SALES_PRICE"  ) ? Float.parseFloat(jsCustomer.getString("SALES_PRICE"  )) : jsCustomer.getString("SALES_PRICE"  ) == "" ? 0.0F : 0.0F);
                Float  PRICE_UNIT     = Float.valueOf(jsCustomer.has("PRICE_UNIT"   ) ? Float.parseFloat(jsCustomer.getString("PRICE_UNIT"   )) : jsCustomer.getString("PRICE_UNIT"   ) == "" ? 0.0F : 0.0F);

                String CREATE_USER = evtHandler.equals("INSERT") ? userId : "";
                String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
                String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

                WMS_MST_CUSTOMER oWMS_MST_CUSTOMER = new WMS_MST_CUSTOMER();
                if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
                {
                    Integer WSID = Integer.valueOf(jsCustomer.has("WSID") ? Integer.parseInt(jsCustomer.getString("WSID")) : 0);
                    LOGGER.debug("setCustomerObjList() WSID: " + WSID);
                    oWMS_MST_CUSTOMER.setWSID(WSID);
                }
                oWMS_MST_CUSTOMER.setCLIENT_ID(CLIENT_ID                );
                oWMS_MST_CUSTOMER.setCOMPANY_CODE(COMPANY_CODE          );
                oWMS_MST_CUSTOMER.setCUST_CODE(CUST_CODE                );
                oWMS_MST_CUSTOMER.setCUST_NAME(CUST_NAME                );
                oWMS_MST_CUSTOMER.setSALES_ORG_CODE(SALES_ORG_CODE      );
                oWMS_MST_CUSTOMER.setSALES_DIV_CODE(SALES_DIV_CODE      );
                oWMS_MST_CUSTOMER.setADDRESS(ADDRESS                    );
                oWMS_MST_CUSTOMER.setSTREET1(STREET1                    );
                oWMS_MST_CUSTOMER.setSTREET2(STREET2                    );
                oWMS_MST_CUSTOMER.setPO_BOX(PO_BOX                      );
                oWMS_MST_CUSTOMER.setCITY(CITY                          );
                oWMS_MST_CUSTOMER.setSTATE(STATE                        );
                oWMS_MST_CUSTOMER.setZIP_CODE(ZIP_CODE                  );
                oWMS_MST_CUSTOMER.setCOUNTRY_CODE(COUNTRY_CODE          );
                oWMS_MST_CUSTOMER.setPHONE(PHONE                        );
                oWMS_MST_CUSTOMER.setCONTACT_NAME(CONTACT_NAME          );
                oWMS_MST_CUSTOMER.setCONTACT_EMAIL(CONTACT_EMAIL        );
                oWMS_MST_CUSTOMER.setCONTACT_PHONE(CONTACT_PHONE        );
                oWMS_MST_CUSTOMER.setPAYMENT_TERM(PAYMENT_TERM          );
                oWMS_MST_CUSTOMER.setDELIVERY_TIME(DELIVERY_TIME        );
                oWMS_MST_CUSTOMER.setCURRENCY(CURRENCY                  );
                oWMS_MST_CUSTOMER.setSALES_PRICE(SALES_PRICE            );
                oWMS_MST_CUSTOMER.setPRICE_UNIT(PRICE_UNIT              );
                oWMS_MST_CUSTOMER.setUNIT(UNIT                          );
                oWMS_MST_CUSTOMER.setDEL_FLAG(DEL_FLAG                  );
                oWMS_MST_CUSTOMER.setCREATE_USER(CREATE_USER            );
                oWMS_MST_CUSTOMER.setUPDATE_USER(UPDATE_USER            );
                oWMS_MST_CUSTOMER.setDELETE_USER(DELETE_USER            );

                oWMS_MST_CUSTOMER.setEO_NO(EO_NO                        );
                oWMS_MST_CUSTOMER.setGI_TEMPLATE_CODE(GI_TEMPLATE_CODE  );

                oWMS_MST_CUSTOMER.setSUPPLIER_CODE(SUPPLIER_CODE        );
                oWMS_MST_CUSTOMER.setSUPPLIER_NAME(SUPPLIER_NAME        );
                oWMS_MST_CUSTOMER.setSUPPLIER_COUNTRY(SUPPLIER_COUNTRY  );
                oWMS_MST_CUSTOMER.setSUPPLIER_STATE(SUPPLIER_STATE      );
                oWMS_MST_CUSTOMER.setSUPPLIER_CITY(SUPPLIER_CITY        );
                oWMS_MST_CUSTOMER.setSUPPLIER_STREET1(SUPPLIER_STREET1  );
                oWMS_MST_CUSTOMER.setSUPPLIER_STREET2(SUPPLIER_STREET2  );
                oWMS_MST_CUSTOMER.setSUPPLIER_ZIP_CODE(SUPPLIER_ZIP_CODE);

                oWMS_MST_CUSTOMER.setSHIP_TO_CODE(SHIP_TO_CODE          );
                oWMS_MST_CUSTOMER.setSHIP_TO_NAME(SHIP_TO_NAME          );
                oWMS_MST_CUSTOMER.setSHIP_TO_COUNTRY(SHIP_TO_COUNTRY    );
                oWMS_MST_CUSTOMER.setSHIP_TO_STATE(SHIP_TO_STATE        );
                oWMS_MST_CUSTOMER.setSHIP_TO_CITY(SHIP_TO_CITY          );
                oWMS_MST_CUSTOMER.setSHIP_TO_STREET1(SHIP_TO_STREET1    );
                oWMS_MST_CUSTOMER.setSHIP_TO_STREET2(SHIP_TO_STREET2    );
                oWMS_MST_CUSTOMER.setSHIP_TO_ZIP_CODE(SHIP_TO_ZIP_CODE  );

                customerList.add(oWMS_MST_CUSTOMER);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            LOGGER.error("WMS_MST_CUSTOMER_Serviec.setCustomerObjList() Exception:" + e.toString() + " #");
        }
        return customerList;
    }

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
