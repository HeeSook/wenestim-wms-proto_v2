/**
 *
 */
package com.wenestim.wms.rest.service.admin;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.impl.admin.WMS_ADM_MENU_Impl;
import com.wenestim.wms.rest.util.StringUtil;
/**
 * @author Andrew
 *
 */
@Path("/Admin/Menu")
public class WMS_ADM_MENU_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_ADM_MENU_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getMenuList(InputStream inputStream)
    {
        LOGGER.info("WMS_ADM_MENU_Service.getMenuList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

//        	String materialCode = (String)paramMap.get("MATERIAL_CODE");
//        	paramMap.put("MATERIAL_CODE", StringUtil.getJsonToObjectList(materialCode));

        	WMS_ADM_MENU_Impl mWMS_ADM_MENU_Impl = new WMS_ADM_MENU_Impl();
        	List<HashMap<String,Object>> allList = mWMS_ADM_MENU_Impl.getMenuList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateMenu(InputStream inputStream)
    {
        LOGGER.info("WMS_ADM_MENU_Service.updateMenu() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	WMS_ADM_MENU_Impl mWMS_ADM_MENU_Impl = new WMS_ADM_MENU_Impl();
        	rtnBoolResult = mWMS_ADM_MENU_Impl.updateMenu(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
