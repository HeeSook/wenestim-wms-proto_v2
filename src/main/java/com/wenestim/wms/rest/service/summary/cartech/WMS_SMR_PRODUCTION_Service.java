/**
 *
 */
package com.wenestim.wms.rest.service.summary.cartech;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.impl.summary.cartech.WMS_SMR_PRODUCTION_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Cartech/Summary/Production")
public class WMS_SMR_PRODUCTION_Service
{
	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_PRODUCTION_Service.class);

    @POST
    @Path("/Total/Production/Qty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getTotalProductionQty(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_PRODUCTION_Service.getTotalProductionQty() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_PRODUCTION_Impl iWMS_SMR_PRODUCTION_Impl = new WMS_SMR_PRODUCTION_Impl();
        	HashMap<String,Object> resultMap = iWMS_SMR_PRODUCTION_Impl.getTotalProductionQty(paramMap);

            Gson gson = new Gson();
            if (resultMap != null)
            {
                result = gson.toJson(resultMap);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Total/Production/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getTotalProductionList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_PRODUCTION_Service.getTotalProductionList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_PRODUCTION_Impl iWMS_SMR_PRODUCTION_Impl = new WMS_SMR_PRODUCTION_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_PRODUCTION_Impl.getTotalProductionList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Detail/Production/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDetailProductionList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_PRODUCTION_Service.getDetailProductionList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_PRODUCTION_Impl iWMS_SMR_PRODUCTION_Impl = new WMS_SMR_PRODUCTION_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_PRODUCTION_Impl.getDetailProductionList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Detail/Result/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getProductionResultList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_PRODUCTION_Service.getProductionResultList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_PRODUCTION_Impl iWMS_SMR_PRODUCTION_Impl = new WMS_SMR_PRODUCTION_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_PRODUCTION_Impl.getProductionResultList(paramMap);

        	Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Detail/RunTime/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getProductionRunTimeList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_PRODUCTION_Service.getProductionRunTimeList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_PRODUCTION_Impl iWMS_SMR_PRODUCTION_Impl = new WMS_SMR_PRODUCTION_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_PRODUCTION_Impl.getProductionRunTimeList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}


