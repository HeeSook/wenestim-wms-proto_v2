/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.entity.production.WMS_EXC_DEMAND;
import com.wenestim.wms.rest.entity.production.WMS_EXC_INVENTORY;
import com.wenestim.wms.rest.entity.production.WMS_EXC_MRP;
import com.wenestim.wms.rest.entity.production.WMS_MST_MRP;
import com.wenestim.wms.rest.impl.production.WMS_EXC_MRP_Impl;
import com.wenestim.wms.rest.util.DateUtil;
import com.wenestim.wms.rest.util.MrpUtil;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Production/Mrp")
public class WMS_EXC_MRP_Service {

    private static final Logger LOGGER = Logger.getLogger(WMS_EXC_MRP_Service.class);

	public  static String result      ;
	private static String clientId    ;
	private static String companyCode ;
	private static String planVer     ;
	public  static String planCurrent ;
	public  static String planType    ;
	private static int    maxPlanSeq  ;

	private static String demandItem  ;
	private static String demandDate  ;
	private static float  demandQty   ;

	public static List<WMS_EXC_DEMAND> reqItemList  = null;
	public static List<WMS_EXC_MRP>    peggingList  = null;

	public static HashMap<String,List<WMS_EXC_MRP>> itemBomMap = null;

	public static List<WMS_EXC_DEMAND>     demandList = null;
	public static List<WMS_MST_BOM>        bomList    = null;
	public static List<WMS_EXC_INVENTORY>  invTotalList     = null;
	public static List<WMS_EXC_INVENTORY>  invDetailList    = null;
	public static List<WMS_EXC_INVENTORY>  invPeggingList   = null;
	public static List<WMS_MST_MRP>        masterList       = null;
	public static List<WMS_EXC_MRP>        itemBomLevelList = null;

	private static HashMap<String,Object>  paramMap = null;
	private static HashMap<String,Object>  dataMap  = null;
	private static WMS_EXC_MRP_Impl        implName = null;


    @POST
    @Path("/Execute")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public void ExecuteMRP(InputStream inputStream)
    {

        LOGGER.info("WMS_EXC_MRP_Service.ExecuteMRP() called");
        result = "S";
        try
        {
        	paramMap = new HashMap<String,Object>();
        	paramMap = StringUtil.getParamMap(inputStream);

    		initPlan()  ;
    		startPlan() ;
    		exportPlan();
    		exportMrp() ;
        }
        catch (Exception e)
        {
        	result = "E";
        	LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
        }
    }

	public void initPlan()
	{
		initConstVariable();
		initClassVariable();
		initData();
	}

	public void initConstVariable()
	{
		maxPlanSeq  = 0;
		clientId    = (String)paramMap.get("CLIENT_ID"   );
		companyCode = (String)paramMap.get("COMPANY_CODE");
		planVer     = (String)paramMap.get("PLAN_VER"    );
		planCurrent = (String)paramMap.get("PLAN_CURRENT");
		planType    = (String)paramMap.get("PLAN_TYPE"   );

		dataMap     = new HashMap<String,Object>();
		dataMap.put("CLIENT_ID"   , clientId   );
		dataMap.put("COMPANY_CODE", companyCode);
		dataMap.put("PLAN_VER"    , planVer    );
		dataMap.put("PLAN_TYPE"   , planType   );
		dataMap.put("PLAN_CURRENT", planCurrent);
	}

	public void initClassVariable()
	{
		itemBomMap       = new HashMap<String,List<WMS_EXC_MRP>>();
		bomList          = new ArrayList<WMS_MST_BOM>();
		masterList       = new ArrayList<WMS_MST_MRP>();
		itemBomLevelList = new ArrayList<WMS_EXC_MRP>();
		implName         = new WMS_EXC_MRP_Impl();

		reqItemList    = new ArrayList<WMS_EXC_DEMAND>();
		peggingList    = new ArrayList<WMS_EXC_MRP>()   ;

		demandList     = new ArrayList<WMS_EXC_DEMAND>()   ;
		invTotalList   = new ArrayList<WMS_EXC_INVENTORY>();
		invDetailList  = new ArrayList<WMS_EXC_INVENTORY>();
		invPeggingList = new ArrayList<WMS_EXC_INVENTORY>();
	}

	public void initData()
	{
		implName.deleteMrpData(dataMap);

		demandList    = implName.getDemandList(clientId,companyCode,planVer,planType);
		bomList       = implName.getBomList(clientId,companyCode)      ;
		masterList    = implName.getMrpMasterList(clientId,companyCode);
		invTotalList  = implName.getInvList(clientId,companyCode)      ;
		invDetailList = implName.getInvDetailList(clientId,companyCode);
	}

	public void startPlan()
	{
		if(planType.equals("NET"))
		{
			startNet();
		}
		else
		{
			startGross();
		}
	}

	public void startNet()
	{
		MrpUtil.createDemandBySafetyStock() ;
		itemBomMap = MrpUtil.createItemBomMap();

		startDemandPlan()         ;
		createTopLevelReqList()   ;
		consumeTopLevelInventory();
		startDownLevelPlan()      ;
	}

	public void startGross()
	{
		List<WMS_EXC_INVENTORY> invGrossList = new ArrayList<WMS_EXC_INVENTORY>();
		invGrossList.addAll(invTotalList);

		itemBomMap   = MrpUtil.createItemBomMap();
		invTotalList = new ArrayList<WMS_EXC_INVENTORY>();

		startDemandPlan()         ;
		createTopLevelReqList()   ;
		consumeTopLevelInventory();
		startDownLevelPlan()      ;

		invTotalList = invGrossList;
	}

	public void startDemandPlan()
	{
		try
		{
			for(int i=0; i<demandList.size(); i++)
			{
				int planSeq = i+1    ;
				maxPlanSeq  = planSeq;

				WMS_EXC_DEMAND demand = demandList.get(i);
				demand.setPLAN_SEQ(planSeq);
			}

			for(WMS_EXC_DEMAND demand : demandList)
			{

				int    demandSeq    = demand.getPLAN_SEQ()     ;
				String orderNo      = demand.getORDER_NO()     ;
				String demandType   = demand.getDEMAND_TYPE()  ;
				String plantCode    = demand.getPLANT_CODE()   ;
				String materialCode = demand.getMATERIAL_CODE();
				String dmdReqDate   = demand.getREQ_DATE()     ;
				float  dmdReqQty    = demand.getREQ_QTY()      ;
				String mergeCode    = MrpUtil.getMergeCode(plantCode, materialCode);

				if( dmdReqQty == 0 )
				{
					continue;
				}

				demandItem  = mergeCode ;
				demandDate  = dmdReqDate;
				demandQty   = dmdReqQty	;

				List<WMS_EXC_MRP> itemBomList = itemBomMap.get(mergeCode);
				createForwardItemDemand(demand, itemBomList, 1, plantCode, materialCode, dmdReqDate, dmdReqQty);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("startDemandPlan:" + ex.getMessage());
			ex.printStackTrace();
		}

//		MrpUtil.printPlanData(planSeqList, "PLAN_SEQ");
	}

	public void createForwardItemDemand(WMS_EXC_DEMAND demand, List<WMS_EXC_MRP> itemBomList, int bomLevel, String plantCode, String materialCode, String dmdReqDate, float dmdReqQty)
	{
		try
		{
			int    demandSeq  = demand.getPLAN_SEQ()   ;
			String orderNo    = demand.getORDER_NO()   ;
			String demandType = demand.getDEMAND_TYPE();

			String mergeCode  = MrpUtil.getMergeCode(plantCode,materialCode);
			String sortKey    = MrpUtil.getSortKey(dmdReqDate, demandType  );

			int i = 0;

			List<WMS_EXC_MRP> dataList = itemBomList.stream()
	                .filter(item ->	item.getMERGE_CODE().equals(mergeCode))
	                .collect(Collectors.toList());

			for(WMS_EXC_MRP itemBom : dataList)
			{
				i++;

				int    compBomLevel     = bomLevel+1                 ;
				String compPlantCode    = itemBom.getCOMP_PLANT_CODE()   ;
				String compMaterialCode = itemBom.getCOMP_MATERIAL_CODE();

				String compMergeCode    = MrpUtil.getMergeCode(compPlantCode,compMaterialCode);
				float  compQty          = itemBom.getCOMP_QTY()    ;
				float  leadTime         = itemBom.getLEAD_TIME()   ;
				float  safetyStock      = itemBom.getSAFETY_STOCK();
				float  lotSize          = itemBom.getLOT_SIZE()    ;

				String compReqDate      = DateUtil.getBeforeDateByString(dmdReqDate, leadTime);
				float  compReqQty       = dmdReqQty*compQty        ;
				float  invQty           = itemBom.getINV_QTY()     ;
				float  availQty         = itemBom.getAVAIL_QTY()   ;
				float  remainQty        = itemBom.getREMAIN_QTY()  ;

				WMS_EXC_MRP excMrp = new  WMS_EXC_MRP();
				excMrp.setORDER_NO(orderNo)          ;
				excMrp.setPLAN_SEQ(demandSeq)        ;
				excMrp.setDEMAND_ITEM(demandItem)    ;
				excMrp.setDEMAND_DATE(demandDate)    ;
				excMrp.setDEMAND_QTY(demandQty)      ;
				excMrp.setCOMP_NO(i)                 ;
				excMrp.setDEMAND_TYPE(demandType)    ;
				excMrp.setBOM_LEVEL(bomLevel)        ;
				excMrp.setPLANT_CODE(plantCode)      ;
				excMrp.setMATERIAL_CODE(materialCode);
				excMrp.setMERGE_CODE(mergeCode)      ;
				excMrp.setPROD_PLANT_CODE(plantCode)      ;
				excMrp.setPROD_MATERIAL_CODE(materialCode);
				excMrp.setPROD_QTY(itemBom.getPROD_QTY()) ;
				excMrp.setBEFORE_COMP_CODE(compMergeCode) ;
				excMrp.setCOMP_PLANT_CODE(compPlantCode)  ;
				excMrp.setCOMP_MATERIAL_CODE(compMaterialCode);
				excMrp.setCOMP_QTY(compQty)                   ;

				excMrp.setLEAD_TIME(leadTime)      ;
				excMrp.setSAFETY_STOCK(safetyStock);
				excMrp.setLOT_SIZE(lotSize)        ;

				excMrp.setREQ_DATE(dmdReqDate)     ;
				excMrp.setREQ_QTY(dmdReqQty)       ;
				excMrp.setPLAN_IN_DATE(compReqDate);
				excMrp.setPLAN_OUT_DATE(dmdReqDate);

				excMrp.setAVAIL_QTY(availQty)      ;
				excMrp.setREMAIN_QTY(remainQty)    ;
				excMrp.setINV_QTY(invQty)          ;
				excMrp.setSORT_KEY(sortKey)        ;

				peggingList.add(excMrp);
				createForwardItemDemand(demand, itemBomList, compBomLevel, compPlantCode, compMaterialCode, compReqDate, compReqQty);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("createForwardItemDemand:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void createTopLevelReqList()
	{
		try
		{
			List<WMS_EXC_MRP> dataList = peggingList.stream()
					.filter(item -> item.getBOM_LEVEL() == 1 &&
							        item.getCOMP_NO()   == 1
					)
					.collect(Collectors.toList())
					;

			for(WMS_EXC_MRP reqItem : dataList)
			{
				String demandType   = reqItem.getDEMAND_TYPE()  ;
				String plantCode    = reqItem.getPLANT_CODE()   ;
				String materialCode = reqItem.getMATERIAL_CODE();
				String reqDate      = reqItem.getREQ_DATE()     ;
				float  reqQty       = reqItem.getREQ_QTY()      ;
				float  lotSize      = reqItem.getLOT_SIZE()     ;
				String sortKey      = reqItem.getSORT_KEY()     ;
				String orderNo      = MrpUtil.getMergeCode(reqItem.getORDER_NO(),String.valueOf(reqItem.getPLAN_SEQ())) ;

				if(planCurrent.compareTo(reqDate) > 0)
				{
					reqDate = planCurrent;
				}

				WMS_EXC_DEMAND reqDemand = null;
				reqDemand = createReqDemand(demandType, orderNo, "-", plantCode, materialCode, reqDate, reqQty, lotSize, sortKey);
				reqItemList.add(reqDemand);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("createTopLevelReqList:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void consumeTopLevelInventory()
	{
		try
		{
			reqItemList.sort((WMS_EXC_DEMAND item1, WMS_EXC_DEMAND item2) -> item1.getSORT_KEY().compareTo(item2.getSORT_KEY()));

			List<WMS_EXC_DEMAND> dataList = reqItemList.stream()
					.filter(item -> item.getPROD_CODE().equals("-")          &&
								   !item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
							)
					.collect(Collectors.toList())
					;

			for(WMS_EXC_DEMAND reqItem : dataList)
			{
				consumeInventory(reqItem);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("consumeTopLevelInventory:" + ex.getMessage());
			ex.printStackTrace();
		}
//		MrpUtil.printDemandData(reqItemList,"TOP");
	}

	private void startDownLevelPlan()
	{
		try
		{
			int maxBomLevel = MrpUtil.getMaxBomLevel(itemBomLevelList);

			for(int i=1; i<=maxBomLevel; i++)
			{
				int curBomLevel = i;
				List<WMS_EXC_MRP> oneLevelList = itemBomLevelList.stream()
		                .filter(item ->	item.getBOM_LEVEL() == curBomLevel)
		                .collect(Collectors.toList());

				for(WMS_EXC_MRP itemLevel : oneLevelList)
				{
					loopBomLevel(itemLevel);
				}
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("startDownLevelPlan:" + ex.getMessage());
			ex.printStackTrace();
		}
//		MrpUtil.printDemandData(reqItemList,"DOWN");
	}

	public void loopBomLevel(WMS_EXC_MRP itemLevel)
	{
		try
		{
			String plantCode    = itemLevel.getPLANT_CODE()   ;
			String materialCode = itemLevel.getMATERIAL_CODE();
			String mergeCode    = MrpUtil.getMergeCode(plantCode, materialCode);

			if( materialCode.equals("-") )
			{
				return;
			}

			List<WMS_EXC_DEMAND> dataList = reqItemList.stream()
					.filter(item -> !item.getPROD_CODE().equals("-")              &&
									 item.getPLANT_CODE().equals(plantCode)       &&
									 item.getMATERIAL_CODE().equals(materialCode) &&
									 item.getREQ_QTY() > 0                        &&
								    !item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
							)
					.filter(MrpUtil.distinctByKey(item->item.getREQ_DATE() + item.getPLANT_CODE() + item.getMATERIAL_CODE() ))
					.collect(Collectors.toList())
					;

			dataList.sort((WMS_EXC_DEMAND item1, WMS_EXC_DEMAND item2) -> item1.getREQ_DATE().compareTo(item2.getREQ_DATE()));

			for(WMS_EXC_DEMAND reqItem : dataList)
			{
				getReqItemByReqDate(dataList, reqItem);// plant, material req date 기준 distinct
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("loopBomLevel:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void getReqItemByReqDate(List<WMS_EXC_DEMAND> materialDataList, WMS_EXC_DEMAND distinctItem)
	{
		try
		{
			String plantCode     = distinctItem.getPLANT_CODE()   ;
			String materialCode  = distinctItem.getMATERIAL_CODE();
			String mergeCode     = MrpUtil.getMergeCode(plantCode, materialCode);
			String curReqDate    = distinctItem.getREQ_DATE();
			float  lotSize       = distinctItem.getLOT_SIZE();
			float  sumBacklogQty = 0;

			List<WMS_EXC_DEMAND> dataList = materialDataList.stream()
					.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
									item.getMATERIAL_CODE().equals(materialCode) &&
									item.getREQ_DATE().equals(curReqDate)        &&
									item.getREQ_QTY() > 0                        &&
								   !item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
							)
					.collect(Collectors.toList())
					;
			for(WMS_EXC_DEMAND reqItem : dataList)
			{
//				System.out.println("mergeCode:"+mergeCode+",plantCode:"+reqItem.getPROD_CODE());// plant, material req date 기준으로 search
				sumBacklogQty += getSumBacklogQty(reqItem);
				if( sumBacklogQty > 0 )
				{
					float prodReqQty = getProdReqQtyByLotSize(plantCode,materialCode,sumBacklogQty,lotSize);
					String sortKey   = MrpUtil.getMergeCode(mergeCode,curReqDate) ;
					String orderNo   = MrpUtil.getMergeCode(sortKey,String.valueOf(++maxPlanSeq)) ;

					WMS_EXC_DEMAND reqDemand = createReqDemand("PRODUCTION_REQ", orderNo, mergeCode, plantCode, materialCode, curReqDate, prodReqQty, lotSize, sortKey);
					reqDemand.setBACKLOG_QTY(sumBacklogQty);
					createProductionReqItemList(reqDemand);

					setForwardReqItem(reqDemand);
				}
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("getReqItemByReqDate:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private float getProdReqQtyByLotSize(String plantCode, String materialCode, float curReqQty, float lotSize)
	{
		float prodReqQty = 0;

		try
		{
			float  invAvailQty  = MrpUtil.getInvInfo(plantCode,materialCode).getREMAIN_QTY();
			float  lotReqQty    = 0;
			float  quotient     = 0;

			if( lotSize > 0 && invAvailQty < lotSize )
			{
				if( lotSize > curReqQty)
				{
					lotReqQty  = lotSize - curReqQty;
				}
				else
				{
					quotient  = (float)Math.floor(curReqQty/lotSize);

					if(curReqQty % lotSize != 0)
					{
						lotReqQty  = ((quotient+1)*lotSize)-curReqQty;
					}
				}
			}
			prodReqQty  = curReqQty + lotReqQty;
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("getProdReqQtyByLotSize:" + ex.getMessage());
			ex.printStackTrace();
		}
		return prodReqQty;
	}

	private float getSumBacklogQty(WMS_EXC_DEMAND reqItem)
	{
		float sumBacklogQty  = 0;

		try
		{
			String plantCode    = reqItem.getPLANT_CODE()   ;
			String materialCode = reqItem.getMATERIAL_CODE();
			String curReqDate   = reqItem.getREQ_DATE()     ;
			float  lotSize      = reqItem.getLOT_SIZE()     ;

			List<WMS_EXC_DEMAND> dataList = reqItemList.stream()
					.filter(item -> !item.getPROD_CODE().equals("-")              &&
									 item.getPLANT_CODE().equals(plantCode)       &&
									 item.getMATERIAL_CODE().equals(materialCode) &&
									 item.getREQ_DATE().equals(curReqDate)        &&
									!item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
							)
					.collect(Collectors.toList())
					;

			for(WMS_EXC_DEMAND prodItem : dataList)
			{
				float  curReqQty  = prodItem.getREQ_QTY();

				if(curReqQty >  0)
				{
					WMS_EXC_DEMAND lotDemand = createReqDemand("-", "-", "-", "-", "-", "-", 0, lotSize, "-");
					WMS_EXC_INVENTORY excInv = MrpUtil.getBacklog(lotDemand, plantCode, materialCode, curReqQty, true);

					float invQty     = excInv.getINV_QTY()    ;
					float availQty   = excInv.getAVAIL_QTY()  ;
					float remainQty  = excInv.getREMAIN_QTY() ;
					float assignQty  = excInv.getASSIGN_QTY() ;
					float backlogQty = excInv.getBACKLOG_QTY();
					sumBacklogQty   += backlogQty;

					prodItem.setINV_QTY(invQty)        ;
					prodItem.setAVAIL_QTY(availQty)    ;
					prodItem.setREMAIN_QTY(remainQty)  ;
					prodItem.setASSIGN_QTY(assignQty)  ;
					prodItem.setBACKLOG_QTY(backlogQty);
//					System.out.println("4 materialCode:"+materialCode+",reqDate:"+curReqDate+",reqQty:"+curReqQty+",availQty:"+availQty+",assignQty:"+assignQty+",backlogQty:"+backlogQty+",remainQty:"+remainQty);
				}
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("getSumBacklogQty:" + ex.getMessage());
			ex.printStackTrace();
		}
		return sumBacklogQty;
	}

	private void consumeInventory(WMS_EXC_DEMAND reqItem)
	{
		WMS_EXC_DEMAND reqDemand = null;

		try
		{
			String demandType   = reqItem.getDEMAND_TYPE()  ;
			String plantCode    = reqItem.getPLANT_CODE()   ;
			String materialCode = reqItem.getMATERIAL_CODE();
			String curReqDate   = reqItem.getREQ_DATE()     ;
			float  curReqQty    = reqItem.getREQ_QTY()      ;
			float  lotSize      = reqItem.getLOT_SIZE()     ;
			float  prodReqQty   = getProdReqQtyByLotSize(plantCode,materialCode,curReqQty,lotSize);
			if( curReqQty > 0 )
			{
				reqDemand = createReqDemand("LOT_SIZE", "-", "-", plantCode, materialCode, curReqDate, curReqQty, lotSize, "-");
				WMS_EXC_INVENTORY excInv = MrpUtil.getBacklog(reqDemand, plantCode, materialCode, curReqQty, true);

				float invQty     = excInv.getINV_QTY()    ;
				float availQty   = excInv.getAVAIL_QTY()  ;
				float remainQty  = excInv.getREMAIN_QTY() ;
				float assignQty  = excInv.getASSIGN_QTY() ;
				float backlogQty = excInv.getBACKLOG_QTY();
				reqItem.setINV_QTY(invQty)        ;
				reqItem.setAVAIL_QTY(availQty)    ;
				reqItem.setREMAIN_QTY(remainQty)  ;
				reqItem.setASSIGN_QTY(assignQty)  ;
				reqItem.setBACKLOG_QTY(backlogQty);

				if( backlogQty > 0 )
				{
					String mergeCode = reqItem.getMERGE_CODE();
					String sortKey = MrpUtil.getMergeCode(mergeCode,curReqDate) ;
					String orderNo = MrpUtil.getMergeCode(sortKey,String.valueOf(++maxPlanSeq)) ;

					reqDemand = createReqDemand("PRODUCTION_REQ", orderNo, mergeCode, plantCode, materialCode, curReqDate, prodReqQty, lotSize, sortKey);
					reqDemand.setINV_QTY(invQty)        ;
					reqDemand.setAVAIL_QTY(availQty)    ;
					reqDemand.setREMAIN_QTY(remainQty)  ;
					reqDemand.setASSIGN_QTY(assignQty)  ;
					reqDemand.setBACKLOG_QTY(backlogQty);
					reqItemList.add(reqDemand);

					setForwardReqItem(reqDemand);
				}
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("consumeInventory:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private void setForwardReqItem(WMS_EXC_DEMAND reqItem)
	{
		try
		{
			String plantCode    = reqItem.getPLANT_CODE()   ;
			String materialCode = reqItem.getMATERIAL_CODE();
			String curReqDate   = reqItem.getREQ_DATE()     ;
			float  curReqQty    = reqItem.getREQ_QTY()      ;
			String mergeCode    = MrpUtil.getMergeCode(plantCode, materialCode);
			List<WMS_EXC_MRP> itemBomList = itemBomMap.get(mergeCode);

			List<WMS_EXC_MRP> dataList = itemBomList.stream()
					.filter(item -> item.getPLANT_CODE().equals(plantCode)       &&
									item.getMATERIAL_CODE().equals(materialCode)
							)
					.collect(Collectors.toList())
					;

			for(WMS_EXC_MRP bomItem : dataList)
			{
				String compPlantCode    = bomItem.getCOMP_PLANT_CODE()   ;
				String compMaterialCode = bomItem.getCOMP_MATERIAL_CODE();
				String compMergeCode    = MrpUtil.getMergeCode(compPlantCode,compMaterialCode);
				float  compQty          = bomItem.getCOMP_QTY()    ;
				float  leadTime         = bomItem.getLEAD_TIME()   ;

				String compReqDate      = DateUtil.getBeforeDateByString(curReqDate, leadTime);
				float  compReqQty       = curReqQty * compQty;
				float  compLotSize      = MrpUtil.getMasterInfo(compPlantCode, compMaterialCode).getLOT_SIZE();
				if( compMaterialCode.equals("-"))
				{
					continue;
				}

				if(planCurrent.compareTo(compReqDate)>0)
				{
					compReqDate = planCurrent;
				}

				String sortKey = MrpUtil.getMergeCode(compMergeCode,compReqDate) ;
				String orderNo = MrpUtil.getMergeCode(sortKey,String.valueOf(++maxPlanSeq)) ;

				WMS_EXC_DEMAND reqDemand = createReqDemand("PARENT_REQ", orderNo, mergeCode, compPlantCode, compMaterialCode, compReqDate, compReqQty, compLotSize, sortKey);
				createParentReqItemList(reqDemand);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("setForwardReqItem:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private static void createParentReqItemList(WMS_EXC_DEMAND reqDemand)
	{
		try
		{
			String prodCode     = reqDemand.getPROD_CODE()    ;
			String plantCode    = reqDemand.getPLANT_CODE()   ;
			String materialCode = reqDemand.getMATERIAL_CODE();
			String curReqDate   = reqDemand.getREQ_DATE()     ;
			float  curReqQty    = reqDemand.getREQ_QTY()      ;

			List<WMS_EXC_DEMAND> dataList = reqItemList.stream()
				.filter(item -> item.getPROD_CODE().equals(prodCode)         &&
								item.getPLANT_CODE().equals(plantCode)       &&
								item.getMATERIAL_CODE().equals(materialCode) &&
								item.getREQ_DATE().equals(curReqDate)        &&
							   !item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
						)
				.collect(Collectors.toList())
				;

			if( dataList.size() == 0)
			{
				reqItemList.add(reqDemand);
			}
			else
			{
				WMS_EXC_DEMAND sumDemand = dataList.get(0);
				float sumReqQty = sumDemand.getREQ_QTY();
				sumDemand.setREQ_QTY(sumReqQty+curReqQty);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("createReqItemList:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	private static void createProductionReqItemList(WMS_EXC_DEMAND reqDemand)
	{
		try
		{
			String prodCode     = reqDemand.getPROD_CODE()    ;
			String plantCode    = reqDemand.getPLANT_CODE()   ;
			String materialCode = reqDemand.getMATERIAL_CODE();
			String curReqDate   = reqDemand.getREQ_DATE()     ;
			float  curReqQty    = reqDemand.getREQ_QTY()      ;
			float  curBacklogQty= reqDemand.getBACKLOG_QTY()  ;

			List<WMS_EXC_DEMAND> dataList = reqItemList.stream()
					.filter(item -> item.getPROD_CODE().equals(prodCode)         &&
									item.getPLANT_CODE().equals(plantCode)       &&
									item.getMATERIAL_CODE().equals(materialCode) &&
									item.getREQ_DATE().equals(curReqDate)        &&
								    item.getDEMAND_TYPE().equals("PRODUCTION_REQ")
							)
					.collect(Collectors.toList())
					;

			if( dataList.size() == 0)
			{
				reqItemList.add(reqDemand);
			}
			else
			{
				WMS_EXC_DEMAND sumDemand = dataList.get(0);
				float sumReqQty     = sumDemand.getREQ_QTY()    ;
				float sumBacklogQty = sumDemand.getBACKLOG_QTY();
				sumDemand.setREQ_QTY(sumReqQty+curReqQty)       ;
				sumDemand.setBACKLOG_QTY(sumBacklogQty+curBacklogQty);
			}
		}
		catch(Exception ex)
		{
			result = "E";
			LOGGER.error("createProductionReqItemList:" + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public WMS_EXC_DEMAND createReqDemand(String demandType, String orderNo, String prodCode, String plantCode, String materialCode, String reqDate, float reqQty, float lotSize, String sortKey)
	{
		return createReqDemand(0,demandType,orderNo,prodCode,plantCode,materialCode,reqDate,reqQty,lotSize,sortKey);
	}

	public WMS_EXC_DEMAND createReqDemand(int demandSeq, String demandType, String orderNo, String prodCode, String plantCode, String materialCode, String reqDate, float reqQty, float lotSize, String sortKey)
	{
		String mergeCode = MrpUtil.getMergeCode(plantCode, materialCode);

		WMS_EXC_DEMAND reqDemand = new WMS_EXC_DEMAND();

		reqDemand.setPLAN_SEQ(demandSeq)    ;
		reqDemand.setORDER_NO(orderNo)      ;
		reqDemand.setDEMAND_TYPE(demandType);
		reqDemand.setPROD_CODE(prodCode)    ;
		reqDemand.setMERGE_CODE(mergeCode)  ;
		reqDemand.setPLANT_CODE(plantCode)  ;
		reqDemand.setMATERIAL_CODE(materialCode);
		reqDemand.setREQ_DATE(reqDate)      ;
		reqDemand.setREQ_QTY(reqQty)        ;
		reqDemand.setSORT_KEY(sortKey)      ;
		reqDemand.setLOT_SIZE(lotSize)      ;
		return reqDemand;
	}

	private void exportPlan()
	{
		Boolean export1 = true;
		Boolean export2 = true;
		Boolean export3 = true;

		dataMap.put("masterList"    ,masterList    );
		dataMap.put("invTotalList"  ,invTotalList  );
		dataMap.put("peggingList"   ,peggingList   );
		dataMap.put("reqItemList"   ,reqItemList   );

		export1 = implName.insertMasterList(dataMap) ;
		export3 = implName.insertPeggingList(dataMap);
		export3 = implName.insertReqItemList(dataMap);

		if( !(export1 && export2 && export3) )
		{
			result = "E";
		}
	}

	private void exportMrp()
	{
		Boolean export = implName.insertDynMrp(dataMap);
		if( !export )
		{
			result = "E";
		}

		dataMap.put("result", result);
		implName.updateEndMrp(dataMap);
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
