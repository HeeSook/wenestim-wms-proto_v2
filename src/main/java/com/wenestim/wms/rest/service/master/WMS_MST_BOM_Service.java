/**
 *
 */
package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_BOM;
import com.wenestim.wms.rest.impl.master.WMS_MST_BOM_Impl;
import com.wenestim.wms.rest.util.StringUtil;



/**
 * @author
 *
 */
@Path("/Master/Bom")
public class WMS_MST_BOM_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_BOM_Service.class);

	@GET
	@Path("/All")
	public Response getAllBom(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE) {
		LOGGER.info("WMS_MST_BOM_Service.getAllBom() called");

		String result = "";
		try {
			WMS_MST_BOM_Impl mWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			List<WMS_MST_BOM> allList = mWMS_MST_BOM_Impl.getAllBom(CLIENT_ID,COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Prod/List")
	public Response getBomProdList(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE) {
		LOGGER.info("WMS_MST_BOM_Service.getAllBom() called");

		String result = "";
		try {

			CLIENT_ID          = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE       = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PROD_MATERIAL_CODE = StringUtil.getEmptyNullPrevString(PROD_MATERIAL_CODE);

			WMS_MST_BOM_Impl mWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			List<WMS_MST_BOM> bomProdList = mWMS_MST_BOM_Impl.getBomProdList(CLIENT_ID, COMPANY_CODE,PROD_MATERIAL_CODE);

			Gson gson = new Gson();
			if (bomProdList != null) {
				result = gson.toJson(bomProdList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Prod/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertBomProdList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.insertBomProdList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsBomProdListArray = new JSONArray(getString);

			HashMap<String, Object> bomProdListMap = new HashMap<String, Object>();
			bomProdListMap.put("bomProdList", setBomProdObjList(jsBomProdListArray, "INSERT", userId));

			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.insertBomProdList(bomProdListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Prod/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateBomProdList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.updateBomProdList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsBomProdListArray = new JSONArray(getString);

			HashMap<String, Object> bomProdListMap = new HashMap<String, Object>();
			bomProdListMap.put("bomProdList", setBomProdObjList(jsBomProdListArray, "UPDATE", userId));

			LOGGER.debug("------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bomProdListMap.toString():"+bomProdListMap.toString());

			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.updateBomProdList(bomProdListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Prod/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteBomProdList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.deleteBomProdList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.debug("------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bomProdListMap.getString().toString():"+getString.toString());
			JSONArray jsBomProdListArray = new JSONArray(getString);

			HashMap<String, Object> bomProdListMap = new HashMap<String, Object>();
			bomProdListMap.put("bomProdList", setBomProdObjList(jsBomProdListArray, "DELETE", userId));

			LOGGER.debug("------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bomProdListMap.toString():"+bomProdListMap.toString());
			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.deleteBomProdList(bomProdListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Comp/List")
	public Response getBomCompList(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("PROD_MATERIAL_CODE") String PROD_MATERIAL_CODE) {
		LOGGER.info("WMS_MST_BOM_Service.getBomCompList() called");

		String result = "";
		try {
			WMS_MST_BOM_Impl mWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			List<WMS_MST_BOM> bomCompList = mWMS_MST_BOM_Impl.getBomCompList(CLIENT_ID, COMPANY_CODE, PROD_MATERIAL_CODE);

			Gson gson = new Gson();
			if (bomCompList != null) {
				result = gson.toJson(bomCompList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Comp/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertBomCompList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.insertBomCompList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsBomCompListArray = new JSONArray(getString);

			HashMap<String, Object> bomCompListMap = new HashMap<String, Object>();
			bomCompListMap.put("bomCompList", setBomCompObjList(jsBomCompListArray, "INSERT", userId));

			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.insertBomCompList(bomCompListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Comp/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateBomCompList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.updateBomCompList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsBomCompListArray = new JSONArray(getString);

			HashMap<String, Object> bomCompListMap = new HashMap<String, Object>();
			bomCompListMap.put("bomCompList", setBomCompObjList(jsBomCompListArray, "UPDATE", userId));

			LOGGER.debug("------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bomCompListMap.toString():"+bomCompListMap.toString());

			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.updateBomCompList(bomCompListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Comp/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteBomCompList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_BOM_Serviec.deleteBomCompList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsBomCompListArray = new JSONArray(getString);

			HashMap<String, Object> bomCompListMap = new HashMap<String, Object>();
			bomCompListMap.put("bomCompList", setBomCompObjList(jsBomCompListArray, "DELETE", userId));

			LOGGER.debug("------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> bomCompListMap.toString():"+bomCompListMap.toString());
			WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
			rtnBoolResult = iWMS_MST_BOM_Impl.deleteBomCompList(bomCompListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	private List<WMS_MST_BOM> setBomProdObjList(JSONArray jsArray, String evtHandler, String userId)
			throws Exception
	{
		List<WMS_MST_BOM> bomProdList = new ArrayList<WMS_MST_BOM>();
		try
		{
			for (int i = 0; i < jsArray.length(); i++)
			{
				JSONObject jsInfoCode = jsArray.getJSONObject(i);
				LOGGER.debug("setBomProdObjList() jsonObj:" + jsInfoCode.toString());

				String CLIENT_ID          = jsInfoCode.has("CLIENT_ID") ? jsInfoCode.getString("CLIENT_ID") : "";
				String COMPANY_CODE       = jsInfoCode.has("COMPANY_CODE") ? jsInfoCode.getString("COMPANY_CODE") : "";
				String PROD_MATERIAL_CODE = jsInfoCode.has("PROD_MATERIAL_CODE") ? jsInfoCode.getString("PROD_MATERIAL_CODE") : "";
				Float  PROD_QTY           = Float.valueOf(jsInfoCode.has("PROD_QTY") ? Float.parseFloat(jsInfoCode.getString("PROD_QTY")) : 0.0F);
				String PROD_UNIT          = jsInfoCode.has("PROD_UNIT") ? jsInfoCode.getString("PROD_UNIT") : "";

				String COMP_MATERIAL_CODE = "";
				Float  COMP_QTY           = Float.valueOf(jsInfoCode.has("COMP_QTY") ? Float.parseFloat(jsInfoCode.getString("COMP_QTY")) : 0.0F);
				String COMP_UNIT          = "";

				Integer COMP_NO      = 0;
				if (evtHandler.equals("INSERT")) {
					COMP_NO            = 0;
					COMP_UNIT          = jsInfoCode.has("PROD_UNIT") ? jsInfoCode.getString("PROD_UNIT") : "";
					COMP_MATERIAL_CODE = PROD_MATERIAL_CODE;
				} else {
					COMP_NO = Integer.valueOf(jsInfoCode.has("COMP_NO") ? Integer.parseInt(jsInfoCode.getString("COMP_NO")) : 0);
					COMP_UNIT = jsInfoCode.has("COMP_UNIT") ? jsInfoCode.getString("COMP_UNIT") : "";
					COMP_MATERIAL_CODE = jsInfoCode.has("COMP_MATERIAL_CODE") ? StringUtil.getConversionDateFormat(jsInfoCode.getString("COMP_MATERIAL_CODE")) : "";
				}

//                String DEL_FLAG      = jsInfoCode.has("DEL_FLAG") ? jsInfoCode.getString("DEL_FLAG") : "";
				String CREATE_USER = jsInfoCode.has("CREATE_USER") ? jsInfoCode.getString("CREATE_USER") : "";
				String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
				String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

				WMS_MST_BOM oWMS_MST_BOM = new WMS_MST_BOM();
				if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
				{
					Integer WSID = Integer.valueOf(jsInfoCode.has("WSID") ? Integer.parseInt(jsInfoCode.getString("WSID")) : 0);
					LOGGER.debug("setInfoCodeObjList() WSID: " + WSID);
					oWMS_MST_BOM.setWSID(WSID);
				}
				oWMS_MST_BOM.setCLIENT_ID(CLIENT_ID);
				oWMS_MST_BOM.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_MST_BOM.setPROD_MATERIAL_CODE(PROD_MATERIAL_CODE);
				oWMS_MST_BOM.setPROD_QTY(PROD_QTY);
				oWMS_MST_BOM.setPROD_UNIT(PROD_UNIT);
				oWMS_MST_BOM.setCOMP_NO(COMP_NO);
				oWMS_MST_BOM.setCOMP_MATERIAL_CODE(COMP_MATERIAL_CODE); // INSERT PROD_MATERIAL_CODE FOR 1st Row VALUE of COMPONENT with COMP_NO '0'
				oWMS_MST_BOM.setCOMP_QTY(COMP_QTY);
				oWMS_MST_BOM.setCOMP_UNIT(COMP_UNIT);

				oWMS_MST_BOM.setCREATE_USER(CREATE_USER);
				oWMS_MST_BOM.setUPDATE_USER(UPDATE_USER);
				oWMS_MST_BOM.setDELETE_USER(DELETE_USER);
				bomProdList.add(oWMS_MST_BOM);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("WMS_MST_BOM_Serviec.setBomProdObjList() Exception:" + e.toString() + " #");
		}
		return bomProdList;
	}

	private List<WMS_MST_BOM> setBomCompObjList(JSONArray jsArray, String evtHandler, String userId)
			throws Exception
	{
		List<WMS_MST_BOM> bomCompList = new ArrayList<WMS_MST_BOM>();
		try
		{
			for (int i = 0; i < jsArray.length(); i++)
			{
				JSONObject jsInfoCode = jsArray.getJSONObject(i);
				LOGGER.debug("setBomCompObjList() jsonObj:" + jsInfoCode.toString());

				String CLIENT_ID          = jsInfoCode.has("CLIENT_ID") ? jsInfoCode.getString("CLIENT_ID") : "";
				String COMPANY_CODE       = jsInfoCode.has("COMPANY_CODE") ? jsInfoCode.getString("COMPANY_CODE") : "";
				String PROD_MATERIAL_CODE = jsInfoCode.has("PROD_MATERIAL_CODE") ? jsInfoCode.getString("PROD_MATERIAL_CODE") : "";
				Float  PROD_QTY           = Float.valueOf(jsInfoCode.has("PROD_QTY") ? Float.parseFloat(jsInfoCode.getString("PROD_QTY") == null ? "0" : jsInfoCode.getString("PROD_QTY")) : 0.0F);
				String PROD_UNIT          = jsInfoCode.has("PROD_UNIT") ? jsInfoCode.getString("PROD_UNIT") : "";

				String COMP_MATERIAL_CODE = jsInfoCode.has("COMP_MATERIAL_CODE") ? jsInfoCode.getString("COMP_MATERIAL_CODE") : "";
				Float  COMP_QTY           = Float.valueOf(jsInfoCode.has("COMP_QTY") ? Float.parseFloat(jsInfoCode.getString("COMP_QTY")) : 0.0F);
				String COMP_UNIT          = jsInfoCode.has("COMP_UNIT") ? jsInfoCode.getString("COMP_UNIT") : "";

				Integer COMP_NO           = Integer.valueOf(jsInfoCode.has("COMP_NO") ? Integer.parseInt(jsInfoCode.getString("COMP_NO")) : 0);

				String CREATE_USER = jsInfoCode.has("CREATE_USER") ? jsInfoCode.getString("CREATE_USER") : "";
				String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
				String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

				WMS_MST_BOM oWMS_MST_BOM = new WMS_MST_BOM();
				if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
				{
					Integer WSID = Integer.valueOf(jsInfoCode.has("WSID") ? Integer.parseInt(jsInfoCode.getString("WSID")) : 0);
					LOGGER.debug("setInfoCodeObjList() WSID: " + WSID);
					oWMS_MST_BOM.setWSID(WSID);
				}

				if (!evtHandler.equals("DELETE")) {
					oWMS_MST_BOM.setCLIENT_ID(CLIENT_ID);
					oWMS_MST_BOM.setCOMPANY_CODE(COMPANY_CODE);
					oWMS_MST_BOM.setPROD_MATERIAL_CODE(PROD_MATERIAL_CODE);
					oWMS_MST_BOM.setPROD_QTY(PROD_QTY);
					oWMS_MST_BOM.setPROD_UNIT(PROD_UNIT);
					oWMS_MST_BOM.setCOMP_NO(COMP_NO);
					oWMS_MST_BOM.setCOMP_MATERIAL_CODE(COMP_MATERIAL_CODE); // INSERT PROD_MATERIAL_CODE FOR 1st Row VALUE of COMPONENT with COMP_NO '0'
					oWMS_MST_BOM.setCOMP_QTY(COMP_QTY);
					oWMS_MST_BOM.setCOMP_UNIT(COMP_UNIT);

					oWMS_MST_BOM.setCREATE_USER(CREATE_USER);
					oWMS_MST_BOM.setUPDATE_USER(UPDATE_USER);
					oWMS_MST_BOM.setDELETE_USER(DELETE_USER);
				}
				bomCompList.add(oWMS_MST_BOM);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("WMS_MST_BOM_Serviec.setBomCompObjList() Exception:" + e.toString() + " #");
		}
		return bomCompList;
	}


    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_BOM_Serviec.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

        	WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
        	List<HashMap<String,Object>> allList = iWMS_MST_BOM_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_BOM_Serviec.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	WMS_MST_BOM_Impl iWMS_MST_BOM_Impl = new WMS_MST_BOM_Impl();
        	rtnBoolResult = iWMS_MST_BOM_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
