/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.production.WMS_DYN_PRODUCTION;
import com.wenestim.wms.rest.impl.production.WMS_DYN_PRODUCTION_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Production/Planning")
public class WMS_DYN_PRODUCTION_Service {

    private static final Logger LOGGER = Logger.getLogger(WMS_DYN_PRODUCTION_Service.class);

    @POST
    @Path("List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPlanList(InputStream inputStream)
    {

        LOGGER.info("WMS_DYN_PRODUCTION_Service.getPlanList() called");
        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dateData    = (String)paramMap.get("dateData");
        	ArrayList dateList = StringUtil.getJsonToHashMapList(dateData);
        	paramMap.put("dateList", dateList);

        	WMS_DYN_PRODUCTION_Impl iWMS_DYN_PRODUCTION_Impl = new WMS_DYN_PRODUCTION_Impl();
            List<HashMap<String,Object>> dataList = iWMS_DYN_PRODUCTION_Impl.getPlanList(paramMap);

            Gson gson = new Gson();
            if (dataList != null)
            {
                result = gson.toJson(dataList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePlanList(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PRODUCTION_Service.updatePlanList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsDataListArray = new JSONArray(getString);

            HashMap rtnMap = setDataObjList(jsDataListArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", rtnMap.get("dataList"));

            WMS_DYN_PRODUCTION_Impl iWMS_DYN_PRODUCTION_Impl = new WMS_DYN_PRODUCTION_Impl();
            rtnBoolResult = iWMS_DYN_PRODUCTION_Impl.updatePlanList(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

//    @POST
//    @Path("/Create/{userId}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON+";charset=utf-8")
//    public Response insertPlanList(InputStream inputStream, @PathParam("userId") String userId) throws Exception {
//        LOGGER.info("WMS_DYN_PLAN_Service.insertPlanList() called");
//
//        Boolean rtnBoolResult = false;
//        try {
//            String getString = StringUtil.getInputStreamToString(inputStream);
//            LOGGER.debug("insertPlanList() [getString]"+getString);
//            JSONObject jsPlan = new JSONObject(getString);
//
//            String CLIENT_ID    = jsPlan.has("CLIENT_ID")    ? jsPlan.getString("CLIENT_ID")    : "";
//            String COMPANY_CODE = jsPlan.has("COMPANY_CODE") ? jsPlan.getString("COMPANY_CODE") : "";
//            String PLANT_CODE   = jsPlan.has("PLANT_CODE")   ? jsPlan.getString("PLANT_CODE")   : "";
//            String PLAN_DATE    = jsPlan.has("PLAN_DATE")    ? jsPlan.getString("PLAN_DATE")    : "";
//
//            WMS_DYN_PLAN_Impl iWMS_DYN_PLAN_Impl = new WMS_DYN_PLAN_Impl();
//            Integer planVer = iWMS_DYN_PLAN_Impl.getPlanMaxVer(CLIENT_ID, COMPANY_CODE, PLANT_CODE, PLAN_DATE);
//            planVer = planVer + 1;
//            String PLAN_VER = String.format("%04d", planVer);
//            LOGGER.debug("insertPlanList() [Params] CLIENT_ID:"+CLIENT_ID+", COMPANY_CODE:"+COMPANY_CODE+",PLANT_CODE:"+PLANT_CODE+",PLAN_DATE:"+PLAN_DATE+",PLAN_VER:"+PLAN_VER);
//
//            rtnBoolResult = iWMS_DYN_PLAN_Impl.insertPlan(CLIENT_ID,COMPANY_CODE,PLANT_CODE,PLAN_DATE,PLAN_VER,userId);
//
//            JsonObject rtnJson = new JsonObject();
//            rtnJson = setTableEventResultHandler(rtnBoolResult, "");
//
//            return Response.status(200).entity(rtnJson.toString()).build();
//
//        } catch (Exception e) {
//            LOGGER.error("Exception:"+e.getMessage());
//            e.printStackTrace();
//            return Response.status(500).entity(e.toString()).build();
//        }
//
//    }


    public HashMap setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
    {
        HashMap rtnMap = new HashMap();

        List<WMS_DYN_PRODUCTION> dataList = null;
        try
        {
            dataList = new ArrayList<WMS_DYN_PRODUCTION>();

            for (int i=0; i<jsArray.length(); i++)
            {
                String PLAN_VER      = jsArray.getJSONObject(i).has("PLAN_VER"     ) ? jsArray.getJSONObject(i).getString("PLAN_VER"     ) : "";
            	String PLANT_CODE    = jsArray.getJSONObject(i).has("PLANT_CODE"   ) ? jsArray.getJSONObject(i).getString("PLANT_CODE"   ) : "";
                String MATERIAL_CODE = jsArray.getJSONObject(i).has("MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MATERIAL_CODE") : "";
                String PLAN_DATE     = jsArray.getJSONObject(i).has("PLAN_DATE"    ) ? jsArray.getJSONObject(i).getString("PLAN_DATE"    ) : "";
                String PLAN_QTY      = jsArray.getJSONObject(i).has("PLAN_QTY"     ) ? jsArray.getJSONObject(i).getString("PLAN_QTY"     ) : null;
                String PLAN_TYPE     = jsArray.getJSONObject(i).has("PLAN_TYPE"    ) ? jsArray.getJSONObject(i).getString("PLAN_TYPE"    ) : null;

                WMS_DYN_PRODUCTION oWMS_DYN_PRODUCTION = new WMS_DYN_PRODUCTION();
                oWMS_DYN_PRODUCTION.setCLIENT_ID(    clientId      );
                oWMS_DYN_PRODUCTION.setCOMPANY_CODE( comapnyCode   );
                oWMS_DYN_PRODUCTION.setPLAN_VER(     PLAN_VER      );
                oWMS_DYN_PRODUCTION.setPLANT_CODE(   PLANT_CODE    );
                oWMS_DYN_PRODUCTION.setMATERIAL_CODE(MATERIAL_CODE );
                oWMS_DYN_PRODUCTION.setPLAN_DATE(    PLAN_DATE     );
                oWMS_DYN_PRODUCTION.setPLAN_QTY(new Float(PLAN_QTY));
                oWMS_DYN_PRODUCTION.setPLAN_TYPE(PLAN_TYPE         );
                oWMS_DYN_PRODUCTION.setCREATE_USER(  userId        );
                oWMS_DYN_PRODUCTION.setUPDATE_USER(  userId        );

                dataList.add(oWMS_DYN_PRODUCTION);
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        rtnMap.put("dataList", dataList);
        return rtnMap;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
