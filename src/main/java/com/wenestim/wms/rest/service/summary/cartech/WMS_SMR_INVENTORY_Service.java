/**
 *
 */
package com.wenestim.wms.rest.service.summary.cartech;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.impl.summary.cartech.WMS_SMR_INVENTORY_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Cartech/Summary/Inventory")
public class WMS_SMR_INVENTORY_Service
{
	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_INVENTORY_Service.class);

    @POST
    @Path("/Total/Inventory/Qty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getTotalInventoryQty(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_INVENTORY_Service.getTotalInventoryQty() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_INVENTORY_Impl iWMS_SMR_INVENTORY_Impl = new WMS_SMR_INVENTORY_Impl();
        	HashMap<String,Object> resultMap = iWMS_SMR_INVENTORY_Impl.getTotalInventoryQty(paramMap);

            Gson gson = new Gson();
            if (resultMap != null)
            {
                result = gson.toJson(resultMap);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Total/Inventory/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getTotalInventoryList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_INVENTORY_Service.getTotalInventoryList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_INVENTORY_Impl iWMS_SMR_INVENTORY_Impl = new WMS_SMR_INVENTORY_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_INVENTORY_Impl.getTotalInventoryList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Detail/Inventory/Qty")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDetailInventoryQty(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_INVENTORY_Service.getDetailInventoryQty() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_INVENTORY_Impl iWMS_SMR_INVENTORY_Impl = new WMS_SMR_INVENTORY_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_INVENTORY_Impl.getDetailInventoryQty(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	@POST
    @Path("/Detail/Inventory/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getDetailInventoryList(InputStream inputStream)
    {
        LOGGER.info("WMS_SMR_INVENTORY_Service.getDetailInventoryList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_SMR_INVENTORY_Impl iWMS_SMR_INVENTORY_Impl = new WMS_SMR_INVENTORY_Impl();
        	List<HashMap<String,Object>> resultList = iWMS_SMR_INVENTORY_Impl.getDetailInventoryList(paramMap);

            Gson gson = new Gson();
            if (resultList != null)
            {
                result = gson.toJson(resultList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
