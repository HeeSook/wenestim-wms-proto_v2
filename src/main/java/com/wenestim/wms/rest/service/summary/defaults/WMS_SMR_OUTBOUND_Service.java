/**
 *
 */
package com.wenestim.wms.rest.service.summary.defaults;

import java.util.HashMap;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.impl.summary.defaults.WMS_SMR_OUTBOUND_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Default/Summary/Outbound")
public class WMS_SMR_OUTBOUND_Service {

	private static final Logger LOGGER = Logger.getLogger(WMS_SMR_OUTBOUND_Service.class);

	/**
	 * GET OUTBOUND ORDER DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return OUTBOUND ORDER DAILY DETAIL LIST
	 */
	@GET
	@Path("/Order/DailyDetail")
	public Response getOutboundOrderDailyDetail(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundOrderDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundOrderDailyDetail(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Order/Customer/Top5")
	public Response getOutboundOrderTop5ByCustomer(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundOrderTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> vendorMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundOrderTop5ByCustomer(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (vendorMapList != null) {
				result = gson.toJson(vendorMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	@GET
	@Path("/Order/TotalQty")
	public Response getOutboundOrderDailyTotalQty(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundOrderTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> orderedTotalQty = iWMS_SMR_OUTBOUND_Impl.getOutboundOrderDailyTotalQty(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (orderedTotalQty != null) {
				result = gson.toJson(orderedTotalQty);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	/**
	 * GET OUTBOUND PICKING DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return OUTBOUND PICKING DAILY DETAIL LIST
	 */
	@GET
	@Path("/Picking/DailyDetail")
	public Response getOutboundPickingDailyDetail(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("POST_DATE")     String POST_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundPickingDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", POST_DATE: " + POST_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			POST_DATE = StringUtil.getEmptyNullPrevString(POST_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundPickingDailyDetail(CLIENT_ID, COMPANY_CODE, POST_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Picking/Customer/Top5")
	public Response getOutboundPickingTop5ByCustomer(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("POST_DATE")     String POST_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundPickingTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", POST_DATE: " + POST_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			POST_DATE     = StringUtil.getEmptyNullPrevString(POST_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> vendorMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundPickingTop5ByCustomer(CLIENT_ID, COMPANY_CODE, POST_DATE);

			Gson gson = new Gson();
			if (vendorMapList != null) {
				result = gson.toJson(vendorMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	@GET
	@Path("/Picking/TotalQty")
	public Response getOutboundPickingDailyTotalQty(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("POST_DATE")     String POST_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundPickingTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + POST_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			POST_DATE     = StringUtil.getEmptyNullPrevString(POST_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> pickingTotalQty = iWMS_SMR_OUTBOUND_Impl.getOutboundPickingDailyTotalQty(CLIENT_ID, COMPANY_CODE, POST_DATE);

			Gson gson = new Gson();
			if (pickingTotalQty != null) {
				result = gson.toJson(pickingTotalQty);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	/**
	 * GET OUTBOUND DELIVERY DAILY DETAIL LIST
	 * @param CLIENT_ID
	 * @param COMPANY_CODE
	 * @param DELIVERY_DATE
	 * @return OUTBOUND DELIVERY DAILY DETAIL LIST
	 */
	@GET
	@Path("/Delivery/DailyDetail")
	public Response getOutboundDeliveryDailyDetail(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundDeliveryDailyDetail() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> detailMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundDeliveryDailyDetail(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (detailMapList != null) {
				result = gson.toJson(detailMapList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Delivery/Customer/Top5")
	public Response getOutboundDeliveryTop5ByCustomer(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundDeliveryTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> deliveryMapList = iWMS_SMR_OUTBOUND_Impl.getOutboundDeliveryTop5ByCustomer(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (deliveryMapList != null) {
				result = gson.toJson(deliveryMapList);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}

	@GET
	@Path("/Delivery/TotalQty")
	public Response getOutboundDeliveryDailyTotalQty(
			@QueryParam("CLIENT_ID")     String CLIENT_ID,
			@QueryParam("COMPANY_CODE")  String COMPANY_CODE,
			@QueryParam("DELIVERY_DATE") String DELIVERY_DATE) {

		LOGGER.info("WMS_SMR_OUTBOUND_Service.getOutboundDeliveryTop5ByCustomer() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", DELIVERY_DATE: " + DELIVERY_DATE);

		String result = "";
		try {
			CLIENT_ID     = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE  = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			DELIVERY_DATE = StringUtil.getEmptyNullPrevString(DELIVERY_DATE);

			WMS_SMR_OUTBOUND_Impl iWMS_SMR_OUTBOUND_Impl = new WMS_SMR_OUTBOUND_Impl();
			List<HashMap<?, ?>> deliveryTotalQty = iWMS_SMR_OUTBOUND_Impl.getOutboundDeliveryDailyTotalQty(CLIENT_ID, COMPANY_CODE, DELIVERY_DATE);

			Gson gson = new Gson();
			if (deliveryTotalQty != null) {
				result = gson.toJson(deliveryTotalQty);
			}
			return Response.status(200).entity(result).build();
		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(result).build();
		}
	}


}
