package com.wenestim.wms.rest.service.eaccounting;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.wenestim.wms.rest.util.StringUtil;


@Path("/sap")
public class sapRestService {

	private final static Logger LOGGER = Logger.getLogger(sapRestService.class);
	// DEV 
	private final static String SAP_URI = "http://sejindev.sejinamerica.com";
	
	@POST
	@Path("/SAPDocumentSet")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response postWmsDocHeaderSet(InputStream inputStream) {
		LOGGER.info("$sapRestService.postWmsDocHeaderSet() called()");
		
		Boolean result = false;
		try {
			String inputString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.debug("postWmsDocHeaderSet.getString():"+inputString);
			
			String putODataUrl = SAP_URI + "/sap/opu/odata/SAP/ZPLAS_COM_DOCUMENT_SRV/WMS_DOC_HEADERSet";
			
			Client client = Client.create();
			WebResource webResource = client.resource(putODataUrl);
			ClientResponse response = webResource
					.accept("application/json")
					.header("Content-Type", "application/json; charset=utf-8")
//	    	        .header("Authorization", "Basic cmZjLTAxOjEyMzQ1Njc=")  // PROD
//	    	        .header("Authorization", "Basic YWJhcC1xcjowOTg3NjU=")  // DEV	    
	    	        .header("sap-client", "900")
	    	        .header("X-Requested-With", "X")
	    	        .post(ClientResponse.class, inputString.toString());
			
			Map<String, List<String>> map = response.getHeaders();
			for (Map.Entry<String, List<String>> entry : map.entrySet()) {
				LOGGER.debug("Key : " + entry.getKey() 
		                           + " ,Value : " + entry.getValue());
			}
			
			// throw exception when error occurred
	        if (response.getStatus() == 200 || response.getStatus() == 201) {
	            result = true;
	        } 
	        
	        Gson gson = new Gson();

	        JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("METHOD", "WmsDocHeaderSet");
			rtnJson.addProperty("DATA", inputString.toString());
			
			if (result) {
				rtnJson.addProperty("RESULT", "S");
			} else {
				rtnJson.addProperty("RESULT", "F");
			}
			rtnJson.addProperty("MSG", response.toString());

			response.bufferEntity();
			
			String jsonString = response.getEntity(String.class);
			LOGGER.info(jsonString);
			
			JsonElement jsonElement = new JsonParser().parse(jsonString);
			JsonObject jsonObject = jsonElement.getAsJsonObject();
			jsonObject = jsonObject.getAsJsonObject("d");
			jsonObject = jsonObject.getAsJsonObject("WMS_DOC_RETURNSet");
			JsonArray jsonArray =  jsonObject.getAsJsonArray("results");
			
			JsonElement jsonArrayElement = gson.toJsonTree(jsonArray);
			
			rtnJson.add("RETURN", jsonArrayElement);

			response.getEntityInputStream().reset();
            
			LOGGER.info("$sapRestService.postWmsDocHeaderSet() End with rtnJson:"+rtnJson.toString());
			
			return Response.status(200).entity(rtnJson.toString()).build();
			
		} catch (Exception e) {
			LOGGER.error("Exception: "+e.getMessage());
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
		
	}

}
