/**
 *
 */
package com.wenestim.wms.rest.service.inventory;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.inventory.WMS_DYN_STOCK_MOVEMENT;
import com.wenestim.wms.rest.entity.production.WMS_DYN_BACKLOG;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.util.StringUtil;


/**
 * @author
 *
 */
@Path("/Inventory/StockMovement")
public class WMS_DYN_STOCK_MOVEMENT_Service
{
    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_STOCK_MOVEMENT_Service.class);

    @GET
    @Path("/InventoryList")
    public Response getInventoryList(
            @QueryParam("CLIENT_ID"       ) String CLIENT_ID    ,
            @QueryParam("COMPANY_CODE"    ) String COMPANY_CODE ,
            @QueryParam("PLANT_CODE"      ) String PLANT_CODE   ,
            @QueryParam("MOVEMENT_TYPE"   ) String MOVEMENT_TYPE,
            @QueryParam("CURR_STORAGE_LOC") String CURR_STORAGE_LOC,
            @QueryParam("MOVE_STORAGE_LOC") String MOVE_STORAGE_LOC,
            @QueryParam("MATERIAL_CODE"   ) String MATERIAL_CODE,
            @QueryParam("POST_DATE"       ) String POST_DATE
            )
    {
        LOGGER.info("WMS_DYN_STOCK_MOVEMENT_Service.getInventoryList() called");
        LOGGER.debug("[PARAM]");
        LOGGER.debug("CLIENT_ID:"       + CLIENT_ID       );
        LOGGER.debug("COMPANY_CODE:"    + COMPANY_CODE    );
        LOGGER.debug("PLANT_CODE:"      + PLANT_CODE      );
        LOGGER.debug("MOVEMENT_TYPE:"   + MOVEMENT_TYPE   );
        LOGGER.debug("CURR_STORAGE_LOC:"+ CURR_STORAGE_LOC);
        LOGGER.debug("MOVE_STORAGE_LOC:"+ MOVE_STORAGE_LOC);
        LOGGER.debug("MATERIAL_CODE:"   + MATERIAL_CODE   );
        LOGGER.debug("POST_DATE:"       + POST_DATE       );

        String result = "";
        try
        {
        	CLIENT_ID        = StringUtil.getEmptyNullPrevString(CLIENT_ID       );
        	COMPANY_CODE     = StringUtil.getEmptyNullPrevString(COMPANY_CODE    );
        	PLANT_CODE       = StringUtil.getEmptyNullPrevString(PLANT_CODE      );
        	MOVEMENT_TYPE    = StringUtil.getEmptyNullPrevString(MOVEMENT_TYPE   );
        	CURR_STORAGE_LOC = StringUtil.getEmptyNullPrevString(CURR_STORAGE_LOC);
        	MOVE_STORAGE_LOC = StringUtil.getEmptyNullPrevString(MOVE_STORAGE_LOC);
        	MATERIAL_CODE    = StringUtil.getEmptyNullPrevString(MATERIAL_CODE   );
        	POST_DATE        = StringUtil.getEmptyNullPrevString(POST_DATE       );

            WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
            List<WMS_DYN_STOCK_MOVEMENT> dataList = iWMS_DYN_STOCK_MOVEMENT_Impl.getInventoryList(
            		CLIENT_ID        ,
            		COMPANY_CODE     ,
            		PLANT_CODE       ,
            		MOVEMENT_TYPE    ,
            		CURR_STORAGE_LOC ,
            		MOVE_STORAGE_LOC ,
            		MATERIAL_CODE    ,
            		POST_DATE
                    );

            Gson gson = new Gson();

            if (dataList != null)
            {
                result = gson.toJson(dataList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @GET
    @Path("/MovementList")
    public Response getMovementList(
            @QueryParam("CLIENT_ID"     ) String CLIENT_ID     ,
            @QueryParam("COMPANY_CODE"  ) String COMPANY_CODE  ,
            @QueryParam("PLANT_CODE"    ) String PLANT_CODE    ,
            @QueryParam("MOVEMENT_TYPE" ) String MOVEMENT_TYPE ,
            @QueryParam("STORAGE_LOC"   ) String STORAGE_LOC   ,
            @QueryParam("MATERIAL_TYPE" ) String MATERIAL_TYPE ,
            @QueryParam("MATERIAL_CODE" ) String MATERIAL_CODE ,
            @QueryParam("FROM_POST_DATE") String FROM_POST_DATE,
            @QueryParam("TO_POST_DATE"  ) String TO_POST_DATE
            )
    {
        LOGGER.info("WMS_DYN_STOCK_MOVEMENT_Service.getMovementList() called");

        String result = "";
        try
        {
            CLIENT_ID      = StringUtil.getEmptyNullPrevString(CLIENT_ID     );
            COMPANY_CODE   = StringUtil.getEmptyNullPrevString(COMPANY_CODE  );
            PLANT_CODE     = StringUtil.getEmptyNullPrevString(PLANT_CODE    );
            MOVEMENT_TYPE  = StringUtil.getEmptyNullPrevString(MOVEMENT_TYPE );
            STORAGE_LOC    = StringUtil.getEmptyNullPrevString(STORAGE_LOC   );
            MATERIAL_TYPE  = StringUtil.getEmptyNullPrevString(MATERIAL_TYPE );
            MATERIAL_CODE  = StringUtil.getEmptyNullPrevString(MATERIAL_CODE );
            FROM_POST_DATE = StringUtil.getEmptyNullPrevString(FROM_POST_DATE);
            TO_POST_DATE   = StringUtil.getEmptyNullPrevString(TO_POST_DATE  );

            WMS_DYN_STOCK_MOVEMENT_Impl mWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
            List<WMS_DYN_STOCK_MOVEMENT> allList = mWMS_DYN_STOCK_MOVEMENT_Impl.getMovementList(
                    CLIENT_ID     ,
                    COMPANY_CODE  ,
                    PLANT_CODE    ,
                    MOVEMENT_TYPE ,
                    STORAGE_LOC   ,
                    MATERIAL_TYPE ,
                    MATERIAL_CODE ,
                    FROM_POST_DATE, TO_POST_DATE
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Create/{userId}/{clientId}/{companyCode}/{inputType}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createStockMovement(
            InputStream inputStream,
            @PathParam("userId"     ) String userId     ,
            @PathParam("clientId"   ) String clientId   ,
            @PathParam("companyCode") String companyCode,
            @PathParam("inputType"  ) String inputType
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_STOCK_MOVEMENT_Service.createStockMovement() called");
        LOGGER.info("userId:"+userId+",clientId:"+clientId+",companyCode:"+companyCode+",inputType:"+inputType);

        Boolean rtnBoolResult = false;
        String rtnMsg = "";
        try
        {

        	userId      = StringUtil.getEmptyNullPrevString(userId     );
        	clientId    = StringUtil.getEmptyNullPrevString(clientId   );
        	companyCode = StringUtil.getEmptyNullPrevString(companyCode);
        	inputType   = StringUtil.getEmptyNullPrevString(inputType  );

        	String getString = StringUtil.getInputStreamToString(inputStream);
        	LOGGER.info("getString : " +getString);
            JSONArray jsDataListArray = new JSONArray(getString);
            // to make array

            WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
            rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement(inputType,jsDataListArray, userId, clientId, companyCode);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_STOCK_MOVEMENT_Service.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

            WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
        	List<HashMap<String,Object>> allList = iWMS_DYN_STOCK_MOVEMENT_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_STOCK_MOVEMENT_Service.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
        	String userId      = (String)paramMap.get("USER_ID"     );
        	String clientId    = (String)paramMap.get("CLIENT_ID"   );
        	String companyCode = (String)paramMap.get("COMPANY_CODE");
        	String postDate    = (String)paramMap.get("POST_DATE"   );

        	String dataItem = (String)paramMap.get("dataItem");
        	JSONArray jsonArray = new JSONArray(dataItem);

            List dataList = setDataObjList(jsonArray, "UPDATE", postDate, userId, clientId, companyCode);

            WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
            rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("MOVEMENT", jsonArray, userId, clientId, companyCode);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public List setDataObjList(JSONArray jsArray, String evtHandler, String postDate, String userId, String clientId, String comapnyCode) throws Exception
    {
        List<WMS_DYN_BACKLOG> dataList = null;

        try
        {
            dataList = new ArrayList<WMS_DYN_BACKLOG>();

            for (int i=0; i<jsArray.length(); i++)
            {
            	String STORAGE_LOC = jsArray.getJSONObject(i).has("STORAGE_LOC") ? jsArray.getJSONObject(i).getString("STORAGE_LOC") : "";
            	Double MOVE_QTY    = jsArray.getJSONObject(i).has("MOVE_QTY"   ) ? jsArray.getJSONObject(i).getDouble("MOVE_QTY")    : 0  ;

				jsArray.getJSONObject(i).put("MOVEMENT_TYPE"   , "100"      );
				jsArray.getJSONObject(i).put("POST_DATE"       , postDate   );
				jsArray.getJSONObject(i).put("CURR_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("CURR_QTY"        , MOVE_QTY   );
				jsArray.getJSONObject(i).put("MOVE_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("MOVE_QTY"        , MOVE_QTY   );
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        return dataList;
    }


    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret" , Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg" , "confirm");
        }
        else
        {
            String rtnMsg = "could not save data";
            rtnJson.addProperty("ret" , Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg" , rtnMsg);
        }
        return rtnJson;
    }
}
