/**
 *
 */
package com.wenestim.wms.rest.service.common;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.common.WMS_MST_COMPANY;
import com.wenestim.wms.rest.impl.common.WMS_MST_COMPANY_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Common/Company")
public class WMS_MST_COMPANY_Service
{

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_COMPANY_Service.class);

	@GET
	@Path("/List")
	public Response getCompanyList(
			@QueryParam("CLIENT_ID"   ) String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE
			)
	{
		LOGGER.info("WMS_MST_COMPANY_Service.getCompanyList() called");

		String result = "";
		try
		{
			CLIENT_ID    = StringUtil.getEmptyNullPrevString(CLIENT_ID   );
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);

			WMS_MST_COMPANY_Impl mWMS_MST_COMPANY_Impl = new WMS_MST_COMPANY_Impl();
			List<WMS_MST_COMPANY> allList = mWMS_MST_COMPANY_Impl.getCompanyList(CLIENT_ID,COMPANY_CODE);

			Gson gson = new Gson();
			if (allList != null)
			{
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateCompany(InputStream inputStream) throws Exception
	{
		LOGGER.info("WMS_MST_COMPANY_Service.updateCompany() called");
        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_MST_COMPANY_Impl mWMS_MST_COMPANY_Impl = new WMS_MST_COMPANY_Impl();
        	rtnBoolResult = mWMS_MST_COMPANY_Impl.updateCompany(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret" , Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg" , "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret" , Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg" , rtnMsg);
		}
		return rtnJson;
	}

}
