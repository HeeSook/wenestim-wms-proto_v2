/**
 *
 */
package com.wenestim.wms.rest.service.outbound;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.outbound.WMS_DYN_PICKING;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.impl.outbound.WMS_DYN_PICKING_Impl;
import com.wenestim.wms.rest.util.StringUtil;




/**
 * @author
 *
 */
@Path("/Outbound/Picking")
public class WMS_DYN_PICKING_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PICKING_Service.class);

	@GET
	@Path("/All")
	public Response getAllPicking() {
		LOGGER.info("WMS_DYN_PICKING_Service.getAllPicking() called");

		String result = "";
		try {
			WMS_DYN_PICKING_Impl mWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			List<WMS_DYN_PICKING> allList = mWMS_DYN_PICKING_Impl.getAllPicking();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/List")
	public Response getAllPickingByCompany(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("PL_NO") String PL_NO,
			@QueryParam("CUST_CODE") String CUST_CODE, @QueryParam("HEADER_FLAG") String HEADER_FLAG,
			@QueryParam("CREATE_DATE1") String CREATE_DATE1, @QueryParam("CREATE_DATE2") String CREATE_DATE2) {
		LOGGER.info("WMS_DYN_PICKING_Service.getAllPickingByCompany() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PL_NO = StringUtil.getEmptyNullPrevString(PL_NO);
			CUST_CODE = StringUtil.getEmptyNullPrevString(CUST_CODE);
			HEADER_FLAG = StringUtil.getEmptyNullPrevString(HEADER_FLAG);
			CREATE_DATE1 = StringUtil.getEmptyNullPrevString(CREATE_DATE1);
			CREATE_DATE2 = StringUtil.getEmptyNullPrevString(CREATE_DATE2);

			WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			List<WMS_DYN_PICKING> dataList = iWMS_DYN_PICKING_Impl.getAllPickingByCompany(CLIENT_ID, COMPANY_CODE,
					PL_NO, CUST_CODE, HEADER_FLAG, CREATE_DATE1, CREATE_DATE2);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/GIList")
	public Response getAllPickingByCompanyGI(@QueryParam("CLIENT_ID") String CLIENT_ID,
			@QueryParam("COMPANY_CODE") String COMPANY_CODE, @QueryParam("PL_NO") String PL_NO,
			@QueryParam("CUST_CODE") String CUST_CODE, @QueryParam("HEADER_FLAG") String HEADER_FLAG,
			@QueryParam("CREATE_DATE1") String CREATE_DATE1, @QueryParam("CREATE_DATE2") String CREATE_DATE2) {
		LOGGER.info("WMS_DYN_PICKING_Service.getAllPickingByCompany() called");

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			PL_NO = StringUtil.getEmptyNullPrevString(PL_NO);
			CUST_CODE = StringUtil.getEmptyNullPrevString(CUST_CODE);
			HEADER_FLAG = StringUtil.getEmptyNullPrevString(HEADER_FLAG);
			CREATE_DATE1 = StringUtil.getEmptyNullPrevString(CREATE_DATE1);
			CREATE_DATE2 = StringUtil.getEmptyNullPrevString(CREATE_DATE2);

			WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			List<WMS_DYN_PICKING> dataList = iWMS_DYN_PICKING_Impl.getAllPickingByCompanyGI(CLIENT_ID, COMPANY_CODE,
					PL_NO, CUST_CODE, HEADER_FLAG, CREATE_DATE1, CREATE_DATE2);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Current/SerialNo")
	public Response getCurrentTodayPickingNo(@QueryParam("CREATE_DATE") String CREATE_DATE) {
		LOGGER.info("WMS_DYN_PICKING_Service.getCurrentTodayPickingNo() called");

		String result = "";
		try {
			CREATE_DATE = StringUtil.getEmptyNullPrevString(CREATE_DATE);

			WMS_DYN_PICKING_Impl mWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			List<WMS_DYN_PICKING> dataList = mWMS_DYN_PICKING_Impl.getCurrentTodayPickingNo(CREATE_DATE);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createPicking(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_DYN_PICKING_Service.createPicking() called");

		Boolean rtnData = false;
		String rtnMsg = "";
		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			int splitPos = getString.indexOf("^");
			String dataString = getString.substring(splitPos + 1);

			dataString = "[" + dataString.substring(1, dataString.length()) + "]";

			JSONArray saveList = new JSONArray(dataString);
			rtnData = setPickingObj("INSERT", "PICKING", saveList, getString);

			JsonObject rtnJson = new JsonObject();
			rtnJson.addProperty("Method", "CREATE");
			rtnJson.addProperty("Object", "WMS_DYN_PICKING");

			JsonObject jsonObject = new JsonObject();

			jsonObject = setTableEventResultHandler(rtnData, "");

			return Response.status(200).entity(jsonObject.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	// GWANGHYUN KIM 8/20/2018
	@GET
	@Path("/GIBarcode")
	public Response getPOItem(
            @QueryParam("CLIENT_ID"    ) String CLIENT_ID    ,
            @QueryParam("COMPANY_CODE" ) String COMPANY_CODE ,
			@QueryParam("SO_NO"        ) String SO_NO        ,
			@QueryParam("SO_ITEM_NO"   ) String SO_ITEM_NO   ,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE,
			@QueryParam("ISSUE_DATE"   ) String ISSUE_DATE   ,
			@QueryParam("GET_BARCODE"  ) String GET_BARCODE

			)
	{

		SO_ITEM_NO  = StringUtil.getEmptyNullPrevString(SO_ITEM_NO );
		GET_BARCODE = StringUtil.getEmptyNullPrevString(GET_BARCODE);

		String result = "";
		//int rSO_ITEM_NO = Integer.parseInt(SO_ITEM_NO);
		try {
			WMS_DYN_PICKING_Impl mWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			List<WMS_DYN_PICKING> dataList = mWMS_DYN_PICKING_Impl.getSOItem(CLIENT_ID,COMPANY_CODE,SO_NO, MATERIAL_CODE, SO_ITEM_NO, ISSUE_DATE, GET_BARCODE);

			LOGGER.info("getPOItem called ISSUE_DATE param = " + ISSUE_DATE);

			Gson gson = new Gson();
			if (dataList != null)
			{
				HashMap<String,Object> dataMap = new HashMap();
				dataMap.put("saveList", dataList);
				mWMS_DYN_PICKING_Impl.updateSOBarCode(dataMap);


				result = gson.toJson(dataList);
//				LOGGER.info("getPOItem called result =  " + result.toString());

				return Response.status(200).entity(result).build();
			}else {
				JsonObject rtnJson = new JsonObject();
				rtnJson.addProperty("MSG", "Not existing entered data. Please check you SO number or Item number or Part No.");
				return Response.status(500).entity(rtnJson.toString()).build();
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	// CHAN 01/12/2019
	@GET
	@Path("/GIBarcodeAllItems")
	public Response getPOItems(
            @QueryParam("CLIENT_ID"    ) String CLIENT_ID   ,
            @QueryParam("COMPANY_CODE" ) String COMPANY_CODE,
			@QueryParam("SO_NO"        ) String SO_NO       ,
			@QueryParam("SO_ITEM_NO"   ) String SO_ITEM_NO  ,
			@QueryParam("MATERIAL_CODE") String MATERIAL_CODE,
			@QueryParam("ISSUE_DATE") 	 String ISSUE_DATE
			)
	{

		String result = "";
		//int rSO_ITEM_NO = Integer.parseInt(SO_ITEM_NO);
		try {
			WMS_DYN_PICKING_Impl mWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			//WMS_DYN_PICKING oWMS_DYN_PICKING = mWMS_DYN_PICKING_Impl.getSOItem(CLIENT_ID,COMPANY_CODE,SO_NO, MATERIAL_CODE, SO_ITEM_NO, ISSUE_DATE);
			List<HashMap<String,Object>> soItemNO = mWMS_DYN_PICKING_Impl.getSoItemNo(CLIENT_ID, COMPANY_CODE, SO_NO);

			List <WMS_DYN_PICKING> oWMS_DYN_PICKING_HASHMAP = new ArrayList<WMS_DYN_PICKING>();

			LOGGER.info("getPOItem called ISSUE_DATE param = " + ISSUE_DATE);
			Gson gson = new Gson();

			if (soItemNO != null) {

				LOGGER.info("soItemNO :" + soItemNO.toString());
				for(int index = 0;index < soItemNO.size();index++)
				{
					LOGGER.info("SO_ITEM_NO " + index +  "= " + soItemNO.get(index));
					LOGGER.info(soItemNO.get(index));
					LOGGER.info(soItemNO.get(index).values());
//					LOGGER.info(soItemNO.get(index).getKey());
					LOGGER.info(soItemNO.get(index).get("SO_ITEM_NO") );
					LOGGER.info(soItemNO.get(index).get("MATERIAL_CODE") );
//					LOGGER.info(soItemNO.get(index).get(SO_ITEM_NO) );
					LOGGER.info(soItemNO.get(index).keySet());
					LOGGER.info(soItemNO.get(index));

					oWMS_DYN_PICKING_HASHMAP.add(
							mWMS_DYN_PICKING_Impl.getSOItem(CLIENT_ID,COMPANY_CODE,SO_NO, soItemNO.get(index).get("MATERIAL_CODE").toString(), soItemNO.get(index).get("SO_ITEM_NO").toString(), ISSUE_DATE, "N").get(0)
							);
					LOGGER.info("/=======================================================================================================");
					LOGGER.info("oWMS_DYN_PICKING_HASHMAP " + index +  "= " + gson.toJson(oWMS_DYN_PICKING_HASHMAP.get(index)).toString());
					LOGGER.info("=======================================================================================================/");
				}

				result = gson.toJson(oWMS_DYN_PICKING_HASHMAP);

				LOGGER.info("getPOItem called result =  " + result.toString());

				return Response.status(200).entity(result).build();
			}else {
				JsonObject rtnJson = new JsonObject();
				rtnJson.addProperty("MSG", "Not existing entered data. Please check you SO number or Item number or Part No.");
				return Response.status(500).entity(rtnJson.toString()).build();
			}
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


	// GWANGHYUN KIM 4/8/2018
	@GET
	@Path("/PDA/CheckPL_NO")
	public Response getPickingNumber(
			@QueryParam("CLIENT_ID"    ) String CLIENT_ID   ,
            @QueryParam("COMPANY_CODE" ) String COMPANY_CODE,
            @QueryParam("PICKING_NO") String pickingNo
			) {
		LOGGER.info("WMS_DYN_PICKING_Service.getPickingNumber() called");

		String result = "";

		try {
			WMS_DYN_PICKING mWMS_DYN_PICKING = new WMS_DYN_PICKING();
			WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();

			mWMS_DYN_PICKING.setCLIENT_ID(CLIENT_ID);
			mWMS_DYN_PICKING.setCOMPANY_CODE(COMPANY_CODE);
			mWMS_DYN_PICKING.setPL_NO(pickingNo);
			//mWMS_DYN_PICKING.setSTATUS("CS");

			Boolean checkPicking = iWMS_DYN_PICKING_Impl.chkExistsPicking(
					mWMS_DYN_PICKING.getCLIENT_ID(), mWMS_DYN_PICKING.getCOMPANY_CODE(), mWMS_DYN_PICKING.getPL_NO());
			Boolean chkPikcingScanStatus = iWMS_DYN_PICKING_Impl.chkPickingScanStatus(
					mWMS_DYN_PICKING.getCLIENT_ID(), mWMS_DYN_PICKING.getCOMPANY_CODE(), mWMS_DYN_PICKING.getPL_NO());
			Gson gson = new Gson();
			JsonObject rtnJson = new JsonObject();
			if(chkPikcingScanStatus) {
				if (checkPicking) {
					rtnJson.addProperty("ret", Integer.valueOf(0));
					rtnJson.addProperty("msg", "STATUS is successfully updated");
					result = gson.toJson(rtnJson);
					// if () {
					// iWMS_DYN_PICKING_Impl.scanUpdatePicking(mWMS_DYN_PICKING);
					// }
				} else {
					rtnJson.addProperty("ret", Integer.valueOf(1));
					rtnJson.addProperty("msg", "Picking Number does not exist : " + mWMS_DYN_PICKING.getPL_NO());
					result = gson.toJson(rtnJson);
				}
			} else {
				rtnJson.addProperty("ret", Integer.valueOf(1));
				rtnJson.addProperty("msg",
						"Eevery Material are scanned in the pikcing list : " + mWMS_DYN_PICKING.getPL_NO());
				result = gson.toJson(rtnJson);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	// GWANGHYUN KIM 4/8/2018
	@POST
	@Path("PDA/GI")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response scanUpdatePicking(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_DYN_PICKING_Service.scanUpdatePicking() called");

		try {
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONObject jsonObject = new JSONObject(getString);

			WMS_DYN_PICKING mWMS_DYN_PICKING = new WMS_DYN_PICKING(); // Object from PDA
			WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();

			String CLIENT_ID = jsonObject.has("CLIENT_ID") ? jsonObject.getString("CLIENT_ID") : "";
			String COMPANY_CODE = jsonObject.has("COMPANY_CODE") ? jsonObject.getString("COMPANY_CODE") : "";
			String BARCODE_NO = jsonObject.has("BARCODE_NO") ? jsonObject.getString("BARCODE_NO") : "";
			// String STATUS = jsonObject.has("STATUS" ) ? jsonObject.getString
			// ("STATUS" ) : "";
			String STORAGE_LOC = jsonObject.has("STORAGE_LOC") ? jsonObject.getString("STORAGE_LOC") : "";
			String SO_NO       = jsonObject.has("SO_NO") ? jsonObject.getString("SO_NO"):"";
			int SO_ITEM_NO     = jsonObject.has("SO_ITEM_NO") ? jsonObject.getInt("SO_ITEM_NO") : 0;


			String PL_NO = jsonObject.has("PL_NO") ? jsonObject.getString("PL_NO") : "";
			String MATERIAL_CODE = jsonObject.has("MATERIAL_CODE") ? jsonObject.getString("MATERIAL_CODE") : "";
			String SCAN_QTY = jsonObject.has("SCAN_QTY") ? jsonObject.getString("SCAN_QTY") : "";

			mWMS_DYN_PICKING.setCLIENT_ID(CLIENT_ID);
			mWMS_DYN_PICKING.setCOMPANY_CODE(COMPANY_CODE);
			mWMS_DYN_PICKING.setBARCODE_NO(BARCODE_NO);
			mWMS_DYN_PICKING.setSTORAGE_LOC(STORAGE_LOC);

			mWMS_DYN_PICKING.setSO_NO(SO_NO);
			mWMS_DYN_PICKING.setSO_ITEM_NO(SO_ITEM_NO);
			mWMS_DYN_PICKING.setPL_NO(PL_NO);
			mWMS_DYN_PICKING.setMATERIAL_CODE(MATERIAL_CODE);
			mWMS_DYN_PICKING.setSCAN_QTY(Float.parseFloat(SCAN_QTY));

			Boolean chkExistsPickingByBarcode_NO = iWMS_DYN_PICKING_Impl
					.chkExistsPickingByBarcode_NO(
							mWMS_DYN_PICKING.getCLIENT_ID(),
							mWMS_DYN_PICKING.getCOMPANY_CODE(),
							mWMS_DYN_PICKING.getBARCODE_NO());

			Boolean chkExistsPickingMaterial = iWMS_DYN_PICKING_Impl
					.chkExistsPickingMaterial(
							mWMS_DYN_PICKING.getCLIENT_ID(),
							mWMS_DYN_PICKING.getCOMPANY_CODE(),
							mWMS_DYN_PICKING.getPL_NO(),
							mWMS_DYN_PICKING.getMATERIAL_CODE());

			Boolean chkExistsItemNo = iWMS_DYN_PICKING_Impl.chkExistsItemNo(
					mWMS_DYN_PICKING.getCLIENT_ID(),
					mWMS_DYN_PICKING.getCOMPANY_CODE(),
					mWMS_DYN_PICKING.getPL_NO(),
					mWMS_DYN_PICKING.getMATERIAL_CODE(),
					mWMS_DYN_PICKING.getSO_ITEM_NO(),
					mWMS_DYN_PICKING.getSO_NO()
					);

			Boolean chkItemScanStatus = iWMS_DYN_PICKING_Impl.chkItemScanStatus(
					mWMS_DYN_PICKING.getCLIENT_ID(),
					mWMS_DYN_PICKING.getCOMPANY_CODE(),
					PL_NO,
					MATERIAL_CODE,
					SO_ITEM_NO,
					SO_NO);

			Boolean chkPikcingScanStatus = iWMS_DYN_PICKING_Impl.chkPickingScanStatus(
					mWMS_DYN_PICKING.getCLIENT_ID(),
					mWMS_DYN_PICKING.getCOMPANY_CODE(),
					PL_NO);

			JsonObject rtnJson = new JsonObject();

			if(chkPikcingScanStatus) {
				if (chkExistsPickingMaterial) { // Check Material
					if (!chkExistsPickingByBarcode_NO) { // Check duplicate bar code
						if(!chkItemScanStatus) {
							if(chkExistsItemNo) {
								int orderQty = (int)iWMS_DYN_PICKING_Impl.getOrderQty(
										mWMS_DYN_PICKING.getCLIENT_ID(),
										mWMS_DYN_PICKING.getCOMPANY_CODE(),
										PL_NO,
										MATERIAL_CODE,
										SO_ITEM_NO,
										SO_NO);
								int scanQty = (int)iWMS_DYN_PICKING_Impl.getScanQty(
										mWMS_DYN_PICKING.getCLIENT_ID(),
										mWMS_DYN_PICKING.getCOMPANY_CODE(),
										PL_NO,
										MATERIAL_CODE,
										SO_ITEM_NO,
										SO_NO
										);
								LOGGER.info("pickingCnt :" + orderQty + "scanEndScnt :" + scanQty);

								if(scanQty == 0) {
									//update
									if((int)Float.parseFloat(SCAN_QTY) == orderQty || (int)Float.parseFloat(SCAN_QTY) > orderQty) {
										mWMS_DYN_PICKING.setSTATUS("CS");
									}else {
										mWMS_DYN_PICKING.setSTATUS("PS");
									}

									if (iWMS_DYN_PICKING_Impl.scanUpdatePicking(mWMS_DYN_PICKING)) {
										rtnJson.addProperty("ret", Integer.valueOf(0));
										rtnJson.addProperty("msg", "Successfully Scanned : " + mWMS_DYN_PICKING.getBARCODE_NO());
									}

								} else {
									//insert
									mWMS_DYN_PICKING = iWMS_DYN_PICKING_Impl.getCurrentPickingData(mWMS_DYN_PICKING);
									LOGGER.info("get current picking data" + mWMS_DYN_PICKING);

									//float iScanQty = mWMS_DYN_PICKING.getSCAN_QTY() + Float.parseFloat(SCAN_QTY);
									LOGGER.info("DOCUMENT_CODE");
									int docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(mWMS_DYN_PICKING.getCLIENT_ID(), COMPANY_CODE, "GI", "DOC_NO", 1));
									LOGGER.info("TOTAL SCAN" + mWMS_DYN_PICKING.getTOTAL_SCAN());
									float totalScan = mWMS_DYN_PICKING.getTOTAL_SCAN() + Float.parseFloat(SCAN_QTY);
									LOGGER.info("DOCUMENT_CODE END" + docNo);

									mWMS_DYN_PICKING.setWSID(null);
									mWMS_DYN_PICKING.setBARCODE_NO(BARCODE_NO);
									mWMS_DYN_PICKING.setSCAN_QTY(Float.parseFloat(SCAN_QTY));
									mWMS_DYN_PICKING.setDOC_NO(docNo);
									if((int)totalScan == orderQty || (int)totalScan > orderQty) {
										mWMS_DYN_PICKING.setSTATUS("CS");
									}
									LOGGER.info("get update picking data" + mWMS_DYN_PICKING);

									Boolean updateScanQty = iWMS_DYN_PICKING_Impl.updateScanQty(mWMS_DYN_PICKING);
									LOGGER.info("get update SCAN Qty" + updateScanQty);

									if(updateScanQty) {
										Boolean insertData = iWMS_DYN_PICKING_Impl.insertPickingPDA(mWMS_DYN_PICKING);
										LOGGER.info("get insert picking data" + insertData);

										if(insertData) {
											rtnJson.addProperty("ret", Integer.valueOf(0));
											rtnJson.addProperty("msg", "Successfully Scanned : " + mWMS_DYN_PICKING.getBARCODE_NO());
										}
									}
								}

							} else {
								rtnJson.addProperty("ret", Integer.valueOf(1));
								rtnJson.addProperty("msg", "Item " + mWMS_DYN_PICKING.getSO_ITEM_NO() + " of : "
										+ mWMS_DYN_PICKING.getMATERIAL_CODE() + " is not in the picking list");
							}
						} else {
							rtnJson.addProperty("ret", Integer.valueOf(1));
							rtnJson.addProperty("msg", "Scan Finished: ItemNo[" + mWMS_DYN_PICKING.getSO_ITEM_NO()
									+ "] Material Code: [" + mWMS_DYN_PICKING.getMATERIAL_CODE() + "]");
						}
					} else {
						rtnJson.addProperty("ret", Integer.valueOf(1));
						rtnJson.addProperty("msg", "Already Scanned Label : " + mWMS_DYN_PICKING.getBARCODE_NO());
					}
				} else {
					rtnJson.addProperty("ret", Integer.valueOf(1));
					rtnJson.addProperty("msg", "Not in the picking list : " + mWMS_DYN_PICKING.getMATERIAL_CODE());
				}
			} else {
				rtnJson.addProperty("ret", Integer.valueOf(1));
				rtnJson.addProperty("msg",
						"Eevery Material are scanned in the pikcing list : " + mWMS_DYN_PICKING.getPL_NO());
			}

			return Response.status(200).entity(rtnJson.toString()).build();
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


    @GET
    @Path("/PickingStatus/List")
    public Response getPickingStatusList(
            @QueryParam("CLIENT_ID"        ) String CLIENT_ID        ,
            @QueryParam("COMPANY_CODE"     ) String COMPANY_CODE     ,
            @QueryParam("PL_NO"            ) String PL_NO            ,
            @QueryParam("CUST_CODE"        ) String CUST_CODE        ,
            @QueryParam("STATUS"           ) String STATUS           ,
            @QueryParam("FROM_PICKING_DATE") String FROM_PICKING_DATE,
            @QueryParam("TO_PICKING_DATE"  ) String TO_PICKING_DATE

            )
    {
        LOGGER.info("WMS_DYN_PICKING_Service.getPickingStatusList() called");
        LOGGER.info("CLIENT_ID:"        + CLIENT_ID        +",COMPANY_CODE:"   + COMPANY_CODE   +",PL_NO:" +PL_NO+",CUST_CODE:"+CUST_CODE);
        LOGGER.info("FROM_PICKING_DATE:"+ FROM_PICKING_DATE+",TO_PICKING_DATE:"+ TO_PICKING_DATE+",STATUS:"+STATUS);

        String result = "";
        try
        {
            CLIENT_ID         = StringUtil.getEmptyNullPrevString(CLIENT_ID        ) ;
        	COMPANY_CODE      = StringUtil.getEmptyNullPrevString(COMPANY_CODE     ) ;
            PL_NO             = StringUtil.getEmptyNullPrevString(PL_NO            ) ;
            CUST_CODE         = StringUtil.getEmptyNullPrevString(CUST_CODE        ) ;
            STATUS            = StringUtil.getEmptyNullPrevString(STATUS           ) ;
            FROM_PICKING_DATE = StringUtil.getEmptyNullPrevString(FROM_PICKING_DATE) ;
            TO_PICKING_DATE   = StringUtil.getEmptyNullPrevString(TO_PICKING_DATE  ) ;


            WMS_DYN_PICKING_Impl mWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
            List<WMS_DYN_PICKING> allList = mWMS_DYN_PICKING_Impl.getPickingStatusList(
                    CLIENT_ID   ,
            		COMPANY_CODE,
            		PL_NO       ,
                    CUST_CODE   ,
                    STATUS      ,
                    FROM_PICKING_DATE, TO_PICKING_DATE
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


    @POST
    @Path("/PickingStatus/Confirm/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response confirmPackingStatus(InputStream inputStream,
            @PathParam("userId"     ) String userId    ,
            @PathParam("clientId"   ) String clientId  ,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PICKING_Service.confirmPackingStatus() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> dataMap  = StringUtil.getParamMap(inputStream);
        	String dataItem   = (String)dataMap.get("dataItem") ;
        	 System.out.println("dataItem:"+dataItem);
        	JSONArray jsArray = new JSONArray(dataItem);


            WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
			String doNo = iWMS_MST_CODERULE_Impl.getDocNo(clientId, companyCode, "DO", "DO_NO", 1);
            List<WMS_DYN_PICKING> dataList = setStatusObjList(jsArray, "UPDATE", userId, clientId, companyCode, doNo);

            dataMap.put("dataList",dataList);
            dataMap.put("DO_NO"   ,doNo);

            WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();

            rtnBoolResult = iWMS_DYN_PICKING_Impl.confirmPackingStatus(dataMap);
            if(rtnBoolResult)
            {
                WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("GI", jsArray, userId, clientId, companyCode);
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

	public boolean setPickingObj(String evtHandler, String inputType, JSONArray saveList, String getString)
			throws Exception {
		Boolean result = true;
		try {
			HashMap<String, Object> paramMap = StringUtil.getParamMap(getString);

			String CLIENT_ID = (String) paramMap.get("CLIENT_ID");
			String COMPANY_CODE = (String) paramMap.get("COMPANY_CODE");
			String PL_NO = (String) paramMap.get("PL_NO");
			String USER_ID = (String) paramMap.get("USER_ID");
			String UPDATE_USER = (String) paramMap.get("UPDATE_USER");
			String UPDATE_DATE = (String) paramMap.get("UPDATE_DATE");
			String DELETE_USER = (String) paramMap.get("DELETE_USER");
			String DELETE_DATE = (String) paramMap.get("DELETE_DATE");
			String DEL_FLAG = (String) paramMap.get("DEL_FLAG");

			WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
			WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();

			List<WMS_DYN_PICKING> dataList = new ArrayList<WMS_DYN_PICKING>();

			for (int i = 0; i < saveList.length(); i++) {
				// WSID
				Integer WSID = saveList.getJSONObject(i).has("WSID") ? saveList.getJSONObject(i).getInt("WSID") : 0;

				// SALES_ORG_CODE
				String SALES_ORG_CODE = saveList.getJSONObject(i).has("SALES_ORG_CODE")
						? saveList.getJSONObject(i).getString("SALES_ORG_CODE") : "";

				// SO_NO
				String SO_NO = saveList.getJSONObject(i).has("SO_NO") ? saveList.getJSONObject(i).getString("SO_NO")
						: "";

				// SO_ITEM_NO
				Integer SO_ITEM_NO = saveList.getJSONObject(i).has("SO_ITEM_NO")
						? saveList.getJSONObject(i).getInt("SO_ITEM_NO") : 0;

				// CUST_CODE
				String CUST_CODE = saveList.getJSONObject(i).has("CUST_CODE")
						? saveList.getJSONObject(i).getString("CUST_CODE") : "";

				// DELIVERY_DATE
				String DELIVERY_DATE = saveList.getJSONObject(i).has("DELIVERY_DATE")
						? saveList.getJSONObject(i).getString("DELIVERY_DATE") : "";

				// MATERIAL_CODE
				String MATERIAL_CODE = saveList.getJSONObject(i).has("MATERIAL_CODE")
						? saveList.getJSONObject(i).getString("MATERIAL_CODE") : "";
				// ORDER_QTY
				Double ORDER_QTY = saveList.getJSONObject(i).has("ORDER_QTY")
						? saveList.getJSONObject(i).getDouble("ORDER_QTY") : 0;
				// UNIT
				String UNIT = saveList.getJSONObject(i).has("UNIT") ? saveList.getJSONObject(i).getString("UNIT") : "";
				// CONV_QTY
				Double CONV_QTY = saveList.getJSONObject(i).has("CONV_QTY")
						? saveList.getJSONObject(i).getDouble("CONV_QTY") : 0;
				// CONV_UNIT
				String CONV_UNIT = saveList.getJSONObject(i).has("CONV_UNIT")
						? saveList.getJSONObject(i).getString("CONV_UNIT") : "";
				// HEADER_FLAG
				String HEADER_FLAG;

				int docNo = saveList.getJSONObject(i).has("DOC_NO") ? saveList.getJSONObject(i).getInt("DOC_NO") : 0;

				if (i == 0) {
					HEADER_FLAG = "Y";
				} else {
					HEADER_FLAG = "N";
				}

				if (docNo == 0) {
					docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID, COMPANY_CODE, "GI", "DOC_NO", 1));
				}

				WMS_DYN_PICKING oWMS_DYN_PICKING = new WMS_DYN_PICKING();

				oWMS_DYN_PICKING.setCLIENT_ID(CLIENT_ID);
				oWMS_DYN_PICKING.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_DYN_PICKING.setPL_NO(PL_NO);
				oWMS_DYN_PICKING.setCREATE_USER(USER_ID);
				oWMS_DYN_PICKING.setUPDATE_USER(UPDATE_USER);
				oWMS_DYN_PICKING.setUPDATE_DATE(UPDATE_DATE);
				oWMS_DYN_PICKING.setDELETE_USER(DELETE_USER);
				oWMS_DYN_PICKING.setDELETE_DATE(DELETE_DATE);
				oWMS_DYN_PICKING.setDEL_FLAG(DEL_FLAG);
				oWMS_DYN_PICKING.setDOC_NO(docNo);

				oWMS_DYN_PICKING.setSALES_ORG_CODE(SALES_ORG_CODE);
				oWMS_DYN_PICKING.setSO_NO(SO_NO);
				oWMS_DYN_PICKING.setSO_ITEM_NO(SO_ITEM_NO);
				oWMS_DYN_PICKING.setCUST_CODE(CUST_CODE);
				oWMS_DYN_PICKING.setDELIVERY_DATE(DELIVERY_DATE);
				oWMS_DYN_PICKING.setMATERIAL_CODE(MATERIAL_CODE);
				oWMS_DYN_PICKING.setORDER_QTY(new Float(ORDER_QTY));
				oWMS_DYN_PICKING.setUNIT(UNIT);
				oWMS_DYN_PICKING.setCONV_QTY(new Float(CONV_QTY));
				oWMS_DYN_PICKING.setCONV_UNIT(CONV_UNIT);
				oWMS_DYN_PICKING.setHEADER_FLAG(HEADER_FLAG);
				oWMS_DYN_PICKING.setWSID(WSID);

				dataList.add(oWMS_DYN_PICKING);
			}

			if (dataList.size() > 0) {
				HashMap<String, Object> dataMap = new HashMap<String, Object>();
				dataMap.put("dataList", dataList);
				LOGGER.info(dataMap);
				LOGGER.info(dataList);
				if (evtHandler.equals("INSERT")) {
					result = iWMS_DYN_PICKING_Impl.insertPicking(dataMap);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			result = false;
		}
		return result;
	}

    @POST
    @Path("/PickingStatus/updateEaccountNo")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateSgiEaccountNo(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_PICKING_Service.updateSgiEaccountNo() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String saveData    = (String)paramMap.get("saveData") ;
        	ArrayList saveList = StringUtil.getJsonToHashMapList(saveData);
        	paramMap.put("saveList", saveList);

        	WMS_DYN_PICKING_Impl iWMS_DYN_PICKING_Impl = new WMS_DYN_PICKING_Impl();
        	rtnBoolResult = iWMS_DYN_PICKING_Impl.updateSgiEaccountNo(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }


	public List<WMS_DYN_PICKING> setStatusObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String companyCode, String doNo) throws Exception
	{
		List<WMS_DYN_PICKING> dataList = new ArrayList<WMS_DYN_PICKING>();

		try
		{
			for (int i=0; i<jsArray.length(); i++)
			{

				Integer WSID   	      = jsArray.getJSONObject(i).has("WSID"         ) ? jsArray.getJSONObject(i).getInt("WSID"   	      ) : 0;
				Double  SCAN_QTY      = jsArray.getJSONObject(i).has("SCAN_QTY"     ) ? jsArray.getJSONObject(i).getDouble("SCAN_QTY"     ) : 0;
				String  SCAN_UNIT     = jsArray.getJSONObject(i).has("SCAN_UNIT"    ) ? jsArray.getJSONObject(i).getString("SCAN_UNIT"    ) : "";
				String  MATERIAL_CODE = jsArray.getJSONObject(i).has("MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MATERIAL_CODE") : "";
				String  PLANT_CODE    = jsArray.getJSONObject(i).has("PLANT_CODE"   ) ? jsArray.getJSONObject(i).getString("PLANT_CODE"   ) : "";
				String  STORAGE_LOC   = jsArray.getJSONObject(i).has("STORAGE_LOC"  ) ? jsArray.getJSONObject(i).getString("STORAGE_LOC"  ) : "";
				int     DOC_NO        = jsArray.getJSONObject(i).has("DOC_NO"       ) ? jsArray.getJSONObject(i).getInt("DOC_NO"          ) : 0;

				String MOVEMENT_TYPE    = "600";
				String CURR_STORAGE_LOC = STORAGE_LOC;
				String MOVE_STORAGE_LOC = STORAGE_LOC;
				Double GI_QTY           = SCAN_QTY*-1;
				String UNIT             = SCAN_UNIT;

				WMS_DYN_PICKING oWMS_DYN_PICKING = new WMS_DYN_PICKING();

				oWMS_DYN_PICKING.setCOMPANY_CODE(companyCode	);
				oWMS_DYN_PICKING.setWSID(WSID					);
				oWMS_DYN_PICKING.setSCAN_QTY(new Float(SCAN_QTY));
				oWMS_DYN_PICKING.setUPDATE_USER(userId			);
				oWMS_DYN_PICKING.setDO_NO(doNo					);
				oWMS_DYN_PICKING.setCONFIRM_USER(userId         );
				oWMS_DYN_PICKING.setMOVEMENT_TYPE(MOVEMENT_TYPE );
				oWMS_DYN_PICKING.setSTORAGE_LOC(STORAGE_LOC     );

				dataList.add(oWMS_DYN_PICKING);

				jsArray.getJSONObject(i).put("MOVEMENT_TYPE"   , MOVEMENT_TYPE   );
				jsArray.getJSONObject(i).put("CURR_STORAGE_LOC", CURR_STORAGE_LOC);
				jsArray.getJSONObject(i).put("CURR_QTY"        , GI_QTY          );

				jsArray.getJSONObject(i).put("MOVE_STORAGE_LOC", MOVE_STORAGE_LOC);
				jsArray.getJSONObject(i).put("MOVE_QTY"        , GI_QTY          );
				jsArray.getJSONObject(i).put("UNIT"            , UNIT            );
			}
		}
		catch(Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
		}
		return dataList;
	}


	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData) {
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue()) {
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		} else {
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}
}
