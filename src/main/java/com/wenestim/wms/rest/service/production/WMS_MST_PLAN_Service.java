/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.production.WMS_MST_PLAN;
import com.wenestim.wms.rest.impl.production.WMS_MST_PLAN_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Production/PlanControl")
public class WMS_MST_PLAN_Service
{

    private static final Logger LOGGER = Logger.getLogger(WMS_MST_PLAN_Service.class);

    @POST
    @Path("List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getPlanVerList(InputStream inputStream)
    {

        LOGGER.info("WMS_MST_PLAN_Service.getPlanVerList() called");
        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_MST_PLAN_Impl iWMS_MST_PLAN_Impl = new WMS_MST_PLAN_Impl();
            List<WMS_MST_PLAN> dataList = iWMS_MST_PLAN_Impl.getPlanVerList(paramMap);

            Gson gson = new Gson();
            if (dataList != null)
            {
                result = gson.toJson(dataList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Create")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response createPlan(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_PLAN_Service.createPlan() called");
        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_MST_PLAN_Impl iWMS_MST_PLAN_Impl = new WMS_MST_PLAN_Impl();
        	String planVer = iWMS_MST_PLAN_Impl.createPlan(paramMap);
        	if( !planVer.equals(""))
        	{
        		rtnBoolResult = true;
        	}

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, planVer);

            return Response.status(200).entity(rtnJson.toString()).build();

        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Confirm")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response confirmPlan(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_PLAN_Service.confirmPlan() called");
        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_MST_PLAN_Impl iWMS_MST_PLAN_Impl = new WMS_MST_PLAN_Impl();
        	rtnBoolResult = iWMS_MST_PLAN_Impl.confirmPlan(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();

        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePlan(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_PLAN_Service.updatePlan() called");
        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String saveData    = (String)paramMap.get("saveData");
        	ArrayList saveList = StringUtil.getJsonToHashMapList(saveData);
        	paramMap.put("saveList", saveList);

        	WMS_MST_PLAN_Impl iWMS_MST_PLAN_Impl = new WMS_MST_PLAN_Impl();
        	rtnBoolResult = iWMS_MST_PLAN_Impl.updatePlan(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();

        } catch (Exception e) {
            LOGGER.error("Exception:" + e.getMessage());
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
