/**
 *
 */
package com.wenestim.wms.rest.service.inventory;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.wenestim.wms.rest.entity.inventory.WMS_DYN_LEDGER;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_LEDGER_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Inventory/Ledger")
public class WMS_DYN_LEDGER_Service {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_LEDGER_Service.class);


    @POST
    @Path("/MaterialList")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getMaterialLedgerList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_LEDGER_Service.getMaterialLedgerList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_DYN_LEDGER_Impl mWMS_DYN_LEDGER_Impl = new WMS_DYN_LEDGER_Impl();
        	List<WMS_DYN_LEDGER> allList = mWMS_DYN_LEDGER_Impl.getMaterialLedgerList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }
}
