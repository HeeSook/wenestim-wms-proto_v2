/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.impl.production.WMS_DYN_PRODUCTION_ORDER_Impl;
import com.wenestim.wms.rest.util.StringUtil;
/**
 * @author Andrew
 *
 */
@Path("/Production/ProductionOrder")
public class WMS_DYN_PRODUCTION_ORDER_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_DYN_PRODUCTION_ORDER_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getProductionOrderList(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_PRODUCTION_ORDER_Service.getProductionOrderList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	WMS_DYN_PRODUCTION_ORDER_Impl mWMS_DYN_PRODUCTION_ORDER_Impl = new WMS_DYN_PRODUCTION_ORDER_Impl();
        	List<HashMap<String,Object>> allList = mWMS_DYN_PRODUCTION_ORDER_Impl.getProductionOrderList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateProductionOrder(InputStream inputStream)
    {
        LOGGER.info("WMS_DYN_PRODUCTION_ORDER_Service.updateProductionOrder() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String saveData    = (String)paramMap.get("saveData") ;
        	ArrayList saveList = StringUtil.getJsonToHashMapList(saveData);
        	paramMap.put("saveList", saveList);

        	WMS_DYN_PRODUCTION_ORDER_Impl mWMS_DYN_PRODUCTION_ORDER_Impl = new WMS_DYN_PRODUCTION_ORDER_Impl();
        	rtnBoolResult = mWMS_DYN_PRODUCTION_ORDER_Impl.updateProductionOrder(paramMap);

//        	System.out.println("sendType:"+sendType);
        	if(rtnBoolResult)
        	{
        		String userId      = (String)paramMap.get("USER_ID"     );
        		String clientId    = (String)paramMap.get("CLIENT_ID"   );
        		String companyCode = (String)paramMap.get("COMPANY_CODE");
        		String postDate    = (String)paramMap.get("POST_DATE"   );

        		JSONArray jsonArray = setDataObjList("UPDATE",saveData, postDate);

        		WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("PROD_ORDER", jsonArray, userId, clientId, companyCode);
        	}

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public JSONArray setDataObjList(String evtHandler,String JSONString, String postDate) throws Exception
    {
    	JSONArray jsArray = new JSONArray(JSONString);

    	try
        {
            for (int i=0; i<jsArray.length(); i++)
            {
            	String UNIT        = jsArray.getJSONObject(i).has("CONV_UNIT") ? jsArray.getJSONObject(i).getString("CONV_UNIT") : "";
            	String STORAGE_LOC = jsArray.getJSONObject(i).has("STORAGE_LOC") ? jsArray.getJSONObject(i).getString("STORAGE_LOC") : "";
            	Double GR_QTY      = jsArray.getJSONObject(i).has("GR_QTY")      ? jsArray.getJSONObject(i).getDouble("GR_QTY") : 0;

            	jsArray.getJSONObject(i).put("POST_DATE"       , postDate   );
            	jsArray.getJSONObject(i).put("CURR_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("CURR_QTY"        , GR_QTY     );
				jsArray.getJSONObject(i).put("MOVE_STORAGE_LOC", STORAGE_LOC);
				jsArray.getJSONObject(i).put("MOVE_QTY"        , GR_QTY     );
				jsArray.getJSONObject(i).put("UNIT"            , UNIT       );
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        return jsArray;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
