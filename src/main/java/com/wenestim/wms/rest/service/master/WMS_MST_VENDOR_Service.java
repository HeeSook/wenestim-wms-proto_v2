/**
 *
 */
package com.wenestim.wms.rest.service.master;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.master.WMS_MST_VENDOR;
import com.wenestim.wms.rest.impl.master.WMS_MST_VENDOR_Impl;
import com.wenestim.wms.rest.util.StringUtil;



/**
 * @author Andrew
 *
 */
@Path("/Master/Vendor")
public class WMS_MST_VENDOR_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_VENDOR_Service.class);

	@GET
	@Path("/All")
	public Response getAllVendor() {
		LOGGER.info("WMS_MST_VENDOR_Service.getAllVendor() called");

		String result = "";
		try {
			WMS_MST_VENDOR_Impl mWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
			List<WMS_MST_VENDOR> allList = mWMS_MST_VENDOR_Impl.getAllVendor();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/List")
	public Response getVendorList(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,@QueryParam("VENDOR_CODE") String VENDOR_CODE,
			@QueryParam("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE,@QueryParam("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE, @QueryParam("VENDOR_NAME") String VENDOR_NAME, @QueryParam("DEL_FLAG") String DEL_FLAG) {
		LOGGER.info("WMS_MST_VENDOR_Serviec.getVendorList() called");
		LOGGER.debug("[PARAM] CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE:" + COMPANY_CODE + ", VENDOR_CODE:" + VENDOR_CODE
				+ ",PURCHASE_ORG_CODE:" + PURCHASE_ORG_CODE + ",PURCHASE_GRP_CODE:" + PURCHASE_GRP_CODE
				+ ",VENDOR_NAME:" + VENDOR_NAME + ",DEL_FLAG:" + DEL_FLAG);

		String result = "";
		try
		{
			CLIENT_ID              = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE           = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			VENDOR_CODE            = StringUtil.getEmptyNullPrevString(VENDOR_CODE);
			PURCHASE_ORG_CODE      = StringUtil.getEmptyNullPrevString(PURCHASE_ORG_CODE);
			PURCHASE_GRP_CODE      = StringUtil.getEmptyNullPrevString(PURCHASE_GRP_CODE);
			VENDOR_NAME            = StringUtil.getEmptyNullPrevString(VENDOR_NAME);

			DEL_FLAG               = StringUtil.getEmptyNullPrevString(DEL_FLAG);

			WMS_MST_VENDOR_Impl mWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();

			List<WMS_MST_VENDOR> vendorList = mWMS_MST_VENDOR_Impl.getVendorList(CLIENT_ID, COMPANY_CODE, VENDOR_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE, VENDOR_NAME, DEL_FLAG);

			Gson gson = new Gson();
			if (vendorList != null) {
				result = gson.toJson(vendorList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Create/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response insertVendorList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_VENDOR_Serviec.insertVendorList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsVendorListArray = new JSONArray(getString);

			HashMap<String, Object> vendorListMap = new HashMap<String, Object>();
			vendorListMap.put("vendorList", setVendorObjList(jsVendorListArray, "INSERT", userId));

			WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
			rtnBoolResult = iWMS_MST_VENDOR_Impl.insertVendorList(vendorListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Update/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response updateVendorList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_VENDOR_Serviec.updateVendor() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsVendorListArray = new JSONArray(getString);

			HashMap<String, Object> vendorListMap = new HashMap<String, Object>();
			vendorListMap.put("vendorList", setVendorObjList(jsVendorListArray, "UPDATE", userId));

			WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
			rtnBoolResult = iWMS_MST_VENDOR_Impl.updateVendorList(vendorListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/Destroy/{userId}")
	@Consumes({"application/json"})
	@Produces({"application/json;charset=utf-8"})
	public Response deleteVendorList(InputStream inputStream, @PathParam("userId") String userId)
			throws Exception
	{
		LOGGER.info("WMS_MST_VENDOR_Serviec.deleteVendorList() called");

		Boolean rtnBoolResult = Boolean.valueOf(false);
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			JSONArray jsVendorListArray = new JSONArray(getString);

			HashMap<String, Object> vendorListMap = new HashMap<String, Object>();
			vendorListMap.put("vendorList", setVendorObjList(jsVendorListArray, "DELETE", userId));

			WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
			rtnBoolResult = iWMS_MST_VENDOR_Impl.deleteVendorList(vendorListMap);

			JsonObject rtnJson = new JsonObject();
			rtnJson = setTableEventResultHandler(rtnBoolResult, "");

			return Response.status(200).entity(rtnJson.toString()).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/Code/List")
	public Response getVendorCodeByCompany(@QueryParam("CLIENT_ID") String CLIENT_ID, @QueryParam("COMPANY_CODE") String COMPANY_CODE,
			@QueryParam("PURCHASE_ORG_CODE") String PURCHASE_ORG_CODE, @QueryParam("PURCHASE_GRP_CODE") String PURCHASE_GRP_CODE)
	{
		LOGGER.info("WMS_MST_VENDOR_Serviec.getMaterialCodeByCompany() called");
		LOGGER.debug("CLIENT_ID: " + CLIENT_ID + ", COMPANY_CODE : " + COMPANY_CODE +
				     ", PURCHASE_ORG_CODE:"+PURCHASE_ORG_CODE+", PURCHASE_GRP_CODE:"+PURCHASE_GRP_CODE);

		String result = "";
		try
		{
			CLIENT_ID         = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE      = StringUtil.getEmptyNullPrevString(COMPANY_CODE);

			PURCHASE_ORG_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_ORG_CODE);
			PURCHASE_GRP_CODE = StringUtil.getEmptyNullPrevString(PURCHASE_GRP_CODE);

			WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
			List<WMS_MST_VENDOR> dataList = iWMS_MST_VENDOR_Impl.getVendorCodeByCompany(CLIENT_ID, COMPANY_CODE, PURCHASE_ORG_CODE, PURCHASE_GRP_CODE);

			Gson gson = new Gson();
			if (dataList != null) {
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();
		}
		catch (Exception e)
		{
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}


    @POST
    @Path("/ExcelUploadCheck")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getExcelUploadCheckList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_VENDOR_Serviec.getExcelUploadCheckList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList" , dataList);
        	paramMap.put("IS_NUMBER", StringUtil.getRegExpIsPlusNumber());

        	WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
        	List<HashMap<String,Object>> allList = iWMS_MST_VENDOR_Impl.getExcelUploadCheckList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/ExcelUpload")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response excelUpload(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_VENDOR_Serviec.excelUpload() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList dataList = StringUtil.getJsonToHashMapList(dataItem);
        	paramMap.put("dataList", dataList);

        	WMS_MST_VENDOR_Impl iWMS_MST_VENDOR_Impl = new WMS_MST_VENDOR_Impl();
        	rtnBoolResult = iWMS_MST_VENDOR_Impl.excelUpload(paramMap);

        	JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    private List<WMS_MST_VENDOR> setVendorObjList(JSONArray jsArray, String evtHandler, String userId)
			throws Exception
	{
		List<WMS_MST_VENDOR> vendorList = new ArrayList<WMS_MST_VENDOR>();
		try
		{
			for (int i = 0; i < jsArray.length(); i++)
			{
				JSONObject jsVendor = jsArray.getJSONObject(i);
				LOGGER.debug("setVendorObjList() jsonObj:" + jsVendor.toString());

				String CLIENT_ID    = jsVendor.has("CLIENT_ID") ? jsVendor.getString("CLIENT_ID") : "";
				String COMPANY_CODE = jsVendor.has("COMPANY_CODE") ? jsVendor.getString("COMPANY_CODE") : "";
				String VENDOR_CODE = jsVendor.has("VENDOR_CODE") ? jsVendor.getString("VENDOR_CODE") : "";
				String VENDOR_NAME = jsVendor.has("VENDOR_NAME") ? jsVendor.getString("VENDOR_NAME") : "";
				String PURCHASE_ORG_CODE = jsVendor.has("PURCHASE_ORG_CODE") ? jsVendor.getString("PURCHASE_ORG_CODE") : "";
				String PURCHASE_GRP_CODE = jsVendor.has("PURCHASE_GRP_CODE") ? jsVendor.getString("PURCHASE_GRP_CODE") : "";
				String ADDRESS = jsVendor.has("ADDRESS") ? jsVendor.getString("ADDRESS") : "";

				String COUNTRY_CODE = jsVendor.has("COUNTRY_CODE") ? jsVendor.getString("COUNTRY_CODE") : "";
				String PHONE = jsVendor.has("PHONE") ? jsVendor.getString("PHONE") : "";

				String CONTACT_NAME = jsVendor.has("CONTACT_NAME") ? jsVendor.getString("CONTACT_NAME") : "";
				String CONTACT_EMAIL = jsVendor.has("CONTACT_EMAIL") ? jsVendor.getString("CONTACT_EMAIL") : "";
				String CONTACT_PHONE = jsVendor.has("CONTACT_PHONE") ? jsVendor.getString("CONTACT_PHONE") : "";

				String INCOTERMS = jsVendor.has("INCOTERMS") ? jsVendor.getString("INCOTERMS") : "";
				String PAYMENT_TERM = jsVendor.has("PAYMENT_TERM") ? jsVendor.getString("PAYMENT_TERM") : "";
				Float  DELIVERY_TIME = Float.valueOf(jsVendor.has("DELIVERY_TIME") ? Float.parseFloat(jsVendor.getString("DELIVERY_TIME")) : jsVendor.getString("DELIVERY_TIME") == "" ? 0.0F : 0.0F);
				String DEL_FLAG = jsVendor.has("DEL_FLAG") ? jsVendor.getString("DEL_FLAG") : "";

				String CREATE_USER = evtHandler.equals("INSERT") ? userId : "";
				String UPDATE_USER = evtHandler.equals("UPDATE") ? userId : "";
				String DELETE_USER = evtHandler.equals("DELETE") ? userId : "";

				WMS_MST_VENDOR oWMS_MST_VENDOR = new WMS_MST_VENDOR();
				if ((evtHandler.equals("UPDATE")) || (evtHandler.equals("DELETE")))
				{
					Integer WSID = Integer.valueOf(jsVendor.has("WSID") ? Integer.parseInt(jsVendor.getString("WSID")) : 0);
					LOGGER.debug("setVendorObjList() WSID: " + WSID);
					oWMS_MST_VENDOR.setWSID(WSID);
				}
				oWMS_MST_VENDOR.setCLIENT_ID(CLIENT_ID);
				oWMS_MST_VENDOR.setCOMPANY_CODE(COMPANY_CODE);
				oWMS_MST_VENDOR.setVENDOR_CODE(VENDOR_CODE);
				oWMS_MST_VENDOR.setVENDOR_NAME(VENDOR_NAME);
				oWMS_MST_VENDOR.setPURCHASE_ORG_CODE(PURCHASE_ORG_CODE);
				oWMS_MST_VENDOR.setPURCHASE_GRP_CODE(PURCHASE_GRP_CODE);
				oWMS_MST_VENDOR.setADDRESS(ADDRESS);
				oWMS_MST_VENDOR.setCOUNTRY_CODE(COUNTRY_CODE);
				oWMS_MST_VENDOR.setPHONE(PHONE);
				oWMS_MST_VENDOR.setCONTACT_NAME(CONTACT_NAME);
				oWMS_MST_VENDOR.setCONTACT_EMAIL(CONTACT_EMAIL);
				oWMS_MST_VENDOR.setCONTACT_PHONE(CONTACT_PHONE);
				oWMS_MST_VENDOR.setINCOTERMS(INCOTERMS);
				oWMS_MST_VENDOR.setPAYMENT_TERM(PAYMENT_TERM);
				oWMS_MST_VENDOR.setDELIVERY_TIME(DELIVERY_TIME);
				oWMS_MST_VENDOR.setDEL_FLAG(DEL_FLAG);
				oWMS_MST_VENDOR.setCREATE_USER(CREATE_USER);
				oWMS_MST_VENDOR.setUPDATE_USER(UPDATE_USER);
				oWMS_MST_VENDOR.setDELETE_USER(DELETE_USER);

				vendorList.add(oWMS_MST_VENDOR);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("WMS_MST_VENDOR_Serviec.setVendorObjList() Exception:" + e.toString() + " #");
		}
		return vendorList;
	}

	private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
	{
		JsonObject rtnJson = new JsonObject();
		if (rtnBoolResult.booleanValue())
		{
			rtnJson.addProperty("ret", Integer.valueOf(0));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", "confirm");
		}
		else
		{
			String rtnMsg = "could not load data";
			rtnJson.addProperty("ret", Integer.valueOf(1));
			rtnJson.addProperty("data", rtnData);
			rtnJson.addProperty("msg", rtnMsg);
		}
		return rtnJson;
	}

}
