/**
 *
 */
package com.wenestim.wms.rest.service.interfaces;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.JsonObject;
import com.wenestim.wms.rest.impl.interfaces.WMS_IFS_RECEIVE_MES_Impl;
import com.wenestim.wms.rest.util.StringUtil;
/**
 * @author Andrew
 *
 */
@Path("/Interface/Receive/MES")
public class WMS_IFS_RECEIVE_MES_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_IFS_RECEIVE_MES_Service.class);

    @POST
    @Path("/Actual")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response insertMesActualData(InputStream inputStream)
    {
        LOGGER.info("WMS_IFS_RECEIVE_MES_Service.insertMesActualData() called");


        Boolean rtnBoolResult = Boolean.valueOf(false);
    	JsonObject rtnJson    = new JsonObject();
    	int status            = 500;
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
        	String  errMsg = validMesReceiveData("HEADER",paramMap);
        	if(!errMsg.equals(""))
        	{
                rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
            	return Response.status(status).entity(rtnJson.toString()).build();
        	}

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList<HashMap<String,Object>> dataList = StringUtil.getJsonToHashMapList(dataItem);
        	errMsg             = validMesReceiveData("DATA",dataList.get(0));
        	System.out.println("errMsg:"+errMsg);
        	if(!errMsg.equals(""))
        	{

        		rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
            	return Response.status(500).entity(rtnJson.toString()).build();
        	}

        	paramMap.put("dataList", dataList);
    		System.out.println("dataList:"+dataList);
    		WMS_IFS_RECEIVE_MES_Impl iWMS_IFS_RECEIVE_MES_Impl = new WMS_IFS_RECEIVE_MES_Impl();
    		errMsg = iWMS_IFS_RECEIVE_MES_Impl.insertMesActualData(paramMap);

    		if( errMsg.equals(""))
    		{
    			rtnBoolResult = Boolean.valueOf(true);
    			status        = 200;
    		}

            rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
        	return Response.status(status).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            rtnJson = setTableEventResultHandler(rtnBoolResult, e.getMessage());
        	return Response.status(status).entity(rtnJson.toString()).build();
        }
    }

    @POST
    @Path("/SPM")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response insertMesSpmData(InputStream inputStream)
    {
        LOGGER.info("WMS_IFS_RECEIVE_MES_Service.insertMesSpmData() called");


        Boolean rtnBoolResult = Boolean.valueOf(false);
    	JsonObject rtnJson    = new JsonObject();
    	int status            = 500;
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);
        	String  errMsg = validMesReceiveData("HEADER",paramMap);
        	if(!errMsg.equals(""))
        	{
                rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
            	return Response.status(status).entity(rtnJson.toString()).build();
        	}

        	String dataItem    = (String)paramMap.get("dataItem") ;
        	ArrayList<HashMap<String,Object>> dataList = StringUtil.getJsonToHashMapList(dataItem);
        	errMsg             = validMesReceiveData("DATA",dataList.get(0));
        	System.out.println("errMsg:"+errMsg);
        	if(!errMsg.equals(""))
        	{

        		rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
            	return Response.status(500).entity(rtnJson.toString()).build();
        	}

        	paramMap.put("dataList", dataList);
    		System.out.println("dataList:"+dataList);
    		WMS_IFS_RECEIVE_MES_Impl iWMS_IFS_RECEIVE_MES_Impl = new WMS_IFS_RECEIVE_MES_Impl();
    		errMsg = iWMS_IFS_RECEIVE_MES_Impl.insertMesSpmData(paramMap);

    		if( errMsg.equals(""))
    		{
    			rtnBoolResult = Boolean.valueOf(true);
    			status        = 200;
    		}

            rtnJson = setTableEventResultHandler(rtnBoolResult, errMsg);
        	return Response.status(status).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            rtnJson = setTableEventResultHandler(rtnBoolResult, e.getMessage());
        	return Response.status(status).entity(rtnJson.toString()).build();
        }
    }

    private String validMesReceiveData(String checkType, HashMap<String,Object> paramMap)
    {
        String  errMsg = "";

        if( checkType.equals("HEADER"))
        {
            if( paramMap.containsKey("errorMsg") )
        	{
        		errMsg = (String)paramMap.get("errorMsg");
        	}
            else if( !paramMap.containsKey("CLIENT_ID") )
        	{
        		errMsg = "There is no 'CLIENT_ID' in your json objects";
        	}
        	else if( !paramMap.containsKey("COMPANY_CODE") )
        	{
        		errMsg = "There is no 'COMPANY_CODE' in your json objects";
        	}
        	else if( !paramMap.containsKey("IF_METHOD") )
        	{
        		errMsg = "There is no 'IF_METHOD' in your json objects";
        	}
        	else if( !paramMap.containsKey("dataItem") )
        	{
        		errMsg = "There is no 'dataItem' in your json objects";
        	}
        }
        else
        {
            if( paramMap.containsKey("errorMsg") )
        	{
            	errMsg = (String)paramMap.get("errorMsg");
        	}
        }

        return errMsg;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret" , Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData  );
            rtnJson.addProperty("msg" , "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret" , Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg" , rtnMsg );
        }
        return rtnJson;
    }
}
