/**
 *
 */
package com.wenestim.wms.rest.service.production;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.production.WMS_MST_ROUTE;
import com.wenestim.wms.rest.impl.production.WMS_MST_ROUTE_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author Andrew
 *
 */
@Path("/Production/Master/Route")
public class WMS_MST_ROUTE_Service
{
    private static final Logger LOGGER = Logger.getLogger(WMS_MST_ROUTE_Service.class);

    @POST
    @Path("/List")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response getRouteList(InputStream inputStream)
    {
        LOGGER.info("WMS_MST_ROUTE_Service.getRouteList() called");

        String result = "";
        try
        {
        	HashMap<String,Object> paramMap = StringUtil.getParamMap(inputStream);

//        	String materialCode = (String)paramMap.get("MATERIAL_CODE");
//        	paramMap.put("MATERIAL_CODE", StringUtil.getJsonToObjectList(materialCode));

//        	String test = (String)paramMap.get("TEST");
//        	ArrayList testList = StringUtil.getJsonToHashMapList(test);


        	WMS_MST_ROUTE_Impl mWMS_MST_ROUTE_Impl = new WMS_MST_ROUTE_Impl();
        	List<WMS_MST_ROUTE> allList = mWMS_MST_ROUTE_Impl.getRouteList(paramMap);

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updateRouteList(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
    		) throws Exception
    {
        LOGGER.info("WMS_MST_ROUTE_Service.updateRouteList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {

            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsonArray = new JSONArray(getString);

            List dataList = setDataObjList(jsonArray, "UPDATE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", dataList);

            WMS_MST_ROUTE_Impl iWMS_MST_ROUTE_Impl = new WMS_MST_ROUTE_Impl();
            rtnBoolResult = iWMS_MST_ROUTE_Impl.updateRouteList(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Delete/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deleteRouteList(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
    		) throws Exception
    {
        LOGGER.info("WMS_MST_ROUTE_Service.deleteRouteList() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {

            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsonArray = new JSONArray(getString);

            List dataList = setDataObjList(jsonArray, "DELETE", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList", dataList);

            WMS_MST_ROUTE_Impl iWMS_MST_ROUTE_Impl = new WMS_MST_ROUTE_Impl();
            rtnBoolResult = iWMS_MST_ROUTE_Impl.deleteRouteList(dataMap);

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public List setDataObjList(JSONArray jsArray, String evtHandler, String userId, String clientId, String comapnyCode) throws Exception
    {

        List<WMS_MST_ROUTE> dataList = null;
        try
        {
            dataList = new ArrayList<WMS_MST_ROUTE>();

            for (int i=0; i<jsArray.length(); i++)
            {

            	int WSID             = jsArray.getJSONObject(i).has("WSID"         ) ? jsArray.getJSONObject(i).getInt("WSID"            ):0;
            	String PLANT_CODE    = jsArray.getJSONObject(i).has("PLANT_CODE"   ) ? jsArray.getJSONObject(i).getString("PLANT_CODE"   ) : "";
            	String MATERIAL_CODE = jsArray.getJSONObject(i).has("MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MATERIAL_CODE") : "";
            	int OP_NO            = jsArray.getJSONObject(i).has("OP_NO"        ) ? jsArray.getJSONObject(i).getInt("OP_NO"           ):0;
            	String OP_NAME       = jsArray.getJSONObject(i).has("OP_NAME"      ) ? jsArray.getJSONObject(i).getString("OP_NAME"      ) : "";
            	String ACTIVITY1     = jsArray.getJSONObject(i).has("ACTIVITY1"    ) ? jsArray.getJSONObject(i).getString("ACTIVITY1"    ) : "";
            	String TIME1         = jsArray.getJSONObject(i).has("TIME1"        ) ? jsArray.getJSONObject(i).getString("TIME1"        ) : "-1";
            	String ACTIVITY2     = jsArray.getJSONObject(i).has("ACTIVITY2"    ) ? jsArray.getJSONObject(i).getString("ACTIVITY2"    ) : "";
            	String TIME2         = jsArray.getJSONObject(i).has("TIME2"        ) ? jsArray.getJSONObject(i).getString("TIME2"        ) : "-1";
            	String ACTIVITY3     = jsArray.getJSONObject(i).has("ACTIVITY3"    ) ? jsArray.getJSONObject(i).getString("ACTIVITY3"    ) : "";
            	String TIME3         = jsArray.getJSONObject(i).has("TIME3"        ) ? jsArray.getJSONObject(i).getString("TIME3"        ) : "-1";
            	String ACTIVITY4     = jsArray.getJSONObject(i).has("ACTIVITY4"    ) ? jsArray.getJSONObject(i).getString("ACTIVITY4"    ) : "";
            	String TIME4         = jsArray.getJSONObject(i).has("TIME4"        ) ? jsArray.getJSONObject(i).getString("TIME4"        ) : "-1";
            	String ACTIVITY5     = jsArray.getJSONObject(i).has("ACTIVITY5"    ) ? jsArray.getJSONObject(i).getString("ACTIVITY5"    ) : "";
            	String TIME5         = jsArray.getJSONObject(i).has("TIME5"        ) ? jsArray.getJSONObject(i).getString("TIME5"        ) : "-1";
            	String COST_CENTER   = jsArray.getJSONObject(i).has("COST_CENTER"  ) ? jsArray.getJSONObject(i).getString("COST_CENTER"  ) : "";

                WMS_MST_ROUTE oWMS_MST_ROUTE = new WMS_MST_ROUTE();

                oWMS_MST_ROUTE.setCLIENT_ID    (clientId        );
                oWMS_MST_ROUTE.setCOMPANY_CODE (comapnyCode     );
                oWMS_MST_ROUTE.setWSID         (WSID            );
                oWMS_MST_ROUTE.setPLANT_CODE   (PLANT_CODE      );
                oWMS_MST_ROUTE.setMATERIAL_CODE(MATERIAL_CODE   );
                oWMS_MST_ROUTE.setOP_NO        (OP_NO           );
                oWMS_MST_ROUTE.setOP_NAME      (OP_NAME         );
                oWMS_MST_ROUTE.setACTIVITY1    (ACTIVITY1       );
                oWMS_MST_ROUTE.setTIME1        (new Float(TIME1));
                oWMS_MST_ROUTE.setACTIVITY2    (ACTIVITY2       );
                oWMS_MST_ROUTE.setTIME2        (new Float(TIME2));
                oWMS_MST_ROUTE.setACTIVITY3    (ACTIVITY3       );
                oWMS_MST_ROUTE.setTIME3        (new Float(TIME3));
                oWMS_MST_ROUTE.setACTIVITY4    (ACTIVITY4       );
                oWMS_MST_ROUTE.setTIME4        (new Float(TIME4));
                oWMS_MST_ROUTE.setACTIVITY5    (ACTIVITY5       );
                oWMS_MST_ROUTE.setTIME5        (new Float(TIME5));
                oWMS_MST_ROUTE.setCOST_CENTER  (COST_CENTER     );
                oWMS_MST_ROUTE.setCREATE_USER  (userId          );
                oWMS_MST_ROUTE.setUPDATE_USER  (userId          );

                dataList.add(oWMS_MST_ROUTE);
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        return dataList;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret", Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", "success");
        }
        else
        {
            String rtnMsg = "could not load data";
            rtnJson.addProperty("ret", Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg", rtnMsg);
        }
        return rtnJson;
    }
}
