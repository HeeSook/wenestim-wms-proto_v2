/**
 *
 */
package com.wenestim.wms.rest.service.common;

import java.io.InputStream;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.common.WMS_MST_COMCODE;
import com.wenestim.wms.rest.impl.common.WMS_MST_COMCODE_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Common/ComCode")
public class WMS_MST_COMCODE_Service {

	private final static Logger LOGGER = Logger.getLogger(WMS_MST_COMCODE_Service.class);

	@GET
	@Path("/All")
	public Response getAllComCode() {
		LOGGER.info("WMS_MST_COMCODE_Service.getAllComCode() called");

		String result = "";
		try {
			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();
			List<WMS_MST_COMCODE> allList = mWMS_MST_COMCODE_Impl.getAllComCode();

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/settingsList")
	public Response getAllComCodeBYCategory(@QueryParam("CLIENT_ID") String CLIENT_ID,@QueryParam("COMPANY_CODE") String COMPANY_CODE,@QueryParam("CATEGORY1") String CATEGORY1,@QueryParam("CATEGORY2") String CATEGORY2) {
		LOGGER.info("WMS_MST_COMCODE_Service.getAllComCode() called");

		String result = "";
		try {
			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();
			List<WMS_MST_COMCODE> allList = mWMS_MST_COMCODE_Impl.getAllComCodeByCategory(CLIENT_ID,COMPANY_CODE,CATEGORY1,CATEGORY2);

			Gson gson = new Gson();
			if (allList != null) {
				result = gson.toJson(allList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@GET
	@Path("/comCodeList")
	public Response getComCodeByCategory(@QueryParam("CLIENT_ID") String CLIENT_ID,@QueryParam("COMPANY_CODE") String COMPANY_CODE,@QueryParam("CATEGORY1") String CATEGORY1,@QueryParam("CATEGORY2") String CATEGORY2)
	{
		LOGGER.info("WMS_MST_COMCODE_Service.getComCodeByCategory() called");
		LOGGER.info("CLIENT_ID : " + CLIENT_ID + ", COMPANY_CODE: " + COMPANY_CODE + ", CATEGORY1 : " + CATEGORY1 + ", CATEGORY2 : " + CATEGORY2);

		String result = "";
		try {
			CLIENT_ID = StringUtil.getEmptyNullPrevString(CLIENT_ID);
			COMPANY_CODE = StringUtil.getEmptyNullPrevString(COMPANY_CODE);
			CATEGORY1    = StringUtil.getEmptyNullPrevString(CATEGORY1);
			CATEGORY2    = StringUtil.getEmptyNullPrevString(CATEGORY2);

			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();
			List<WMS_MST_COMCODE> dataList = mWMS_MST_COMCODE_Impl.getComCodeByCategory(CLIENT_ID,COMPANY_CODE,CATEGORY1,CATEGORY2);

			Gson gson = new Gson();
			if (dataList != null)
			{
				result = gson.toJson(dataList);
			}
			return Response.status(200).entity(result).build();

		} catch (Exception e) {
			LOGGER.error("Exception:" + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/create/setting")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response createSetting(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_COMCODE_Service.createSetting() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings Insert Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			String  CLIENT_ID    = jsonArray.getJSONObject(0).has("CLIENT_ID"     ) ? jsonArray.getJSONObject(0).getString("CLIENT_ID"    ) : "";
			String  COMPANY_CODE = jsonArray.getJSONObject(0).has("COMPANY_CODE"  ) ? jsonArray.getJSONObject(0).getString("COMPANY_CODE"  ) : "";
			String  CATEGORY1    = jsonArray.getJSONObject(0).has("CATEGORY1"  ) ? jsonArray.getJSONObject(0).getString("CATEGORY1"  ) : "";
			String  CATEGORY2    = jsonArray.getJSONObject(0).has("CATEGORY2"  ) ? jsonArray.getJSONObject(0).getString("CATEGORY2"  ) : "";
			String  CODE         = jsonArray.getJSONObject(0).has("CODE"  ) ? jsonArray.getJSONObject(0).getString("CODE"  ) : "";
			String  CODE_DESC    = jsonArray.getJSONObject(0).has("CODE_DESC"  ) ? jsonArray.getJSONObject(0).getString("CODE_DESC"  ) : "";
			Integer SORT1        = Integer.valueOf(jsonArray.getJSONObject(0).has("SORT1") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("SORT1")) : 0);
			Integer SORT2        = Integer.valueOf(jsonArray.getJSONObject(0).has("SORT2") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("SORT2")) : 0);
			String  ATTR1        = jsonArray.getJSONObject(0).has("ATTR1"  ) ? jsonArray.getJSONObject(0).getString("ATTR1"  ) : "";
			String  COMMENT      = jsonArray.getJSONObject(0).has("COMMENT"  ) ? jsonArray.getJSONObject(0).getString("COMMENT"  ) : "";
			String  CREATE_USER  = jsonArray.getJSONObject(0).has("CREATE_USER"  ) ? jsonArray.getJSONObject(0).getString("CREATE_USER"  ) : "";


			WMS_MST_COMCODE oWMS_MST_COMCODE = new WMS_MST_COMCODE();
			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();

			oWMS_MST_COMCODE.setCLIENT_ID(CLIENT_ID);
			oWMS_MST_COMCODE.setCOMPANY_CODE(COMPANY_CODE);
			oWMS_MST_COMCODE.setCATEGORY1(CATEGORY1);
			oWMS_MST_COMCODE.setCATEGORY2(CATEGORY2);
			oWMS_MST_COMCODE.setCODE(CODE);
			oWMS_MST_COMCODE.setCODE_DESC(CODE_DESC);
			oWMS_MST_COMCODE.setSORT2(SORT2);
			oWMS_MST_COMCODE.setCOMMENT(COMMENT);
			oWMS_MST_COMCODE.setATTR1(ATTR1);
			oWMS_MST_COMCODE.setCREATE_USER(CREATE_USER);

			Integer count = mWMS_MST_COMCODE_Impl.countSORT1(CATEGORY1, CATEGORY2);
			oWMS_MST_COMCODE.setSORT1(count + 1);

			boolean result = true;
			if( !CODE.equals(""))
			{
				result = mWMS_MST_COMCODE_Impl.insertComCode(oWMS_MST_COMCODE);
			}

			JsonObject rtnJson = new JsonObject();

			LOGGER.info("Setting Insert Result: " + result);

			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Setting is instered");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		} catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/update/setting")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response updateSetting(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_COMCODE_Service.updateSetting() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings update Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			Integer WSID         = Integer.valueOf(jsonArray.getJSONObject(0).has("WSID") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("WSID")) : 0);
			String  CLIENT_ID    = jsonArray.getJSONObject(0).has("CLIENT_ID"     ) ? jsonArray.getJSONObject(0).getString("CLIENT_ID"    ) : "";
			String  COMPANY_CODE = jsonArray.getJSONObject(0).has("COMPANY_CODE"  ) ? jsonArray.getJSONObject(0).getString("COMPANY_CODE"  ) : "";
			String  CATEGORY1    = jsonArray.getJSONObject(0).has("CATEGORY1"  ) ? jsonArray.getJSONObject(0).getString("CATEGORY1"  ) : "";
			String  CATEGORY2    = jsonArray.getJSONObject(0).has("CATEGORY2"  ) ? jsonArray.getJSONObject(0).getString("CATEGORY2"  ) : "";
			String  CODE         = jsonArray.getJSONObject(0).has("CODE"  ) ? jsonArray.getJSONObject(0).getString("CODE"  ) : "";
			String  CODE_DESC    = jsonArray.getJSONObject(0).has("CODE_DESC"  ) ? jsonArray.getJSONObject(0).getString("CODE_DESC"  ) : "";
			Integer SORT1        = Integer.valueOf(jsonArray.getJSONObject(0).has("SORT1") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("SORT1")) : 0);
			Integer SORT2        = Integer.valueOf(jsonArray.getJSONObject(0).has("SORT2") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("SORT2")) : 0);
			String  ATTR1        = jsonArray.getJSONObject(0).has("ATTR1"  ) ? jsonArray.getJSONObject(0).getString("ATTR1"  ) : "";
			String  COMMENT      = jsonArray.getJSONObject(0).has("COMMENT"  ) ? jsonArray.getJSONObject(0).getString("COMMENT"  ) : "";
			String  UPDATE_USER  = jsonArray.getJSONObject(0).has("UPDATE_USER"  ) ? jsonArray.getJSONObject(0).getString("UPDATE_USER"  ) : "";

			WMS_MST_COMCODE oWMS_MST_COMCODE = new WMS_MST_COMCODE();
			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();

			oWMS_MST_COMCODE.setWSID(WSID);
			oWMS_MST_COMCODE.setCLIENT_ID(CLIENT_ID);
			oWMS_MST_COMCODE.setCOMPANY_CODE(COMPANY_CODE);
			oWMS_MST_COMCODE.setCATEGORY1(CATEGORY1);
			oWMS_MST_COMCODE.setCATEGORY2(CATEGORY2);
			oWMS_MST_COMCODE.setCODE(CODE);
			oWMS_MST_COMCODE.setCODE_DESC(CODE_DESC);
			oWMS_MST_COMCODE.setSORT1(SORT1);
			oWMS_MST_COMCODE.setSORT2(SORT2);
			oWMS_MST_COMCODE.setATTR1(ATTR1);
			oWMS_MST_COMCODE.setCOMMENT(COMMENT);
			oWMS_MST_COMCODE.setUPDATE_USER(UPDATE_USER);

			boolean result = mWMS_MST_COMCODE_Impl.updateComCode(oWMS_MST_COMCODE);

			JsonObject rtnJson = new JsonObject();

			LOGGER.info("Setting update Result: " + result);

			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Setting is updated");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		} catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

	@POST
	@Path("/delete/setting")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response deleteSetting(InputStream inputStream) throws Exception {
		LOGGER.info("WMS_MST_COMCODE_Service.deleteSetting() called");
		try
		{
			String getString = StringUtil.getInputStreamToString(inputStream);
			LOGGER.info("Settings delete Data: " + getString);
			JSONArray jsonArray = new JSONArray(getString);

			Integer WSID        = Integer.valueOf(jsonArray.getJSONObject(0).has("WSID") ? Integer.parseInt(jsonArray.getJSONObject(0).getString("WSID")) : 0);

			WMS_MST_COMCODE oWMS_MST_COMCODE = new WMS_MST_COMCODE();
			WMS_MST_COMCODE_Impl mWMS_MST_COMCODE_Impl = new WMS_MST_COMCODE_Impl();

			oWMS_MST_COMCODE.setWSID(WSID);
			boolean result = mWMS_MST_COMCODE_Impl.deleteComCode(oWMS_MST_COMCODE);

			JsonObject rtnJson = new JsonObject();

			LOGGER.info("Setting delete Result: " + result);

			if(result) {
				rtnJson.addProperty("ret", Integer.valueOf(0));
				rtnJson.addProperty("msg", "Setting is deleted");
			}
			return Response.status(200).entity(rtnJson.toString()).build();
		} catch (Exception e)
		{
			LOGGER.error("Excption: " + e.getMessage() + "#");
			e.printStackTrace();
			return Response.status(500).entity(e.toString()).build();
		}
	}

}
