/**
 *
 */
package com.wenestim.wms.rest.service.inbound;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING;
import com.wenestim.wms.rest.entity.inbound.WMS_DYN_PURCHASING_GR;
import com.wenestim.wms.rest.impl.common.WMS_MST_CODERULE_Impl;
import com.wenestim.wms.rest.impl.inbound.WMS_DYN_PURCHASING_GR_Impl;
import com.wenestim.wms.rest.impl.inbound.WMS_DYN_PURCHASING_Impl;
import com.wenestim.wms.rest.impl.inventory.WMS_DYN_STOCK_MOVEMENT_Impl;
import com.wenestim.wms.rest.util.StringUtil;

/**
 * @author
 *
 */
@Path("/Inbound/PurchasingGr")
public class WMS_DYN_PURCHASING_GR_Service {

    private final static Logger LOGGER = Logger.getLogger(WMS_DYN_PURCHASING_GR_Service.class);

    @GET
    @Path("/All")
    public Response getAllPurchasingGr(
            @QueryParam("CLIENT_ID"         ) String CLIENT_ID         ,
            @QueryParam("COMPANY_CODE"      ) String COMPANY_CODE      ,
            @QueryParam("PLANT_CODE"        ) String PLANT_CODE        ,
            @QueryParam("PO_NO"             ) String PO_NO             ,
            @QueryParam("VENDOR_CODE"       ) String VENDOR_CODE       ,
            @QueryParam("FROM_PO_DATE"      ) String FROM_PO_DATE      ,
            @QueryParam("TO_PO_DATE"        ) String TO_PO_DATE        ,
            @QueryParam("FROM_DELIVERY_DATE") String FROM_DELIVERY_DATE,
            @QueryParam("TO_DELIVERY_DATE"  ) String TO_DELIVERY_DATE  ,
            @QueryParam("POST_DATE"         ) String POST_DATE
            )
    {
        LOGGER.info("WMS_DYN_PURCHASING_GR_Service.getAllPurchasingGr() called");

        String result = "";
        try
        {
            CLIENT_ID          = StringUtil.getEmptyNullPrevString(CLIENT_ID         ) ;
            COMPANY_CODE       = StringUtil.getEmptyNullPrevString(COMPANY_CODE      ) ;
            PLANT_CODE         = StringUtil.getEmptyNullPrevString(PLANT_CODE        ) ;
            PO_NO              = StringUtil.getEmptyNullPrevString(PO_NO             ) ;
            VENDOR_CODE        = StringUtil.getEmptyNullPrevString(VENDOR_CODE       ) ;
            FROM_PO_DATE       = StringUtil.getEmptyNullPrevString(FROM_PO_DATE      ) ;
            TO_PO_DATE         = StringUtil.getEmptyNullPrevString(TO_PO_DATE        ) ;
            FROM_DELIVERY_DATE = StringUtil.getEmptyNullPrevString(FROM_DELIVERY_DATE) ;
            TO_DELIVERY_DATE   = StringUtil.getEmptyNullPrevString(TO_DELIVERY_DATE  ) ;
            POST_DATE          = StringUtil.getEmptyNullPrevString(POST_DATE         ) ;

            LOGGER.info("PLANT_CODE:"  +PLANT_CODE  +",PO_NO:"     +PO_NO     +",VENDOR_CODE:"       +VENDOR_CODE);
            LOGGER.info("FROM_PO_DATE:"+FROM_PO_DATE+",TO_PO_DATE:"+TO_PO_DATE+",FROM_DELIVERY_DATE:"+FROM_DELIVERY_DATE+",TO_DELIVERY_DATE:"+TO_DELIVERY_DATE);
            LOGGER.info("POST_DATE:"   +POST_DATE);

            WMS_DYN_PURCHASING_GR_Impl iWMS_DYN_PURCHASING_GR_Impl = new WMS_DYN_PURCHASING_GR_Impl();
            List<WMS_DYN_PURCHASING_GR> allList = iWMS_DYN_PURCHASING_GR_Impl.getAllPurchasingGr(
                    CLIENT_ID    ,
                    COMPANY_CODE ,
                    PLANT_CODE   ,
                    PO_NO        ,
                    VENDOR_CODE  ,
                    FROM_PO_DATE ,     TO_PO_DATE,
                    FROM_DELIVERY_DATE,TO_DELIVERY_DATE,
                    POST_DATE
                    );

            Gson gson = new Gson();
            if (allList != null)
            {
                result = gson.toJson(allList);
            }
            return Response.status(200).entity(result).build();

        }
        catch (Exception e)
        {
            LOGGER.error("Exception:" + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @GET
    @Path("/LotNo")
    public Response getSystemLotNumber(
    		@QueryParam("CLIENT_ID"         ) String CLIENT_ID         ,
            @QueryParam("COMPANY_CODE"      ) String COMPANY_CODE
    		)
    {
        LOGGER.info("WMS_DYN_PURCHASING_GR_Service.getSystemLotNumber() called");

        try
        {
        	WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();
        	int LotNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(CLIENT_ID, COMPANY_CODE, "LOT", "LOT_NO", 1));

            JsonObject rtnJson = new JsonObject();
            rtnJson.addProperty("LOTNO", LotNo);

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Update/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response updatePurchasingGr(InputStream inputStream,
            @PathParam("userId"     ) String userId,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PURCHASING_GR_Service.updatePurchasingGr() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String    getString  = StringUtil.getInputStreamToString(inputStream);
            JSONArray jsArray    = new JSONArray(getString);
            HashMap   rtnMap     = setDataObjList(jsArray, "UPDATE", "100", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList"    , rtnMap.get("dataList"));
            dataMap.put("CLIENT_ID"   , clientId   );
            dataMap.put("COMPANY_CODE", companyCode);

            WMS_DYN_PURCHASING_GR_Impl iWMS_DYN_PURCHASING_GR_Impl = new WMS_DYN_PURCHASING_GR_Impl();

            rtnBoolResult = iWMS_DYN_PURCHASING_GR_Impl.updatePurchasingGr(dataMap);
            if(rtnBoolResult)
            {
                WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("GR_INPUT", jsArray, userId, clientId, companyCode);
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    @POST
    @Path("/Delete/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response deletePurchasingGr(InputStream inputStream,
            @PathParam("userId"     ) String userId  ,
            @PathParam("clientId"   ) String clientId,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PURCHASING_GR_Service.deletePurchasingGr() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONArray jsArray = new JSONArray(getString);

            HashMap rtnMap = setDataObjList(jsArray, "RETURN", "101", userId, clientId, companyCode);

            HashMap<String, Object> dataMap = new HashMap<String, Object>();
            dataMap.put("dataList"      , rtnMap.get("dataList"));
            dataMap.put("CLIENT_ID"     , clientId   );
            dataMap.put("COMPANY_CODE"  , companyCode);
            dataMap.put("DELETE_USER"   , userId);
            dataMap.put("MOVEMENT_TYPE" , "101");

            WMS_DYN_PURCHASING_GR_Impl iWMS_DYN_PURCHASING_GR_Impl = new WMS_DYN_PURCHASING_GR_Impl();

            rtnBoolResult = iWMS_DYN_PURCHASING_GR_Impl.deletePurchasingGr(dataMap);
            if(rtnBoolResult)
            {
                WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("GR_RETURN", jsArray, userId, clientId, companyCode);
            }

            JsonObject rtnJson = new JsonObject();
            rtnJson = setTableEventResultHandler(rtnBoolResult, "");

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    public HashMap setDataObjList(JSONArray jsArray, String evtHandler, String movementType, String userId, String clientId, String comapnyCode) throws Exception
    {
        HashMap rtnMap = new HashMap();

        List<WMS_DYN_PURCHASING_GR> dataList = new ArrayList<WMS_DYN_PURCHASING_GR>();
        try
        {
            WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();

            for (int i=0; i<jsArray.length(); i++)
            {
                int     docNo           = jsArray.getJSONObject(i).has("DOC_NO"       ) ? jsArray.getJSONObject(i).getInt(   "DOC_NO"       ) : 0 ;
                int     WSID            = jsArray.getJSONObject(i).has("WSID"         ) ? jsArray.getJSONObject(i).getInt(   "WSID"         ) : 0;
                String  PO_NO           = jsArray.getJSONObject(i).has("PO_NO"        ) ? jsArray.getJSONObject(i).getString("PO_NO"        ) : "";
                Integer PO_ITEM_NO      = jsArray.getJSONObject(i).has("PO_ITEM_NO"   ) ? jsArray.getJSONObject(i).getInt(   "PO_ITEM_NO"   ) : 0 ;
                String  VENDOR_CODE     = jsArray.getJSONObject(i).has("VENDOR_CODE"  ) ? jsArray.getJSONObject(i).getString("VENDOR_CODE"  ) : "";
                String  MATERIAL_CODE   = jsArray.getJSONObject(i).has("MATERIAL_CODE") ? jsArray.getJSONObject(i).getString("MATERIAL_CODE") : "";
                String  GR_DATE         = jsArray.getJSONObject(i).has("POST_DATE"    ) ? jsArray.getJSONObject(i).getString("POST_DATE"    ) : "";
                Double  GR_QTY          = jsArray.getJSONObject(i).has("GR_QTY"       ) ? jsArray.getJSONObject(i).getDouble("GR_QTY"       ) : 0 ;
                String  GR_UNIT         = jsArray.getJSONObject(i).has("GR_UNIT"      ) ? jsArray.getJSONObject(i).getString("GR_UNIT"      ) : "";
                String  STATUS          = jsArray.getJSONObject(i).has("STATUS"       ) ? jsArray.getJSONObject(i).getString("STATUS"       ) : "";
                Double  ITEM_QTY        = jsArray.getJSONObject(i).has("ITEM_QTY"     ) ? jsArray.getJSONObject(i).getDouble("ITEM_QTY"     ) : 0 ;
                Double  MOVE_QTY        = jsArray.getJSONObject(i).has("MOVE_QTY"     ) ? jsArray.getJSONObject(i).getDouble("MOVE_QTY"     ) : 0 ;
                String  CONV_UNIT       = jsArray.getJSONObject(i).has("CONV_UNIT"    ) ? jsArray.getJSONObject(i).getString("CONV_UNIT"    ) : "EA";
                String  MST_STORAGE_LOC = jsArray.getJSONObject(i).has("MST_STORAGE_LOC"  ) ? jsArray.getJSONObject(i).getString("MST_STORAGE_LOC"  ) : "";

                String  CURR_STORAGE_LOC = MST_STORAGE_LOC;
                String  MOVE_STORAGE_LOC = MST_STORAGE_LOC;
                String  UNIT             = CONV_UNIT;

                if(docNo == 0)
                {
                    docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(clientId, comapnyCode, "INV", "DOC_NO", 1));
                }

                WMS_DYN_PURCHASING_GR oWMS_DYN_PURCHASING_GR = new WMS_DYN_PURCHASING_GR();

                oWMS_DYN_PURCHASING_GR.setCLIENT_ID(clientId             );
                oWMS_DYN_PURCHASING_GR.setCOMPANY_CODE(comapnyCode       );
                oWMS_DYN_PURCHASING_GR.setWSID(WSID                      );
                oWMS_DYN_PURCHASING_GR.setDOC_NO(docNo                   );
                oWMS_DYN_PURCHASING_GR.setPO_NO(PO_NO                    );
                oWMS_DYN_PURCHASING_GR.setPO_ITEM_NO(PO_ITEM_NO          );
                oWMS_DYN_PURCHASING_GR.setVENDOR_CODE(VENDOR_CODE        );
                oWMS_DYN_PURCHASING_GR.setMATERIAL_CODE(MATERIAL_CODE    );
                oWMS_DYN_PURCHASING_GR.setGR_DATE(GR_DATE                );
                oWMS_DYN_PURCHASING_GR.setGR_QTY(new Float(GR_QTY)       );
                oWMS_DYN_PURCHASING_GR.setMOVE_QTY(new Float(MOVE_QTY)   );
                oWMS_DYN_PURCHASING_GR.setITEM_QTY(new Float(ITEM_QTY)   );
                oWMS_DYN_PURCHASING_GR.setGR_UNIT(GR_UNIT                );
                oWMS_DYN_PURCHASING_GR.setMST_STORAGE_LOC(MST_STORAGE_LOC);
                oWMS_DYN_PURCHASING_GR.setMOVEMENT_TYPE(movementType     );
                oWMS_DYN_PURCHASING_GR.setSTATUS(STATUS                  );
                oWMS_DYN_PURCHASING_GR.setCREATE_USER(userId             );
                dataList.add(oWMS_DYN_PURCHASING_GR);

                if(evtHandler.equals("RETURN"))
                {
                    MOVE_QTY = MOVE_QTY * -1;
                }
                jsArray.getJSONObject(i).put("MOVEMENT_TYPE"   , movementType     );
                jsArray.getJSONObject(i).put("CURR_STORAGE_LOC", CURR_STORAGE_LOC );
                jsArray.getJSONObject(i).put("CURR_QTY"        , MOVE_QTY         );
                jsArray.getJSONObject(i).put("MOVE_STORAGE_LOC", MOVE_STORAGE_LOC );
                jsArray.getJSONObject(i).put("MOVE_QTY"        , MOVE_QTY         );
                jsArray.getJSONObject(i).put("UNIT"            , UNIT             );
            }
        }
        catch(Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
        }
        rtnMap.put("dataList", dataList);
        return rtnMap;
    }

    // GWANGHYUN KIM 4/7/2018
    @POST
    @Path("PDA/Create/{userId}/{clientId}/{companyCode}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Response insertPurchasingGr(InputStream inputStream,
            @PathParam("userId"     ) String userId    ,
            @PathParam("clientId"   ) String clientId   ,
            @PathParam("companyCode") String companyCode
            ) throws Exception
    {
        LOGGER.info("WMS_DYN_PURCHASING_GR_Service.insertPurchasingGr() called");

        Boolean rtnBoolResult = Boolean.valueOf(false);
        try
        {
            String getString = StringUtil.getInputStreamToString(inputStream);

            JSONObject jsonObject = new JSONObject(getString);

            LOGGER.info("GR PDA JSON INFO: " + jsonObject);

            WMS_DYN_PURCHASING_GR oWMS_DYN_PURCHASING_GR = new WMS_DYN_PURCHASING_GR();
            oWMS_DYN_PURCHASING_GR = setObjectByJson("Insert", jsonObject, userId, clientId, companyCode);


            WMS_DYN_PURCHASING_GR_Impl iWMS_DYN_PURCHASING_GR_Impl = new WMS_DYN_PURCHASING_GR_Impl();
            WMS_DYN_PURCHASING_Impl    iWMS_DYN_PURCHASING_Impl    = new WMS_DYN_PURCHASING_Impl();

            Boolean chkExist = iWMS_DYN_PURCHASING_GR_Impl.chkExistsPurchasingByBarcode_NO(
					oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
					oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
					oWMS_DYN_PURCHASING_GR.getBARCODE_NO()
					);

            Boolean chkExistMaterial = iWMS_DYN_PURCHASING_Impl.chkExistsMaterial(
    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
    				oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
    				oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
    				);

            Boolean chkExistPoNumber = iWMS_DYN_PURCHASING_Impl.chkExistsPoNumber(
    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
    				oWMS_DYN_PURCHASING_GR.getPO_NO()
    				);

            Boolean chkGRStauts = iWMS_DYN_PURCHASING_Impl.chkGRStauts(
    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
    				oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
    				oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
    				oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
    				);

            JsonObject rtnJson = new JsonObject();

            if(chkExistPoNumber) {
                if(!chkExist) {

                    if (!chkExistMaterial) {

                        rtnJson.addProperty("ret", Integer.valueOf(1));
                        rtnJson.addProperty("msg", "Not in the list scanned material : " + oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE() + " ");

                    } else {

//                      if(!chkGRStauts) {

                    		float PO_Qty = iWMS_DYN_PURCHASING_Impl.getPO_Qty(
                    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
                    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
            						oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
            						oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
            						oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
            						);

                    		LOGGER.info("PO_QTY is "+PO_Qty+"");

                    		float GR_Qty = iWMS_DYN_PURCHASING_GR_Impl.getGR_Qty(
                    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
                    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
            						oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
            						oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
            						oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
            						);
                    		LOGGER.info("GR_QTY is "+GR_Qty+"");

                    		GR_Qty += oWMS_DYN_PURCHASING_GR.getGR_QTY();
                    		LOGGER.info("GR_QTY +++ is "+GR_Qty+"");

                    		Boolean grStatusUpdateFlag = true;
                    		WMS_DYN_PURCHASING tempPurchasing = new WMS_DYN_PURCHASING();
                    		tempPurchasing = iWMS_DYN_PURCHASING_Impl.getPurchasing(
                    				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
                    				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
                    				oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
                    				oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
                    				oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
                    				);

                    		LOGGER.info("tempPurchasing is "+tempPurchasing+"");

                    		// Unit Conversion
                    		int qtyConv = (int)oWMS_DYN_PURCHASING_GR.getGR_QTY();
                    		if(!oWMS_DYN_PURCHASING_GR.getGR_UNIT().equals(tempPurchasing.getMST_UNIT())) { // If unit is different.
                    			if(oWMS_DYN_PURCHASING_GR.getGR_UNIT().equals("BOX")) {
                    				qtyConv = (int)oWMS_DYN_PURCHASING_GR.getGR_QTY() * tempPurchasing.getMST_QTY().intValue();
                    			}
                    		}

                    		LOGGER.info("qtyConv is "+qtyConv+"");
                    		oWMS_DYN_PURCHASING_GR.setITEM_QTY(new Float(qtyConv));


                    		if(GR_Qty >= PO_Qty) {
                    			grStatusUpdateFlag = iWMS_DYN_PURCHASING_GR_Impl.updateGRstatus(
                    					"CLOSE",
                        				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
                        				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
                    					oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
                    					oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
										oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
                    					);
                    			LOGGER.info("grStatusUpdateFlag is "+grStatusUpdateFlag+"");

                    			oWMS_DYN_PURCHASING_GR.setSTATUS("CLOSE");
                    		}else {
                    			grStatusUpdateFlag = iWMS_DYN_PURCHASING_GR_Impl.updateGRstatus(
                    					"OPEN",
                        				oWMS_DYN_PURCHASING_GR.getCLIENT_ID()   ,
                        				oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE(),
                    					oWMS_DYN_PURCHASING_GR.getPO_NO()       ,
                    					oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO()  ,
										oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE()
										);
                    			LOGGER.info("grStatusUpdateFlag is "+grStatusUpdateFlag+"");
                    			oWMS_DYN_PURCHASING_GR.setSTATUS("OPEN");
                    		}

                    		if(GR_Qty <= PO_Qty) {
                    			oWMS_DYN_PURCHASING_GR.setMOVE_QTY(new Float(qtyConv));

                    			JSONArray jsArray = new JSONArray();
                    			jsonObject.put("MOVEMENT_TYPE"   , "100");
                    			jsonObject.put("CURR_STORAGE_LOC", oWMS_DYN_PURCHASING_GR.getSTORAGE_LOC());
                    			jsonObject.put("MOVE_STORAGE_LOC", oWMS_DYN_PURCHASING_GR.getSTORAGE_LOC());
                    			jsonObject.put("MOVE_QTY"        , oWMS_DYN_PURCHASING_GR.getMOVE_QTY());
                    			jsonObject.put("CURR_QTY"        , oWMS_DYN_PURCHASING_GR.getMOVE_QTY());
                    			jsonObject.put("UNIT"            , tempPurchasing.getMST_UNIT());
                    			jsonObject.put("POST_DATE"       , oWMS_DYN_PURCHASING_GR.getGR_DATE());
                    			jsArray.put(jsonObject);
                    			LOGGER.info("jsonObject is "+jsonObject+"");
                    			LOGGER.info("jsArray is "+jsArray+"");

                    			//Insert to Stock Movement Table
                    			WMS_DYN_STOCK_MOVEMENT_Impl iWMS_DYN_STOCK_MOVEMENT_Impl = new WMS_DYN_STOCK_MOVEMENT_Impl();
                                rtnBoolResult = iWMS_DYN_STOCK_MOVEMENT_Impl.insertStockMovement("GR_INPUT", jsArray, userId, clientId, companyCode);
                    		}

                    		//set Vendor Code
                    		oWMS_DYN_PURCHASING_GR.setVENDOR_CODE(tempPurchasing.getVENDOR_CODE());

                            rtnBoolResult = iWMS_DYN_PURCHASING_GR_Impl.insertPurchasingGr(oWMS_DYN_PURCHASING_GR);
                            Boolean updateGRFlag = false;

                            WMS_DYN_PURCHASING oWMS_DYN_PURCHASING = new WMS_DYN_PURCHASING();
                            oWMS_DYN_PURCHASING.setCLIENT_ID(oWMS_DYN_PURCHASING_GR.getCLIENT_ID());
                            oWMS_DYN_PURCHASING.setCOMPANY_CODE(oWMS_DYN_PURCHASING_GR.getCOMPANY_CODE());
                            oWMS_DYN_PURCHASING.setPO_ITEM_NO(oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO());
                            oWMS_DYN_PURCHASING.setPO_NO(oWMS_DYN_PURCHASING_GR.getPO_NO());
                            oWMS_DYN_PURCHASING.setMATERIAL_CODE(oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE());

                            updateGRFlag = iWMS_DYN_PURCHASING_Impl.updatePurchasingFromPDA(oWMS_DYN_PURCHASING);

                            if(rtnBoolResult)
                            {
                                if(updateGRFlag) {
                                    rtnJson.addProperty("ret", Integer.valueOf(0));
                                    rtnJson.addProperty("msg", "Insert Purchasing Data Success : " + oWMS_DYN_PURCHASING_GR.getBARCODE_NO() + " ");
                                } else {
                                    rtnJson.addProperty("ret", Integer.valueOf(0));
                                    rtnJson.addProperty("msg", "Insert Purchasing Data Success : " + oWMS_DYN_PURCHASING_GR.getBARCODE_NO() + " "
                                            + ", but " + oWMS_DYN_PURCHASING_GR.getPO_NO() + " GR_FLAG update failed.");
                                }
                            }
//                      } else {
//                          rtnJson.addProperty("ret", Integer.valueOf(1));
//                          rtnJson.addProperty("msg", "GR FLAG IS ON PO NUMBER [" + oWMS_DYN_PURCHASING_GR.getPO_NO() + "] MATERIAL CODE ["
//                                  + oWMS_DYN_PURCHASING_GR.getMATERIAL_CODE() + "] PO_ITEM_NO [" + oWMS_DYN_PURCHASING_GR.getPO_ITEM_NO() + "]");
//                      }
                    }
                } else {

                    rtnJson.addProperty("ret", Integer.valueOf(1));
                    rtnJson.addProperty("msg", "Already Scanned : " + oWMS_DYN_PURCHASING_GR.getBARCODE_NO() + " ");
                }
            } else {

                rtnJson.addProperty("ret", Integer.valueOf(1));
                rtnJson.addProperty("msg", "PO Number " + oWMS_DYN_PURCHASING_GR.getPO_NO() + " does not exist.");
            }

            return Response.status(200).entity(rtnJson.toString()).build();
        }
        catch (Exception e)
        {
            LOGGER.error("Excption: " + e.getMessage() + "#");
            e.printStackTrace();
            return Response.status(500).entity(e.toString()).build();
        }
    }

    //GWANGHYUN 4/7/2018
    private WMS_DYN_PURCHASING_GR setObjectByJson(String tblProc, JSONObject jsonObj, String userId, String clientId, String comapnyCode) throws Exception {

        WMS_DYN_PURCHASING_GR mWMS_DYN_PURCHASING_GR = new WMS_DYN_PURCHASING_GR();

        try {
            LOGGER.debug("setObjectByJson() jsonObj:" + jsonObj.toString());

            WMS_MST_CODERULE_Impl iWMS_MST_CODERULE_Impl = new WMS_MST_CODERULE_Impl();

            clientId = StringUtil.getEmptyNullPrevString(clientId);
            userId = StringUtil.getEmptyNullPrevString(userId);
            comapnyCode = StringUtil.getEmptyNullPrevString(comapnyCode);

            int     docNo         = jsonObj.has("DOC_NO"       )   ? jsonObj.getInt    ("DOC_NO"       ) : 0;
            String  PO_NO         = jsonObj.has("PO_NO"        )   ? jsonObj.getString ("PO_NO"        ) : "";
            int     PO_ITEM_NO    = jsonObj.has("PO_ITEM_NO"   )   ? jsonObj.getInt    ("PO_ITEM_NO"   ) : 0;
            String  BARCODE_NO    = jsonObj.has("BARCODE_NO"   )   ? jsonObj.getString ("BARCODE_NO"   ) : "";
            String  VENDOR_CODE   = jsonObj.has("VENDOR_CODE"  )   ? jsonObj.getString ("VENDOR_CODE"  ) : "";
            String  MATERIAL_CODE = jsonObj.has("MATERIAL_CODE")   ? jsonObj.getString ("MATERIAL_CODE") : "";
            String  VENDOR_LOT_NO = jsonObj.has("VENDOR_LOT_NO")   ? jsonObj.getString ("VENDOR_LOT_NO") : "";
            String  LOT_NO        = jsonObj.has("LOT_NO"       )   ? jsonObj.getString ("LOT_NO"       ) : "";
            String  GR_DATE       = jsonObj.has("GR_DATE"      )   ? jsonObj.getString ("GR_DATE"      ) : "";
            Double  GR_QTY        = jsonObj.has("GR_QTY"       )   ? jsonObj.getDouble ("GR_QTY"       ) : 0;
            String  GR_UNIT       = jsonObj.has("GR_UNIT"      )   ? jsonObj.getString ("GR_UNIT"      ) : "";
            Double  PER_QTY       = jsonObj.has("PER_QTY"      )   ? jsonObj.getDouble ("PER_QTY"      ) : 0;
            String  STORAGE_LOC   = jsonObj.has("STORAGE_LOC"  )   ? jsonObj.getString ("STORAGE_LOC"  ) : "";

            if(docNo == 0)
            {
                docNo = Integer.parseInt(iWMS_MST_CODERULE_Impl.getDocNo(clientId, comapnyCode, "GR", "DOC_NO", 1));
            }

            LOGGER.info("Po Number Is:"+PO_NO+"#");

            mWMS_DYN_PURCHASING_GR.setCLIENT_ID(clientId)         ;
            mWMS_DYN_PURCHASING_GR.setDOC_NO(docNo)               ;
            mWMS_DYN_PURCHASING_GR.setCOMPANY_CODE(comapnyCode)   ;
            mWMS_DYN_PURCHASING_GR.setPO_NO(PO_NO)                ;
            mWMS_DYN_PURCHASING_GR.setPO_ITEM_NO(PO_ITEM_NO)      ;
            mWMS_DYN_PURCHASING_GR.setBARCODE_NO(BARCODE_NO)      ;
            mWMS_DYN_PURCHASING_GR.setVENDOR_CODE(VENDOR_CODE)    ;
            mWMS_DYN_PURCHASING_GR.setMATERIAL_CODE(MATERIAL_CODE);
            mWMS_DYN_PURCHASING_GR.setVENDOR_LOT_NO(VENDOR_LOT_NO);
            mWMS_DYN_PURCHASING_GR.setLOT_NO(LOT_NO)              ;
            mWMS_DYN_PURCHASING_GR.setGR_DATE(GR_DATE)            ;
            mWMS_DYN_PURCHASING_GR.setGR_QTY(new Float(GR_QTY))   ;
            mWMS_DYN_PURCHASING_GR.setGR_UNIT(GR_UNIT)            ;
            mWMS_DYN_PURCHASING_GR.setPER_QTY(new Float(PER_QTY)) ;
            mWMS_DYN_PURCHASING_GR.setSTORAGE_LOC(STORAGE_LOC)    ;
            mWMS_DYN_PURCHASING_GR.setCREATE_USER(userId)         ;
            mWMS_DYN_PURCHASING_GR.setMOVE_QTY(new Float(GR_QTY)) ;
            mWMS_DYN_PURCHASING_GR.setMOVEMENT_TYPE("100")        ;

        } catch (Exception e) {
            LOGGER.error("Exception :"+e.getMessage()+"#");
            e.printStackTrace();
        }
        return mWMS_DYN_PURCHASING_GR;
    }

    private JsonObject setTableEventResultHandler(Boolean rtnBoolResult, String rtnData)
    {
        JsonObject rtnJson = new JsonObject();
        if (rtnBoolResult.booleanValue())
        {
            rtnJson.addProperty("ret" , Integer.valueOf(0));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg" , "confirm");
        }
        else
        {
            String rtnMsg = "could not save data";
            rtnJson.addProperty("ret" , Integer.valueOf(1));
            rtnJson.addProperty("data", rtnData);
            rtnJson.addProperty("msg" , rtnMsg);
        }
        return rtnJson;
    }


}
